package com.mishuto.pingtest.service

import android.content.*
import android.os.IBinder
import com.mishuto.pingtest.App.Companion.appContext
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.model.PingTest
import com.mishuto.pingtest.service.PingService.PingSrvBinder

/**
 * Фасад для доступа к PingService одновременно нескольких клиентов.
 * Чтобы получить доступ к сервису, клиент должен выполнить bind(). Чтобы освободить - unbind()
 */

object ServiceManager {
    private var pingService: PingService? = null // объект сервиса
    private val serviceIntent get() = Intent(appContext, PingService::class.java)
    
    private val clients = mutableMapOf<String, Client>()
    
    val pingTest: PingTest
        get() = pingService?.getPingTest() ?: throw IllegalStateException("Service not bound")
    
    private var serviceConnection: Connection? = null
    
    fun isBound() = serviceConnection?.state == State.BOUND
    
    fun isStarted(): Boolean = pingService?.isRunning ?: throw IllegalStateException("Service not bound")
    
    fun startService() = appContext.startForegroundService(serviceIntent)
    
    fun stopService() = pingService?.stopCommand()
    
    fun bind(tag: String, onBind: Runnable) {
        d("prepare to binding: $tag")
        clients[tag] = Client(onBind, State.BINDING)
        
        if (isBound()) {
            d("ServiceManager already bound. Start binding of client $tag")
            clients[tag]!!.onBound()
        }
        else if ( serviceConnection?.state == State.BINDING)
            d("binding of ServiceManager is in progress")
        else {
            d("start ServiceManager binding")
            serviceConnection = Connection()
            appContext.bindService(serviceIntent, serviceConnection!!, Context.BIND_AUTO_CREATE)
        }
    }
    
    fun unbind(tag: String) {
        d("unbinding: $tag")
        clients[tag]?.state = State.UNBOUND
        unbindSelfIFNoMoreClients()
    }
    
    private fun unbindSelfIFNoMoreClients() {
        if (isBound() && (clients.isEmpty() || clients.values.all { it.state == State.UNBOUND })) {
            d("ServiceManager is unbinding")
            appContext.unbindService(serviceConnection!!)
            pingService = null
            serviceConnection = null
        }
    }
    
    fun status(tag: String) = clients[tag]?.state
    
    enum class State { BINDING, BOUND, UNBOUND }
    
    private data class Client(val onBoundListener: Runnable, var state: State) {
        fun onBound() {
            state = State.BOUND
            onBoundListener.run()
        }
    }
    
    private class Connection : ServiceConnection {
        var state: State = State.BINDING
        
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            tag()
            state = State.BOUND
            pingService = (service as PingSrvBinder).service
            clients.filter { it.value.state == State.BINDING }.forEach {
                d("client ${it.key} is binding")
                it.value.onBound()
            }
            unbindSelfIFNoMoreClients() // если клиент успел сделать unbind до того как получил onBind
        }
        
        override fun onServiceDisconnected(name: ComponentName) {
            tag()
            state = State.UNBOUND
            pingService = null
        }
        
        override fun onBindingDied(name: ComponentName) {
            tag()
            pingService = null
            serviceConnection = null
            clients.forEach { it.value.state = State.UNBOUND }
        }
    }
}
