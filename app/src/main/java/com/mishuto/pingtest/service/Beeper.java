package com.mishuto.pingtest.service;

import android.media.AudioManager;
import android.media.ToneGenerator;

/**
 * Изадет сигнал через динамик
 * Created by Michael Shishkin on 12.09.2020
 */
public enum Beeper {
    SHORT(70, ToneGenerator.TONE_PROP_BEEP, 50),                    // короткий гудок
    LONG(90, ToneGenerator.TONE_CDMA_ALERT_AUTOREDIAL_LITE, 125);   // длинный гудок


    private final int mTone;
    private final int mDuration;
    private final ToneGenerator mToneGenerator;

    Beeper(int volume, int tone, int duration) {
        mTone = tone;
        mDuration = duration;
        mToneGenerator = new ToneGenerator(AudioManager.STREAM_MUSIC, volume);
    }

    public void play() {
        mToneGenerator.startTone(mTone, mDuration);
    }
}
