package com.mishuto.pingtest.service;

import static com.mishuto.pingtest.common.Utils.eventDispatcher;
import static com.mishuto.pingtest.common.Utils.intentDispatcher;
import static com.mishuto.pingtest.common.Utils.logger;
import static com.mishuto.pingtest.controller.services.ProcessEventsObserver.APP_IN_BACKGROUND;
import static com.mishuto.pingtest.common.Const.HOUR;
import static com.mishuto.pingtest.common.Const.INFINITY;
import static com.mishuto.pingtest.common.Const.PING_TIMEOUT_LIMIT;
import static com.mishuto.pingtest.common.net.NetworkTypeKt.NET_TYPE_CHANGED;
import static com.mishuto.pingtest.model.HostNameValidator.IP4_MATCHER;
import static com.mishuto.pingtest.service.PingNotification.STOP_TEST_ACTION;
import static java.util.regex.Pattern.matches;
import static com.mishuto.pingtest.common.CrashlyticsKeys.Tag.*;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.CrashlyticsKeys;
import com.mishuto.pingtest.controller.ui.FloatingPing;
import com.mishuto.pingtest.model.ErrorMessageEventImp;
import com.mishuto.pingtest.model.NetTypeChangeEvent;
import com.mishuto.pingtest.common.tickers.Ticker;
import com.mishuto.pingtest.common.wrappers.WakeLockWrapper;
import com.mishuto.pingtest.common.wrappers.WiFiLockWrapper;
import com.mishuto.pingtest.data.AppDatabaseKt;
import com.mishuto.pingtest.data.PersistData;
import com.mishuto.pingtest.model.HostInfo;
import com.mishuto.pingtest.model.PingException;
import com.mishuto.pingtest.model.PingHolder;
import com.mishuto.pingtest.model.PingTest;
import com.mishuto.pingtest.model.ping.Ping;
import com.mishuto.pingtest.model.stat.BoundlessStat;
import com.mishuto.pingtest.model.stat.LivePingStatistic;
import com.mishuto.pingtest.model.stat.StreamPingStat;
import com.mishuto.pingtest.settings.Settings;
import com.mishuto.pingtest.settings.SettingsPrefManager;
import com.mishuto.pingtest.settings.preferences.sound.SoundScope;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

/**
 * Сервис для запуска пинга.
 * Задача сервиса - работать в бэке, даже, когда активность выгружается (например, при смене ориентации экрана) или уничтожается
 */

public class PingService extends Service implements PingHolder.PingListener {

    public static final String HOST_UPDATE_EVENT = "PingService::Host_update_event";
    public static final String PING_EVENT = "PingService::Ping_event";
    public static final String SERVICE_FAULT_EVENT = "PingService::Service_fault_event";
    public static final String SERVICE_STOP_EVENT = "PingService::Service_stop_event";

    private final static long MAX_WAKELOCK_TIMEOUT = HOUR;  // максимальный таймаут для вейклока
    private final static String PING_SERVICE = "PingService";

    public class PingSrvBinder extends Binder {
        public PingService getService() {
            return PingService.this;
        }
    }

    private LivePingStatistic pingStat;
    private Ticker doPingTicker;                // тикер для периодического запуска пинга
    private boolean isRunning = false;          // статус сервиса
    private PingNotification pingNotification;
    private PingHolder pingHolder;
    SettingsPrefManager prefManager = SettingsPrefManager.INSTANCE;
    Settings pref;
    PingTest pingTest;
    private final WiFiLockWrapper wifiLock = WiFiLockWrapper.getLock(PING_SERVICE);
    private final WakeLockWrapper wakeLock = WakeLockWrapper.getLock(PING_SERVICE);
    private PersistData persistentData;
    private final FloatingPing floatingPing = new FloatingPing(this);

    @Override
    public void onCreate() {
        logger.tag();
        pingNotification = new PingNotification();
        pref = prefManager.getPreferences();
    }

    @Override
    public IBinder onBind(Intent intent) {
        logger.tag();
        return new PingSrvBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        logger.tag();
        return super.onUnbind(intent);
    }

    // вся полезная нагрузка выполняется в onStartCommand, инициируемая командой startService
    // синхронизация нужна, т.к. doPing может прийти до того как завершится onStartCommand, сразу после вызова pingHolder.resolve()
    @Override
    synchronized public int onStartCommand(Intent intent, int flags, int startId) {
        if(isRunning)
            return START_NOT_STICKY;

        logger.tag();
        isRunning = true;

        wifiLock.lock();
        wakeLock.lock(MAX_WAKELOCK_TIMEOUT);

        // инициализация объектов
        pingTest = new PingTest();
        // нужно заполнить pingTest.hostInfo до резолвинга на случай если он будет запрошен раньше чем произойдет резолвинг (например, выход и возвращение в MainFragment во время резолвинга)
        pingTest.hostInfo = getDraftHostInfo();
        CrashlyticsKeys.INSTANCE.update(PREF, BILLING, SERVICE);
        prefManager.registerOnSPChangeListener(PING_SERVICE, (sp, key) -> onSettingsChanged(key));
        pingStat = createStatistic();

        pingHolder = new PingHolder(this, pref.getPingTimeout());                           // пересоздаем pingHolder чтобы не получить колбэк от
        pingHolder.resolve(pref.getHost());                                                         // последнего пинга предыдущего пингхолдера

        doPingTicker = new Ticker(pingHolder::doPing, false, pref.getPingInterval(), "--PingService.doPingTicker");

        persistentData = new PersistData(pingTest, pref);
        eventDispatcher.register(APP_IN_BACKGROUND, PING_SERVICE, p -> onBackground());
        eventDispatcher.register(NET_TYPE_CHANGED, PING_SERVICE, p -> onNetChanged((NetTypeChangeEvent) p));
        intentDispatcher.register(STOP_TEST_ACTION, PING_SERVICE, i -> stopCommand());

        startForeground(PingNotification.NOTIFICATION_ID, pingNotification.createNotification());   // вывод сервиса на передний план
        floatingPing.start();                                                                       // отображение плавающего окна
        logger.d("initialize has complete");
        return START_NOT_STICKY;                                                                    // убитый сервис не перезапускается
    }

    // получение предварительных данных по хосту до резолвинга.
    private HostInfo getDraftHostInfo() {
        String host = pref.getHost();
        assert host != null;
        if(matches(IP4_MATCHER, host))
            return new HostInfo(host, host);
        else
            return new HostInfo("0.0.0.0", host);
    }

    private LivePingStatistic createStatistic() {
        return pref.getMovingPingsNumber() == INFINITY ? new BoundlessStat() :
                new StreamPingStat(pref.getMovingPingsNumber());
    }

    private void onSettingsChanged(String key) {
        // в случае изменения настроек пинга - стопим тест.
        if (Arrays.asList(
                prefManager.getHostKey(),
                prefManager.getPingTimeoutKey(),
                prefManager.getPingIntervalKey(),
                prefManager.getMovingPingsNumberKey()).contains(key)
        )
            stopCommand();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        wifiLock.unlock();
        wakeLock.unlock();
        logger.d("Service destroyed");
    }

    /**
     * остановка запущенного сервиса.
     * поскольку сервис привязан, сам он не останавливается по команде stopService, поэтому требуется отдельная команда
     */
    synchronized public void stopCommand() {
        if (!isRunning)
            return;

        logger.tag();
        isRunning = false;
        pingHolder.unregister();
        doPingTicker.stop();
        wifiLock.unlock();
        wakeLock.unlock();
        pingTest.finish();
        eventDispatcher.unregister(NET_TYPE_CHANGED, PING_SERVICE);
        eventDispatcher.unregister(APP_IN_BACKGROUND, PING_SERVICE);
        intentDispatcher.unregister(STOP_TEST_ACTION, PING_SERVICE);
        prefManager.unregisterOnSPChangeListener(PING_SERVICE);
        persistentData.saveResult();
        floatingPing.stop();

        stopForeground(STOP_FOREGROUND_REMOVE);   // удаляем сервис с переднего плана, отменяем уведомление
        stopSelf();
        eventDispatcher.post(SERVICE_STOP_EVENT);
    }

    public boolean isRunning() {
        return isRunning;
    }

    PingTest getPingTest() {
        return pingTest;
    }

    /* обработка PingListener */

    @Override
    synchronized public void onResolve(PingHolder pingHolder) {
        if(!isRunning)  // событие, пришедшее после останова сервиса игнорируется
            return;

        logger.tag();
        pingHolder.doPing();

        updateHostInfo(pingHolder.getHostInfo());
    }

    @Override
    public void onLookUp(HostInfo hostInfo) {
        if(isRunning)
            updateHostInfo(hostInfo);
    }

    private void updateHostInfo(HostInfo hostInfo) {
        pingTest.hostInfo = hostInfo;
        eventDispatcher.post(HOST_UPDATE_EVENT, hostInfo);
    }

    @Override
    synchronized public void onPing(Ping ping) {
        if (!isRunning)  // события, пришедшие после останова сервиса игнорируется
            return;

        logger.d("latency: %d ms", ping.getLatency());

        processTimeoutSinceLastPing(ping);
        // обновляем статистику
        pingStat.addNew(ping);
        persistentData.add(ping);
        pingTest.setStatistic(pingStat);    //todo не пихать объект в каждом пинге
        pingNotification.updateNotification(pingTest);
        beep(ping);

        floatingPing.update(ping.getLatency());

        eventDispatcher.post(PING_EVENT, ping);

        if (pingTest.isTestTimeExpired()) {
            logger.d("test stops due to timeout expired");
            stopCommand();
        }
        else
            doPingTicker.start();                                                // стартуем таймер для нового пинга
    }

    private void processTimeoutSinceLastPing(Ping ping) {
        if (AppDatabaseKt.getPingDAO().getCountForTest(pingTest.getTestId()) > 0) {
            Ping lastPing = AppDatabaseKt.getPingDAO().getLastElementsForTest(pingTest.getTestId(), 1).get(0);
            // если разница между двумя пингами превысила предельный таймаут, значит сервис был выгружен и снова загружен.
            if (Duration.between(lastPing.getTimeStamp().plus(pref.getPingInterval(), ChronoUnit.MILLIS), ping.getTimeStamp()).toMillis() > PING_TIMEOUT_LIMIT) {
                pingStat.clear();       // очищаем статистику и начинаем считать заново
                pingTest.renew();
                logger.d("statistics renewed due to extra-long latency");
            }
        }
    }

    private void beep(Ping ping) {
        if (pref.getSoundSettings().isSoundOn())
            if (ping.isLost())
                Beeper.LONG.play();
            else if(pref.getSoundSettings().getScope() == SoundScope.ALL)
                Beeper.SHORT.play();
    }

    @Override
    synchronized public void onPingError(PingException exception) {
        logger.tag();
        if (exception.getMessage() != null) {
            eventDispatcher.post(SERVICE_FAULT_EVENT, exception.getMessage());
            persistentData.add(new ErrorMessageEventImp(exception.getMessage()));
        }
        stopCommand();
    }

    private void onBackground() {
        Toast.makeText(getApplicationContext(), R.string.app_in_back, Toast.LENGTH_SHORT)
                .show();
    }

    private void onNetChanged(NetTypeChangeEvent event) {
        persistentData.add(event);
    }
}
