package com.mishuto.pingtest.service;

import static android.app.PendingIntent.FLAG_IMMUTABLE;
import static com.mishuto.pingtest.common.Const.SEC;
import static com.mishuto.pingtest.common.Utils.MAX_PERCENT;
import static com.mishuto.pingtest.common.Utils.getString;
import static com.mishuto.pingtest.common.Utils.logger;

import static java.lang.Math.floor;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.mishuto.pingtest.App;
import com.mishuto.pingtest.common.Assert;
import com.mishuto.pingtest.controller.main.MainActivity;
import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.NotificationHelper;
import com.mishuto.pingtest.common.Timer;
import com.mishuto.pingtest.model.PingTest;
import com.mishuto.pingtest.model.indicators.GroupIndicators;
import com.mishuto.pingtest.model.stat.PingStatData;
import com.mishuto.pingtest.model.stat.PingStatistic;

import java.util.stream.Stream;

/**
 * Уведомление, используемое для рабооты сервиса в Foreground
 * Created by Michael Shishkin on 11.07.2020
 *
 * @noinspection FieldMayBeFinal
 */
public class PingNotification {
    static final int NOTIFICATION_ID = 1;
    public static final String STOP_TEST_ACTION = "PingNotification.stopAction";
    private static final int MIN_UPDATE_INTERVAL = SEC;
    // из-за бага на самсунге level-list не отображается на белом фоне. Поэтому используется массив иконок
    private final static int[] NOTIFY_ICONS_ID = {R.drawable.notify0, R.drawable.notify1, R.drawable.notify2, R.drawable.notify3, R.drawable.notify4};

    private Timer updateIntervalTimer = new Timer(MIN_UPDATE_INTERVAL);

    private NotificationHelper mHelper;
    private PingStatistic prevStatistic = new PingStatData();

    public PingNotification() {
        mHelper = new NotificationHelper();
    }

    // создание "пустого" уведомления
    synchronized public Notification createNotification() {
        mHelper.createNotificationChannel();
        var builder = getNotificationBuilder().setCustomBigContentView(createRemoteViews());
        if (isEachNotifyIconResolved())
            builder.setSmallIcon(NOTIFY_ICONS_ID[NOTIFY_ICONS_ID.length - 1]);
        else
            builder.setSmallIcon(android.R.drawable.ic_media_pause);

        return builder.build();
    }

    // обновление уведомления
    synchronized public void updateNotification(PingTest pingTest) {
        logger.tag();
        var stat = pingTest.getStatistic();

        if (updateIntervalTimer.isTimeUp() && !sameAsPrev(stat)) {  // фильтруем нотификацию по времени и содержимому
            prevStatistic = new PingStatData(stat);
            new NotificationUpdater(stat, pingTest.getHostInfo().name).update();
        }
    }

    private boolean sameAsPrev(PingStatistic stat) {
        return stat.getAverage() == prevStatistic.getAverage() &&
                stat.getJitter() == prevStatistic.getJitter() &&
                stat.getLost() == prevStatistic.getLost();
    }

    private boolean isEachNotifyIconResolved() {
        String[] iconsNames = {"notify0", "notify1", "notify2", "notify3", "notify4"};
        boolean allResolved = Stream.of(iconsNames)
                .map(this::resolveDrawable)
                .noneMatch(i -> i == 0);

        logger.d("icons resolved: %b", allResolved);
        return allResolved;
    }

    @SuppressLint("DiscouragedApi")
    private int resolveDrawable(String name) {
        Context context = App.getAppContext();
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }

    // создание предустановленного билдера уведомлений
    private NotificationCompat.Builder getNotificationBuilder() {
        Context ctx = App.getAppContext();
        PendingIntent openActivityPI = PendingIntent.getActivity(    // PendingIntent для запуска активности из уведомления
                ctx,
                0,
                new Intent(ctx, MainActivity.class),                // активность запускается как singleTask (см. манифест)
                FLAG_IMMUTABLE);                                    // если PendingIntent существует (при обновлении уведомления), он не пересоздается

        PendingIntent stopPI = PendingIntent.getBroadcast(ctx,0,
                new Intent(STOP_TEST_ACTION).setPackage(ctx.getPackageName()),
                FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = mHelper.getBuilder();

        builder.setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setShowWhen(false)
                .addAction(R.drawable.ic_baseline_stop_24, getString(R.string.stop_action), stopPI)
                .setColor(ContextCompat.getColor(ctx, R.color.colorSecondary))
                .setContentIntent(openActivityPI);

        return builder;
    }

    private RemoteViews createRemoteViews() {
        return new RemoteViews(App.getAppContext().getPackageName(), R.layout.service_notification);
    }

    private class NotificationUpdater {
        PingStatistic stat;
        String host;
        int progress;
        RemoteViews views = createRemoteViews();
        GroupIndicators overall;

        NotificationUpdater(PingStatistic stat, String host) {
            this.stat = stat;
            this.host = host;
            overall = GroupIndicators.overallFromStat(stat);
            progress = PingStatistic.overallQuality(stat);
        }

        void update() {
            logger.tag();
            setViews();
            Notification notification = buildNotification().build();
            mHelper.notificationManager().notify(NOTIFICATION_ID, notification);
        }

        private void setViews() {
            logger.d("set notify avg: %d. jitter: %d. lost: %d", stat.getAverage(), stat.getJitter(), stat.getLost());
            views.setProgressBar(R.id.overall_progress, MAX_PERCENT, progress, false);                // прогрессбар
            views.setTextViewText(R.id.overall_value, getString(R.string.percent, progress));         // значение прогресбара
            views.setTextViewText(R.id.ping_val, getString(R.string.num_ms, stat.getAverage()));      // пинг
            views.setTextViewText(R.id.jitter_value, getString(R.string.num_ms, stat.getJitter()));   // джиттер
            views.setTextViewText(R.id.lost_val, getString(R.string.percent1, stat.getLostPermille()/10f)); // лост
            views.setTextViewText(R.id.host_val, host);
        }

        private NotificationCompat.Builder buildNotification() {
            NotificationCompat.Builder builder = getNotificationBuilder();
            builder.setCustomBigContentView(views);
            builder.setContentTitle(getString(R.string.notif_title, stat.getAverage()));                     // для collapsed режима устанавливаем ping в заголовке

            if (isEachNotifyIconResolved())
                builder.setSmallIcon(getIconIdByIndicator(overall));
            else
                builder.setSmallIcon(android.R.drawable.ic_media_pause);

            return builder;
        }

        // выбираем иконку в зависимости от индикатора.
        // разбиваем значение на сегменты: 0 = 0 палок, (0-0.25] - 1 палка, (0.25-0.5], (0.5-0.75], (0.75-1] - 4 палки
        private int getIconIdByIndicator(GroupIndicators indicator) {
            try {
                final int index = (int) floor(indicator.getTotalIndicator().computeIndicatorValue() * 4.0 + 0.99);
                Assert.that( 0 <= index && index < NOTIFY_ICONS_ID.length, "index=" + index );
                logger.d("iconIndex = %d", index);
                return NOTIFY_ICONS_ID[index];
            }
            catch (ArrayIndexOutOfBoundsException e) {
                // по необъяснимым причинам index в редких случаях "ломается" вызывая ошибку ArrayIndexOutOfBoundsException
                FirebaseCrashlytics.getInstance().recordException(e);
                return android.R.drawable.ic_media_pause;
            }
        }
    }
}
