package com.mishuto.pingtest.widget;

import static com.mishuto.pingtest.common.Utils.logger;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.controller.main.MainActivity;
import com.mishuto.pingtest.settings.widget.WidgetSettingsActivity;

/**
 * Обработчик внутренних интентов виджета
 * Created by Michael Shishkin on 10.04.2021
 */
public class WidgetEventHandler extends BroadcastReceiver {

    final private static String EXTRA_ARG = "extraArg";     // ключ передаваемого в интенте аргумента
    final private static String EXTRA_WID_ID = "extraID";   // ключ передаваемого в интенте id виджета

    // события виджета
    public enum WidgetEvent {
        REFRESH,        // нажатие кнопки рефреш
        SHOW_SETTINGS,  // показать окно настроек
        RUN_APP,        // запустить приложение
        SERIES,         // очередной цикл серии пингов
        STOP_SERIES     // прерывание серии пингов
    }

    public static PendingIntent getEventPI(WidgetEvent event, Integer widgetId, Bundle arg) {
        Intent intent = createIntent(event, widgetId, arg);
        return PendingIntent.getBroadcast(App.getAppContext(),0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
    }

    public static PendingIntent getEventPI(WidgetEvent event) {
        return getEventPI(event, null, null);
    }

    static Intent createIntent(WidgetEvent event, Integer widgetId, Bundle arg) {
        return new Intent(App.getAppContext(), WidgetEventHandler.class)
                .setAction(event.name())
                .putExtra(EXTRA_WID_ID, widgetId)
                .putExtra(EXTRA_ARG, arg);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        logger.d("WidgetEventHandler::onReceive %s", action);

        int widgetId = intent.getIntExtra(EXTRA_WID_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        WidgetEvent event = WidgetEvent.valueOf(action);
        ExecutorManager keeper = ExecutorManager.getInstance();

        switch(event) {
            case REFRESH:
                keeper.executors().forEach(WidgetPingExecutor::refresh);
                break;

            case SHOW_SETTINGS:
                WidgetSettingsActivity.startActivityForConfigure(context, widgetId);
                break;

            case RUN_APP:
                context.startActivity(new Intent(context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;

            case SERIES:
                keeper.executors().stream()
                        .filter(e-> e.getId() == widgetId)
                        .forEach(WidgetPingExecutor::onPeriodicStart);
                break;

            case STOP_SERIES:
                keeper.executors().stream()
                        .filter(e-> e.getId() == widgetId)
                        .forEach(WidgetPingExecutor::stopSeries);
                break;
        }
        logger.d("work %s is done", action);
    }
}
