package com.mishuto.pingtest.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mishuto.pingtest.common.Logger;

import java.util.Arrays;
import java.util.Optional;

/**
 * Обработчик событий виджета
 * Created by Michael Shishkin on 16.01.2021
 */
public class WidgetProvider extends AppWidgetProvider {


    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.INSTANCE.d("WidgetProvider:onReceive: " + intent.getAction());
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Logger.INSTANCE.d("WidgetProvider:onUpdate " + Arrays.toString(appWidgetIds));
        for (int id: appWidgetIds)
            getController(id).ifPresent(WidgetViewController::refreshWidget);
    }

    @Override
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
        Logger.INSTANCE.d("WidgetProvider:onAppWidgetOptionsChanged");
        getController(appWidgetId).ifPresent(WidgetViewController::adjustWidget);
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
    }

    @Override
    public void onEnabled(Context context) {
        Logger.INSTANCE.d("WidgetProvider:onEnabled");
        super.onEnabled(context);
    }

    @Override
    public void onDisabled(Context context) {
        Logger.INSTANCE.d("WidgetProvider:onDisabled");
        super.onDisabled(context);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        Logger.INSTANCE.d("WidgetProvider:onDeleted " + Arrays.toString(appWidgetIds));
        for (int id: appWidgetIds)
            getController(id).ifPresent(c->ExecutorManager.getInstance().removeWidget(c));
    }

    private Optional<WidgetViewController> getController(int id) {
        return Optional.ofNullable(ExecutorManager.getInstance().getViewController(id));
    }

    // фейковые классы для указания в манифесте для виджетов разных размеров
    public static class MediumWidgetProvider extends WidgetProvider {}
    public static class LargeWidgetProvider extends WidgetProvider {}
}

  /*
  копирование картинки в виджет
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.widget1x1, null);
        TextView ping = root.findViewById(R.id.wid_ping_val);
        ping.setText(context.getString(R.string.num_ms, 2500));
        root.measure(makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        root.layout(0, 0, root.getMeasuredWidth(), root.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(root.getMeasuredWidth(), root.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        root.getBackground().draw(canvas);
        root.draw(canvas);
        views.setImageViewBitmap(R.id.wid_main_widget, bitmap);
*/
