package com.mishuto.pingtest.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.SystemClock;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.common.Logger;

import static android.app.AlarmManager.INTERVAL_HOUR;

/**
 * Реализация "гарантированного" алярма
 * Аларм срабатывает всегда только если не является exact, имеет тип ELAPSED_REALTIME и время, кратное 15 мин.
 * Created by Michael Shishkin on 07.02.2021
 */
public class Alarm {

    private final AlarmManager mAlarmManager;
    private final PendingIntent mPendingIntent;
    private final static long INTERVAL = INTERVAL_HOUR;

    public Alarm(PendingIntent pi) {
        mAlarmManager = (AlarmManager) App.getAppContext().getSystemService(Context.ALARM_SERVICE);
        mPendingIntent = pi;
    }

    public void setAlarm() {
        mAlarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + INTERVAL,
                INTERVAL,
                mPendingIntent);
        Logger.INSTANCE.d("alarm was set");
    }

    public void stopAlarm() {
        mAlarmManager.cancel(mPendingIntent);
        Logger.INSTANCE.d("alarm was stopped");
    }
}
