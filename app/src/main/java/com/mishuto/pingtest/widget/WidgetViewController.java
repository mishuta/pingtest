package com.mishuto.pingtest.widget;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.mishuto.pingtest.common.Const.SEC;
import static com.mishuto.pingtest.common.Utils.percent;
import static com.mishuto.pingtest.common.wrappers.PMUtils.isScreenOn;
import static com.mishuto.pingtest.view.Utils.TextSize;
import static com.mishuto.pingtest.view.Utils.dimen2dp;
import static com.mishuto.pingtest.widget.WidgetEventHandler.WidgetEvent.REFRESH;
import static com.mishuto.pingtest.widget.WidgetEventHandler.WidgetEvent.RUN_APP;
import static com.mishuto.pingtest.widget.WidgetEventHandler.WidgetEvent.SHOW_SETTINGS;
import static com.mishuto.pingtest.widget.WidgetViewController.AppWidgetLayout.BIG;
import static com.mishuto.pingtest.widget.WidgetViewController.AppWidgetLayout.MEDIUM;
import static com.mishuto.pingtest.widget.WidgetViewController.AppWidgetLayout.SMALL;

import android.appwidget.AppWidgetManager;
import android.os.TransactionTooLargeException;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.Assert;
import com.mishuto.pingtest.common.Logger;
import com.mishuto.pingtest.common.TextExtender;
import com.mishuto.pingtest.common.Timer;
import com.mishuto.pingtest.common.wrappers.WidgetOptions;
import com.mishuto.pingtest.model.indicators.GroupIndicators;
import com.mishuto.pingtest.model.stat.PingStatData;
import com.mishuto.pingtest.model.stat.PingStatistic;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.stream.Stream;

/**
 * Контроллер вьюх на виджете
 * Created by Michael Shishkin on 06.02.2021
 */
public class WidgetViewController {

    private final String host;             // строка с хостом/ip
    private final int widgetId;
    private AppWidgetLayout appWidgetLayout;
    private RemoteViews remoteViews;
    private WidgetData widgetData;
    private boolean inProgress = false;             // кэшированное состояние иконки рефреш
    private final Timer updateTimer = new Timer(6 * SEC); // апдейт виджета не чаще чем раз в 6 сек
    private final ErrMessage errMessage = new ErrMessage();

    public WidgetViewController(String host, int widgetId) {
        Logger.INSTANCE.d("WidgetViewController creating " + widgetId);
        this.host = host;
        this.widgetId = widgetId;
        widgetData =  new WidgetData(new PingStatData(), host);
        adjustWidget();
    }

    public String getHost() {
        return host;
    }

    public int getWidgetId() {
        return widgetId;
    }

    public void onError(String error) {
        errMessage.pushMessage(error);
    }

    public void onUpdate(PingStatistic stat) {
        widgetData = new WidgetData(stat, host);
        if(updateTimer.isTimeUp())
            updateWidget();
    }

    public void refreshWidget() {
        updateWidget();
    }

    private void updateWidget() {
        if(errMessage.isError())
            appWidgetLayout.showError(getRemoteViews(), errMessage.popMessage());
        else
            appWidgetLayout.updateData(getRemoteViews(), widgetData);
        updateRemoteViews();
        Logger.INSTANCE.d("[" + widgetId + "] %s", widgetData.toString());
    }

    public void onStartUpdate() {
        if(isScreenOn()) {     // для минимизации апдейтов виджета, иконка рефреша меняется только при включенном экране
            Logger.INSTANCE.d("WidgetViewController[" + widgetId + "]");
            setInProgressIcon(true);
            updateRemoteViews();
        }
    }

    public void onStopUpdate() {
        Logger.INSTANCE.d("series stat [%d]: %s", widgetId, widgetData.toString());
        if (inProgress) {
            Logger.INSTANCE.d("WidgetViewController[%d]", widgetId);
            setInProgressIcon(false);
            updateWidget(); // еще раз апдейтим виджет, поскольку последний апдейт мог быть отфильтрован updateTimer
        }
    }

    // изменение состояния иконки рефреш. RemoteViews меняется только если состояние было действительно изменено
    private void setInProgressIcon(boolean inProgress) {
        this.inProgress = inProgress;
        getRemoteViews().setImageViewResource(R.id.wid_refresh_btn, inProgress ?
                R.drawable.progress_clock : R.drawable.ic_baseline_refresh_24);
        Logger.INSTANCE.d("WidgetViewController: progress set to %b",inProgress);
    }

    // вычисление нужного layout под размеры виджета
    public void adjustWidget() {
        WidgetOptions sizes = new WidgetOptions(widgetId);
        Logger.INSTANCE.d("WidgetViewController::adjustWidget: [%d]. width: %d/%d/%d. height: %d/%d/%d ",
                widgetId, sizes.getMinWidth(), sizes.getWidth(), sizes.getMaxWidth(), sizes.getMinHeight(), sizes.getHeight(), sizes.getMaxHeight());

        if(sizes.getWidth() >= dimen2dp(R.dimen.big_widget_width) )
            appWidgetLayout = BIG;
        else if(sizes.getWidth() > dimen2dp(R.dimen.medium_widget_width))
            appWidgetLayout = MEDIUM;
        else
            appWidgetLayout = SMALL;

        //подбор виджетов тулбара
        adjustToolbar(sizes.getWidth());

        getRemoteViews().setOnClickPendingIntent(R.id.wid_refresh_btn, WidgetEventHandler.getEventPI(REFRESH, widgetId, null));
        getRemoteViews().setOnClickPendingIntent(R.id.wid_settings_btn, WidgetEventHandler.getEventPI(SHOW_SETTINGS, widgetId, null));
        getRemoteViews().setOnClickPendingIntent(R.id.main, WidgetEventHandler.getEventPI(RUN_APP));

        updateWidget();
    }

    // адаптация тулбара под размер (отображатся кнопки, которые помещаются на тулбаре)
    private void adjustToolbar(int widgetWidth) {
        class ToolbarElement {
            final int id;
            final int width;
            static final float K = 1.3f;

            ToolbarElement(int id, String text, int padding) {
                this.id = id;
                width = (int)((TextSize.ofDimen(R.dimen.widgetText, text).width() + padding) * K);
            }

            ToolbarElement(int id, int width) {
                this.id = id;
                this.width = (int)(width * K);
            }

            void show(boolean isVisible) {
                getRemoteViews().setViewVisibility(id, isVisible ? VISIBLE : GONE);
            }
        }

        ToolbarElement[] toolbarElements = {
                new ToolbarElement(R.id.wid_host, host, 0),
                new ToolbarElement(R.id.wid_time, getLocalTimeString(), 8),
                new ToolbarElement(R.id.wid_settings_btn, 16 + 4),
                new ToolbarElement(R.id.wid_refresh_btn, 16)
        };

        int widthToolbar = Stream.of(toolbarElements)
                .mapToInt(e->e.width)
                .sum();

        for (ToolbarElement e : toolbarElements) {
            if(widthToolbar < widgetWidth)
                e.show(true);
            else {
                e.show(false);
                widthToolbar -= e.width;
            }
        }
    }

    private RemoteViews getRemoteViews() {
        if(remoteViews == null || remoteViews.getLayoutId() != appWidgetLayout.layoutId)
            remoteViews = new RemoteViews(App.getAppContext().getPackageName(), appWidgetLayout.layoutId);

        return remoteViews;
    }

    private static String getLocalTimeString() {
        return LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm"));
    }

    private void updateRemoteViews() {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(App.getAppContext());
        RemoteViews views = getRemoteViews();
        Assert.that(appWidgetManager != null && views != null );
        try {
            appWidgetManager.updateAppWidget(widgetId, views);
        }
        catch (NullPointerException npe) {
            Logger.INSTANCE.e(npe);    // обход NPE в android.widget.RemoteViews$Action.getActionTag()
            remoteViews = null;
        }
        catch (RuntimeException e) {
            if(e.getCause()!=null && e.getCause().getClass() == TransactionTooLargeException.class) {
                Logger.INSTANCE.e(e);   // обход TransactionTooLargeException
                remoteViews = null;
            }
            else
                throw e;
        }
    }

    private static class ErrMessage {
        private boolean isError = false;
        private String message;

        public void pushMessage(String message) {
            this.message = message;
            isError = true;
        }

        public String popMessage() {
            isError = false;
            return message;
        }

        public boolean isError() {
            return isError;
        }
    }

    // представление данных, отображаемых виджетом
    private static class WidgetData {
        int level = 0;
        double overallValue = 0;
        long avg = 0;
        int lostPPM = 0;
        long jitter = 0;
        String host;

        WidgetData(PingStatistic stat, String host) {
            this.host = host;
            if(!stat.isEmpty()) {
                avg = stat.getAverage();
                lostPPM = stat.getLostPermille();
                jitter = stat.getJitter();
                GroupIndicators overall = GroupIndicators.overallFromStat(stat);
                level = overall.getTotalIndicator().getPosition().ordinal() + 1;
                overallValue = overall.getTotalIndicator().computeIndicatorValue();
            }
        }

        String getPing() {
            return App.getAppContext().getString(R.string.num_ms, avg);
        }

        String getJitter() {
            return App.getAppContext().getString(R.string.num_ms, jitter);
        }

        String getLost() {
            return App.getAppContext().getString(R.string.percent1, lostPPM /10f);
        }

        @NonNull
        @Override
        public String toString() {
            return String.format(Locale.ENGLISH, "Overall: %d%%. Ping: %d. Jitter: %d. Lost: %d%%", percent(overallValue), avg, jitter, lostPPM/10);
        }
    }

    // Экземпляры AppWidget разных размеров, реализующие updateData() и showError()
    enum AppWidgetLayout {
        SMALL(R.layout.widget_small) {
            @Override
            void updateData(RemoteViews views, WidgetData widgetData) {
                super.updateData(views, widgetData);
                views.setTextViewText(R.id.wid_ping_val, widgetData.getPing());
            }
        },

        MEDIUM(R.layout.widget_medium) {
            @Override
            void updateData(RemoteViews views, WidgetData widgetData) {
                super.updateData(views, widgetData);

                String lost = widgetData.getLost();
                String jitter = widgetData.getJitter();

                TextExtender textExtender = new TextExtender(lost, jitter);

                views.setTextViewText(R.id.wid_ping_val, widgetData.getPing());
                views.setTextViewText(R.id.wid_lost, textExtender.expandString(lost));
                views.setTextViewText(R.id.wid_jitter, textExtender.expandString(jitter));
            }

            @Override
            void showError(RemoteViews views, String errMes) {
                super.showError(views, errMes);
                views.setTextViewText(R.id.wid_err, errMes);
            }
        },
        BIG(R.layout.widget_big) {
            @Override
            void updateData(RemoteViews views, WidgetData widgetData) {
                super.updateData(views, widgetData);

                String ping = widgetData.getPing();
                String lost = widgetData.getLost();
                String jitter = widgetData.getJitter();

                TextExtender textExtender = new TextExtender(ping, lost, jitter);
                views.setTextViewText(R.id.wid_ping_val, textExtender.expandString(ping));
                views.setTextViewText(R.id.wid_lost, textExtender.expandString(lost));
                views.setTextViewText(R.id.wid_jitter, textExtender.expandString(jitter));
            }

            @Override
            void showError(RemoteViews views, String errMes) {
                super.showError(views, errMes);
                views.setTextViewText(R.id.wid_err, errMes);
            }
        };

        final private int layoutId;

        AppWidgetLayout(int layoutId) {
            this.layoutId = layoutId;
        }

        void updateData(RemoteViews views, WidgetData widgetData) {
            views.setViewVisibility(R.id.wid_err, GONE);
            views.setInt(R.id.wid_status, "setImageLevel", widgetData.level);
            views.setTextViewText(R.id.wid_host, widgetData.host);
            views.setTextViewText(R.id.wid_time, getLocalTimeString());
        }
        void showError(RemoteViews views, String errMes) {
            views.setViewVisibility(R.id.wid_err, VISIBLE);
            views.setTextViewText(R.id.wid_time, getLocalTimeString());
        }
    }
}
