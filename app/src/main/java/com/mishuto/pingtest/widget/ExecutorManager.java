package com.mishuto.pingtest.widget;

import static com.mishuto.pingtest.common.Utils.logger;
import static com.mishuto.pingtest.settings.widget.Settings.toStringSet;

import com.mishuto.pingtest.common.Assert;
import com.mishuto.pingtest.common.CrashlyticsKeys;
import com.mishuto.pingtest.settings.SettingsPrefManager;
import com.mishuto.pingtest.settings.widget.Settings;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Менеджер списка WidgetPingExecutor
 * Created by Michael Shishkin on 10.04.2021
 */
public class ExecutorManager {
    final private static ExecutorManager sInstance = new ExecutorManager();

    final private Map<String, WidgetPingExecutor> mExecutorMap = new HashMap<>();         // список экзекьютеров по одному для хоста
    // режим работы виджетов: можно или нет менять хосты. Пока возможен только один режим, в будущем будет определяться дистрибутивом
    final private boolean isSingleHostMode = true;

    public static ExecutorManager getInstance() {
        return sInstance;
    }

    private ExecutorManager() {
        logger.d("ExecutorManager creating... found executors: [%s]", String.join(",", toStringSet(Settings.persistentIDS().getIDS())));
        if(!Settings.persistentIDS().getIDS().isEmpty())
            CrashlyticsKeys.INSTANCE.update(CrashlyticsKeys.Tag.WIDGETS);
        setChangeAppSettingsHandler();
        instantiateExecutors();
    }

    public boolean isSingleHostMode() {
        return isSingleHostMode;
    }

    public WidgetPingExecutor getExecutor(String host) {
        return mExecutorMap.get(host);
    }

    WidgetPingExecutor getExecutor(WidgetViewController controller) {
        return mExecutorMap.get(controller.getHost());
    }

    Collection<WidgetPingExecutor> executors() {
        return mExecutorMap.values();
    }

    WidgetViewController getViewController(Integer id) {
        return executors().stream()
                .flatMap(e->e.getViewControllers().stream())
                .filter(c->c.getWidgetId() == id)
                .findAny()
                .orElse(null);
    }

    // событие завершения WidgetSettingsActivity
    public void applySettings(Settings settings) {
        if(!mExecutorMap.containsKey(settings.host))
            mExecutorMap.put(settings.host, new WidgetPingExecutor(settings));

        getExecutor(settings.host).applySettings(settings);
    }

    void removeWidget(WidgetViewController controller) {
        WidgetPingExecutor executor = getExecutor(controller);
        Settings settings = new Settings(executor.getId());

        settings.removeWidget(controller.getWidgetId());
        executor.applySettings(settings);

        if(executor.getViewControllers().isEmpty()) {   // если удален последний виджет в экзекуторе
            executor.destroy();
            Settings.persistentIDS().remove(settings);  // удаляем преференсы экзекьютора
            mExecutorMap.remove(executor.getHost());    // удаляем экзекутор
            logger.d("executor " + executor.getHost() + " removed");
        }
    }

    private void instantiateExecutors() {
        Settings.persistentIDS().getIDS().forEach(id ->
                {
                    Settings settings = new Settings(id);
                    mExecutorMap.put(settings.host, new WidgetPingExecutor(settings));
                }
        );
        executors().forEach(WidgetPingExecutor::start);
    }

    // установка обработчика изменения app settings
    private void setChangeAppSettingsHandler() {
        SettingsPrefManager pref = SettingsPrefManager.INSTANCE;

        pref.registerOnSPChangeListener(getClass().getSimpleName(), (sp, key) ->
        {
            if (!mExecutorMap.isEmpty()) {
                // смена хоста в настройках в SingleHosMode режиме меняет хост и в экзекьютере
                if (isSingleHostMode && pref.getHostKey().equals(key)) {
                    Assert.that(executors().size() == 1, "only one host allowed for the SingleHost mode");
                    //noinspection OptionalGetWithoutIsPresent
                    WidgetPingExecutor executor = executors().stream().findFirst().get(); // получить экзекьютор из списка из одного экзекьютора
                    String newHost = pref.getPreferences().getHost();
                    Settings.Builder builder = new Settings.Builder(executor.getId());
                    Settings settings = builder.setHost(newHost).build();   // перезаписываем хост в сеттингах экзекьютера
                    mExecutorMap.clear();
                    mExecutorMap.put(newHost, executor);
                    executor.applySettings(settings);
                }
            }
        });
    }
}
