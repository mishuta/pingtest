package com.mishuto.pingtest.widget;

import static com.mishuto.pingtest.common.Const.MIN;
import static com.mishuto.pingtest.common.Timer.millsSince;
import static com.mishuto.pingtest.common.Utils.logger;
import static com.mishuto.pingtest.common.wrappers.PMUtils.isPowerConnected;
import static com.mishuto.pingtest.common.wrappers.PMUtils.isScreenOn;
import static com.mishuto.pingtest.widget.WidgetEventHandler.WidgetEvent.SERIES;
import static com.mishuto.pingtest.widget.WidgetEventHandler.WidgetEvent.STOP_SERIES;
import static com.mishuto.pingtest.widget.WidgetEventHandler.getEventPI;
import static com.mishuto.pingtest.settings.widget.Settings.toStringSet;
import static java.time.Instant.now;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.mishuto.pingtest.common.FixCircularBuffer;
import com.mishuto.pingtest.common.Job;
import com.mishuto.pingtest.common.dispatchers.IntentDispatcher;
import com.mishuto.pingtest.common.net.NetInfo;
import com.mishuto.pingtest.common.tickers.Ticker;
import com.mishuto.pingtest.common.wrappers.WakeLockWrapper;
import com.mishuto.pingtest.common.wrappers.WiFiLockWrapper;
import com.mishuto.pingtest.model.HostInfo;
import com.mishuto.pingtest.model.PingException;
import com.mishuto.pingtest.model.PingHolder;
import com.mishuto.pingtest.model.ping.Ping;
import com.mishuto.pingtest.model.ping.cmd.AsyncPingCmd;
import com.mishuto.pingtest.model.stat.PingStatistic;
import com.mishuto.pingtest.model.stat.StreamPingStat;
import com.mishuto.pingtest.settings.widget.Settings;

import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Логика реализации пинга в виджете.
 * Один WidgetPingExecutor создается на один уникальный хост. События PingListener вызываются во всех виджетах для данного хоста
 * Created by Michael Shishkin on 06.02.2021
 */
public class WidgetPingExecutor {

    private static final int MIN_SERIES_DURATION = 3*MIN;
    private Settings mSettings;
    private Set<WidgetViewController> mViewControllers = new HashSet<>();
    private Instant lastSeriesTime = now();             // время последней серии
    private Instant lastSuccessfulSeriesTime = now();   // время последней успешной серии пингов. Серия успешна, если не было ошибок, прерываний или в idle не был 100% lost
    private final PingSeriesController mSeries;
    private Job mJob;                                   // джоба для периодического запуска серии пингов
    private boolean isSeriesStarted = false;            // идет ли серия пингов
    private final NetInfo mNetInfo = NetInfo.getInstance();
    private final WiFiLockWrapper wifiLock = WiFiLockWrapper.getLock("WidgetPingExecutor");
    private final WakeLockWrapper wakeLock = WakeLockWrapper.getLock("WidgetPingExecutor");


    public WidgetPingExecutor(Settings settings) {
        logger.d("WidgetPingExecutor creating");
        IntentDispatcher.INSTANCE.register(Intent.ACTION_SCREEN_ON, "WidgetPingExecutor", this::onInteractiveMode);
        initialize(settings);
        mSeries = new PingSeriesController();
        mSeries.usePacker();
    }

    synchronized public void applySettings(Settings settings) {
        logger.d("WidgetPingExecutor::applySettings");
        mSeries.useSlicer();

        stop();
        initialize(settings);
        start();
    }

    synchronized private void initialize(Settings settings) {
        mSettings = settings;
        logger.d("executor initialize.  " + settings.toString());
        logger.d("executor founds widgets: [%s]", String.join(",", toStringSet(settings.widgetIDs)));
        mViewControllers = settings.widgetIDs.stream()
                .map((id)-> new WidgetViewController(mSettings.host, id))
                .collect(Collectors.toSet());

        if(mJob == null)
            mJob = new Job(mSettings.id,
                    getEventPI(SERIES, getId(), null),
                    getEventPI(STOP_SERIES, getId(), null),
                    mSettings.seriesInterval,
                    true);
        else
            mJob.resetFireTime(mSettings.seriesInterval);
    }

    public void destroy() {
        stop();
        IntentDispatcher.INSTANCE.unregister(Intent.ACTION_SCREEN_ON, "WidgetPingExecutor");
    }

    // запуск экзекьютера
    synchronized public void start() {
        logger.tag();
        mJob.schedule();
        startSeries();
    }

    // останов экзекьютера
    synchronized public void stop() {
        mJob.cancel();
        stopSeries();
    }

    // старт серии
    synchronized void startSeries() {
        if(!isSeriesStarted) {
            isSeriesStarted = true;

            // не стартуем серию, если экран погашен, вай-фай включен, галка в настройках установлена
            if(mSettings.notPingWiFiInIdle && !isScreenOn() && mNetInfo.isWiFiOn() && !isPowerConnected()) {
                logger.d("series won't be produced due to notPingInIdle flag");
                onStopSeries(false);
            }
            else {
                mViewControllers.forEach(WidgetViewController::onStartUpdate);
                logger.d("%s started %d minutes after the last successful finish", mSeries, Duration.between(lastSuccessfulSeriesTime, now()).toMinutes());
                logger.d("WiFi: %b. Screen: %b. Plugged: %s. Blocked: %b", mNetInfo.isWiFiOn(), isScreenOn(), isPowerConnected(), mNetInfo.isBlocked());
                mSeries.start();
            }
        }
    }

    // останов серии
    synchronized void stopSeries() {
        if(isSeriesStarted) {
            mSeries.stop();
        }
    }

    private void onStopSeries(boolean isSuccessful) {
        logger.d("%s stopSeries: %b", mSeries.toString(), isSuccessful);
        mViewControllers.forEach(WidgetViewController::onStopUpdate);
        lastSeriesTime = now();
        if(isSuccessful)
            lastSuccessfulSeriesTime = lastSeriesTime;
        mJob.jobFinished(true);
        isSeriesStarted = false;
    }

    synchronized public void refresh() {
        logger.d("Refresh");
        mSeries.useSlicer();
        stopSeries();
        startSeries();
    }

    synchronized void onPeriodicStart() {
        if (!isSeriesStarted) {
            // серия стартует по таймеру, если с момента последнего успешного запуска прошло более половины интервала
            if(millsSince(lastSuccessfulSeriesTime) > mSettings.seriesInterval / 2) {
                mSeries.chooseSeriesLauncher();
                startSeries();
            }
            else {
                mJob.jobFinished(true);
                logger.d("series cancelled due to interval too small");
            }
        }
    }

    void onInteractiveMode(Intent intent) {
        // запускаем серию, когда экран включили, а с момента прошлой УСПЕШНОЙ серии прошло больше интервала (т.е. прошлые серии не были прерваны и в айдле не было 100% lost)
        // И с момента последней попытки больше чем MIN_SERIES_DURATION
        if(millsSince(lastSuccessfulSeriesTime) > mSettings.seriesInterval && millsSince(lastSeriesTime) > MIN_SERIES_DURATION) {
            logger.d("PingExecutor::start series due to Interactive Mode");
            mSeries.useSlicer();
            startSeries();
        }
    }

    public Set<WidgetViewController> getViewControllers() {
        return mViewControllers;
    }

    public String getHost() {
        return mSettings.host;
    }

    public int getId() {
        return mSettings.id;
    }

    //интерфейс объектов, управляющих серией
    private interface SeriesLauncher {
        void start();
        void stop();
    }

    // запускает серию одиночных стандартных пингов
    private class Slicer implements SeriesLauncher, PingHolder.PingListener {
        private final StreamPingStat mPingStat;
        int mPingCount;
        private Ticker mPingTicker;
        PingHolder pingHolder;

        public Slicer() {
            mPingStat = new StreamPingStat(new FixCircularBuffer<>(mSettings.seriesLen));
        }

        @Override
        public void start() {
            logger.tag();
            wakeLock.lock((long) mSettings.seriesLen * (mSettings.timeout + mSettings.seriesInterval));
            wifiLock.lock();
            pingHolder = new PingHolder(this, mSettings.timeout);
            mPingTicker = new Ticker(pingHolder::doPing, false, mSettings.interval, "--WidgetDoPingTicker");
            mPingCount = 1;             // счетчик пингов в серии
            mPingStat.clear();
            pingHolder.resolve(mSettings.host);
        }

        @Override
        public void stop() {
            logger.d("Slicer stop");
            mPingTicker.stop();
            pingHolder.unregister();
            stopSeries(false);
        }

        private void stopSeries(boolean isSuccessful) {
            wifiLock.unlock();
            wakeLock.unlock();
            onStopSeries(isSuccessful);
        }

        @Override
        public void onResolve(PingHolder pingHolder) {
            pingHolder.doPing();
        }

        @Override
        public void onPing(Ping ping) {
            if (!isSeriesStarted)
                return;

            logger.d(ping.asString());

            mPingStat.addNew(ping);
            mViewControllers.forEach(p -> p.onUpdate(mPingStat));

            if(mPingCount++ <= mSettings.seriesLen )
                mPingTicker.start();
            else
                stopSeries(true);
        }

        @Override
        public void onPingError(PingException exception) {
            logger.d("Slicer::onPingError: %s. %s ", exception.getClass().getSimpleName(), exception.getMessage());
            mViewControllers.forEach(c -> c.onError(exception.getMessage()));
            stop();
        }

        @Override
        public void onLookUp(HostInfo hostInfo) {}
    }

    // запускает командный пакет пингов
    private class Packer implements SeriesLauncher, AsyncPingCmd.OnAsyncPingCmdListener {

        private final AsyncPingCmd mPingCmd = new AsyncPingCmd();

        @Override
        public void start() {
            int pingDeadline = mSettings.seriesLen * mSettings.timeout * 3/2;
            wakeLock.lock(pingDeadline);
            wifiLock.lock();

            mPingCmd.execAsyncSeries(this, mSettings.host, mSettings.seriesLen, mSettings.interval, pingDeadline);
        }

        @Override
        public void stop() {
            logger.d("Packer stop");
            mPingCmd.interrupt();
            stopSeries(false);
        }

        @Override
        public void onPingCmdSuccess(PingStatistic stat) {
            logger.d("Packer onPingCmdSuccess");
            mViewControllers.forEach(p -> p.onUpdate(stat));
            stopSeries( !( !isScreenOn() && mNetInfo.isWiFiOn() && stat.getLostPermille() == 1000));
        }

        @Override
        public void onPingCmdFault(Exception e) {
            logger.d("Packer onPingCmdFault::%s. %s", e.getClass().getSimpleName(), e.getMessage());
            mViewControllers.forEach(p -> p.onError(e.getMessage()));
            stopSeries(false);
        }

        private void stopSeries(boolean isSuccessful) {
            // операции по завершению серии
            wifiLock.unlock();
            wakeLock.unlock();
            onStopSeries(isSuccessful);
        }
    }

    // управляет выбором объекта серии для текущей итерации
    private class PingSeriesController {
        final SeriesLauncher slicer;
        final SeriesLauncher packer;
        SeriesLauncher current;     // current и next необходимы,
        SeriesLauncher next;        // поскольку методы useSlicer/usePacker могут быть вызваны до вызова stop(), а значит информация о текущем объекте не должна потеряться

        public PingSeriesController() {
            slicer = new Slicer();
            packer = new Packer();
            next = packer;
        }

        public void useSlicer() {
            next = slicer;
        }

        public void usePacker() {
            next = packer;
        }

        public void chooseSeriesLauncher() {
            if(isScreenOn())
                useSlicer();
            else
                usePacker();
        }

        public void start() {
            current = next;
            current.start();
        }

        public void stop() {
            current.stop();
        }

        @NonNull
        @Override
        public String toString() {
            return current == slicer ? "Slicer" : "Packer";
        }
    }
}
