package com.mishuto.pingtest.backend.rest

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Environment
import com.mishuto.pingtest.common.Resources.getBoolean
import com.mishuto.pingtest.common.Resources.getInt
import com.mishuto.pingtest.common.Resources.getString

/**
 * Конфиг рест-сервера
 * Created by Michael Shishkin on 03.07.2024
 */
object ServerConfig {
    val serverHost
        get() = if (!Environment.isDebug || getBoolean(R.bool.use_prod_host_in_debug_mode))
            getString(R.string.server_host_prod)
        else
            getString(R.string.server_host_debug)
    
    val restApiBaseUrl get() = "$serverHost/api/v${getString(R.string.server_api_ver)}/"
    
    val purchaseCompleteDeepLink = getString(R.string.purchase_complete_deep_link)
    
    val minReactivationPeriodDays = getInt(R.integer.min_reactivation_period_days).toLong()
}