package com.mishuto.pingtest.backend.rest

import com.mishuto.pingtest.backend.rest.dto.ActivationRequest
import com.mishuto.pingtest.backend.rest.dto.LicenseInfoResponse
import com.mishuto.pingtest.backend.rest.dto.LicenseResponse
import retrofit2.http.*


/**
 * REST-интерфейс к серверу биллинга
 * Created by Michael Shishkin on 17.05.2024
 */
interface RestApi {
    @POST("license")
    suspend fun createLicense(@Body deviceInfo: String): LicenseResponse
    
    @GET("license/status")
    suspend fun getLicenseStatus(@Query("licenseKey") licenseKey: String): LicenseInfoResponse
    
    @POST("activation")
    suspend fun createActivation(@Body activationRequest: ActivationRequest): LicenseInfoResponse
    
    @DELETE("activation/{appGuid}")
    suspend fun deleteActivation(@Path("appGuid") appGuid: String)
    
}