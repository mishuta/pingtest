package com.mishuto.pingtest.backend.rest.dto.error

import com.mishuto.pingtest.R
import com.mishuto.pingtest.backend.rest.ServerConfig.minReactivationPeriodDays
import com.mishuto.pingtest.common.Utils.getString
import com.mishuto.pingtest.common.shortDateFormat
import java.time.LocalDate
import java.time.LocalDateTime

class ActivationPeriodViolationResponse(
    val activationDate: LocalDate,
    val activationDevice: String,
    responseCode: Int,
    errorClassName: String,
    description: String,
    timestamp: LocalDateTime
) : ServerErrorResponse(responseCode, errorClassName, description, timestamp) {
    override fun errorMessage(): String = getString(
        R.string.activation_period_violation,
        activationDate.shortDateFormat(),
        activationDevice,
        activationDate.plusDays(minReactivationPeriodDays).shortDateFormat()
    )
}


