package com.mishuto.pingtest.backend.rest

import com.google.gson.JsonElement
import com.mishuto.pingtest.backend.rest.GsonUtils.toJsonElement
import com.mishuto.pingtest.backend.rest.GsonUtils.toObject
import com.mishuto.pingtest.backend.rest.dto.error.*
import com.mishuto.pingtest.common.Logger
import retrofit2.HttpException

/**
 * Стандартный эксепшн для реста
 * Created by Michael Shishkin on 04.06.2024
 */
class ServerError(serverErrorResponse: ServerErrorResponse) : Exception(serverErrorResponse.errorMessage())

object ServerErrorHandler {
    fun Throwable.tryRetrieveServerErrorResponse(): Throwable {
        if (this is HttpException) {
            try {
                response()?.errorBody()?.let { errorBody ->
                    val serverErrorResponse = convertToServerErrorResponse(errorBody.toJsonElement())
                    return ServerError(serverErrorResponse)
                }
            }
            catch (t: Throwable) {
                Logger.e(t)
                return this
            }
        }
        return this
    }
    
    private fun convertToServerErrorResponse(root: JsonElement) : ServerErrorResponse {
        val className = root.asJsonObject["errorClassName"].asString
        Logger.d("className: $className")
        
        val classes = listOf(
            ServerErrorResponse::class,
            LicenseNotPurchasedResponse::class,
            LicenseNotFoundResponse::class,
            TooManyLicensesResponse::class,
            ActivationNotFoundResponse::class,
            TooManyActivationsResponse::class,
            ActivationPeriodViolationResponse::class,
            UnsupportedApiVersion::class,
        )
        
        return classes.firstOrNull { it.simpleName == className}
            ?.let { root.toObject(it.java) }
            ?: root.toObject(ServerErrorResponse::class.java) // если у сервера появился новый класс, которого еще нет у клиента, отобразится нативное сообщение
    }
}
