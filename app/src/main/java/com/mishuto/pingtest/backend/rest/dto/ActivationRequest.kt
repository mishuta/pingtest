package com.mishuto.pingtest.backend.rest.dto

data class ActivationRequest(
    val licenseKey: String,
    val appGuid: String,
    val model: String
)