package com.mishuto.pingtest.backend.rest.dto.error

import java.time.LocalDateTime

@Suppress("unused")
open class ServerErrorResponse(
    val responseCode: Int,
    val errorClassName: String,
    val description: String,
    val timestamp: LocalDateTime
) {
    open fun errorMessage(): String = description
}