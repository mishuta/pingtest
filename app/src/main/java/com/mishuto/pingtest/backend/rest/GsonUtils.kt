package com.mishuto.pingtest.backend.rest

import com.google.gson.*
import okhttp3.ResponseBody
import java.lang.reflect.Type
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Locale


/**
 * Gson конфиг и утилиты
 * Created by Michael Shishkin on 04.06.2024
 */
object GsonUtils {
    const val DATE_PATTERN = "yyyy-MM-dd"
    const val DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss"
    
    private val jsonParser = JsonParser()
    
    val gson: Gson = GsonBuilder()
        .registerTypeAdapter(LocalDate::class.java, LocalDateDeserializer())
        .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeDeserializer())
        .create()
    
    fun ResponseBody.toJsonElement(): JsonElement = jsonParser.parse(charStream())
    
    fun <T> JsonElement.toObject(javaClass: Class<T>): T = gson.fromJson(this, javaClass)
    
    private class LocalDateDeserializer : JsonDeserializer<LocalDate?> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext?): LocalDate =
            LocalDate.parse(json.asString, DateTimeFormatter.ofPattern(DATE_PATTERN).withLocale(Locale.ENGLISH))
    }
    
    private class LocalDateTimeDeserializer : JsonDeserializer<LocalDateTime> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): LocalDateTime =
            LocalDateTime.parse(json.asString, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN).withLocale(Locale.ENGLISH))
    }
    
    
}