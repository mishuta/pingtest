package com.mishuto.pingtest.backend.rest.dto.error

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Utils
import com.mishuto.pingtest.controller.features.premium.billing.alternative.License
import java.time.LocalDateTime

class LicenseNotFoundResponse(
    val key: String,
    responseCode: Int,
    errorClassName: String,
    description: String,
    timestamp: LocalDateTime
) : ServerErrorResponse(responseCode, errorClassName, description, timestamp) {
    override fun errorMessage(): String = Utils.getString(R.string.license_not_found, License.formatKey(key))
}