package com.mishuto.pingtest.backend.rest.dto.error

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Utils
import com.mishuto.pingtest.controller.features.premium.billing.alternative.License
import java.time.LocalDateTime

class TooManyActivationsResponse(
    val key: String,
    val count: Int,
    val permitted: Int,
    responseCode: Int,
    errorClassName: String,
    description: String,
    timestamp: LocalDateTime
) : ServerErrorResponse(responseCode, errorClassName, description, timestamp) {
    override fun errorMessage(): String = Utils.getString(R.string.too_many_activations, count, License.formatKey(key), permitted)
}