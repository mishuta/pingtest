package com.mishuto.pingtest.backend.rest.dto

data class LicenseResponse(val licenseKey: String)