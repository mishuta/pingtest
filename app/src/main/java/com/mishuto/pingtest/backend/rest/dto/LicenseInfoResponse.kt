package com.mishuto.pingtest.backend.rest.dto

data class LicenseInfoResponse(
    val exist: Boolean,
    val paid: Boolean,
    val permittedDevices: Int,
    val activatedDevices: MutableList<ActivatedDevice>,
)