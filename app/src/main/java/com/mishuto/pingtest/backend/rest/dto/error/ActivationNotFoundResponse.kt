package com.mishuto.pingtest.backend.rest.dto.error

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Utils
import java.time.LocalDateTime

class ActivationNotFoundResponse(
    val appGuid: String,
    responseCode: Int,
    errorClassName: String,
    description: String,
    timestamp: LocalDateTime
) : ServerErrorResponse(responseCode, errorClassName, description, timestamp) {
    override fun errorMessage(): String = Utils.getString(R.string.activation_not_found, appGuid)
}