package com.mishuto.pingtest.backend.rest

import com.mishuto.pingtest.backend.rest.GsonUtils.gson
import com.mishuto.pingtest.backend.rest.ServerConfig.restApiBaseUrl
import com.mishuto.pingtest.common.Logger
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Конфигурация ретрофита
 * Created by Michael Shishkin on 06.06.2024
 */
object RetrofitConfig {
    val restService: RestApi by lazy { retrofit.create(RestApi::class.java) }
    
    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(restApiBaseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(clientConfigure())
            .build()
    }
    
    private fun clientConfigure(): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(loggerInterceptor())
        .addNetworkInterceptor(response204WorkaroundInterceptor())
        .build()
    
    private fun loggerInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor { message -> Logger.d(message) }
        .setLevel(HttpLoggingInterceptor.Level.BODY)
    
    private fun response204WorkaroundInterceptor(): Interceptor = Interceptor { chain ->
        val request = chain.request()
        val response: Response = chain.proceed(request)
        if (response.code() == 204) response.newBuilder().code(200).build()
        else response
    }
}