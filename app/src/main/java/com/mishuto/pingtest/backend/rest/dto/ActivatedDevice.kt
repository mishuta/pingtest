package com.mishuto.pingtest.backend.rest.dto

import java.time.LocalDate

data class ActivatedDevice(
    val appGuid: String,
    val model: String,
    val activationDate: LocalDate
)