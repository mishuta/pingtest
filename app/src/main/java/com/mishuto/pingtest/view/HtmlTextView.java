package com.mishuto.pingtest.view;

import android.content.Context;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.text.HtmlCompat;

/**
 * Класс для отображения html текста в TextView, в том числе в EditMode Android Studio
 * Created by Michael Shishkin on 01.10.2023
 */
public class HtmlTextView extends androidx.appcompat.widget.AppCompatTextView {
    public HtmlTextView(@NonNull Context context) {
        super(context);
        init();
    }

    public HtmlTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HtmlTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        super.setText(convertHtml(getText()));
        setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void setText(String text) {
        super.setText(convertHtml(text));
    }

    public void setFromResource(int resourceId) {
        setText(getContext().getString(resourceId));
    }

    private Spanned convertHtml(CharSequence text) {
        return HtmlCompat.fromHtml(text==null ? "" : text.toString(), HtmlCompat.FROM_HTML_MODE_COMPACT);
    }
}
