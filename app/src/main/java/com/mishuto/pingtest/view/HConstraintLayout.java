package com.mishuto.pingtest.view;

import static android.view.View.MeasureSpec.makeMeasureSpec;
import static com.mishuto.pingtest.view.Utils.dimen2pix;
import static com.mishuto.pingtest.view.Utils.getScreenSize;
import static com.mishuto.pingtest.view.Utils.getStatusBarSize;

import android.content.Context;
import android.util.AttributeSet;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.mishuto.pingtest.R;

/**
 * ConstraintLayout, подстраивающий высоту под высоту экрана
 * Используется для горизонтального положения экрана в скролируемых разметках
 * Created by Michael Shishkin on 01.07.2020
 */
public class HConstraintLayout extends ConstraintLayout {
    private final static int SIZE_FOR_LAYOUT_EDITOR = 700;

    public HConstraintLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = isInEditMode() ? SIZE_FOR_LAYOUT_EDITOR : getWindowHeight();

       heightMeasureSpec = makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private int getWindowHeight() {
        int screenSize = getScreenSize(getContext()).y;
        int statusBarSize = getStatusBarSize(getContext());
        int toolBarSize = dimen2pix(R.dimen.toolBarHeight);

        return screenSize - (statusBarSize + toolBarSize);
    }
}
