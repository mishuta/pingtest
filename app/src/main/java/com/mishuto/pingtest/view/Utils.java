package com.mishuto.pingtest.view;

import static android.view.View.LAYOUT_DIRECTION_RTL;
import static java.lang.Math.round;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.R;

/**
 * Утилиты для графической части
 * Created by Michael Shishkin on 13.07.2020
 */
public class Utils {

    // размеры экрана
    public static Point getScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    // высота StatusBar
    public static int getStatusBarSize(Context context) {
        Rect rectangle = new Rect();
        Window window = getActivityFromViewContext(context).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        return rectangle.top - contentViewTop;
    }

    public static Activity getActivityFromViewContext(Context context) {
        while (context != null) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    public static int dp2pix(int dp) {
        return dp2pix(App.getAppContext(), dp);
    }

    // специальный метод для вызова в EditMode
    public static int dp2pix(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return round(dp * displayMetrics.density);
    }

    public static int pix2dp(int pix) {
        DisplayMetrics displayMetrics = App.getAppContext().getResources().getDisplayMetrics();
        return round(pix / displayMetrics.density);
    }

    public static int dimen2pix(int dimenID) {
        return App.getAppContext().getResources().getDimensionPixelSize(dimenID);
    }

    public static int dimen2dp(int dimenID) {
        return pix2dp(dimen2pix(dimenID));
    }

    /**
     * Класс для вычисления длины/ширины текста в dp
     */
    public static class TextSize {
        Rect mTextBounds = new Rect();

        private TextSize(int pix, String text) {
            Paint paint = new Paint();
            paint.setTextSize(pix);
            paint.getTextBounds(text, 0, text.length(), mTextBounds);
        }

        public static TextSize ofDp(int dp, String text) {
            return new TextSize(dp2pix(dp), text);
        }

        public static TextSize ofDimen(int dimenId, String text) {
            return new TextSize(dimen2pix(dimenId), text);
        }

        public int width() {
            return pix2dp(mTextBounds.width());
        }

        public int height() {
            return pix2dp(mTextBounds.height());
        }
    }

    public static Paint getPaint(Context context, int colorID, Paint.Style style) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(context.getColor(colorID));
        paint.setStyle(style);

        return paint;
    }

    public static boolean isLtr(View v) {
        return v.getLayoutDirection() != LAYOUT_DIRECTION_RTL;
    }

    public final static int ANIMATE_DURATION = 250;

    public static void changeAnimatedBackground(View view, boolean selected) {
        int colorFrom = view.getContext().getColor(selected ? R.color.colorSurface : R.color.colorItemSelected);
        int colorTo = view.getContext().getColor(selected ? R.color.colorItemSelected : R.color.colorSurface);

        ObjectAnimator.ofObject(view, "backgroundColor", new ArgbEvaluator(), colorFrom, colorTo)
                .setDuration(ANIMATE_DURATION)
                .start();

    }

    // аналог View.isInEditMode(), для случаев, когда view недоступна. Например, отображение Preferences
    public static boolean isInEditMode(Context context) {
        return new View(context).isInEditMode();
    }
}
