package com.mishuto.pingtest.view

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Assert
import com.mishuto.pingtest.view.Utils.getPaint
import kotlin.math.*

/**
 * Сегментированный Progressbar
 * Имеет вертикальный и горизонтальный режимы и возможность выводить несколько цветов (градиентная заливка)
 * Created by Michael Shishkin on 17.10.2020. Refactored on kotlin on 08.01.2024
 */
const val PROGRESS_FULL_ANIMATION_TIME = 700L
private const val MAX_SEGMENTS = 60

class SegmentedProgressBar(context: Context, attrs: AttributeSet) : View(context, attrs) {
    private val isHorizontalOrientation: Boolean   // ориентация
    private var dashSize: Int                      // размер сегмента (длина или высота). По умолчанию размер такой, чтобы количество сегментов = количеству цветов
    private val dashGap: Int                       // расстояние между сегментами
    private val backgroundTint: Int                // цвет бэкграунда (цвет между сегментами)
    private val dashColor: Int                     // цвет незаполненного сегмента
    private val paints: Array<Paint>               // Палитра цветов сегментов. Массив цветов должен задаваться в integer-array
    private lateinit var dashPaint: Paint          // Палитра для незаполненного сегмента
    private val cornerRadius: Float                // радиус скруглений
    private var sizePx: Int = 0                    // размер прогрессбара
    private val tag: String
    private lateinit var clipPath: Path
    private val animator: ValueAnimator = ValueAnimator()
    
    // максимальное значение прогресса
    var maxProgress: Int = 0
        set(value) {
            field = value
            invalidate()
        }
    
    // целевое значение прогресса
    var progress: Int = 0
        set(value) {
            if (value != field) {
                field = max(0, min(value, maxProgress))
                animator.animateUpdate(from = instantProgress, to = field)
            }
        }
    
    // Текущее мнгновенное значение прогресса во время анимации. Должно соответствовать целевому значению progress после отработки анимации,
    // но может отставать, если новое значение пришло раньше завершения анимации, что дает иллюзию инерционности индикатору
    private var instantProgress: Int = 0
        set(value) {
            Assert.that(instantProgress in 0..maxProgress)
            if (field != value) {
                field = value
                invalidate()
            }
        }
    
    private fun ValueAnimator.animateUpdate(from: Int, to: Int) {
        cancel()
        setIntValues(from, to)
        duration = abs(to - from) * PROGRESS_FULL_ANIMATION_TIME / maxProgress
        addUpdateListener { a -> instantProgress = a.animatedValue as Int }
        start()
    }
    
    init {
        with(context.obtainStyledAttributes(attrs, R.styleable.SegmentedProgressBar)) {
            isHorizontalOrientation = getInt(R.styleable.SegmentedProgressBar_android_orientation, 0) == 0
            dashSize = getDimensionPixelSize(R.styleable.SegmentedProgressBar_android_dashWidth, 0)
            dashGap = getDimensionPixelSize(R.styleable.SegmentedProgressBar_android_dashGap, 0)
            paints = createColorSegmentsPalette(getResourceId(R.styleable.SegmentedProgressBar_colorArray, 0))
            maxProgress = getInt(R.styleable.SegmentedProgressBar_android_max, 100)
            instantProgress = getInt(R.styleable.SegmentedProgressBar_android_progress, 0)
            backgroundTint = getInt(R.styleable.SegmentedProgressBar_android_backgroundTint, R.color.colorIndicatorDash)
            dashColor = getResourceId(R.styleable.SegmentedProgressBar_dashColor, R.color.colorBackground)
            cornerRadius = getDimension(R.styleable.SegmentedProgressBar_cornerRadius, 0f)
            tag = getString(R.styleable.SegmentedProgressBar_android_tag) ?: ""
            recycle()
        }
    }
    
    private fun createColorSegmentsPalette(resourceArrayId: Int): Array<Paint> =
        if (resourceArrayId == 0)
            arrayOf(getPaint(context, R.color.colorSecondary, Paint.Style.FILL)) // по умолчанию палитра состоит из одного цвета colorSecondary
        else {
            val typedArray = resources.obtainTypedArray(resourceArrayId)
            Array(typedArray.length()) { getPaint(context, typedArray.getResourceId(it, 0), Paint.Style.FILL) }
                .also { typedArray.recycle() }
        }
    
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        sizePx = if (isHorizontalOrientation) width else height
        val minDashSize = sizePx / MAX_SEGMENTS - dashGap
        if (dashSize == 0) dashSize = sizePx / paints.size - dashGap // Размер сегмента по умолчанию. Количество сегментов = количеству цветов
        dashSize = max(minDashSize, dashSize)
        dashPaint = getPaint(context, dashColor, Paint.Style.FILL)
        clipPath = computeClipPath()
    }
    
    // область рисования = canvas - скругления углов
    private fun computeClipPath(): Path {
        val r = Utils.dp2pix(context, cornerRadius.toInt())
        val path = Path()
        path.addRoundRect(0f, 0f, width.toFloat(), height.toFloat(), r.toFloat(), r.toFloat(), Path.Direction.CW)
        return path
    }
    
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.clipPath(clipPath)
        canvas.drawColor(backgroundTint)
        drawSegments(canvas)
    }
    
    private fun drawSegments(canvas: Canvas) {
        val progressInPx = sizePx * instantProgress / maxProgress
        
        for (pos in 0..sizePx step dashSize + dashGap) {
            val paint = if (pos < progressInPx) paints[colorIndexByPosition(pos)] else dashPaint
            val dashRect: Rect = getDrawingRect(pos, dashSize.coerceAtMost(sizePx - pos))
            canvas.drawRect(dashRect, paint)
        }
    }
    
    private fun colorIndexByPosition(position: Int) = (position.toFloat() / sizePx * paints.size).roundToInt().coerceAtMost(paints.size - 1)
    
    private fun getDrawingRect(pos: Int, delta: Int): Rect =
        with(Rect()) {
            val ltr = Utils.isLtr(this@SegmentedProgressBar)
            if (isHorizontalOrientation) {
                left = if (ltr) pos else width - pos - delta
                right = if (ltr) pos + delta else width - pos
                top = 0
                bottom = height
            }
            else {
                left = 0
                right = width
                bottom = height - pos
                top = height - pos - delta
            }
            return this
        }
}