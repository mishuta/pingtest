package com.mishuto.pingtest.view

import android.content.Context
import android.content.res.ColorStateList
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap

/**
 * Утилиты GUI
 * Created by Michael Shishkin on 17.02.2024
 */
fun ImageView.setTintColor(resId: Int) = setColorFilter(ContextCompat.getColor(context, resId))

fun Context.createImageFrom(resId: Int): ImageView = ImageView(this).apply { setImageResource(resId) }

fun ImageView.sameAs(secondImage: ImageView) = drawable.toBitmap().sameAs(secondImage.drawable.toBitmap())

//найти view по типу, если id неизвестен
@Suppress("UNCHECKED_CAST")
fun <T : View> ViewGroup.findViewByType(c: Class<T>): T? {
    for (i in 0 until childCount) {
        val view = getChildAt(i)
        if (view.javaClass == c)
            return view as T
        else if (view is ViewGroup)
            view.findViewByType(c)?.let { return it }
    }
    return null
}

fun View.setBackgroundTintColor(colorResId: Int) {
    val color = ContextCompat.getColor(context,colorResId)
    backgroundTintList = ColorStateList.valueOf(color)
}

