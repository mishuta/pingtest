package com.mishuto.pingtest.view

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.view.children
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Assert

/**
 * Виджет для отображения параметра в layout, состоящего из значения value и метки label
 * Наиболее простой и эффективный способ сделать параметризуемый sab-layout, который бы отображался в EditMode в AndroidStudio.
 * (Простой include не параметризуется. include + binding не отображается в студии)
 * Виджету передается layout, который должен содержать два TextView: value и label
 *
 * Created by Michael Shishkin on 05.08.2022
 */
class ParameterWidget(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {
    
    private val valueInitialText: String?
    private val labelInitialText: String?
    
    private lateinit var valueView: TextView
    private lateinit var labelView: TextView
    
    fun setText(text: String) {
        valueView.text = text
    }
    
    init {
        val typedArray: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.ParameterWidget)
        valueInitialText = typedArray.getString(R.styleable.ParameterWidget_value)
        labelInitialText = typedArray.getString(R.styleable.ParameterWidget_label)
        val layoutId = typedArray.getResourceId(R.styleable.ParameterWidget_layout, 0)
        typedArray.recycle()
        
        inflate(layoutId)
    }
    
    private fun inflate(layout: Int) {
        Assert.that(layout != 0, "layout attribute cannot be empty")
        inflate(context, layout, this)
    }
    
    override fun onViewAdded(child: View?) {
        fun TextView.setTextAttr() {
            when (id) {
                R.id.value -> {
                    valueView = this
                    if (isInEditMode) text = valueInitialText
                }
                
                R.id.label -> {
                    labelView = this
                    text = labelInitialText
                }
            }
        }
        
        fun ViewGroup.setTextAttrs() {
            for (v: View in children)
                if (v is TextView) v.setTextAttr()
                else if (v is ViewGroup) v.setTextAttrs()
        }
        
        
        super.onViewAdded(child)
        
        if (child is ViewGroup) child.setTextAttrs()
        else if (child is TextView) child.setTextAttr()
    }
}