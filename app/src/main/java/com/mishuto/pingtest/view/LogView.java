package com.mishuto.pingtest.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.method.MovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * TextView, отображающий список строк (лог)
 * Строки могут иметь форматирование (Spannable)
 * Created by Michael Shishkin on 02.08.2020
 */
public class LogView extends AppCompatTextView {

    private Runnable mOnInitCompleteListener;                                       // колбэк для процедуры завершения инициализации
    final private MovementMethod mScrolling = new ScrollingMovementMethod();

    public LogView(Context context) {
        super(context);
    }

    public LogView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LogView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        setMovementMethod(mScrolling);                                                          // делаем LogView скролируемым
        setOnTouchListener((v, event) -> {                                                      // для скролируемого TextView внутри тега Scroll
            v.getParent().requestDisallowInterceptTouchEvent(true);                             // запрещаем родителю обрабатывать событие прокрутки
            return false;
        });

        if(mOnInitCompleteListener != null)
            mOnInitCompleteListener.run();
    }

    public void setOnInitCompleteListener(Runnable onInitCompleteListener) {
        mOnInitCompleteListener = onInitCompleteListener;
    }

    public void addLine(CharSequence s) {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(s).append("\n");
        append(stringBuilder);

        if(getLayout() != null) {
            int scrollAmount = getLayout().getLineTop(getLineCount()) - getHeight();   // вычисляем разницу между размерами всего текста в px и высотой окна
            scrollTo(0, Math.max(scrollAmount, 0));                                 // если scrollAmount > 0, то скроллим на нужное кол-во px
        }
    }

    // вычислить количество напечатанных строк, в отличие от количества физических строк, отдаваемых в getLineCount()
    public int getPrintedLineCount() {
        return (int)getText().toString().chars()
                .filter(c -> c == '\n')
                .count();
    }

    // удалить первые n строк из лога
    public void removeFirstLines(int n) {
        if(n >= getPrintedLineCount())
            clear();

        else if(n > 0) {
            int index = getIndexOfNthOccurrence(getText().toString(), '\n', n);
            if(index != -1)
                getEditableText().delete(0, index + 1);
        }
    }

    public void clear() {
        setText(null);
    }

    //получить n-e вхождение c в text
    @SuppressWarnings("SameParameterValue")
    private int getIndexOfNthOccurrence(String text, char c, int n) {
        int index = text.indexOf(c);
        while (--n > 0 && index != -1)
            index = text.indexOf(c, index + 1);

        return index;
    }
}
