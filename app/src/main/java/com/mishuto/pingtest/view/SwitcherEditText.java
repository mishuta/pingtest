package com.mishuto.pingtest.view;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.mishuto.pingtest.App;

import java.util.stream.Stream;

/**
 * Класс-обертка ViewSwitcher, который объединяет TextView и EditText для поочередного отображения и редактирования.
 * Created by Michael Shishkin on 10.10.2021
 */
public class SwitcherEditText {
    private final static int ANIMATION_DURATION = 500;

    private String text;
    final private ViewSwitcher viewSwitcher;
    Integer textIndex, editIndex;

    public SwitcherEditText(ViewSwitcher viewSwitcher) {
        this(viewSwitcher, null);
    }

    public SwitcherEditText(ViewSwitcher viewSwitcher, String text) {
        this.text = text;
        this.viewSwitcher = viewSwitcher;
        calculateViewsIndex();
        setAnimation();

    }

    public void setText(String text) {
        this.text = text;
    }

    public void setText(int id) {
        text = App.getAppContext().getString(id);
    }

    public String getText() {
        return text;
    }

    public TextView switchToText() {
        viewSwitcher.setDisplayedChild(textIndex);
        TextView textView = (TextView) viewSwitcher.getCurrentView();
        textView.setText(text);
        return  textView;
    }

    public EditText switchToEdit() {
        viewSwitcher.setDisplayedChild(editIndex);
        EditText editText = (EditText) viewSwitcher.getCurrentView();
        editText.setText(text);
        return editText;
    }

    private void setAnimation() {
        Animation animationIn = new AlphaAnimation(0, 1);
        Animation animationOut = new AlphaAnimation(1, 0);
        Stream.of(animationIn, animationOut).forEach(a-> a.setDuration(ANIMATION_DURATION));
        viewSwitcher.setInAnimation(animationIn);
        viewSwitcher.setOutAnimation(animationOut);
    }

    // вычисляем индексы вложенных TextView и EditText
    private void calculateViewsIndex() {
        TextView view = (TextView) viewSwitcher.getCurrentView();
        if(view instanceof EditText)
            editIndex = viewSwitcher.getDisplayedChild();
        else
            textIndex = viewSwitcher.getDisplayedChild();

        viewSwitcher.showNext();

        view = (TextView) viewSwitcher.getCurrentView();
        if(view instanceof EditText && editIndex == null)
                editIndex = viewSwitcher.getDisplayedChild();
        else if(!(view instanceof EditText) && textIndex == null)
                textIndex = viewSwitcher.getDisplayedChild();
        else
            throw new IllegalArgumentException("unexpected two views of the same type in ViewSwitchers");

        viewSwitcher.showNext();    // возвращем View в исходную
    }

}
