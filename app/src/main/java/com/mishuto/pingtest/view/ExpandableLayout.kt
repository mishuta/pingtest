package com.mishuto.pingtest.view

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import androidx.transition.TransitionSet
import com.mishuto.pingtest.R

/**
 * ExpandableLayout содержит в себе заголовок с кнопкой expand/collapse и View которое должно открываться/скрываться при нажатии на кнопку
 * Created by Michael Shishkin on 03.10.2023
 */

const val ANIMATE_DURATION = 350L

class ExpandableLayout(context: Context, attrs : AttributeSet) : LinearLayout(context, attrs) {

    private val expandId : Int
    private val collapseId: Int
    private val tint: Int
    private var isCollapsed : Boolean
    private val isCollapseInEditMode: Boolean
    private lateinit var header: TextView
    private lateinit var hidedView: View

    init {
        val typedArray: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.ExpandableLayout)
        expandId = typedArray.getResourceId(R.styleable.ExpandableLayout_expandIcon, 0)
        collapseId = typedArray.getResourceId(R.styleable.ExpandableLayout_collapseIcon, 0)
        tint = typedArray.getResourceId(R.styleable.ExpandableLayout_tint, 0)
        isCollapsed = typedArray.getBoolean(R.styleable.ExpandableLayout_collapsed, true)
        isCollapseInEditMode = typedArray.getBoolean(R.styleable.ExpandableLayout_collapseInEditMode, isCollapsed)
        typedArray.recycle()

        if(expandId == 0) throw IllegalArgumentException("expandIcon must be specified")
        if(collapseId == 0) throw IllegalArgumentException("collapseIcon must be specified")
    }

    override fun onViewAdded(child: View) {
        fun validate(child: View) {
            if(!this::header.isInitialized) {
                if(child !is TextView)
                    throw IllegalArgumentException("First view in ExpandableLayout is a header and it must be a TextView")
            }
            else if(this::hidedView.isInitialized)
                throw IllegalArgumentException("ExpandableLayout must contain only a header and a collapsed view/viewGroup")
        }

        super.onViewAdded(child)

        validate(child)

        if(!this::header.isInitialized)
            header = child as TextView
        else
            hidedView = child
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        header.setOnClickListener {
            isCollapsed = !isCollapsed
            updateState()
        }
        updateState()
    }

    private fun updateState() {
        val collapsed = if(isInEditMode) isCollapseInEditMode else isCollapsed
        header.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, if(collapsed) expandId else collapseId, 0)
        if(!isInEditMode && tint !=0 ) header.compoundDrawables.filterNotNull().forEach{it.setTint(resources.getColor(tint, null))}
        animateOn()
        hidedView.visibility = if(collapsed) GONE else VISIBLE

    }

    private fun animateOn() {
        if(!isInEditMode) {
            var root : ViewGroup = this
            while (root.parent is ViewGroup) root = root.parent as ViewGroup

            TransitionManager.beginDelayedTransition(root, TransitionSet()
                .addTransition(ChangeBounds())
                .setDuration(ANIMATE_DURATION)
            )
        }
    }
}