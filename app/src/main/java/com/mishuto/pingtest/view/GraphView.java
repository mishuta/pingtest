package com.mishuto.pingtest.view;

import static com.mishuto.pingtest.view.Utils.dp2pix;
import static com.mishuto.pingtest.view.Utils.getPaint;
import static com.mishuto.pingtest.view.Utils.getScreenSize;
import static com.mishuto.pingtest.view.Utils.isLtr;
import static com.mishuto.pingtest.view.Utils.pix2dp;
import static java.lang.Integer.max;
import static java.lang.Math.pow;
import static java.lang.Math.round;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.DeferredExecutor;
import com.mishuto.pingtest.common.FixCircularBuffer;
import com.mishuto.pingtest.model.ping.Ping;

import java.util.Collection;

/**
 * View, отрисовывающий гистограмму
 * Created by Michael Shishkin on 03.06.2020
 */
public class GraphView extends View {

    final private static int TEXT_SIZE_DP = 14;    // размер текста
    final private static int BAR_WIDTH_DP = 4;     // ширина столбца гистограммы. Состоит из столбца и промежутка между столбцами
    final private int BAR_WITH_PIX;

    // график должен иметь значения сетки:  a, a * LOG_BASE, a * LOG_BASE^2, ... , a * LOG_BASE^PITCHES
    final private static int LOG_BASE = 5;      // основание логорифма графика
    final private static int MAX_VALUE = 12500; // максимальное значение графика. Должно быть кратно LOG_BASE**PITCHES
    final private static int PITCHES = 6;       // всего линий сетки, где нулевая линия соответствует линии 0, а MAX_VALUE соответствует линии PITCHES

    private final Paint paintDraw;       // Paint для рисунка
    private final PaintText paintText;   // Paint для текста
    private final Paint paintBar;        // Paint для столбца гистограммы обычный
    private final Paint paintBarLost;    // Paint для столбца гистограммы с таймаутом
    private final Paint paintBarErr;     // Paint для столбца гистограммы с ошибкой
    private final Paint paintBarWarn;    // Paint для столбца гистограммы с ошибкой первой попытки

    private final int textWidth;         // ширина подписей
    private final int textHigh;          // высота подписей

    private final Drawer drawer = new Drawer();

    private boolean isLayoutOk = false;

    private final DeferredExecutor mDeferredExecutor = new DeferredExecutor();

    FixCircularBuffer<Ping> pingBuffer; // хранилище последних N столбцов

    public GraphView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GraphView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        paintDraw =    getPaint(context, R.color.colorSecondaryText, Paint.Style.STROKE);
        paintBar =     getPaint(context, R.color.colorPrimary, Paint.Style.FILL);
        paintBarLost = getPaint(context, R.color.colorError, Paint.Style.FILL);
        paintBarErr =  getPaint(context, R.color.colorSecondaryText, Paint.Style.FILL);
        paintBarWarn = getPaint(context, R.color.colorWarning, Paint.Style.FILL);
        paintText = new PaintText();

        pingBuffer = new FixCircularBuffer<>();
        BAR_WITH_PIX = dp2pix(context, BAR_WIDTH_DP);

        Rect bounds = paintText.getTextBounds(String.valueOf(MAX_VALUE/LOG_BASE));  // получаем значение границ самого длинного текста
        textWidth = bounds.width();
        textHigh = bounds.height();
    }

    // вычисление теоретически максимального размера буфера под mPings для сервиса
    public static int getMaxBufferSize(Context context) {
        Point screenSize = getScreenSize(context);
        int longest = max(screenSize.x, screenSize.y);
        return pix2dp(longest) / BAR_WIDTH_DP;
    }

    // отрисовка текущего значения пинга
    public void drawPing(Ping ping) {
        pingBuffer.add(ping);
        invalidate();
    }

    // очистка графика
    public void clear() {
        pingBuffer.clear();
        invalidate();
    }

    // заполнить буфер графика из внешней коллекции
    // выполняется при смене ориентации экрана, рестарте активности при запущенном сервисе
    public void fill(Collection<? extends Ping> newPingBuffer) {
        mDeferredExecutor.execute(() -> {   // отложенное исполнение в onLayout, когда станет известен размер view
            pingBuffer.clear();                 // а значит и размер буфера
            pingBuffer.addAll(newPingBuffer);
            invalidate();
        });
    }

    // событие инициализации окна
    @Override
    protected void onLayout (boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        drawer.initDrawer(left, top, right, bottom);

        int sizeOfPingsBuffer = (drawer.rightGraph - drawer.leftGraph) / BAR_WITH_PIX + 1;
        isLayoutOk = sizeOfPingsBuffer > FixCircularBuffer.MIN_BUFFER_LENGTH;
        if(isLayoutOk) {
            pingBuffer.setLength(sizeOfPingsBuffer);
            mDeferredExecutor.onDeferredEvent();
        }
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);
        if (isLayoutOk)
            drawer.drawGraph(canvas);
    }

    private class Drawer {
        boolean ltr;
        Canvas canvas;
        // координаты сторон графика
        int leftGraph = 1;
        int topGraph = 1;
        int rightGraph;
        int bottomGraph;
        int textIndent = dp2pix(getContext(), 8);

        float gridPitch; // шаг сетки

        void initDrawer(int left, int top, int right, int bottom) {
            rightGraph = right - left - 1 - (textWidth + textIndent);
            bottomGraph = bottom - top - 1;
            gridPitch = (bottomGraph - topGraph) / (float) PITCHES;    // шаг сетки = высота графика/количества промежутков

            ltr = isLtr(GraphView.this);
            paintText.setTextAlign(ltr ? Paint.Align.LEFT : Paint.Align.RIGHT);
        }

        void drawGraph(Canvas canvas) {
            this.canvas = canvas;
            drawBlankChart();
            drawBars();
        }

        // отрисовка пустографки
        private void drawBlankChart() {
            // отрисовка прямоугольника графика
            drawRect(leftGraph, topGraph, rightGraph, bottomGraph, paintDraw);

            for (int i = 1, gridVale = MAX_VALUE; i < PITCHES; i++) {
                int yGrid = round(i * gridPitch);
                gridVale /= LOG_BASE;

                // отрисовка линий сетки
                drawHorizontalLine(leftGraph, rightGraph, yGrid);
                // отрисовка цифр
                drawText(String.valueOf(gridVale), rightGraph + textIndent, yGrid + textHigh / 2);
            }

            drawText(getContext().getString(R.string.ms), rightGraph + textIndent, textHigh);
        }

        // отрисовка гистограммы
        private void drawBars() {

            int x = leftGraph;

            for (Ping ping : pingBuffer) {
                Paint paintBar = ping.isError() ? paintBarErr :                    // если ошибка соединения - выводим пинг серым
                        ping.isLost() ? paintBarLost :                             // если таймаут - красным,
                                ping.wasICMPFail() ? paintBarWarn : GraphView.this.paintBar;     // если ICMP-таймаут - желтым, иначе синим

                drawBar(x, ping.getLatency(), paintBar);

                if (ping.wasICMPFail() && !ping.isLost())
                    drawBar(x, ping.getEffectiveLatency(), GraphView.this.paintBar);      // если был ICMP-таймаут рисуем вторую попытку поверх первой

                x += BAR_WITH_PIX;
            }
        }

        // отрисовка столбца
        private void drawBar(int x, long height, Paint paint) {
            int barHeight = round((bottomGraph - topGraph) * getBarPitch(height) / PITCHES);            // высота гистограммы в px
            drawRect(x, bottomGraph, x + BAR_WITH_PIX / 2, bottomGraph - barHeight, paint);
        }

        void drawText(String text, int x, int y) {
            canvas.drawText(text, convertX(x), y, paintText);
        }

        void drawHorizontalLine(int startX, int endX, int y) {
            canvas.drawLine(convertX(startX), y, convertX(endX), y, paintDraw);
        }

        void drawRect(int start, int top, int end, int bottom, Paint paint) {
            canvas.drawRect(convertX(start), top, convertX(end), bottom, paint);
        }

        int convertX(int x) {
            return ltr ? x : canvas.getWidth() - x;
        }
    }

    /* высота столбца в линиях сетки, где
            latency = 0;            getBarPitch = 0
            latency = pitch1;       getBarPitch = 1
            latency = MAX_VALUE;    getBarPitch = PITCHES
            Интервал 0..PITCH_1 вне области действия логарифмического графика, поэтому на нем используется линейная функция
    */
    final static double PITCH_1 = (MAX_VALUE/pow(LOG_BASE, PITCHES - 1));   // вычисленное значение первой линии

    float getBarPitch(long latency) {
        double highInPitches = latency / PITCH_1;
        return (float) (latency < PITCH_1 ? highInPitches : 1 + Math.log(highInPitches)/Math.log(LOG_BASE));
    }

    private class PaintText extends Paint {

        public PaintText() {
            super(getPaint (getContext(), R.color.colorSecondaryText, android.graphics.Paint.Style.FILL_AND_STROKE));
            setTextSize(dp2pix(getContext(), TEXT_SIZE_DP));
        }

        Rect getTextBounds(String text) {
            Rect rect = new Rect();
            getTextBounds(text, 0, text.length(), rect);
            return rect;
        }
    }
}
