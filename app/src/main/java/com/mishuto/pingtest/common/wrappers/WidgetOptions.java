package com.mishuto.pingtest.common.wrappers;

import android.appwidget.AppWidgetManager;
import android.content.res.Configuration;

import com.mishuto.pingtest.App;

/**
 * Враппер над AppWidgetManager.AppWidgetOptions
 * Created by Michael Shishkin on 10.09.2021
 */
public class WidgetOptions {
    private final int widgetId;

    public WidgetOptions(int widgetId) {
        this.widgetId = widgetId;
    }

    public int getMaxWidth() {
        return getOption(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH);
    }

    public int getMinWidth() {
        return getOption(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH);
    }

    public int getMaxHeight() {
        return getOption(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT);
    }

    public int getMinHeight() {
        return getOption(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT);
    }

    public int getWidth() {
        return isPortraitOrientation() ? getMinWidth() : getMaxWidth();
    }

    public int getHeight() {
        return isPortraitOrientation() ? getMaxHeight() : getMinHeight();
    }

    private static boolean isPortraitOrientation() {
        return App.getAppContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    private int getOption(String option) {
        return AppWidgetManager.getInstance(App.getAppContext()).getAppWidgetOptions(widgetId).getInt(option);
    }
}
