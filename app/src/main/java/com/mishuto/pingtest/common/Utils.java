package com.mishuto.pingtest.common;

import static android.os.Build.VERSION.SDK_INT;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.mishuto.pingtest.App;
import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.dispatchers.EventDispatcher;
import com.mishuto.pingtest.common.dispatchers.IntentDispatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Утилиты
 * Created by Michael Shishkin on 02.06.2020
 */
public class Utils {

    public static final EventDispatcher eventDispatcher = EventDispatcher.INSTANCE;
    public static final Logger logger = Logger.INSTANCE;
    public static final IntentDispatcher intentDispatcher = IntentDispatcher.INSTANCE;
    public static final FBAnalytics fbAnalytics = FBAnalytics.INSTANCE;

    // преобразование 0..1 -> 0..100
    public static int MAX_PERCENT = 100;

    public static int percent(double p) {
        return (int)(p * MAX_PERCENT);
    }

    // подготовка снека к выводу
    public static Snackbar prepareSnack(View view, String msg) {
        final int SNACK_MAX_LINES = 10;  // Максимальное количество строк снекбара
        final int MSEC_ON_LETTER = 40;   // Время экспонирование снека зависит от длины сообщения. Эмпирически выбрано 40мс на букву
        final int MSEC_MINIMAL = 1000;   // Минимальное время экспонирования (для коротких сообщений)

        int duration = msg.length() * MSEC_ON_LETTER + MSEC_MINIMAL;
        Snackbar snackbar = Snackbar.make(view, msg, duration);
        TextView textView = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setMaxLines(SNACK_MAX_LINES);

        return snackbar;
    }

    public static Snackbar prepareSnack(View view, int msgId) {
        return prepareSnack(view, view.getContext().getString(msgId));
    }

    public static MaterialAlertDialogBuilder createDialogBuilder(Context activityContext) {
        return new MaterialAlertDialogBuilder(activityContext, R.style.MaterialAlertDialog);
    }

    // подготовка AlertDialog к выводу статьи
    public static MaterialAlertDialogBuilder buildArticleAlert(Context activityContext, int titleId, int htmlResId) {
        MaterialAlertDialogBuilder builder = createDialogBuilder(activityContext).
                setTitle(activityContext.getString(titleId));
        builder.setMessage(Html.fromHtml(activityContext.getString(htmlResId), Html.FROM_HTML_MODE_COMPACT));
        return builder;
    }

    public static String getString(int resId, Object... args) {
        return App.getAppContext().getString(resId, args);
    }

    public static JSONObject getJsonFromRawResource(int resId) throws IOException, JSONException {
        InputStream is = App.getAppContext().getResources().openRawResource(resId);
        return new JSONObject(readInputStreamToString(is));
    }

    public static String readInputStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder out = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null)
            out.append(line).append("\n");

        return out.toString();
    }

    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    public static <T extends Parcelable> T getParcelable(Bundle bundle, String key, Class<T> tClass) {
        return (SDK_INT >= Build.VERSION_CODES.TIRAMISU ) ?
                bundle.getParcelable(key, tClass) : bundle.getParcelable(key);
    }
}
