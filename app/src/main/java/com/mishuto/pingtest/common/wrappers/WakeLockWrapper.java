package com.mishuto.pingtest.common.wrappers;

import android.os.PowerManager;

import com.mishuto.pingtest.common.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Хелпер для получения лока для вайфай
 * Created by Michael Shishkin on 09.08.2021
 */
public class WakeLockWrapper {
    private static final Map<String, WakeLockWrapper> lockNames = new HashMap<>();
    private final PowerManager.WakeLock lock;
    private final String name;

    public static WakeLockWrapper getLock(String lockName) {
        if(!lockNames.containsKey(lockName))
            lockNames.put(lockName, new WakeLockWrapper(lockName));
        return lockNames.get(lockName);

    }

    private WakeLockWrapper(String name) {
        this.name = name;
        lock = PMUtils.getPM().newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, name);
    }

    synchronized public void lock(long timeout) {
        if (!lock.isHeld()) {
            lock.acquire(timeout);
            Logger.INSTANCE.d("WakeLock [%s] acquired", name);
        }
    }

    synchronized public void unlock() {
        if (lock.isHeld()) {
            lock.release();
            Logger.INSTANCE.d("WakeLock [%s] released", name);
        }
    }
}
