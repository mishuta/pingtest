package com.mishuto.pingtest.common.tickers;

import static com.mishuto.pingtest.common.CallerUtils.CLASS;
import static com.mishuto.pingtest.common.CallerUtils.LINE;
import static com.mishuto.pingtest.common.CallerUtils.METHOD;

import android.os.Handler;
import android.os.HandlerThread;

import com.mishuto.pingtest.common.CallerUtils;
import com.mishuto.pingtest.common.Logger;

/**
 * Реализация тикера на Android HandleThread
 * Created by Michael Shishkin on 19.03.2021
 */
public class HandlerTicker implements TickerStrategy {

    HandlerThread mThread;
    Handler mHandler;
    boolean isStarted = false;

    public HandlerTicker() {
        mThread = new HandlerThread(new CallerUtils(getClass()).format(CLASS+"."+METHOD+":"+LINE));
        Logger.INSTANCE.d("name=%s", mThread.getName());
        mThread.start();
        mHandler = new Handler(mThread.getLooper());
    }

    @Override
    synchronized public void start(Runnable listener, boolean isInfinitely, int interval) {
        isStarted = true;

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                listener.run();
                if(isInfinitely)
                    mHandler.postDelayed(this, interval);
                else
                    isStarted = false;
            }
        }, interval);
    }

    @Override
    synchronized public void stop() {
        mHandler.removeCallbacksAndMessages(null);
        isStarted = false;
    }

    @Override
    synchronized public boolean isStarted() {
        return isStarted;
    }
}
