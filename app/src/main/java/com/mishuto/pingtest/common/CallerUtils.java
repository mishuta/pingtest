package com.mishuto.pingtest.common;

/**
 * Вспомогательный класс для вычисления и форматирования информации о вызывающем классе/методе
 * Created by Michael Shishkin on 31.01.2022
 */
public class CallerUtils {
    final StackTraceElement parentElement;
    public final static String CLASS = "$class";
    public final static String METHOD = "$method";
    public final static String LINE = "$line";

    public CallerUtils(Class<?> childClass) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        StackTraceElement parent = null;
        boolean childClassFound = false;

        for (StackTraceElement element : stackTraceElements) {
            if(childClassFound) {
                if (!element.getClassName().equals(childClass.getName())) {
                    parent = element;
                    break;
                }
            }
            else
                childClassFound = element.getClassName().equals(childClass.getName());
        }
        parentElement = parent;
    }

    public String getClassName() {
        String[] classNameParts = parentElement.getClassName().split("\\.");
       return classNameParts[classNameParts.length - 1];
    }

    public String getMethod() {
        return parentElement.getMethodName();
    }

    public String getLine() {
        return String.valueOf(parentElement.getLineNumber());
    }

    // в исходной строке заменяет константы CLASS, METHOD, LINE соответствующими значениями
    public String format(String formattedString) {
        return formattedString.replace(CLASS, getClassName())
                .replace(METHOD, getMethod())
                .replace(LINE, getLine());
    }
}
