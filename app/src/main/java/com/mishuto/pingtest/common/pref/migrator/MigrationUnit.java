package com.mishuto.pingtest.common.pref.migrator;

/**
 * Миграционный юнит для миграции shared pref
 * Created by Michael Shishkin on 12.10.2021
 */
public interface MigrationUnit extends Comparable<MigrationUnit> {
    int getVersion();
    void migrate();
    String getDescription();

    @Override
    default int compareTo(MigrationUnit o) {
        return getVersion() - o.getVersion();
    }
}
