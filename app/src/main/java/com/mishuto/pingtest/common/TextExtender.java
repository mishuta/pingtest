package com.mishuto.pingtest.common;

import com.mishuto.pingtest.view.Utils;

import java.util.stream.Stream;

/**
 * Класс расширяет несколько заданных строк невидимыми непробельными символами слева и справа до одинкаовой ширины.
 * Используется в автосайз-TextView, чтобы получить несколько TextView одинакового размера
 *
 * Created by Michael Shishkin on 03.11.2021
 */
public class TextExtender {
    // для какого размера текста будет вычисляться ширина. Поскольку сравниваются текст одного размера, то это произвольная величина
    private final static int INITIAL_SIZE_DP = 14;
    private final static char NBSP = '\u00A0'; // символ расширения. Пробел не воспринимается автосайзом за символ
    Integer toDp;

    public String expandString(String s) {

        StringBuilder expandedString = new StringBuilder(s);
        boolean leftSide = true;

        do {
            if (leftSide)
                expandedString.insert(0, NBSP);
            else
                expandedString.append(NBSP);

            leftSide = !leftSide;
        }
        while (width(expandedString.toString()) < toDp);

        return expandedString.toString();
    }

    //ширина расшириной строки. Для корректного вычисления слева и справа добавляются видимые символы
    int width(String s) {
        return Utils.TextSize.ofDp(INITIAL_SIZE_DP, '[' + s + ']').width() - Utils.TextSize.ofDp(INITIAL_SIZE_DP, "[]").width();
    }

    public TextExtender(String... arr) {
        toDp = Stream.of(arr).mapToInt(this::width).max().orElseThrow(IllegalArgumentException::new);   // максимальное значение длины текста
    }
}
