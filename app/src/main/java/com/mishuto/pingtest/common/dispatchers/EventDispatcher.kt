package com.mishuto.pingtest.common.dispatchers

import androidx.lifecycle.Lifecycle
import java.util.function.Consumer

/**
 * Реализация диспетчера сообщений, использующая прямой вызов колбэков командой post
 * Created by Michael Shishkin on 29.11.2023
 */
@Suppress("UNCHECKED_CAST")
object EventDispatcher : AbstractDispatcher<Any?>() {
    fun post(action: String) = post(action, null)
    
    @JvmName("registerTyped")
    fun <T> register(action: String, tag: String, onReceive: Consumer<T>) = super.register(action, tag, onReceive as Consumer<Any?>)
    
    @JvmName("bindTyped")
    fun <T> bind(action: String, tag: String, lifecycle: Lifecycle, onReceive: Consumer<T>) = super.bind(action, tag, lifecycle, onReceive as Consumer<Any?>)
}