package com.mishuto.pingtest.common.pref;

import android.content.pm.PackageManager;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.BuildConfig;
import com.mishuto.pingtest.common.Logger;
import com.mishuto.pingtest.common.SharedPref;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * Для заданного ключа вытаскивает сохраненную в нем версию приложения и текущую версию билда.
 * Служит для обработки события смены версии
 * Нужно иметь несколько ключей, поскольку виджет может загружаться раньше MainActivity, но все должны иметь возможность обрабатывать событие смены версии
 * Created by Michael Shishkin on 15.09.2021
 */
public class Version {
    public static int CURRENT = BuildConfig.VERSION_CODE;

    public static long firstInstallTime() throws PackageManager.NameNotFoundException {
        return App.getAppContext().getPackageManager().getPackageInfo(App.getAppContext().getPackageName(), 0).firstInstallTime;
    }

    public static long lastUpdateTime() throws PackageManager.NameNotFoundException {
        return App.getAppContext().getPackageManager().getPackageInfo(App.getAppContext().getPackageName(), 0).lastUpdateTime;
    }

    private static final Integer EARLIEST_VER = 54;                             // версия для которой еще не существовало этого кода
    private static final Map<String, Version> versionMap = new HashMap<>();     // мапа ключей и кешированных версий

    private final String key;                                                   // ключ для SP
    private int old;                                                        // последняя сохраненная версия

    // получить версию по ключу
    public static Version getVersion(String key) {
        if(!versionMap.containsKey(key))
            versionMap.put(key, new Version(key));

        return versionMap.get(key);
    }

    private Version(String key) {
        this.key = key;
        old = SharedPref.INSTANCE.getInt(key, EARLIEST_VER);
        Logger.INSTANCE.d("Version [%s]. Old: %d, Current: %d", key, old, CURRENT);

        if (isFirstInstall())
            accept();
    }

    public boolean isVersionChanged() {
        return CURRENT != old;
    }

    public Integer getOld() {
        return old;
    }

    // сохраняет текущую версию в ключе
    public void accept() {
        if(isVersionChanged()) {
            old = CURRENT;
            SharedPref.INSTANCE.putInt(key, CURRENT);
            Logger.INSTANCE.d("Version [%s]=%d. accepted", key, CURRENT);
        }
    }

    // является ли билд первой установкой или был апдейт
    private boolean isFirstInstall() {
        try {
            Logger.INSTANCE.d("install time: %s, update time: %s", Instant.ofEpochMilli(firstInstallTime()), Instant.ofEpochMilli(lastUpdateTime()));
            return firstInstallTime() == lastUpdateTime();
        } catch (PackageManager.NameNotFoundException e) {
            Logger.INSTANCE.e(e);
            return true;
        }
    }
}
