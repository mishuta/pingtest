package com.mishuto.pingtest.common

import android.os.Bundle
import com.google.firebase.Firebase
import com.google.firebase.analytics.analytics

/**
 * Обертка для работы с эвентами гугл-аналитики
 * Created by Michael Shishkin on 10.04.2024
 */
object FBAnalytics {
    private val firebaseAnalytics = Firebase.analytics
    
    fun log(eventName: String, vararg params: BundleParams) {
        val bundle = Bundle()
        
        params.forEach { it.putParam(bundle) }
        firebaseAnalytics.logEvent(eventName, bundle)
    }
}

object AnalyticEvents {
    const val IN_APP_REVIEW = "in_app_review"
    const val LICENSE_CREATION = "license_creation"
    const val LICENSE_ACTIVATION = "license_activation"
    const val LICENSE_DEACTIVATION = "license_deactivation"
    const val LICENSE_PURCHASE = "license_purchase"
    
    fun logPremiumAccessViolation(accessedObject: String) = FBAnalytics.log("premium_access_violation", StringParam("object", accessedObject))
}

interface BundleParams {
    fun putParam(b: Bundle)
}

data class StringParam(val name: String, val value: String) : BundleParams {
    override fun putParam(b: Bundle) = b.putString(name, value)
}