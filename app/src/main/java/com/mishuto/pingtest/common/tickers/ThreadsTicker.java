package com.mishuto.pingtest.common.tickers;

import static java.lang.Thread.sleep;

import com.mishuto.pingtest.common.Logger;

/**
 * Реализация тикера на потоках
 * Created by Michael Shishkin on 12.06.2020
 */
@SuppressWarnings("BusyWait")
public class ThreadsTicker implements TickerStrategy {

    private Thread mThread;
    volatile private boolean isStarted = false;
    private final String name;

    public ThreadsTicker(String name) {
        this.name = name;
    }

    @Override
    synchronized public void start(Runnable listener, boolean isInfinitely, int interval) {
        isStarted = true;
        mThread = new Thread(() -> loop(listener, isInfinitely, interval));
        mThread.setName(name);
        mThread.start();
    }

    private void loop(Runnable listener, boolean isInfinitely, int interval) {
        try {
            while (isStarted) {
                sleep(interval);
                listener.run();
                if(!isInfinitely || mThread.isInterrupted())
                    isStarted = false;
            }
        }
        catch (InterruptedException e) {
            isStarted = false;
        }
    }

    @Override
    public void stop() {    // здесь не нужен synchronized, поскольку внешний может заблокироваться на join, а внутренний, пытается вызвать stop
        if(Thread.currentThread().equals(mThread))  // если stop() вызван из listener.run()
            isStarted = false;
        else if(isStarted() || mThread.isAlive()) {  // дожидаемся завершения потока, прежде чем вернуть управление
            mThread.interrupt();
            try {
                mThread.join();
            }
            catch (InterruptedException e) {
                Logger.INSTANCE.d("stopping thread %s was interrupted", mThread.getName());
            }
        }
    }

    @Override
    synchronized public boolean isStarted() {
        isStarted &= mThread != null && mThread.isAlive(); //синхронизируем isStarted (если isStarted еще true, а поток уже умер)
        return isStarted;
    }
}
