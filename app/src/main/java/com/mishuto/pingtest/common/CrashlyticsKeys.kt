package com.mishuto.pingtest.common

import android.content.res.Configuration
import android.os.Build
import android.os.Build.VERSION.SDK_INT
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.mishuto.pingtest.App.Companion.appContext
import com.mishuto.pingtest.App.Companion.appGuid
import com.mishuto.pingtest.common.CrashlyticsKeys.Tag.*
import com.mishuto.pingtest.controller.features.premium.billing.Billing
import com.mishuto.pingtest.settings.SettingsPrefManager.preferences
import com.mishuto.pingtest.settings.preferences.sound.SoundPrefManager
import com.mishuto.pingtest.settings.widget.Settings
import com.mishuto.pingtest.view.Utils.pix2dp
import java.util.Locale

/**
 * Управление ключами крашлитика из единого места
 * Created by Michael Shishkin on 16.03.2024
 */
object CrashlyticsKeys {
    
    enum class Tag(val tagName: String) {
        PREF("settings"),
        BILLING("billing"),
        SERVICE("service"),
        APP("app"),
        DISPLAY("display"),
        WIDGETS("widget_1")
    }
    
    private val widSettings = Settings(1)
    
    private val keys = listOf<CKey<*>>(
        CKey("ping_timeout", PREF) { preferences.pingTimeout },
        CKey("ping_interval", PREF) { preferences.pingInterval },
        CKey("host", PREF) { preferences.host },
        CKey("moving_pings_number", PREF) { preferences.movingPingsNumber },
        CKey("test_duration", PREF) { preferences.testDuration },
        CKey("keep_screen_on", PREF) { preferences.isKeepScreenOn },
        CKey("sound", PREF) { SoundPrefManager.persistValue },
        CKey("auto_start", PREF) { preferences.isAutostart },
        CKey("floating_window", PREF) { preferences.showFloatingWindow },
        
        CKey("premium", BILLING) { Billing.isPremium },
        CKey("premium_type", BILLING) { Billing.billingType.name },
        CKey("locales", BILLING) { locales().joinToString { it.country } },
        CKey("gp_country", BILLING) { Billing.googleBilling.gpCountry },
        
        CKey("locale", SERVICE) { Locale.getDefault().displayName },
        
        CKey("installed", APP) { getInstalledSource() },
        CKey("build_hardware", APP) { Build.HARDWARE },
        CKey("guid", APP) { appGuid },
        
        CKey("orientation", DISPLAY) { getOrientation() },
        CKey("height", DISPLAY) { pix2dp(appContext.resources.displayMetrics.heightPixels) },
        CKey("width", DISPLAY) { pix2dp(appContext.resources.displayMetrics.widthPixels) },
        
        CKey("executors", WIDGETS) { "[${Settings.persistentIDS().ids.joinToString()}]" },
        CKey("widgets", WIDGETS) { "[${widSettings.widgetIDs.joinToString()}]" },
        CKey("host", WIDGETS) { widSettings.host },
        CKey("interval", WIDGETS) { widSettings.interval },
        CKey("ser_length", WIDGETS) { widSettings.seriesLen },
        CKey("not_ping_wifi_in_idle", WIDGETS) { widSettings.notPingWiFiInIdle },
    )
    
    fun update(vararg tags: Tag) {
        keys.filter { tags.contains(it.tag) }
            .forEach { setKey("${it.tag.tagName}_${it.key}", it.getValue()) }
    }
    
    fun <T> setKey(key: String, value: T): T {
        Logger.t("$key: $value")
        when (value) {
            null -> Firebase.crashlytics.setCustomKey(key, "null")
            is Int -> Firebase.crashlytics.setCustomKey(key, value)
            is Boolean -> Firebase.crashlytics.setCustomKey(key, value)
            is String -> Firebase.crashlytics.setCustomKey(key, value)
            else -> throw UnsupportedOperationException("type is not recognized")
        }
        return value
    }
    
    data class CKey<T>(
        val key: String,
        val tag: Tag,
        val getValue: () -> T
    )
    
    private fun getInstalledSource(): String? =
        with(appContext.packageManager) {
            @Suppress("DEPRECATION")
            if (SDK_INT >= Build.VERSION_CODES.R)
                getInstallSourceInfo(appContext.packageName).installingPackageName
            else
                getInstallerPackageName(appContext.packageName)
        }
    
    private fun getOrientation(): String = when(appContext.resources.configuration.orientation) {
        Configuration.ORIENTATION_PORTRAIT -> "Portrait"
        Configuration.ORIENTATION_LANDSCAPE -> "Landscape"
        else -> "Unknown"
    }
}