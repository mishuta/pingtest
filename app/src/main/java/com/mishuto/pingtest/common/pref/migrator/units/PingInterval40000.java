package com.mishuto.pingtest.common.pref.migrator.units;

import static com.mishuto.pingtest.common.Utils.getString;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.SharedPref;
import com.mishuto.pingtest.common.pref.migrator.MigrationUnit;

/**
 * Т.к.был вставлен 1-й элемент в массив интервалов (200мс), сдвигаем индекс на 1, если текущий > 0
 * Created by Michael Shishkin on 05.12.2021
 */
public class PingInterval40000 implements MigrationUnit {

    @Override
    public int getVersion() {
        return 40_000;
    }

    @Override
    public void migrate() {
        int intervalIndex = SharedPref.INSTANCE.getInt(getString(R.string.ping_interval_key), 0);
        if(intervalIndex > 0)
            SharedPref.INSTANCE.putInt(getString(R.string.ping_interval_key), intervalIndex + 1);
    }

    @Override
    public String getDescription() {
        return "Shifting index of ping interval array";
    }
}
