package com.mishuto.pingtest.common;

import static java.time.Instant.now;

import java.time.Duration;
import java.time.Instant;

/**
 * Простой таймер. Отмеряет время с момента установки в мс.
 * Created by Michael Shishkin on 25.12.2021
 */
public class Timer {
    private Instant startTime;
    private final long markMills;

    public static long millsSince(Instant instant) {
        return Duration.between(instant, now()).toMillis();
    }

    public Timer(long markMills) {
        this.markMills = markMills;
    }

    public void start() {
        startTime = now();
    }

    public long elapsed() {
        if (startTime != null)
            return millsSince(startTime);
        else
            return 0;
    }

    //время вышло? Если да, переустанавливает момент старта
    public boolean isTimeUp() {
        if(startTime == null || millsSince(startTime) > markMills) {
            start();
            return true;
        }
        return false;
    }
}
