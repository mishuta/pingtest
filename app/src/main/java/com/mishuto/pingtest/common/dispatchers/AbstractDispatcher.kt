package com.mishuto.pingtest.common.dispatchers

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.t
import com.mishuto.pingtest.common.wrap
import java.util.function.Consumer

/**
 * Базовый класс для диспетчеров сообщений между компонентами. Использует паттерн Обсервер и Медиатор, чтобы развязать обменивающиеся сообщениями компоненты
 * Created by Michael Shishkin on 01.12.2023
 */
abstract class AbstractDispatcher<T> {
    //map(action:map(tag:listener))
    protected val subscribers = mutableMapOf<String, MutableMap<String, Consumer<T>>>()
    
    //авто register/unregister объекта по его lifecycle
    @Synchronized
    open fun bind(action: String, tag: String, lifecycle: Lifecycle, onReceive: Consumer<T>) {
        t("action:$action. tag: $tag")
        lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onStart(owner: LifecycleOwner) {
                register(action, tag, onReceive)
            }
            
            override fun onStop(owner: LifecycleOwner) {
                unregister(action, tag)
            }
        })
    }
    
    @Synchronized
    open fun register(action: String, tag: String, onReceive: Consumer<T>) {
        t("action:$action. tag: $tag")
        if (subscribers[action] == null) subscribers[action] = mutableMapOf()
        
        subscribers[action]!![tag] = onReceive
    }
    
    @Synchronized
    open fun unregister(action: String, tag: String) {
        t("action:$action. tag: $tag")
        subscribers[action]?.remove(tag)
    }
    
    open fun post(action: String, payload: T) {
        val safeConsumers: Map<String, Consumer<T>>?
        synchronized(this) {
            d("posted: action='$action'. payload='${payload?.toString()?.wrap(100)}'")
            safeConsumers = subscribers[action]?.let { HashMap(it) }
        }
        
        safeConsumers?.forEach { subscriber ->
            d("send to ${subscriber.key}")
            subscriber.value.accept(payload)
        }
    }
}