package com.mishuto.pingtest.common;

import android.app.NotificationChannel;
import android.app.NotificationManager;

import androidx.core.app.NotificationCompat;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.R;

/**
 * Общие методы для создания нотификацицй
 * Created by Michael Shishkin on 03.04.2021
 */
public class NotificationHelper {
    private static final String CHANNEL_ID = "PingNotification";

    public NotificationManager notificationManager() {
        return App.getAppContext().getSystemService(NotificationManager.class);
    }

    public void createNotificationChannel() {
        if(notificationManager().getNotificationChannel(CHANNEL_ID) == null) {
            CharSequence name = App.getAppContext().getString(R.string.channel_name);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager().createNotificationChannel(channel);
        }
    }

    public NotificationCompat.Builder getBuilder() {
        return new NotificationCompat.Builder(App.getAppContext(), CHANNEL_ID);
    }

}
