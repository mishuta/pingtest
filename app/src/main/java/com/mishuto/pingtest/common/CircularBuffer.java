package com.mishuto.pingtest.common;

import androidx.annotation.NonNull;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Реализация кольцевого буфера. Условие переполнения буфера задается в абстрактном методе
 * Элементы буфера не могут быть null
 * Created by Michael Shishkin on 07.06.2020
 */
abstract public class CircularBuffer<E> extends AbstractQueue<E> {
    private final LinkedList<E> mList = new LinkedList<>();

    /**
     * условие по которому будут последовательно удаляться первые элементы очереди при добавлении нового.
     * Если условие выполняется, то первый элемент удаляется и рассматривается следующий, который становится первым.
     * Если условие не выполняется, то дальнейший перебор прекращается
     *
     * @param offered добавляемый элемент
     * @param head первый элемент
     * @return требуется ли удалить первый элемент
     */
    abstract boolean isOverFlow(E offered, E head);

    @NonNull
    @Override
    public Iterator<E> iterator() {
        return mList.iterator();
    }

    @Override
    public int size() {
        return mList.size();
    }

    @Override
    public boolean add(E e) {
        boolean r = super.add(e);
        while (isOverFlow(e, peek()))
            remove();
        return r;
    }

    @Override
    public boolean offer(E e) {
        Assert.that(e!=null, "The element of CircularBuffer cannot be null");

        while (isOverFlow(e, peek()))
            poll();
        return mList.offer(e);
    }

    @Override
    public E poll() {
        return mList.poll();
    }

    @Override
    public E peek() {
        return mList.peek();
    }

    public E getLast() {
        if(mList.size() == 0)
            return null;
        else
            return mList.getLast();
    }

}
