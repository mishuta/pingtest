package com.mishuto.pingtest.common.tickers;

/**
 * Таймер, который тикает с заданным интервалом в мс
 * Created by Michael Shishkin on 12.06.2020
 */
public class Ticker {

    final private Runnable mListener;
    final private boolean isInfinitely;
    final private int mInterval;
    final TickerStrategy mTicker;

    /**
     * Создание тикера
     * @param listener колбэк после каждого тика
     * @param isInfinitely если true, будет тикать до прерывания командой stop(), иначе тикнет 1 раз.
     * @param interval интервал таймера в мс
     * @param ticker конкретная реализация
     */
    public Ticker(Runnable listener, boolean isInfinitely, int interval, TickerStrategy ticker) {
        mListener = listener;
        this.isInfinitely = isInfinitely;
        mInterval = interval;
        mTicker = ticker;
    }

    public Ticker(Runnable listener, boolean isInfinitely, int interval, String tickerName) {
        this(listener, isInfinitely, interval, new ThreadsTicker(tickerName));
    }

    public void start() {
        stop(); // если предыдущий тикер не закончился - сначала завершаем его.
        mTicker.start(mListener, isInfinitely, mInterval);
    }

    /**
     * прерывание тикера, если тикер был запущен
     * @return состояние тикера пред вызовом stop()
     */
    public boolean stop() {
       if(isStarted()) {
           mTicker.stop();
           return true;
       }
       else
        return false;
    }

    public boolean isStarted() {
        return mTicker.isStarted();
    }
}
