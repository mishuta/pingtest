package com.mishuto.pingtest.common.pref.migrator;

import com.mishuto.pingtest.common.Logger;
import com.mishuto.pingtest.common.pref.migrator.units.HostListUnt62;
import com.mishuto.pingtest.common.pref.migrator.units.PingInterval40000;
import com.mishuto.pingtest.common.pref.Version;

import java.util.stream.Stream;

/**
 * Мигрирует юниты, которые должны выполниться при обновлении версии
 * Created by Michael Shishkin on 12.10.2021
 */
public class Migrator {
    private final static String VERSION_KEY = "VER_Migrator";
    private final static MigrationUnit[] UNITS = {
            new HostListUnt62(),
            new PingInterval40000()
    };
    Version version = Version.getVersion(VERSION_KEY);

    public void checkForMigration() {
        if(version.isVersionChanged()) {
            int old = version.getOld();
            version.accept();   // фиксируем текущую версию, чтобы не было циклической ошибки при загрузке в случае эксепшена в юните

            Stream.of(UNITS).sorted()
                    .filter(unit-> unit.getVersion() > old && unit.getVersion() <= Version.CURRENT)
                    .forEach(u->
                    {
                        Logger.INSTANCE.d("Ver: [%d]. Migrate description: [%s]", u.getVersion(), u.getDescription());
                        u.migrate();
                    });
        }
    }
}
