package com.mishuto.pingtest.common.pref.migrator.units;

import com.mishuto.pingtest.common.Logger;
import com.mishuto.pingtest.common.SharedPref;
import com.mishuto.pingtest.settings.preferences.host.HostSetting;
import com.mishuto.pingtest.common.pref.migrator.MigrationUnit;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * See getDescription()
 * Created by Michael Shishkin on 12.10.2021
 */
public class HostListUnt62 implements MigrationUnit {
    private static final String OLD_KEY = "ListOfHosts";

    @Override
    public int getVersion() {
        return 62;
    }

    @Override
    public String getDescription() {
        return "String:ListOfHosts -> Set:Hosts";
    }

    @Override
    public void migrate() {
        String oldHosts = SharedPref.INSTANCE.getString(OLD_KEY, "");
        if(!oldHosts.isEmpty()) {
            Logger.INSTANCE.d("Unit [%d] migrates", getVersion());
            Set<String> oldHostSet = Stream.of(oldHosts.split(",")).collect(Collectors.toSet());
            HostSetting.getInstance().setHostSet(oldHostSet);
            removeOldKey();
        }
    }

    private void removeOldKey() {
        SharedPref.INSTANCE.edit().remove(OLD_KEY).apply();
    }
}
