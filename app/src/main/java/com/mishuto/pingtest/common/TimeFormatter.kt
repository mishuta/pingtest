package com.mishuto.pingtest.common

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Const.*
import com.mishuto.pingtest.common.Utils.getString
import java.lang.Math.round
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.time.temporal.ChronoUnit
import java.time.temporal.Temporal

/**
 * Форматирование объектов в виде строк со временем
 * Created by Michael Shishkin on 08.08.2022
 */

operator fun Temporal.minus(begin : Temporal) : Long = Duration.between(begin, this).toMillis()
operator fun Temporal.minus(durationMs: Long) : Temporal = this.minus(durationMs, ChronoUnit.MILLIS)

//12:00
fun LocalDateTime.shortTimeFormat(): String = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(this)

// ru:25.05.2022 en:5/25/22
fun LocalDateTime.shortDateFormat(): String = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).format(this)

//12:00:00
fun LocalDateTime.mediumTimeFormat(): String =
    try {
        DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM).format(this)
    }
    catch (e: IllegalArgumentException) {
        DateTimeFormatter.ofPattern("HH:mm:ss").format(this) // обход бага vivo 1906: падение на стандартном FormatStyle.MEDIUM
    }

// ru:25.05.2022 en:5/25/22
fun LocalDate.shortDateFormat(): String = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).format(this)

// long -> H:MM:SS
fun Long.asDurationFormat() = round(this / 1000f)
    .let { "%d:%02d:%02d".format(it / 3600, (it / 60) % 60, it % 60) }

// перевод значения в мс в подходящую временную единицу
fun msToTimeUnits(ms: Int): String {
    return when (ms) {
        INFINITY -> "\u221E"
        in 0 until SEC -> getString(R.string.num_ms, ms)
        in SEC until MIN -> getString(R.string.num_sec, round(ms / SEC.toDouble()))
        in MIN until HOUR -> getString(R.string.num_min, round(ms / MIN.toDouble()))
        else -> getString(R.string.num_hour, round(ms / HOUR.toDouble()))
    }
}