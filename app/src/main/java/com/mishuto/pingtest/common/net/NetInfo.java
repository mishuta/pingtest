package com.mishuto.pingtest.common.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.common.Logger;
import com.mishuto.pingtest.common.dispatchers.EventDispatcher;

/**
 * Колбэки состояния сети
 * Created by Michael Shishkin on 22.07.2021
 */
public class NetInfo {

    public final static String NET_STATE = "NetInfo.State";

    enum NetState { AVAILABLE, LOST }

    private final static NetInfo sNetInfo = new NetInfo();
    private NetworkCapabilities mCapabilities;
    private boolean mBlockedStatus = false;


    public static NetInfo getInstance() {
        return sNetInfo;
    }

    public static boolean isAirplaneModeOn() {
        return Settings.Global.getInt(App.getAppContext().getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    public boolean isWiFiOn() {
        NetworkCapabilities capabilities = mCapabilities;

        if(capabilities == null)
            try {
                capabilities = getConManager().getNetworkCapabilities(getConManager().getActiveNetwork());
            }
            catch (SecurityException e) {
                // обход бага под Android 11: иногда возникает SecurityException при запросе getNetworkCapabilities()
            }

        return capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
    }

    public boolean isBlocked() {
        return mBlockedStatus;
    }

    private NetInfo() {
        ConnectivityManager cm = getConManager();
        cm.addDefaultNetworkActiveListener(()-> {});
        try {
            cm.registerDefaultNetworkCallback(new NetCallbacks());
        }
        catch (SecurityException e) {                       // bug в Android 11 https://issuetracker.google.com/issues/175055271
            Logger.INSTANCE.d("Error of NetCallbacks registration");    // регистрация колбэка некритична
        }

    }

    private static ConnectivityManager getConManager() {
        return (ConnectivityManager) App.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    private class NetCallbacks extends ConnectivityManager.NetworkCallback {
        @Override
        public void onAvailable(@NonNull Network network) {
            EventDispatcher.INSTANCE.post(NET_STATE, NetState.AVAILABLE);
        }

        @Override
        public void onLosing(@NonNull Network network, int maxMsToLive) {
            Logger.INSTANCE.d("NetCallbacks::onLosing %s ", network);
        }

        @Override
        public void onLost(@NonNull Network network) {
            mCapabilities = null;
            EventDispatcher.INSTANCE.post(NET_STATE, NetState.LOST);
        }

        @Override
        public void onUnavailable() {
            Logger.INSTANCE.d("NetCallbacks::onUnavailable");
        }

        @Override
        public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities nc) {
            //d("NetCallbacks::onCapabilitiesChanged");
            mCapabilities = nc;
        }

        @Override
        public void onLinkPropertiesChanged(@NonNull Network network, @NonNull LinkProperties linkProperties) {
        }

        @Override
        public void onBlockedStatusChanged(@NonNull Network network, boolean blocked) {
            Logger.INSTANCE.d("NetCallbacks::onBlockedStatusChanged [%s] is %b", network, blocked);
            mBlockedStatus = blocked;
        }
    }
}
