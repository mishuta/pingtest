package com.mishuto.pingtest.common.dispatchers

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import com.mishuto.pingtest.App.Companion.appContext
import com.mishuto.pingtest.common.Logger.t
import java.util.function.Consumer

/**
 * Реализация диспетчера сообщений, использующая непрямой вызов через интенты,
 * Удобно там, где нельзя использовать прямые ссылки (например нотификации), либо подписка на системные интенты.
 * Created by Michael Shishkin on 01.12.2023
 */
object IntentDispatcher : AbstractDispatcher<Intent>() {
    private val receiver = Receiver()

    override fun register(action: String, tag: String, onReceive: Consumer<Intent>) {
        super.register(action, tag, onReceive)
        if(!receiver.filteredActions().contains(action))
            receiver.updateRegistration()
    }

    override fun unregister(action: String, tag: String) {
        super.unregister(action, tag)
        if(subscribers[action] == null || subscribers[action]!!.isEmpty())
            receiver.updateRegistration()
    }

    internal class Receiver : BroadcastReceiver() {
        private var isRegistered = false
        lateinit var filter: IntentFilter

        override fun onReceive(context: Context, intent: Intent) {
            post(intent.action!!, intent)
        }

        @SuppressLint("UnspecifiedRegisterReceiverFlag")
        fun updateRegistration() {
            if (isRegistered) {
                appContext.unregisterReceiver(this)
                t("unregister receiver")
                isRegistered = false
            }
            updateFilter()
            if (subscribers.entries.any{ it.value.isNotEmpty() }) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                    appContext.registerReceiver(this, filter,Context.RECEIVER_NOT_EXPORTED)
                else
                    appContext.registerReceiver(this, filter)
                t("register receiver: ${filteredActions().joinToString() }")
                isRegistered = true
            }
        }

        private fun updateFilter(): IntentFilter =
            IntentFilter()
                .apply {
                    subscribers.entries
                    .filter { it.value.isNotEmpty() }
                    .map { it.key }
                    .forEach { addAction(it) }
                }
                .also {  filter = it }

        fun filteredActions() : Set<String> =
            if(this::filter.isInitialized)
                filter.actionsIterator().asSequence().toHashSet()
            else setOf()
    }
}