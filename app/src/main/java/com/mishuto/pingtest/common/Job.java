package com.mishuto.pingtest.common;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.mishuto.pingtest.App;

import java.util.Locale;

/**
 * Хелпер для работы с JobService для периодического запуска задач с доступом к сети.
 * При срабатывании/останове джобы, джоба посылает интенты, переданные ей в конструкторе.
 * Интент при срабатывании нужен, поскольку ссылок на объект в момент срабатывания джобы может и не быть, если процесс был уничтожен.
 * Хелпер делает работу с JobService простой, скрывая реализацию на JobInfo, JobService и JobScheduler, и объектной, уходя от id
 *
 * Created by Michael Shishkin on 04.07.2021
 */
public class Job {
    private static final String START_INTENT_KEY = "start_intent_key";  // ключ в бандле для хранения интента события старта джобы
    private static final String STOP_INTENT_KEY = "stop_intent_key";    // ключ для интента события прерывания джобы
    private static final String SUCCESS_KEY = "success_key";            // ключ для хранения резульата jobFinished
    private static final String JOB_FINISH_ACTION = "JOB_FINISH";
    private static final int RESERVED_JOB_ID = 1000;                    // минимальный id задач с автоназначаемым id

    private static int sAutoId = RESERVED_JOB_ID; // счетчик id задач
    private final Params mParams = new Params(); // параметры текущей задачи

    /**
     * Объект для манипуляции с джобой
     * @param clientId уникальный id клиента. Используется для периодических задач, которые могут срабатывть после уничтожения приложения. Не может быть больше чем RESERVED_JOB_ID
     * @param fireIntent интент, который будет вызван в момент срабатывания джобы
     * @param stopIntent интент, который будет вызван при принудительном останове джобы (может быть null)
     * @param fireTime время срабатывания
     * @param isInfinitely джоба периодическая, если true или одноразовая если false
     */
    public Job(int clientId, PendingIntent fireIntent, PendingIntent stopIntent, long fireTime, boolean isInfinitely) {
        mParams.jobId = clientId;
        mParams.startIntent = fireIntent;
        mParams.stopIntent = stopIntent;
        mParams.fireTime = fireTime;
        mParams.isInfinitely = isInfinitely;
    }

    public Job(PendingIntent fireIntent, PendingIntent stopIntent, long fireTime, boolean isInfinitely) {
        this(++sAutoId, fireIntent, stopIntent, fireTime, isInfinitely);
    }

    // установка нового времени срабатывания для той же джобы
    public void resetFireTime(long newFireTime) {
        mParams.fireTime = newFireTime;
    }

    /**
     * запуск задачи. Враппер для JobScheduler.schedule()
     */
    public void schedule() {
        //noinspection EqualsBetweenInconvertibleTypes
        if(!mParams.equals(getPendingJob())) {
            cancel();
            boolean b = jobScheduler().schedule(mParams.createJobInfo()) == JobScheduler.RESULT_SUCCESS;
            Logger.INSTANCE.d("job scheduled %s. [%s]", b ? "successfully" : "NOT SUCCESSFULLY", mParams.toString());
        }
    }

    /**
     * отмена зашедуленной задачи. Враппер для JobScheduler.cancel()
     */
    public void cancel() {
        if(getPendingJob()!=null) {
            jobScheduler().cancel(mParams.jobId);
            Logger.INSTANCE.d("job %d cancelled", mParams.jobId);
        }
    }

    private JobInfo getPendingJob() {
        return jobScheduler().getPendingJob(mParams.jobId);
    }

    /**
     * сигнал завершения задачи, запускаемой джобой.
     * Враппер для JobService.jobFinished()
     * @param isSuccess задача выполнена успешна, повтор не требуется
     */
    public void jobFinished(boolean isSuccess) {
        Intent intent = new Intent(JOB_FINISH_ACTION);
        intent.putExtra(SUCCESS_KEY, isSuccess);
        LocalBroadcastManager.getInstance(App.getAppContext()).sendBroadcast(intent);
    }

    private static JobScheduler jobScheduler() {
        return (JobScheduler) App.getAppContext().getSystemService(Context.JOB_SCHEDULER_SERVICE);
    }

    // класс для хранения параметров задачи и генерации из них JobInfo
    private static class Params {
        int jobId;
        PendingIntent startIntent;
        PendingIntent stopIntent;
        long fireTime;
        boolean isInfinitely;

        JobInfo createJobInfo() {
            Bundle extras = new Bundle();
            extras.putParcelable(START_INTENT_KEY, startIntent);
            extras.putParcelable(STOP_INTENT_KEY, stopIntent);

            @SuppressLint("JobSchedulerService")
            ComponentName componentName = new ComponentName(App.getAppContext(), ScheduledService.class);
            JobInfo.Builder builder = new JobInfo.Builder(jobId, componentName)
                    .setPersisted(false)
                    .setTransientExtras(extras)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
            if(isInfinitely)
                builder.setPeriodic(fireTime, fireTime/3);
            else
                builder.setMinimumLatency(fireTime);

            return builder.build();
        }

        @NonNull
        @Override
        public String toString() {
            return String.format(Locale.ENGLISH, "jobId: %d, startIntent: %s, stopIntent: %s, fireTime: %d sec, isInfinitely %b",
                    jobId, startIntent == null ? "null" : "ok", stopIntent == null ? "null" : "ok", fireTime/1000, isInfinitely);
        }

        @Override
        public boolean equals(@Nullable Object obj) {
            if(obj instanceof JobInfo) {
                JobInfo jobInfo = (JobInfo) obj;
                return jobId == jobInfo.getId() &&
                        startIntent.equals(jobInfo.getTransientExtras().getParcelable(START_INTENT_KEY)) &&
                        stopIntent.equals(jobInfo.getTransientExtras().getParcelable(STOP_INTENT_KEY)) &&
                        isInfinitely == jobInfo.isPeriodic() &&
                        (isInfinitely && fireTime == jobInfo.getIntervalMillis() || !isInfinitely && fireTime == jobInfo.getMinLatencyMillis());

            }
            else
                return false;
        }
    }

    public static class ScheduledService extends JobService {
        JobParameters mJobParameters;
        final JobIntentReceiver mReceiver = new JobIntentReceiver();

        @Override
        public boolean onStartJob(JobParameters params) {
            Logger.INSTANCE.d("---------------- onStartJob [%d] ---------------", params.getJobId());
            mJobParameters = params;
            LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(JOB_FINISH_ACTION));
            sendIntentFrom(START_INTENT_KEY, params);
            return true;
        }

        @Override
        public boolean onStopJob(JobParameters params) {
            Logger.INSTANCE.d("onStopJob [%d]", params.getJobId());
            sendIntentFrom(STOP_INTENT_KEY, params);
            return false;
        }

        private void sendIntentFrom(String key, JobParameters params) {
            PendingIntent pi = params.getTransientExtras().getParcelable(key);
            if (pi != null)
                try {
                    pi.send();
                }
                catch (PendingIntent.CanceledException e) {
                    throw new Error(e);
                }
        }

        // локальный ресивер, используется для связи с сервисом из Job и вызова jobFinished()
        private class JobIntentReceiver extends BroadcastReceiver {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isSuccess = intent.getBooleanExtra(SUCCESS_KEY, false);
                LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
                jobFinished(mJobParameters, !isSuccess);
                Logger.INSTANCE.d("jobFinished [%d]", mJobParameters.getJobId());
            }
        }
    }
}
