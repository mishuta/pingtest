package com.mishuto.pingtest.common;

/**
 * Реализация assert
 * Created by Michael Shishkin on 14.11.2021
 */
public class Assert {
    public static void that(boolean condition) {
        that(condition, null);
    }

    public static void that(boolean condition, String reason) {
        if(!condition)
            throw reason == null ? new  AssertionError() : new AssertionError(reason);

    }
}
