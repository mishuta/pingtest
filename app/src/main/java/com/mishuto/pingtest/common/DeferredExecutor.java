package com.mishuto.pingtest.common;

/**
 * Вспомогательный класс, выполняющий отложенное исполнение процедуры после наступления специального события
 * Created by Michael Shishkin on 07.08.2020
 */
public class DeferredExecutor {
    private boolean hasEventCome = false;
    private Runnable mRunnable;

    public void execute(Runnable runnable) {
        if (hasEventCome)
            runnable.run();
        else
            mRunnable = runnable;
    }

    public void onDeferredEvent() {
        hasEventCome = true;
        if (mRunnable != null) {
            mRunnable.run();
            mRunnable = null;
        }
    }
}
