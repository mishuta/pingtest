package com.mishuto.pingtest.common;

import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * Реализация кольцевого буфера фиксированной длинны.
 * Элементы буфера не могут быть null
 * Created by Michael Shishkin on 07.06.2020
 */
public class FixCircularBuffer<E> extends CircularBuffer<E> {
    public final static int MIN_BUFFER_LENGTH = 1;  // минимальный размер буфера
    private int mLength;                            // текущий размер буфера

    public FixCircularBuffer() {
        this(MIN_BUFFER_LENGTH);
    }

    public FixCircularBuffer(int length) {
        setLength(length);
    }

    @Override
    boolean isOverFlow(E offered, E head) {
        return size() > mLength;
    }

    public int getLength() {
        return mLength;
    }

    /**
     * Установить новую длину буфера.
     * Если новая длина меньше текущего размера, то лишние элементы удаляются из начала списка
     * Если новая длина больше или такая же - текущий буфер не изменяется
     * @param length новая длина буфера. Не может быть меньше 1
     */
    public void setLength(int length) {
        Assert.that(length >= MIN_BUFFER_LENGTH, "trying to set " + length + " as length of CircularBuffer. Min length should be " + MIN_BUFFER_LENGTH);

        while (size() > length)
            remove();

        mLength = length;
    }

    // при выполнении addAll, оставляем только mLength последних элементов коллекции, что сокращает время за счет бесполезных вставок
    @Override
    public boolean addAll(Collection<? extends E> c) {
        if(c.size() > mLength)
            c = c.stream()
                    .skip(c.size() - mLength)
                    .collect(Collectors.toCollection(LinkedList::new));
        return super.addAll(c);
    }
}
