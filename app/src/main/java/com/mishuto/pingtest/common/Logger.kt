package com.mishuto.pingtest.common

import android.util.Log.*
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase

/**
 * Вывод в лог.
 * Created by Michael Shishkin on 11.06.2021, refactored on Kotlin on 26.01.2024
 */
@Suppress("unused")
object Logger {
    private const val TAG = "PingMonitor" // Тэг не должен совпадать с названием пакета, чтоб можно было фильтровать только свои
    
    var stdOutOnly = false
    
    private val levelMap = mapOf(VERBOSE to "Trace", DEBUG to "Debug", INFO to "Info", WARN to "Warning", ERROR to "Error")
    
    fun e(mes: String) = log(ERROR, mes)
    fun e(e: Throwable) = e(e.stackTraceToString())
    fun w(mes: String) = log(WARN, mes)
    fun i(mes: String) = log(INFO, mes)
    fun d(mes: String) = log(DEBUG, mes)
    fun d(formatStr: String, vararg args: Any) = d(String.format(formatStr, *args))
    fun t(mes: String) = log(VERBOSE, mes)
    fun tag() = log(DEBUG, "")
    fun flushLog() = Firebase.crashlytics.sendUnsentReports()
    
    @Synchronized
    private fun log(level: Int, mes: String) {
        val logMsg = mes.logFormat()
        
        if (stdOutOnly) println(logMsg.addLevelPrefix(level))
        else {
            println(level, TAG, logMsg)
            if (level != VERBOSE)
                Firebase.crashlytics.log(logMsg.addLevelPrefix(level))
        }
    }
    
    private fun String.addLevelPrefix(level: Int): String = if (level != DEBUG) "[${levelMap[level]!!}] " + this else this
    
    private fun String.logFormat(): String =
        with(CallerUtils(Logger.javaClass)) { "[$className.$method]" }
            .replace("lambda$", "").removeSuffix("$")
            .replace("""\$(\w+-)+\w+""".toRegex(), "")  // удаляем подстроки вида: $com-mishuto-pingtest-controller-main-MainFragment
            .plus(" $this")
}