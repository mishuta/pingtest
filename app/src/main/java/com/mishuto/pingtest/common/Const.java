package com.mishuto.pingtest.common;

/**
 * Константы
 * Created by Michael Shishkin on 29.06.2020
 */
public interface Const {

    int SEC = 1000;
    int MIN = 60*SEC;
    int HOUR = 60*MIN;
    int INFINITY = -1;

    // Константы для настроек
    //todo вынести в settings.xml
    interface APPSettings {
        Integer[] PING_TIMEOUT = {SEC, 2*SEC, 3*SEC, 4*SEC, 5*SEC, 10*SEC};
        Integer[] PING_INTERVAL = {100, 200, 500, SEC, 2*SEC, 4*SEC, 10*SEC, 15*SEC, 30*SEC, MIN};
        Integer[] MOVING_PINGS_NUMBER = { 30, 50, 100, 300, 1000, INFINITY };
        Integer[] TEST_DURATION = {MIN, 5*MIN, 10*MIN, 15*MIN, 30*MIN, HOUR, 2*HOUR, INFINITY };
    }

    interface WidgetSettings {
        Integer[] SERIES_INTERVAL = { 15*MIN, 30*MIN, HOUR, 2*HOUR, 3*HOUR, 4*HOUR };
        Integer[] SERIES_LENGTH = { 10, 20, 40, 60 };
    }

    long PING_TIMEOUT_LIMIT = 30*SEC;   // предельное значение таймаута, после которого дальнейший сбор статистики должен быть остановлен
}
