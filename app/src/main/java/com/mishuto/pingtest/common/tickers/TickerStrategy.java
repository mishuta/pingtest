package com.mishuto.pingtest.common.tickers;

/**
 * Интерфейс для имплементации конкретного тикера
 * Created by Michael Shishkin on 19.03.2021
 */
public interface TickerStrategy {
    void start(Runnable listener, boolean isInfinitely, int interval);
    void stop();
    boolean isStarted();
}
