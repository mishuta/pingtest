package com.mishuto.pingtest.common

import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle.State.RESUMED
import androidx.lifecycle.Lifecycle.State.STARTED
import androidx.lifecycle.LifecycleOwner
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.tag
import java.util.LinkedList
import java.util.Queue

/**
 * Класс для последовательного отображения нескольких AlertDialog-ов.
 * Запросы на отображение множества диалогов при запуске активности, упорядочиваются в очереди и отображаются в фазе
 * между START и STOP.
 * При пересоздании активности (смене ориентации экрана) очередь тоже пересоздается и клиенты должны самостоятельно
 * повторно добавлять диалоги в очередь (поскольку клиенты так же пересоздаются)
 *
 * Created by Michael Shishkin on 17.10.2021 (refactored on 03.12.2023)
 */
class AlertQueManager(val activity: AppCompatActivity) : DefaultLifecycleObserver {
    private val que = Que()
    private val currentDialog: AlertDialog?
        get() = que.first()?.dialog
    
    init {
        activity.lifecycle.addObserver(this)
    }
    
    // Добавить диалог в очередь.
    // Если в диалоге есть dismissListener, то он будет перезаписан, поэтому необходимо добавить его тут, а не в билдере
    fun enqueue(tag: String, dialog: AlertDialog, onDismissListener: DialogInterface.OnDismissListener? = null) {
        d("tag='$tag'")
        if (que.find(tag) != null)
            d("dialog with tag '$tag' already exists in queue")
        else {
            que.add(DialogQueItem(tag, dialog))
            dialog.setOnDismissListener { onDismiss(onDismissListener) }
            tryShowNext()
        }
    }
    
    private fun onDismiss(onDismissListener: DialogInterface.OnDismissListener?) {
        // фильтруем события принудительного закрытия диалога в onStop или случайного dismiss из отработанной активности или случайного повторного onDismiss
        if (isActivityInStartStopPhase() && que.isNotEmpty()) {
            tag()
            que.release()
            onDismissListener?.onDismiss(currentDialog)
            tryShowNext()
        }
    }
    
    override fun onStart(owner: LifecycleOwner) {
        tag()
        tryShowNext()
    }
    
    // принудительное гашение диалога, чтобы не было ошибки детача
    override fun onStop(owner: LifecycleOwner) {
        tag()
        if (currentDialog?.isShowing == true) {
            currentDialog!!.dismiss()
        }
    }
    
    private fun tryShowNext() {
        if (isActivityInStartStopPhase() && currentDialog?.isShowing == false) {
            currentDialog!!.show()
            d("dialog ${que.first()!!.tag} is showing")
        }
    }
    
    private fun isActivityInStartStopPhase() = activity.lifecycle.currentState in listOf(RESUMED, STARTED)
    
    data class DialogQueItem(
        val tag: String,
        val dialog: AlertDialog
    )
    
    class Que {
        private val dialogQueue = DialogQueue()
        
        fun add(item: DialogQueItem) = dialogQueue.add(item)
            .also { d("Dialog ${item.tag} enqueued") }
        
        fun release(): DialogQueItem = dialogQueue.remove()
            .also { d("Dialog ${it.tag} removed") }
        
        fun find(tag: String): DialogQueItem? = dialogQueue.find { it.tag == tag }
        
        fun first(): DialogQueItem? = dialogQueue.firstOrNull()
        
        fun isNotEmpty() = dialogQueue.isNotEmpty()
        
        private class DialogQueue(private val queue: LinkedList<DialogQueItem> = LinkedList()) : Queue<DialogQueItem> by queue
    }
}