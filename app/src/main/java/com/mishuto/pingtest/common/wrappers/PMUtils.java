package com.mishuto.pingtest.common.wrappers;

import static android.content.Context.BATTERY_SERVICE;
import static android.content.Context.POWER_SERVICE;

import android.os.BatteryManager;
import android.os.PowerManager;

import com.mishuto.pingtest.App;

/**
 * Утилиты над PowerManager
 * Created by Michael Shishkin on 10.08.2021
 */
public class PMUtils {
    public static PowerManager getPM() {
        return ((PowerManager) App.getAppContext().getSystemService(POWER_SERVICE));
    }

    public static boolean isScreenOn() {
      return getPM().isInteractive();
    }

    public static Boolean isPowerConnected() {
        return ((BatteryManager) App.getAppContext().getSystemService(BATTERY_SERVICE)).isCharging();
    }

    public static boolean isIgnoringBatteryOptimizations() {
        return getPM().isIgnoringBatteryOptimizations(App.getAppContext().getPackageName());
    }
}
