package com.mishuto.pingtest.common

import kotlin.math.max
import kotlin.math.min

/**
 * Агрегация значений, позволяющая значительно сократить логирование быстроменяющихся переменных.
 * Пока поддерживаются только целочисленные значения, но легко добавляются другие типы.
 * Created by Michael Shishkin on 30.03.2024
 */
class VarsAggregator(
    private val aggregators: List<Aggregator<*>>,   // набор агрегируемых величин
    private val maxCount: Int = Int.MAX_VALUE,      // количество изменений переменной, необходимых для вызова колбэка
    maxDelay: Int = Int.MAX_VALUE,                  // время, прошедшее после первого изменения, перед вызовом колбэка
    private val onLimit: (aggregators: List<Aggregator<*>>) -> Unit // колбэк с агрегируемыми значениями. После вызова агрегатор обнуляется.
) {
    private val intAggregators: Map<String, IntAggregator> = aggregators.filterIsInstance<IntAggregator>().associateBy({ it.name }, { it })
    private var count = 0
    private var timer = Timer(maxDelay.toLong())
    
    fun add(vararg aggregatedVales: AggregatedValue<*>) {
        count++
        for (v in aggregatedVales) {
            when (v) {
                is IntValue -> intAggregators[v.name]?.add(v.value)
            }
        }
        checkLimits()
    }
    
    private fun checkLimits() {
        if (count == 1) timer.start()
        if (count >= maxCount || timer.isTimeUp) {
            onLimit(aggregators)
            count = 0
            aggregators.forEach { it.clear() }
        }
    }
}

sealed interface AggregatedValue<T> {
    val name: String
    val value: T
}

data class IntValue(override val name: String, override val value: Int) : AggregatedValue<Int>

interface Aggregator<T> {
    val name: String
    val count: Int
    fun aggregatedResults(): Map<String, T>
    fun add(value: T)
    fun clear()
}

class IntAggregator(override val name: String) : Aggregator<Int> {
    companion object {
        const val MIN = "Min"
        const val MAX = "Max"
        const val AVG = "Avg"
    }
    
    override var count: Int = 0
    private var min: Int = Int.MAX_VALUE
    private var max: Int = Int.MIN_VALUE
    private var sum: Int = 0
    private val avg: Int get() = sum / count
    
    override fun aggregatedResults(): Map<String, Int> = mapOf(MIN to min, MAX to max, AVG to avg)
    
    override fun add(value: Int) {
        count++
        min = min(min, value)
        max = max(max, value)
        sum += value
    }
    
    override fun clear() {
        count = 0
        min = Int.MAX_VALUE
        max = Int.MIN_VALUE
        sum = 0
    }
    
    override fun toString(): String {
        return "$name: ($count, $min - $avg - $max)"
    }
}