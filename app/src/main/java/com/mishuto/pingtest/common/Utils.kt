package com.mishuto.pingtest.common

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.preference.PreferenceManager
import com.mishuto.pingtest.App.Companion.appContext
import com.mishuto.pingtest.BuildConfig
import com.mishuto.pingtest.R
import java.util.Locale

/**
 * Утилиты
 * Created by Michael Shishkin on 06.01.2024
 */

object Environment {
    val isEmulator: Boolean get() = Build.HARDWARE.contains("ranchu") // используем get() для константы, чтобы mockkObject(Environment) не падал на инициализации object
    val isDebug: Boolean get() = BuildConfig.DEBUG
    val isDebugOnEmulator: Boolean get() = isEmulator && isDebug
    
    val deviceModel: String
        get() = if (Build.MODEL.startsWith(Build.MANUFACTURER, true)) Build.MODEL else "${Build.MODEL} ${Build.MANUFACTURER}"
}

object SharedPref {
    private val sp: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext)
    
    fun edit(): SharedPreferences.Editor = sp.edit()
    
    fun getBoolean(key: String, default: Boolean) = sp.getBoolean(key, default)
    fun putBoolean(key: String, value: Boolean) = edit().putBoolean(key, value).apply()
    fun checkFlagAndSet(key: String) = getBoolean(key, false).also { putBoolean(key, true) }
    
    fun getLong(key: String, default: Long) = sp.getLong(key, default)
    fun putLong(key: String, value: Long) = edit().putLong(key, value).apply()
    
    fun getInt(key: String, default: Int) = sp.getInt(key, default)
    fun putInt(key: String, value: Int) = edit().putInt(key, value).apply()
    
    fun getString(key: String, default: String): String = sp.getString(key, default)!!
    fun putString(key: String, value: String) = edit().putString(key, value).apply()
    
    fun getStringSet(key: String, default: Set<String>): Set<String> = sp.getStringSet(key, default)!!
    fun putStringSet(key: String, value: Set<String>) = edit().putStringSet(key, value).apply()
    
    fun registerSPChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) = sp.registerOnSharedPreferenceChangeListener(listener)
    fun unregisterSPChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) = sp.unregisterOnSharedPreferenceChangeListener(listener)
}

object Resources {
    fun getString(resId: Int): String = appContext.getString(resId)
    fun getInt(resId: Int): Int = appContext.resources.getInteger(resId)
    fun getBoolean(resId: Int): Boolean = appContext.resources.getBoolean(resId)
}

fun String.wrap(limit: Int): String = if (length <= limit) this else "${substring(0..limit)}..."

fun locales(): Sequence<Locale> = object : Iterator<Locale> {
    var i = 0
    val locales = appContext.resources.configuration.locales
    override fun hasNext(): Boolean = i < locales.size()
    override fun next(): Locale = locales[i++]
}.asSequence()

fun Activity.startActivity(activityClass: Class<*>) = Intent(this, activityClass).also { startActivity(it) }

fun FragmentActivity.replaceFragment(fragment: Fragment, popToStack: Boolean = false, fragmentId: Int = R.id.fragment) {
    if (supportFragmentManager.findFragmentById(fragmentId)?.javaClass != fragment.javaClass)
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.fragment_slide_in, R.anim.fragment_fade_out, R.anim.fragment_fade_in, R.anim.fragment_slide_out)
            .replace(fragmentId, fragment)
            .apply { if (popToStack) addToBackStack(null) }
            .commit()
}

fun AppCompatActivity.getToolbar(): Toolbar = findViewById<Toolbar>(R.id.toolbar)
    .also { setSupportActionBar(it) }

fun AppCompatActivity.pressBack() = onBackPressedDispatcher.onBackPressed()