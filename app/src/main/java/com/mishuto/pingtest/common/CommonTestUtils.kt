package com.mishuto.pingtest.common

import kotlin.reflect.full.memberFunctions
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

/**
 * Общие утилиты для тестов test & androidTest
 * Created by Michael Shishkin on 30.03.2024
 */

/**
 * Reflect method and prop
 * calling
 * obj.callPrivateFunc("somePrivateFunction")
 * obj.callPrivateFunc("somePrivateFunctionWithParams", "test arg")
 * val prop = obj.getPrivateProperty<ObjType, PropType>("propName")
 */

inline fun <reified T> T.callPrivateFunc(name: String, vararg args: Any?): Any? =
    T::class
        .memberFunctions
        .firstOrNull { it.name == name }
        ?.apply { isAccessible = true }
        ?.call(this, *args)

@Suppress("UNCHECKED_CAST")
inline fun <reified T : Any, R> T.getPrivateProperty(name: String): R? =
    T::class
        .memberProperties
        .firstOrNull { it.name == name }
        ?.apply {isAccessible = true }
        ?.let {
            if (T::class.objectInstance != null)
                it.getter.call()
            else
                it.get(this)
        } as? R?

inline fun <reified T : Any, reified V : Any?> T.setPrivateProperty(name: String, value: V) {
    T::class
        .memberProperties
        .firstOrNull { it.name == name }
        ?.apply {
            isAccessible = true
            (this as kotlin.reflect.KMutableProperty<*>).setter
                .also {
                    if (T::class.objectInstance != null)
                        it.call(value)
                    else
                        it.call(this@setPrivateProperty, value)
                }
        }
}

object Assertions {
    @JvmStatic
    fun assertEqualsWithAccuracy(expected: Long, value: Long, accuracy: Long) {
        if (value < expected - accuracy || value > expected + accuracy)
            throw AssertionError("Value is out of range. Expected: value in [${expected - accuracy}..${expected + accuracy}] but got: $value")
    }
}