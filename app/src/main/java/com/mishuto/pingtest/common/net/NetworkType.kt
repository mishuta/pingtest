package com.mishuto.pingtest.common.net

import android.Manifest.permission.READ_PHONE_STATE
import android.annotation.SuppressLint
import android.content.Context
import android.telephony.TelephonyManager
import android.telephony.TelephonyManager.*
import com.mishuto.pingtest.App.Companion.appContext
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Const.SEC
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.common.net.NetInfo.NET_STATE
import com.mishuto.pingtest.common.net.NetworkType.UNKNOWN_CELL
import com.mishuto.pingtest.common.tickers.Ticker
import com.mishuto.pingtest.controller.services.permissions.Permission
import com.mishuto.pingtest.model.NetTypeChangeEventImp


/**
 * Менеджер типа сети
 * Created by Michael Shishkin on 01.07.2022
 */
const val NET_TYPE_CHANGED = "NetworkType.NetTypeChanged"

object NetworkType {
    private const val NET_STATE_UPDATE_TIME = 7 * SEC
    private const val WIFI = -1
    private const val AIRPLANE = -2
    const val UNKNOWN_CELL = NETWORK_TYPE_UNKNOWN
    
    @Suppress("DEPRECATION")
    @SuppressLint("InlinedApi")
    val types = listOf(
        //WiFi
        NetTypeObject(WIFI, "WiFi", R.drawable.ic_baseline_wifi_24),
        NetTypeObject(NETWORK_TYPE_IWLAN, "IWLAN", R.drawable.ic_baseline_wifi_24),
        //2G (GSM - 1G)
        NetTypeObject(NETWORK_TYPE_GSM, "GSM", R.drawable.ic_baseline_g_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_GPRS, "GPRS", R.drawable.ic_baseline_g_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_1xRTT, "1xRTT", R.drawable.ic_baseline_g_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_IDEN, "iDEN", R.drawable.ic_baseline_g_mobiledata_24),
        //2.75G
        NetTypeObject(NETWORK_TYPE_EDGE, "EDGE", R.drawable.ic_baseline_e_mobiledata_24),
        //3G
        NetTypeObject(NETWORK_TYPE_UMTS, "UMTS", R.drawable.ic_baseline_3g_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_CDMA, "CDMA", R.drawable.ic_baseline_3g_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_EVDO_0, "EV-DO Rel.0", R.drawable.ic_baseline_3g_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_EVDO_A, "EV-DO Rev.A", R.drawable.ic_baseline_3g_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_EVDO_B, "EV-DO Rev.B", R.drawable.ic_baseline_3g_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_EHRPD, "eHRPD", R.drawable.ic_baseline_3g_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_TD_SCDMA, "TD-SCDMA", R.drawable.ic_baseline_3g_mobiledata_24),
        //3G H
        NetTypeObject(NETWORK_TYPE_HSDPA, "HSDPA", R.drawable.ic_baseline_h_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_HSUPA, "HSUPA", R.drawable.ic_baseline_h_mobiledata_24),
        NetTypeObject(NETWORK_TYPE_HSPA, "HSPA", R.drawable.ic_baseline_h_mobiledata_24),
        //3G H+
        NetTypeObject(NETWORK_TYPE_HSPAP, "HSPA+", R.drawable.ic_baseline_h_plus_mobiledata_24),
        //LTE
        NetTypeObject(NETWORK_TYPE_LTE, "LTE", R.drawable.ic_baseline_lte_mobiledata_24),
        //5G
        NetTypeObject(NETWORK_TYPE_NR, "5G", R.drawable.ic_baseline_5g_24),
        //UNKNOWN
        NetTypeObject(UNKNOWN_CELL, "Cell", R.drawable.ic_baseline_signal_cellular_alt_24),
        // airplane
        NetTypeObject(AIRPLANE, "Airplane mode", R.drawable.ic_outline_airplanemode_active_24)
    )
    
    private val telephoneManager = appContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    
    private var lastNetType: NetTypeObject = getNetType()
    private val ticker = Ticker({ onEvent() }, true, NET_STATE_UPDATE_TIME, "--NetworkType")
    
    init {
        EventDispatcher.register(NET_STATE, "NetworkType") { onEvent() }
        ticker.start()
    }
    
    @SuppressLint("MissingPermission")
    fun getNetType(): NetTypeObject {
        return when {
            NetInfo.getInstance().isWiFiOn -> typeObject(WIFI)
            NetInfo.isAirplaneModeOn() -> typeObject(AIRPLANE)
            Permission.hasPermission(READ_PHONE_STATE) -> typeObject(telephoneManager.dataNetworkType)
            else -> typeObject(UNKNOWN_CELL)
        }
    }
    
    private fun onEvent() {
        val netType = getNetType()
        if (lastNetType != netType) {
            d("last=${lastNetType.description} got=${netType.description}")
            EventDispatcher.post(NET_TYPE_CHANGED, NetTypeChangeEventImp(lastNetType, netType))
            lastNetType = netType
        }
    }
}

fun typeObject(type: Int): NetTypeObject = NetworkType.types.firstOrNull { it.type == type } ?: NetworkType.types.first { it.type == UNKNOWN_CELL }

data class NetTypeObject(val type: Int, val description: String, val imageId: Int)