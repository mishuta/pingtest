package com.mishuto.pingtest.common.wrappers;

import android.content.Context;
import android.net.wifi.WifiManager;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.common.Logger;
import com.mishuto.pingtest.common.net.NetInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * Хелпер для получения лока для вайфай
 * Created by Michael Shishkin on 09.08.2021
 */
public class WiFiLockWrapper {
    private static final Map<String, WiFiLockWrapper> lockNames = new HashMap<>();
    private final WifiManager.WifiLock lock;
    private final String name;

    public static WiFiLockWrapper getLock(String lockName) {
        if(!lockNames.containsKey(lockName))
            lockNames.put(lockName, new WiFiLockWrapper(lockName));
        return lockNames.get(lockName);

    }

    private WiFiLockWrapper(String lockName) {
        name = lockName;
        WifiManager manager = (WifiManager) App.getAppContext().getSystemService(Context.WIFI_SERVICE);
        lock = manager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, name);
        lock.setReferenceCounted(false);
    }

    public void lock() {
        if(NetInfo.getInstance().isWiFiOn()) {
            if (!lock.isHeld()) {
                lock.acquire();
                Logger.INSTANCE.d("WIFI lock [%s] acquired", name);

            }
        }
    }

    public void unlock() {
        if (lock.isHeld()) {
            lock.release();
            Logger.INSTANCE.d("WIFI lock [%s] released", name);
        }
    }
}
