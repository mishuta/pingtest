package com.mishuto.pingtest.data

import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.model.ErrorMessageEvent
import com.mishuto.pingtest.model.NetTypeChangeEvent
import com.mishuto.pingtest.model.PingTest
import com.mishuto.pingtest.model.ping.Ping
import com.mishuto.pingtest.settings.Settings

/**
 * Манипуляции с данными теста, сохраняемыми в БД
 * Created by Michael Shishkin on 13.08.2022
 */
class PersistData(private val pingTest: PingTest, val settings: Settings) {

    @Synchronized fun add(ping: Ping){
        val entity = PingEntity(ping, pingTest.testId)
        d("insert PingEntity. key=${entity.testId}")
        pingDAO.insert(entity)
    }

    @Synchronized fun add(netTypeChangeEvent: NetTypeChangeEvent) {
        val entity = NetTypeEventEntity(netTypeChangeEvent, pingTest.testId)
        d("insert NetTypeEventEntity. key=${entity.id}, from=${entity.from.description}, to=${entity.to.description}")
        netChangeDAO.insert(entity)
    }

    @Synchronized fun add(errorMessageEvent: ErrorMessageEvent) {
        val entity = ErrMessageEntity(errorMessageEvent, pingTest.testId)
        d("insert ErrMessageEntity. key=${entity.id}, message=${entity.message}")
        errMessageDAO.insert(entity)
    }

    fun saveResult() {
        if (pingDAO.getCountForTest(pingTest.testId) > 0)
            resultDAO.insert(ResultEntity.createEntity(settings, pingTest))
    }
}