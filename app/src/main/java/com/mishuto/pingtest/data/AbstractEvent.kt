package com.mishuto.pingtest.data

import java.time.LocalDateTime

/**
 * Интерфейсы для сохраняемых событий
 * Created by Michael Shishkin on 21.07.2022
 */
interface EventEntity {
    val id: Long?                   // id события
    val testId: Long                // id теста в рамках которого произошло событие
    val timestamp: LocalDateTime    // время события
}

interface EventDao<T : EventEntity> {
    fun insert(entity: T)
    fun getTest(key: Long) : List<T>
    fun deleteTestAndOlder(key: Long)
    fun deleteTests(keys: Collection<Long>)
    fun getCount() : Int
    fun getCountForTest(key: Long) : Int
    fun getCountForTests(keys: Collection<Long>) : Int

    companion object {
        val events = listOf(pingDAO, netChangeDAO, errMessageDAO)

        fun deleteTestAndOlderAnywhere(key: Long) = events.forEach{ it.deleteTestAndOlder(key) }

        fun deleteTestsAnywhere(keys: Collection<Long>) = events.forEach{ it.deleteTests(keys) }
    }
}