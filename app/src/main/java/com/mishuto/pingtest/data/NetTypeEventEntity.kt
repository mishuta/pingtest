package com.mishuto.pingtest.data

import androidx.room.*
import com.mishuto.pingtest.model.NetTypeChangeEvent
import com.mishuto.pingtest.common.net.NetTypeObject
import java.time.LocalDateTime

/**
 * Событие изменение типа сети
 * Created by Michael Shishkin on 17.07.2022
 */
@Entity
data class NetTypeEventEntity(
    @PrimaryKey
    override val id: Long?,
    override val testId: Long,
    override val timestamp: LocalDateTime,
    override val from: NetTypeObject,
    override val to: NetTypeObject,
) : EventEntity, NetTypeChangeEvent {
        constructor(event: NetTypeChangeEvent, testId: Long) : this (null, testId, event.timestamp, event.from, event.to)
}

@Dao
interface NetChangeDao : EventDao<NetTypeEventEntity> {
    @Insert
    override fun insert(entity: NetTypeEventEntity)

    @Query("DELETE from NetTypeEventEntity where testId<=:key")
    override fun deleteTestAndOlder(key: Long)

    @Query("DELETE from NetTypeEventEntity where testId in (:keys)")
    override fun deleteTests(keys: Collection<Long>)

    @Query("SELECT * from NetTypeEventEntity where testId=:key")
    override fun getTest(key: Long) : List<NetTypeEventEntity>

    @Query("SELECT count(*) FROM NetTypeEventEntity")
    override fun getCount() : Int

    @Query("SELECT count(*) from NetTypeEventEntity where testId=:key")
    override fun getCountForTest(key: Long) : Int

    @Query("SELECT count(*) from NetTypeEventEntity where testId in (:keys)")
    override fun getCountForTests(keys: Collection<Long>): Int
}

