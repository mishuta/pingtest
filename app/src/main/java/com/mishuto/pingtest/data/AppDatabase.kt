package com.mishuto.pingtest.data

import androidx.room.*
import com.mishuto.pingtest.App.Companion.appContext
import com.mishuto.pingtest.common.net.NetTypeObject
import com.mishuto.pingtest.common.net.typeObject
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

/**
 * Создание БД
 * Created by Michael Shishkin on 15.07.2022
 */

@Database(entities = [
    PingEntity::class,
    NetTypeEventEntity::class,
    ErrMessageEntity::class,
    ResultEntity::class],
    version = 2)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun pingDao(): PingDao
    abstract fun netChangeDAO(): NetChangeDao
    abstract fun resultDAO() : ResultDao
    abstract fun errMessageDao() : ErrMessageDao

    companion object {
        var db = Room.databaseBuilder(appContext, AppDatabase::class.java, "pingtest.db")
            .allowMainThreadQueries()
            .addMigrations(*MIGRATION_SCRIPTS)
            .build()
    }
}

val netChangeDAO
    get() = AppDatabase.db.netChangeDAO()
val pingDAO
    get() = AppDatabase.db.pingDao()
val errMessageDAO
    get() = AppDatabase.db.errMessageDao()
val resultDAO
    get() = AppDatabase.db.resultDAO()

fun generateId() = Instant.now().toEpochMilli()

class Converters {
    @TypeConverter
    fun localDateTimeToMilli(time: LocalDateTime) : Long = ZonedDateTime.of(time, ZoneId.systemDefault()).toInstant().toEpochMilli()

    @TypeConverter
    fun milliToLocalDateTime(milli: Long) : LocalDateTime = LocalDateTime.ofInstant(
        Instant.ofEpochMilli(milli),
        TimeZone.getDefault().toZoneId())

    @TypeConverter
    fun netTypeObjectToInt(netTypeObject: NetTypeObject) : Int = netTypeObject.type

    @TypeConverter
    fun intToNetTypeObject(type: Int) : NetTypeObject = typeObject(type)
}