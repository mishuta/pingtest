 package com.mishuto.pingtest.data

import android.annotation.SuppressLint
import androidx.room.*
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.data.EventDao.Companion.deleteTestAndOlderAnywhere
import com.mishuto.pingtest.model.ping.Ping
import java.time.LocalDateTime

 /**
 * Entity пинга
 * Created by Michael Shishkin on 12.07.2022
 */
private const val PING_TABLE_LIMIT = 256 * 1024 // макс размер таблицы под пинг 256К записей

@SuppressLint("ParcelCreator")
@Entity
data class PingEntity(
    @PrimaryKey
    override val id: Long?,
    override val testId : Long,
    override val timestamp: LocalDateTime,
    private val ordinal: Int,
    private val latency: Long,
    private val effectiveLatency: Long,
    private val isLost: Boolean,
    private val isError: Boolean,
    private val wasICMPFail: Boolean
) : Ping, EventEntity {
    
    constructor(ping: Ping, testId: Long) :
            this(null, testId, ping.timeStamp, ping.ordinal, ping.latency, ping.effectiveLatency, ping.isLost, ping.isError, ping.wasICMPFail())
    
    override fun getLatency() = latency
    override fun getEffectiveLatency() = effectiveLatency
    override fun isLost() = isLost
    override fun getTimeStamp() = timestamp
    override fun isError() = isError
    override fun getOrdinal() = ordinal
    override fun wasICMPFail() = wasICMPFail
}

@Dao
abstract class PingDao : EventDao<PingEntity> {
    @Insert
    protected abstract fun insertPing(entity: PingEntity)
    
    @Query("SELECT count(*) FROM PingEntity")
    abstract override fun getCount() : Int
    
    @Query("SELECT * from PingEntity where testId=:key")
    abstract override fun getTest(key: Long): List<PingEntity>
    
    @Query("SELECT count(*) from PingEntity where testId=:key")
    abstract override fun getCountForTest(key: Long) : Int
    
    @Query("SELECT count(*) from PingEntity where testId in (:keys)")
    abstract override fun getCountForTests(keys: Collection<Long>) : Int
    
    @Query("SELECT min(testId) from PingEntity")
    abstract fun getFirstTestKey() : Long?
    
    @Query("SELECT max(testId) from PingEntity")
    abstract fun getLastTestKey() : Long?
    
    @Query("SELECT * FROM (SELECT * from PingEntity where testId = :key order by id desc limit :n) order by id")
    abstract fun getLastElementsForTest(key: Long, n: Int) : List<PingEntity>
    
    @Query("DELETE from PingEntity where id in (SELECT id from PingEntity order by id limit :n)")
    abstract fun deleteFirstElements(n: Int)
    
    @Query("DELETE from PingEntity where testId<=:key")
    abstract override fun deleteTestAndOlder(key: Long)
    
    @Query("DELETE from PingEntity where testId in (:keys)")
    abstract override fun deleteTests(keys: Collection<Long>)
    
    
    override fun insert(entity: PingEntity) = insert(entity, PING_TABLE_LIMIT)
    
    fun insert(entity: PingEntity, tableLimit: Int) {
        insertPing(entity)
        if (getCount() > tableLimit)
            wrapTable(entity.testId, tableLimit)
    }
    
    // Заворачивание таблицы до требуемой длины. Возможно длинная операция, но она выполняется в потоке таймера пинга, потому не влияет на главный поток
    // удаляются все тесты, начиная с ранних до тех пор, пока размер таблицы не станет меньше лимита.
    // Если все кроме текущего удалены, а текущий тест превысил лимит, то удаляется половина
    private fun wrapTable(currentKey: Long, tableLimit: Int) {
        do {
            d("PingEntity has ${getCount()} rows. It gonna to wrap up to limit: $tableLimit")
            val firstKey = getFirstTestKey()!!
            if (currentKey == firstKey) {
                deleteFirstElements(getCount() / 2)
                break
            }
            
            deleteTestAndOlderAnywhere(firstKey)
        } while (getCount() > tableLimit)
    }
}
