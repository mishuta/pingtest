package com.mishuto.pingtest.data

import androidx.room.*
import com.mishuto.pingtest.common.Assert
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.minus
import com.mishuto.pingtest.common.net.NetTypeObject
import com.mishuto.pingtest.common.net.NetworkType
import com.mishuto.pingtest.data.EventDao.Companion.deleteTestsAnywhere
import com.mishuto.pingtest.model.PingTest
import com.mishuto.pingtest.model.stat.PingStatData
import com.mishuto.pingtest.settings.Settings
import java.time.LocalDateTime
import java.util.TreeMap

/**
 * Хранение в БД результатов тестов
 * Created by Michael Shishkin on 17.07.2022
 */

private const val MAX_ROWS = 800
private const val WRAP_ROWS = 500

@Entity
data class ResultEntity(
    @PrimaryKey
    val testId: Long,
    val startTime: LocalDateTime,
    val finishTime: LocalDateTime,
    val host: String,
    val ip: String,
    val pingTimeout: Int,
    val pingInterval: Int,
    val startNetType: NetTypeObject,
    val mainNetType: NetTypeObject,
    @Embedded val statistics: PingStatData
): Comparable<ResultEntity> {
    
    fun testDuration() = finishTime - startTime
    
    companion object {
        fun createEntity(settings: Settings, test: PingTest): ResultEntity {
            Assert.that(pingDAO.getCountForTest(test.testId) > 0, "must be at least one ping in result set")
            val fullTestStat = resultDAO.getStatistics(test.testId)
            
            with(test) {
                val testParams = TestParams(testId, fullTestStat.duration, finished!!)
                return ResultEntity(
                    testId = testId,
                    startTime = started,
                    finishTime = finished!!,
                    host = hostInfo.name,
                    ip = hostInfo.ip,
                    pingTimeout = settings.pingTimeout,
                    pingInterval = settings.pingInterval,
                    startNetType = testParams.startNetType,
                    mainNetType = testParams.calculateMainNetType(),
                    statistics = fullTestStat
                )
            }
        }
        
        private class TestParams(testId: Long, statDuration: Long, val finished: LocalDateTime) {
            val statStart = finished - statDuration
            
            private val netChangeEvents = netChangeDAO.getTest(testId)
            val startNetType get() = if (netChangeEvents.isEmpty()) NetworkType.getNetType() else netChangeEvents[0].from
            
            fun calculateMainNetType(): NetTypeObject {
                if (netChangeEvents.isEmpty()) return startNetType
                
                val durationNetTypeMap: TreeMap<Long, NetTypeObject> = TreeMap()
                var prevTime = statStart
                
                netChangeEvents.forEach {
                    durationNetTypeMap[it.timestamp - prevTime] = it.from
                    prevTime = it.timestamp
                }
                
                durationNetTypeMap[finished - prevTime] = netChangeEvents.last().to
                
                return durationNetTypeMap.lastEntry()!!.value
            }
        }
    }
    
    override fun compareTo(other: ResultEntity): Int = testId.compareTo(other.testId)
}

@Dao
abstract class ResultDao {
    @Insert
    protected abstract fun insertResult(resultEntity: ResultEntity)
    
    @Query("SELECT * FROM ResultEntity ORDER BY testId DESC LIMIT 1")
    abstract fun getLast(): ResultEntity?
    
    @Query("SELECT count(*) FROM ResultEntity")
    abstract fun getCount(): Int
    
    @Query("SELECT * FROM ResultEntity ORDER BY testId DESC")
    abstract fun getResultsInReverseOrder(): List<ResultEntity>
    
    @Query("SELECT * FROM ResultEntity LIMIT 1 OFFSET :arg0")
    abstract fun getByIndexInReverseOrder(arg0: Int): ResultEntity?
    
    @Query("SELECT * FROM ResultEntity WHERE testId < :id ORDER BY testId DESC")
    abstract fun getResultsInReverseOrderBefore(id: Long): List<ResultEntity>
    
    @Query("DELETE FROM ResultEntity WHERE testId IN (:ids)")
    abstract fun deleteEntitiesByIDs(ids: Collection<Long>): Int
    
    @Query(
        """
        SELECT
            avg(effectiveLatency) AS average,
            quantity-count(*) AS lost,
            jitter,
            min(effectiveLatency) AS best,
            max(effectiveLatency) AS worst,
            quantity,
            finish-start+latency AS duration
        FROM (SELECT effectiveLatency FROM pingentity WHERE isLost=0 and testId=:testId)
        -- duration
        CROSS JOIN (SELECT count(*) AS quantity, min(timestamp) AS start, max(timestamp) AS finish FROM pingentity WHERE testId=:testId)
        CROSS JOIN (SELECT latency FROM pingentity WHERE testId=:testId ORDER BY timestamp DESC LIMIT 1)
        -- jitter
        CROSS JOIN (
            SELECT CASE
                WHEN jitter_count < 2 THEN 0
                ELSE avg_diff
            END AS jitter
            FROM(
                SELECT count(*) AS jitter_count,  avg(diff) AS avg_diff
                FROM (
                    SELECT ABS(p1.EffectiveLatency - p2.EffectiveLatency) AS diff
                    FROM pingentity p1 JOIN pingentity p2 on p1.id = p2.id-1
                    WHERE p1.isLost=0 AND p1.testId=:testId
                )
            )
        )
    """
    )
    abstract fun getStatistics(testId: Long): PingStatData
    
    fun deleteEntries(resultEntities: Collection<ResultEntity>) {
        d("deleting: ${resultEntities.size} entities")
        val ids = resultEntities.map { it.testId }
        val count = deleteEntitiesByIDs(ids)
        deleteTestsAnywhere(ids)
        d("deleted $count entities")
    }
    
    fun insert(resultEntity: ResultEntity) { // для тестирования использовать сигнатуру insert(resultEntity: ResultEntity, maxRows=MAX_ROWS, wrapRows=WRAP_ROWS). Повторить в PingEntity
        if (getCount() > MAX_ROWS) {
            d("wrapping ResultEntity because size is ${getCount()}")
            val borderId = getByIndexInReverseOrder(MAX_ROWS - WRAP_ROWS)!!.testId
            val deletedEntities = getResultsInReverseOrderBefore(borderId)
            deleteEntries(deletedEntities)
            d("ResultEntity wrapped to ${getCount()}")
        }
        insertResult(resultEntity)
    }
}