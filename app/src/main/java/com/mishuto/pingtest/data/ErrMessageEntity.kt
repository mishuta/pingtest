package com.mishuto.pingtest.data

import androidx.room.*
import com.mishuto.pingtest.model.ErrorMessageEvent
import java.time.LocalDateTime

/**
 * Логируемое событие-сообщение
 * Created by Michael Shishkin on 21.07.2022
 */
@Entity
data class ErrMessageEntity (
    @PrimaryKey
    override val id: Long?,
    override val testId: Long,
    override var timestamp: LocalDateTime,
    override val message: String
    ) : EventEntity, ErrorMessageEvent {
        constructor(event: ErrorMessageEvent, testId: Long) : this(null, testId, event.timestamp, event.message)
}

@Dao
interface ErrMessageDao : EventDao<ErrMessageEntity> {
    @Insert
    override fun insert(entity: ErrMessageEntity)

    @Query("SELECT * from ErrMessageEntity where testId=:key")
    override fun getTest(key: Long): List<ErrMessageEntity>

    @Query("DELETE from ErrMessageEntity where testId<=:key")
    override fun deleteTestAndOlder(key: Long)

    @Query("DELETE from ErrMessageEntity where testId in (:keys)")
    override fun deleteTests(keys: Collection<Long>)

    @Query("SELECT count(*) FROM ErrMessageEntity")
    override fun getCount() : Int

    @Query("SELECT count(*) from ErrMessageEntity where testId=:key")
    override fun getCountForTest(key: Long) : Int

    @Query("SELECT count(*) from ErrMessageEntity where testId in (:keys)")
    override fun getCountForTests(keys: Collection<Long>): Int
}