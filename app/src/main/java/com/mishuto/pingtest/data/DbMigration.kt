package com.mishuto.pingtest.data

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Assert
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Resources.getString

/**
 * Миграции версий БД
 * Created by Michael Shishkin on 29.12.2023
 */
val MIGRATION_SCRIPTS : Array<Migration> = arrayOf(
    DbMigration(1,2, R.string.migration_1_2).toMigration(),
)

private data class DbMigration(
    val verFrom: Int,
    val verTo: Int,
    val scriptId: Int
) {
    fun toMigration() = object : Migration(verFrom, verTo) {
        override fun migrate(db: SupportSQLiteDatabase) {
            d("migrate db from ver $verFrom to $verTo")
            db.beginTransaction()
            try {
                parseScriptToListOfCommands(scriptId).forEach {
                    d(it)
                    db.execSQL(it)
                }
                db.setTransactionSuccessful()
                d("migration was successful completed")
            }
            finally {
                db.endTransaction()
            }
        }
    }

    fun parseScriptToListOfCommands(scriptId: Int) : List<String> {
        val sqlCommands = listOf("CREATE","DROP","ALTER","INSERT","DELETE","UPDATE")
        val minCmdLen = sqlCommands.minOf { it.length }
        val script = getString(scriptId)
        val result = mutableListOf<String>()
        var currentIndex = 0

        while (currentIndex < script.length - minCmdLen) {
            if(sqlCommands.any { it.contains(script.substring(currentIndex, currentIndex + minCmdLen), true) }) {
                val endCmdIndex = script.indexOf(";", currentIndex)
                Assert.that(endCmdIndex >= 0, "any command in script must ended by ';'")
                result.add(script.substring(currentIndex, endCmdIndex).replace('\n',' '))
                currentIndex = endCmdIndex
            }
            currentIndex++
        }

        return result
    }
}