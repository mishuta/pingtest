package com.mishuto.pingtest

import android.app.Application
import android.content.Context
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.mishuto.pingtest.common.CrashlyticsKeys
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.SharedPref
import com.mishuto.pingtest.common.net.NetInfo
import com.mishuto.pingtest.common.pref.migrator.Migrator
import com.mishuto.pingtest.controller.services.ProcessEventsObserver
import com.mishuto.pingtest.settings.preferences.theme.AppTheme
import com.mishuto.pingtest.widget.ExecutorManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import java.util.UUID

/**
 * Основной процесс приложения
 * Created by Michael Shishkin on 26.05.2021 (refactored on kotlin 16/03/24)
 */
class App : Application() {
    
    override fun onCreate() {
        super.onCreate()
        d("----- Application's STARTED ------")
        appContext = applicationContext
        initGlobalObjects()
    }
    
    private fun initGlobalObjects() {
        NetInfo.getInstance()
        ExecutorManager.getInstance()
        ProcessEventsObserver.registerObserver()
        Migrator().checkForMigration()
        AppTheme.getCurrentTheme().activate()
        CrashlyticsKeys.update(CrashlyticsKeys.Tag.APP)
        Firebase.crashlytics.setUserId(appGuid)
    }
    
    companion object {
        @JvmStatic
        lateinit var appContext: Context
        
        val applicationScope by lazy { CoroutineScope(SupervisorJob() + Dispatchers.IO) }
        
        val appGuid: String by lazy { retrieveAppGuid() }
        
        private fun retrieveAppGuid(): String {
            var guid = SharedPref.getString("app_guid", "")
            if (guid.isBlank()) {
                guid = UUID.randomUUID().toString()
                SharedPref.putString("app_guid", guid)
            }
            return guid
        }
    }
}
