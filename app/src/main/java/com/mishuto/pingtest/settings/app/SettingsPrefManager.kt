package com.mishuto.pingtest.settings

import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Const.APPSettings.*
import com.mishuto.pingtest.common.Resources
import com.mishuto.pingtest.common.SharedPref
import com.mishuto.pingtest.common.Utils.*
import com.mishuto.pingtest.settings.SettingsPrefManager.isAutostartKey
import com.mishuto.pingtest.settings.SettingsPrefManager.isKeepScreenOnKey
import com.mishuto.pingtest.settings.SettingsPrefManager.movingPingsNumberKey
import com.mishuto.pingtest.settings.SettingsPrefManager.pingIntervalKey
import com.mishuto.pingtest.settings.SettingsPrefManager.pingTimeoutKey
import com.mishuto.pingtest.settings.SettingsPrefManager.showFloatingWindowKey
import com.mishuto.pingtest.settings.SettingsPrefManager.testDurationKey
import com.mishuto.pingtest.settings.preferences.host.HostSetting
import com.mishuto.pingtest.settings.preferences.sound.SoundPrefManager
import com.mishuto.pingtest.settings.preferences.sound.SoundSettings

/**
 * Объекты для доступа к настройкам
 * Created by Michael Shishkin on 28.06.2022
 */

interface Settings {
    val pingTimeout: Int        // таймаут между пингами, мс
    val host: String?           // хост
    val pingInterval: Int       // интервал между пингами, мс
    val movingPingsNumber: Int  // интервал статистики, мс
    val testDuration: Int       // длительность теста, мс
    val isKeepScreenOn: Boolean // экран всегда включен
    val soundSettings: SoundSettings
    val isAutostart: Boolean
    val showFloatingWindow: Boolean
}

object SettingsPrefManager {
    var preferences: Settings = SettingsImpl()
    
    val pingTimeoutKey = getString(R.string.ping_timeout_key)
    val hostKey = getString(R.string.ping_host_key)
    val pingIntervalKey = getString(R.string.ping_interval_key)
    val movingPingsNumberKey = getString(R.string.ping_moving_number_key)
    val testDurationKey = getString(R.string.ping_test_duration_key)
    val isKeepScreenOnKey = getString(R.string.screen_on_key)
    val soundKey = getString(R.string.sound_key)
    val isAutostartKey = getString(R.string.ping_autostart_key)
    val showFloatingWindowKey = getString(R.string.floating_window_key)
    
    private val onSPChangeListeners: MutableMap<String, OnSharedPreferenceChangeListener> = HashMap()
    
    fun registerOnSPChangeListener(tag: String, listener: OnSharedPreferenceChangeListener) {
        //необходимо лисенер сохранять в инстанс-переменную, чтобы передаваемая функция не была уничтожена GC. tag защищает от повторного создания лисенера для того же объекта
        onSPChangeListeners[tag] = listener
        SharedPref.registerSPChangeListener(listener)
    }
    
    fun unregisterOnSPChangeListener(tag: String) {
        val listener = onSPChangeListeners[tag]
        if (listener != null) {
            SharedPref.unregisterSPChangeListener(listener)
            onSPChangeListeners.remove(tag)
        }
    }
}

class SettingsImpl : Settings {
    override val pingTimeout get() = getArrayedVal(pingTimeoutKey, R.integer.ping_timeout_def, PING_TIMEOUT)
    override val host: String get() = HostSetting.getInstance().selectedHost
    override val pingInterval get() = getArrayedVal(pingIntervalKey, R.integer.ping_interval_def, PING_INTERVAL)
    override val movingPingsNumber get() = getArrayedVal(movingPingsNumberKey, R.integer.ping_moving_number_def, MOVING_PINGS_NUMBER)
    override val testDuration get() = getArrayedVal(testDurationKey, R.integer.test_duration_def, TEST_DURATION)
    override val isKeepScreenOn get() = SharedPref.getBoolean(isKeepScreenOnKey, false)
    override val soundSettings get() = SoundPrefManager
    override val isAutostart get() = SharedPref.getBoolean(isAutostartKey, false)
    override val showFloatingWindow get() = SharedPref.getBoolean(showFloatingWindowKey, false)
    
    private fun getArrayedVal(key: String, defId: Int, values: Array<Int>): Int {
        val defVal = Resources.getInt(defId)
        val i = SharedPref.getInt(key, defVal)
        return values[i]
    }
}