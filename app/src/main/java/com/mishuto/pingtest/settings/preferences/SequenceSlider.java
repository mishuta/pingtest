package com.mishuto.pingtest.settings.preferences;

import android.view.View;
import android.widget.TextView;

import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;

import java.util.function.Function;
import java.util.stream.IntStream;

/**
 * Управление SeekBarPref
 * Заменяет плоскую шкалу слайдера 0..N на массив значений Array[0]..Array[N] с возможностью задания поведения отображения
 * Created by Michael Shishkin on 26.06.2020
 */
public class SequenceSlider<T> {

    private final SeekBarPref seekBarPref;
    private final T[] sliderValues;
    private TextView seekBarValueTextView;     // вьюха, для вывода текущего значения
    private Function<T, String> mToTextFunc;    // функция перевода значения слайдера в отображаемый текст
    private Function<T, Boolean> onChangeListener;

    public SequenceSlider(SeekBarPref seekBarPref, T[] sliderValues) {
        this.seekBarPref = seekBarPref;
        this.sliderValues = sliderValues;

        this.seekBarPref.setMax(sliderValues.length - 1);   // значений в слайдере столько сколько элементов в массиве
        this.seekBarPref.setUpdatesContinuously(true);      // отображать значение слайдера во время перемещения бегунка

        // запретить отображать значение слайдера.
        // После выполнения этой операции SeekBar перестанет самостоятельно апдейтить вьюху с текущим значением
        // Это дает возможность записывать в нее кастомизированные значения
        this.seekBarPref.setShowSeekBarValue(false);

        this.seekBarPref.setOnPreferenceChangeListener(this::onChange);
        this.seekBarPref.setOnBindViewListener(this::onBindView);
        mToTextFunc = Object::toString; // по умолчанию отображается текстовое представление значения
    }

    public void setToTextFunc(Function<T, String> textFunction) {
        mToTextFunc = textFunction;
    }

    public void setValue(T value) {
        int index = IntStream.range(0, sliderValues.length)
            .filter(i -> sliderValues[i].equals(value))
            .findFirst()
            .orElseThrow(IllegalArgumentException::new);

        seekBarPref.setValue(index);
    }

    public T getValue() {
        return sliderValues[seekBarPref.getValue()];
    }

    // событие отрисовки текущего значения слайдера
    private void updateValue(int index) {
        T val = sliderValues[index];
        String text = mToTextFunc.apply(val);
        seekBarValueTextView.setText(text);
    }

    // вытаскиваем из seekBar вьюху со значением слайдера
    public void onBindView(PreferenceViewHolder viewHolder ) {

        seekBarValueTextView = (TextView) viewHolder.findViewById(androidx.preference.R.id.seekbar_value);
        seekBarValueTextView.setVisibility(View.VISIBLE);

        updateValue(seekBarPref.getValue());
    }

    public void setOnChangeListener(Function<T, Boolean> listener) {
        onChangeListener = listener;
    }

    public boolean onChange(Preference preference, Object newValue) {
        int index = (Integer)newValue;

        if(onChangeListener != null && !onChangeListener.apply(sliderValues[index]))
            return false;

        updateValue(index);
        return true;
    }
}
