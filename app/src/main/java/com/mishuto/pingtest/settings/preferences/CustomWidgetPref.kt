package com.mishuto.pingtest.settings.preferences

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import com.mishuto.pingtest.R

/**
 * Абстрактный класс для кастомных виджетов
 * Created by Michael Shishkin on 17.02.2024
 */
abstract class CustomWidgetPref<T : View>(context: Context, attributeSet: AttributeSet) : Preference(context, attributeSet) {
    protected lateinit var widget: T
    
    protected fun isWidgetInitialized() = this::widget.isInitialized
    
    init {
        widgetLayoutResource = R.layout.pref_widget_place
    }
    
    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)
        if (!isWidgetInitialized()) {   // обход дублирования добавления виджета
            val container = holder.findViewById(R.id.pref_widget) as FrameLayout
            widget = onCreateWidget()
            container.addView(widget)
        }
    }
    
    protected abstract fun onCreateWidget(): T
    
    protected fun createImage(id: Int): ImageView = ImageView(context).apply { setImageResource(id) }
}