package com.mishuto.pingtest.settings.preferences

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.provider.Settings.ACTION_MANAGE_OVERLAY_PERMISSION
import android.util.AttributeSet
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.widget.SwitchCompat
import androidx.preference.PreferenceViewHolder
import androidx.preference.SwitchPreferenceCompat
import com.google.android.material.materialswitch.MaterialSwitch
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.Resources.getString
import com.mishuto.pingtest.common.Utils.createDialogBuilder
import com.mishuto.pingtest.common.Utils.prepareSnack
import com.mishuto.pingtest.controller.features.premium.billing.Billing
import com.mishuto.pingtest.controller.ui.FloatingPing
import com.mishuto.pingtest.view.Utils.isInEditMode
import com.mishuto.pingtest.view.createImageFrom
import com.mishuto.pingtest.view.findViewByType
import com.mishuto.pingtest.view.setTintColor


/**
 * Настройка включения FloatingWindow
 * Created by Michael Shishkin on 17.02.2024
 */
class FloatingWindowPref(context: Context, attributeSet: AttributeSet) : SwitchPreferenceCompat(context, attributeSet), OnPrefChangedListener {
    
    private lateinit var switch: SwitchCompat
    private val lockIcon = context.createImageFrom(R.drawable.outline_lock_24).apply { setTintColor(R.color.colorError) }
    private val handler = Handler(Looper.getMainLooper())
    
    private enum class State {
        NOT_INITIALIZED, NOT_ALLOWED, OFF, NOT_PERMITTED, ON;
    }
    
    private val currentState: State
        get() =
            if (!Billing.isPremium) State.NOT_ALLOWED
            else if (!isChecked) State.OFF
            else if (!FloatingPing.hasPermission) State.NOT_PERMITTED
            else State.ON
    
    private var lastState: State = State.NOT_INITIALIZED
    
    override fun refresh() {
        tag()
        if (this::switch.isInitialized)
            update()
    }
    
    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)
        switch = (holder.itemView as ViewGroup).findViewByType(MaterialSwitch::class.java)!!
        update()
    }
    
    private fun update() {
        if (!isInEditMode(context) && lastState != currentState) {
            tag()
            lastState = currentState
            
            handler.post {
                when (lastState) {
                    State.NOT_ALLOWED -> setWidget(iconDrawable = lockIcon.drawable, switchVisible = false)
                    State.NOT_PERMITTED -> setWidget(iconId = R.drawable.outline_warning_amber_24, summaryId = R.string.floating_window_no_permission_summary)
                    else -> setWidget()
                }
            }
            
        }
    }
    
    private fun setWidget(
        iconId: Int? = null,
        iconDrawable: Drawable? = null,
        switchVisible: Boolean = true,
        summaryId: Int = R.string.floating_window_on_summary
    ) {
        if (iconId != null) setIcon(iconId)
        else icon = iconDrawable
        
        switch.visibility = if (switchVisible) VISIBLE else GONE
        summary = getString(summaryId)
    }
    
    override fun onClick() {
        super.onClick()
        d("state: $currentState")
        when(currentState) {
            State.NOT_ALLOWED -> prepareSnack(switch, R.string.floating_window_premium_alert).show()
            State.NOT_PERMITTED -> requestPermissionDialog()
            else -> Unit
        }
    }
    
    private fun requestPermissionDialog() {
        createDialogBuilder(context)
            .setMessage(R.string.floating_window_permission_request)
            .setPositiveButton(R.string.open_settings) { _, _ ->
                context.startActivity(Intent(ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:${context.packageName}")))
            }
            .show()
    }
}