package com.mishuto.pingtest.settings.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.widget.ExecutorManager;

/**
 * Активность настроек виджета. Создает фрагмент SettingsFragment
 */
public class WidgetSettingsActivity extends AppCompatActivity {

    private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    private static final String EDIT_MODE = "EditMode";
    private boolean isEditMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(RESULT_CANCELED);     // результат для widget host на случай выхода из активности
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            isEditMode = extras.getBoolean(EDIT_MODE, false);
        }

        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID)
            finish();

        setContentView(R.layout.settings_activity);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.settings, new SettingsFragment())
                    .commit();
        }

    }

    public int getAppWidgetId() {
        return mAppWidgetId;
    }

    public boolean isEditMode() {
        return isEditMode;
    }

    // применение настроек
    void onApply(Settings settings) {
        ExecutorManager.getInstance().applySettings(settings);
        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_OK, resultValue);
        finish();
    }

    // принудительный старт активности для конфигурации виджета
    public static void startActivityForConfigure(Context context, int widgetId) {
        Intent settingsIntent = new Intent(context, WidgetSettingsActivity.class);
        settingsIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
        settingsIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        settingsIntent.putExtra(EDIT_MODE, true);
        settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(settingsIntent);
    }
}