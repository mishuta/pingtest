package com.mishuto.pingtest.settings.preferences.sound

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.mishuto.pingtest.R
import com.mishuto.pingtest.databinding.SoundPrefActivityBinding
import com.mishuto.pingtest.settings.preferences.sound.SoundScope.*

/**
 * Активность настройки параметров звука
 * Created by Michael Shishkin on 31.08.2022
 */


class SoundActivity : AppCompatActivity() {
    private lateinit var binding: SoundPrefActivityBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SoundPrefActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(SoundPrefManager) {
            binding.soundSwitch.isChecked = isSoundOn
            binding.all.isChecked = scope == ALL
            update(isSoundOn)
        }
        binding.soundSwitch.setOnCheckedChangeListener{_,on -> update(on)}

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    @Deprecated("")
    override fun onBackPressed() {
        SoundPrefManager.store(
            binding.soundSwitch.isChecked,
            if(binding.all.isChecked) ALL else LOST)
        super.onBackPressed()
    }

    private fun update(on: Boolean) {
        binding.soundSwitch.text = getString( if(on) R.string.on else R.string.off )
        binding.all.isEnabled = on
        binding.lostOnly.isEnabled = on
    }
}

