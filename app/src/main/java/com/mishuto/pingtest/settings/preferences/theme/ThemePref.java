package com.mishuto.pingtest.settings.preferences.theme;

import static com.mishuto.pingtest.common.Utils.getString;

import android.content.Context;
import android.util.AttributeSet;

import androidx.preference.ListPreference;
import androidx.preference.Preference;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.SharedPref;

import java.util.stream.Stream;

/**
 * Список с выбором темы
 * Created by Michael Shishkin on 31.10.2021
 */
public class ThemePref extends ListPreference {

    private final static AppTheme DEFAULT_THEME = AppTheme.SYSTEM;

    public static AppTheme getSavedTheme() {
        var value = SharedPref.INSTANCE.getString(getString(R.string.theme_key), DEFAULT_THEME.name());
        return getThemeValue(value);
    }

    public ThemePref(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnPreferenceChangeListener(this::onChange);
    }

    private boolean onChange(Preference preference, Object o) {
        AppTheme theme = getThemeValue((String)o);
        theme.activate();
        setSummary(theme.getTitle());
        return true;
    }

    @Override
    public void onAttached() {
        super.onAttached();
        setListEntries();
        setInitialValue();
        setSummary(getThemeValue().getTitle());
    }

    private void setListEntries() {
        setEntries(Stream.of(AppTheme.values())
                .map(AppTheme::getTitle)
                .toArray(String[]::new));

        setEntryValues(Stream.of(AppTheme.values())
                .map(Enum::name)
                .toArray(String[]::new));
    }

    private void setInitialValue() {
        setDefaultValue(DEFAULT_THEME);
        if(getValue() == null)
            setValue(DEFAULT_THEME);
    }

    private static AppTheme getThemeValue(String val) {
        return AppTheme.valueOf(val);
    }

    private AppTheme getThemeValue() {
        return getThemeValue(getValue());
    }

    public void setDefaultValue(AppTheme theme) {
        setDefaultValue(theme.name());
    }

    public void setValue(AppTheme value) {
        super.setValue(value.name());
    }
}
