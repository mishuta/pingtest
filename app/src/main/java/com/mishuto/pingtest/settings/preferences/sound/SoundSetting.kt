package com.mishuto.pingtest.settings.preferences.sound

import com.mishuto.pingtest.common.SharedPref
import com.mishuto.pingtest.settings.SettingsPrefManager.soundKey

enum class SoundScope { ALL, LOST }

interface SoundSettings {
    val isSoundOn: Boolean
    val scope: SoundScope
}

object SoundPrefManager : SoundSettings {
    private const val ON_BIT = 2
    private const val SCOPE_BIT = 1
    
    var persistValue: Int = SharedPref.getInt(soundKey, 0)
    
    override var isSoundOn: Boolean = persistValue and ON_BIT != 0
    override var scope: SoundScope = SoundScope.entries[persistValue and SCOPE_BIT]
    
    fun store(soundOn: Boolean, soundScope: SoundScope) {
        isSoundOn = soundOn
        scope = soundScope
        persistValue = (if (soundOn) ON_BIT else 0) + soundScope.ordinal
        
        SharedPref.putInt(soundKey, persistValue)
    }
}