package com.mishuto.pingtest.settings.preferences

import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.util.AttributeSet
import android.widget.ImageView
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.Utils.buildArticleAlert
import com.mishuto.pingtest.common.Utils.prepareSnack
import com.mishuto.pingtest.common.wrappers.PMUtils
import com.mishuto.pingtest.view.Utils.isInEditMode

/**
 * Preference settings, отображающий статус оптимизации батареи
 * Created by Michael Shishkin on 11.09.2021 (refactored 17.02.2024)
 */
class BatteryOptimizationPref(context: Context, attributeSet: AttributeSet) : CustomWidgetPref<ImageView>(context, attributeSet), OnPrefChangedListener {
    
    override fun onCreateWidget(): ImageView = createImage(getIconResource())
    
    private fun getIconResource(): Int =
        if (!isInEditMode(context) && PMUtils.isIgnoringBatteryOptimizations()) R.drawable.ic_baseline_check_circle_24
        else R.drawable.ic_error
    
    override fun refresh() {
        tag()
        if (isWidgetInitialized())
            widget.setImageResource(getIconResource())
        setSummary(if (PMUtils.isIgnoringBatteryOptimizations()) R.string.ok_enabled else R.string.not_ok_disabled)
    }
    
    override fun onClick() {
        buildArticleAlert(context, R.string.ignore_bat_optimization_title, R.string.ignore_bat_optimization)
            .setPositiveButton(R.string.open_wl_activity) { d, _ ->
                val intent = Intent().setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS)
                if (intent.resolveActivity(context.packageManager) != null) context.startActivity(intent)
                else prepareSnack(widget, R.string.opt_activity_not_resolved)
                d.dismiss()
            }
            .show()
    }
}