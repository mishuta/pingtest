package com.mishuto.pingtest.settings.widget;

import static com.mishuto.pingtest.common.Const.WidgetSettings.SERIES_INTERVAL;
import static com.mishuto.pingtest.common.Const.WidgetSettings.SERIES_LENGTH;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.TimeFormatterKt;
import com.mishuto.pingtest.controller.ui.ArticleFragment;
import com.mishuto.pingtest.settings.preferences.BatteryOptimizationPref;
import com.mishuto.pingtest.settings.preferences.host.HostSetting;
import com.mishuto.pingtest.settings.preferences.SeekBarPref;
import com.mishuto.pingtest.settings.preferences.SequenceSlider;
import com.mishuto.pingtest.widget.ExecutorManager;
import com.mishuto.pingtest.widget.WidgetPingExecutor;

/**
 * Фрагмент настроек виджета
 * В будущем добавится поле host. При создании нового виджета, пользователь может выбрать host из списка, добавлять, удалять (кроме дефолтного).
 * Created by Michael Shishkin on 28.04.2021
 */
public class SettingsFragment extends PreferenceFragmentCompat {
    SeekBarPref seriesLen;
    SeekBarPref seriesInterval;
    SwitchPreferenceCompat noPingWiFiInIdle;
    Settings mSettings;
    BatteryOptimizationPref batteryOptimizationPref;

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        View.inflate(getContext(), R.layout.wid_apply_button, (ViewGroup) root);    // создание кнопки Apply

        Button btn = root.findViewById(R.id.apply_btn);

        btn.setOnClickListener(p -> onApply());
        if(getParent().isEditMode())
            btn.setText(R.string.update_widget);

        return root;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.widget_preferences, rootKey);

        mSettings = getSetting(HostSetting.getInstance().getSelectedHost());
        seriesLen = findPreference(getString(R.string.wid_series_len_key));
        seriesInterval = findPreference(getString(R.string.wid_series_interval_key));
        noPingWiFiInIdle = findPreference(getString(R.string.wid_no_ping_wifi_in_idle_key));
        batteryOptimizationPref = findPreference(getString(R.string.ignore_bat_opt_key));
        Preference notes = findPreference(getString(R.string.wid_notes_key));

        // инициируем слайдеры
        new SequenceSlider<>(seriesLen, SERIES_LENGTH);
        new SequenceSlider<>(seriesInterval, SERIES_INTERVAL).setToTextFunc(TimeFormatterKt::msToTimeUnits);

        seriesLen.setDefaultValue(mSettings.seriesLen);
        seriesInterval.setDefaultValue(mSettings.seriesInterval);
        noPingWiFiInIdle.setDefaultValue(mSettings.notPingWiFiInIdle);

        notes.setOnPreferenceClickListener(showNotesAlert());
    }

    @Override
    public void onResume() {
        super.onResume();
        if(batteryOptimizationPref != null)
            batteryOptimizationPref.refresh();
    }

    void onApply() {
        mSettings = new Settings.Builder(mSettings.id)
                .setSeriesLen(seriesLen.getValue())
                .setSeriesInterval(seriesInterval.getValue())
                .setNoPingWiFiInIdle(noPingWiFiInIdle.isChecked())
                .setHost((ExecutorManager.getInstance().isSingleHostMode()) ? HostSetting.getInstance().getSelectedHost() : null) // потом null заменить на host из формы
                .addWidget(getParent().getAppWidgetId())
                .build();

        getParent().onApply(mSettings);
    }

    WidgetSettingsActivity getParent() {
        return (WidgetSettingsActivity)getActivity();
    }

    private Settings getSetting(String host) {
        final WidgetPingExecutor executor = ExecutorManager.getInstance().getExecutor(host);
        int id = (executor != null) ? executor.getId() : Settings.persistentIDS().createId();
        return new Settings(id);
    }

    private Preference.OnPreferenceClickListener showNotesAlert() {
        return preference -> {
            getParentFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fragment_slide_in, R.anim.fragment_fade_out, R.anim.fragment_fade_in, R.anim.fragment_slide_out)
                    .replace(R.id.settings, new ArticleFragment(R.string.important_notes_title, R.string.important_notes))
                    .addToBackStack(null)
                    .commit();
            return true;
        };
    }
}
