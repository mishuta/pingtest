package com.mishuto.pingtest.settings.preferences.host

import android.content.Context
import android.util.AttributeSet
import androidx.preference.Preference
import com.mishuto.pingtest.settings.preferences.OnPrefChangedListener
import com.mishuto.pingtest.view.Utils

/**
 * Custom Preference для списка хостов
 * Created by Michael Shishkin on 02.10.2021 (refactored 13.02.2024)
 */
class HostPref(context: Context, attributeSet: AttributeSet) : Preference(context, attributeSet), OnPrefChangedListener {
    override fun onClick() {
        HostListActivity.start(context)
    }
    
    override fun getSummary(): CharSequence = if (Utils.isInEditMode(context)) "site.com" else HostSetting.getInstance().selectedHost
    
    override fun refresh() = notifyChanged()
}