package com.mishuto.pingtest.settings.app

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.settings.preferences.OnPrefChangedListener

/**
 * Фрагмент активности настроек
 * Created by Michael Shishkin on 29.06.2020 refactored on 15.02.2024
 */
class SettingsFragment : PreferenceFragmentCompat() {
    
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        Logger.tag()
        setPreferencesFromResource(R.xml.app_preferences, rootKey)
    }
    
    override fun onResume() {
        super.onResume()
        (activity as SettingsActivity).supportActionBar?.title = getString(R.string.title_activity_settings)
        listOf(
            findPreference<Preference>(R.string.ping_host_key) as OnPrefChangedListener,
            findPreference<Preference>(R.string.ignore_bat_opt_key) as OnPrefChangedListener,
            findPreference<Preference>(R.string.sound_key) as OnPrefChangedListener,
            findPreference<Preference>(R.string.floating_window_key) as OnPrefChangedListener
        ).forEach { it.refresh() }
    }
}

fun <T : Preference> PreferenceFragmentCompat.findPreference(id: Int): T = findPreference(getString(id))!!