package com.mishuto.pingtest.settings.preferences.sound

import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import androidx.preference.Preference
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Utils.getString
import com.mishuto.pingtest.settings.preferences.OnPrefChangedListener
import com.mishuto.pingtest.settings.preferences.sound.SoundScope.ALL
import com.mishuto.pingtest.view.Utils.isInEditMode

/**
 * Настройка звука
 * Created by Michael Shishkin on 31.08.2022
 */
class SoundPref(context: Context, attrs: AttributeSet) : Preference(context, attrs), OnPrefChangedListener {
    
    override fun getSummary(): CharSequence =
        if (!isInEditMode(context)) {
            getString(
                when {
                    !SoundPrefManager.isSoundOn -> R.string.off
                    SoundPrefManager.scope == ALL -> R.string.all_pings
                    else -> R.string.lost_only
                }
            )
        }
        else "Off"
    
    override fun onClick() {
        context.startActivity(Intent(context, SoundActivity::class.java))
    }
    
    override fun refresh() = notifyChanged()
}