package com.mishuto.pingtest.settings.preferences.host;

import static com.mishuto.pingtest.common.Utils.getString;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.SharedPref;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Управление списком хостов, в SharedPreference
 * Created by Michael Shishkin on 11.10.2021
 */
public class HostSetting {
    private static final HostSetting instance = new HostSetting();
    private static final SharedPref sp = SharedPref.INSTANCE;

    private final static String HOST_SET_KEY = "Hosts";
    private final static String HOST_KEY = App.getAppContext().getString(R.string.ping_host_key);
    private Set<String> hostSet;
    private String host;

    public static HostSetting getInstance() {
        return instance;
    }

    private HostSetting() {}

    public Set<String> getHostSet() {
        if (hostSet == null) {
            hostSet = sp.getStringSet(HOST_SET_KEY, Stream.of(getDefaultHostsArr()).collect(Collectors.toSet()));
        }
        return new HashSet<>(hostSet);
    }

    public void setHostSet(Set<String> changed) {
        if (hostSet == null || !hostSet.equals(changed)) {
            hostSet = changed;
            sp.putStringSet(HOST_SET_KEY, hostSet);
        }
    }

    public void changeHost(String old, String changed) {
        Set<String> set = getHostSet();
        set.remove(old);
        set.add(changed);
        setHostSet(set);

        if (getSelectedHost().equals(old))
            setSelectedHost(changed);
    }

    public void addHost(String newHost) {
        Set<String> set = getHostSet();
        set.add(newHost);
        setHostSet(set);
    }

    public void removeHost(String removedHost) {
        Set<String> set = getHostSet();
        set.remove(removedHost);
        setHostSet(set);
        if (set.size() == 0)
            setSelectedHost(getDefaultHostsArr()[0]);
        else if (getSelectedHost().equals(removedHost))
            setSelectedHost(set.stream().findFirst().orElseThrow(IllegalStateException::new));
    }

    public String getSelectedHost() {
        if (host == null) {
            host = sp.getString(HOST_KEY, getDefaultHostsArr()[sp.getInt(getString(R.integer.ping_host_def), 0)]);
        }
        return host;
    }

    public void setSelectedHost(String host) {
        this.host = host;
        sp.putString(HOST_KEY, host);
    }

    static String[] getDefaultHostsArr() {
        return App.getAppContext().getResources().getStringArray(R.array.host_set_def);
    }
}
