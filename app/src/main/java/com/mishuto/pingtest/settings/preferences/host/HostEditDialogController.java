package com.mishuto.pingtest.settings.preferences.host;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;
import com.mishuto.pingtest.R;

import java.util.stream.Stream;

/**
 * Управление диалогом ввода хоста
 * Created by Michael Shishkin on 15.12.2021
 */
public class HostEditDialogController {

    public interface DialogListener {
        void onOk(String host);
    }

    private final AlertDialog dialog;
    private TextInputLayout inputLayout;
    private final Validator<String> validator;
    private final DialogListener dialogListener;

    public HostEditDialogController(Context context, Validator<String> validator, String currentHost, DialogListener listener) {
        this.validator = validator;
        dialogListener = listener;

        dialog = showDialog(context);
        initDialog(currentHost);
    }

    private AlertDialog showDialog(Context context) {
        return new MaterialAlertDialogBuilder(context, R.style.MaterialAlertDialog)
                .setView(R.layout.alert_host_editor)
                .setPositiveButton(android.R.string.ok, (d,w)->dialogListener.onOk(getTextFromEdit()))
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void initDialog(String initHost) {
        inputLayout = dialog.findViewById(R.id.host_name_layout);
        getEdit().setText(initHost);
        getEdit().addTextChangedListener(getWatcher());
        getEdit().setFilters(new InputFilter[]{new NoWhiteSpacesFilter()});
        setOkButtonEnabled(validator.test(getTextFromEdit()));
    }

    private void setOkButtonEnabled(boolean enabled) {
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(enabled);
    }

    private EditText getEdit() {
        return inputLayout.getEditText();
    }

    private String getTextFromEdit() {
        return getEdit().getText().toString();
    }

    private TextWatcher getWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                validator.test(s.toString());
                inputLayout.setError(validator.getReasonOfInvalidity());
                setOkButtonEnabled(validator.isValid());
            }
        };
    }

    private static class NoWhiteSpacesFilter implements InputFilter {
        Character[] WHITE_SPACES = {' ', '\n' };

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dStart, int dEnd) {
            StringBuilder buf = new StringBuilder();
            source.chars()
                    .skip(start)
                    .limit(end - start)
                    .filter(c-> Stream.of(WHITE_SPACES).noneMatch(ws->c==ws))
                    .forEach(c-> buf.append((char)c));

            return buf.length() == end-start ? null : buf; // если изменений не было - фильтрация не требуется, возвращаем null
        }
    }
}
