package com.mishuto.pingtest.settings.widget;

import static com.mishuto.pingtest.common.Const.SEC;
import static com.mishuto.pingtest.common.Const.WidgetSettings.SERIES_INTERVAL;
import static com.mishuto.pingtest.common.Const.WidgetSettings.SERIES_LENGTH;

import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.Logger;
import com.mishuto.pingtest.common.SharedPref;
import com.mishuto.pingtest.settings.preferences.host.HostSetting;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Класс для хранения параметров WidgetPingExecutor, в т.ч. в SharedPref
 * Created by Michael Shishkin on 21.02.2021
 */
final public class Settings {
    final private static int PING_INTERVAL = SEC;
    final private static int PING_TIMEOUT = 2*SEC;

    final public String host;               // хост пинга
    final public int timeout;               // таймаут пинга
    final public int interval;              // интервал между пингами
    final public int seriesInterval;        // интервал между сериями
    final public int seriesLen;             // количество пингов в серии
    final public int id;                    // id экзекьютора в shared preference
    final public boolean notPingWiFiInIdle; // не пинговать в простое при включенном вайфае
    final public Set<Integer> widgetIDs;    // id виджетов, привязанных к экзекьютору

    final private static PersistentIDS sIDS = new PersistentIDS();

    final static private String WIDGETS_KEY = "Widgets_";
    final PreferenceHelper mPref;

    // при  создании объекта, он грузит себя из SharedPref
    public Settings(int id) {
        this.id = id;
        mPref = new PreferenceHelper(id);
        timeout = PING_TIMEOUT;
        interval = PING_INTERVAL;
        host = mPref.getString(R.string.wid_host_key, HostSetting.getInstance().getSelectedHost());
        seriesInterval = SERIES_INTERVAL[mPref.getInt(R.string.wid_series_interval_key, 0)];
        seriesLen = SERIES_LENGTH[mPref.getInt(R.string.wid_series_len_key, 0)];
        notPingWiFiInIdle = mPref.getBoolean(R.string.wid_no_ping_wifi_in_idle_key, false);
        widgetIDs = toIntSet(mPref.getStringSet(WIDGETS_KEY));
    }

    // удаление виджета из параметров экзекьютора
    public void removeWidget(int widgetId) {
        widgetIDs.remove(widgetId);
        mPref.putStringSet(WIDGETS_KEY, toStringSet(widgetIDs));
        mPref.apply();
    }

    @NonNull
    @Override
    public String toString() {
        return "[host: " + host +
                "; timeout: " + timeout +
                "; interval: " + interval +
                "; series interval: " + seriesInterval +
                "; series len: " + seriesLen + "]";
    }

    public static PersistentIDS persistentIDS() {
        return sIDS;
    }

    // удаление настроек экзекьютера
    private void removePrefs() {
        mPref.remove(R.string.wid_series_interval_key);
        mPref.remove(R.string.wid_series_len_key);
        mPref.remove(R.string.wid_no_ping_wifi_in_idle_key);
        mPref.remove(R.string.wid_host_key);
        mPref.remove(WIDGETS_KEY);
        mPref.apply();
    }

    public static Set<Integer> toIntSet(Set<String> stringSet) {
        return stringSet.stream().map(Integer::parseInt).collect(Collectors.toSet());
    }

    public static Set<String> toStringSet(Set<Integer> intSet) {
        return intSet.stream().map(String::valueOf).collect(Collectors.toSet());
    }

    /**
     *  класс для создания/изменения settings из SettingsFragment
     */
    public static class Builder {
        final PreferenceHelper mPref;

        public Builder(int id) {
            if (!sIDS.getIDS().contains(id))
                throw new IllegalArgumentException("executor id not found");
            mPref = new PreferenceHelper(id);
        }

        public Builder setSeriesInterval(int interval) {
            mPref.putInteger(R.string.wid_series_interval_key, interval);
            return this;
        }

        public Builder setSeriesLen(int len) {
            mPref.putInteger(R.string.wid_series_len_key, len);
            return this;
        }

        public Builder setNoPingWiFiInIdle(boolean f) {
            mPref.putBoolean(R.string.wid_no_ping_wifi_in_idle_key, f);
            return this;
        }

        public Builder setHost(String host) {
            mPref.putString(R.string.wid_host_key, host);
            return this;
        }

        public Builder addWidget(int id) {
            Set<String> widgets = mPref.getStringSet(WIDGETS_KEY);
            widgets.add(String.valueOf(id));
            mPref.putStringSet(WIDGETS_KEY, widgets);
            return this;
        }

        public Settings build() {
            mPref.apply();
            return new Settings(mPref.mId);
        }
    }

    /**
     * список id экзекьютеров - отдельная сущность в SharedPref
     */
    public static class PersistentIDS {
        final private static String IDS_KEY = "executors";
        private Set<Integer> mIDS;

        private PersistentIDS() {
        }

        public Set<Integer> getIDS() {
            if(mIDS == null)
                mIDS = toIntSet(SharedPref.INSTANCE.getStringSet(IDS_KEY, new HashSet<>()));
            return mIDS;
        }

        public int createId() {
            int id = getIDS().stream().mapToInt(Integer::intValue).max().orElse(0) + 1;
            mIDS.add(id);
            saveIds();
            return id;
        }

        public void remove(Settings settings) {
            settings.removePrefs();
            getIDS().remove(settings.id);
            saveIds();

        }

        private void saveIds() {
            Set<String> ids = toStringSet(getIDS());
            SharedPref.INSTANCE.putStringSet(IDS_KEY, ids);
            Logger.INSTANCE.d("PersistentSetting::ids updated: [%s]", String.join(",", ids));
        }
    }

    /**
     * Сохраняет и запрашивает префы под конкретным id
     */
    @SuppressWarnings("SameParameterValue")
    private static class PreferenceHelper {
        final private int mId;
        final SharedPreferences.Editor mEditor = SharedPref.INSTANCE.edit();

        public PreferenceHelper(int id) {
            mId = id;
        }

        String makeKey(int stringRes) {
            return App.getAppContext().getString(stringRes) + mId;
        }

        String makeKey(String id) {
            return id + mId;
        }

        Boolean getBoolean(int strId, boolean def) {
            return SharedPref.INSTANCE.getBoolean(makeKey(strId), def);
        }

        void putBoolean(int strId, Boolean val) {
            mEditor.putBoolean(makeKey(strId), val);
        }

        Integer getInt(int strId, int def) {
            return SharedPref.INSTANCE.getInt(makeKey(strId), def);
        }

        void putInteger(int strId, Integer val) {
            mEditor.putInt(makeKey(strId), val);
        }

        String getString(int strId, String def) {
            return SharedPref.INSTANCE.getString(makeKey(strId), def);
        }

        void putString(int strId, String val) {
            mEditor.putString(makeKey(strId), val);
        }

        Set<String> getStringSet(String id) {
            // возвращаемые SharedPreference значения должны быть immutable
            return new HashSet<>(SharedPref.INSTANCE.getStringSet(makeKey(id), new HashSet<>()));
        }

        void putStringSet(String id, Set<String> val) {
            mEditor.putStringSet(makeKey(id), val);
        }

        void remove(int strId) {
            mEditor.remove(makeKey(strId));
        }

        void remove(String id) {
            mEditor.remove(makeKey(id));
        }

        void apply() {
            mEditor.apply();
        }
    }
}
