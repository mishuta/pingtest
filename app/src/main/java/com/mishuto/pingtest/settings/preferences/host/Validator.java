package com.mishuto.pingtest.settings.preferences.host;

import java.util.function.Predicate;

public interface Validator<T> extends Predicate<T> {
    String getReasonOfInvalidity();
    boolean isValid();
}
