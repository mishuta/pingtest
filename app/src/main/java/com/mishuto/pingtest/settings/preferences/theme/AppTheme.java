package com.mishuto.pingtest.settings.preferences.theme;

import static androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY;
import static androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM;
import static androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_NO;
import static androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES;
import static com.mishuto.pingtest.common.Utils.getString;

import android.content.res.Configuration;
import android.os.Build;

import androidx.appcompat.app.AppCompatDelegate;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.R;

import java.util.function.IntSupplier;

/**
 * Установка темы
 * Created by Michael Shishkin on 01.11.2021
 */
public enum AppTheme {
    LIGHT(R.string.light, () -> MODE_NIGHT_NO),
    DARK(R.string.dark, () -> MODE_NIGHT_YES),
    SYSTEM(R.string.system, () ->
            (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) ? MODE_NIGHT_FOLLOW_SYSTEM : MODE_NIGHT_AUTO_BATTERY) ;

    public static AppTheme getCurrentTheme() {
        return ThemePref.getSavedTheme();
    }

    private final int titleId;
    private final IntSupplier mode;

    AppTheme(int titleId, IntSupplier mode) {
        this.titleId = titleId;
        this.mode = mode;
    }

    public void activate() {
        AppCompatDelegate.setDefaultNightMode(mode.getAsInt());
    }

    public String getTitle() {
        return getString(titleId);
    }

    public static boolean isDarkThemeSet() {
        return getCurrentTheme() == DARK ||
                getCurrentTheme() == SYSTEM &&
                        (App.getAppContext().getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES;
    }
}
