package com.mishuto.pingtest.settings.preferences

/**
 * Интерфейс для настроек, которые открывают свои активности и требуют обновления summary на основном фрагменте Settings после закрытия дочерней активности
 * Created by Michael Shishkin on 28.08.2022
 */
interface OnPrefChangedListener {
    fun refresh()
}