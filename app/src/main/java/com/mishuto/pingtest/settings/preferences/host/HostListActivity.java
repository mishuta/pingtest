package com.mishuto.pingtest.settings.preferences.host;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

import static com.mishuto.pingtest.view.Utils.ANIMATE_DURATION;
import static com.mishuto.pingtest.view.Utils.changeAnimatedBackground;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.databinding.HostsActivityBinding;
import com.mishuto.pingtest.model.HostNameValidator;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Активность для редактирования списка хостов
 */
//todo требует рефакторинга: 1. перевод списка хостов в бд, 2. более чистая реализация (вынос логики из) адаптера и холдера (см. ResultActivity)

public class HostListActivity extends AppCompatActivity {

    HostsActivityBinding binding;
    final HostSetting hostSetPref = HostSetting.getInstance();
    ActionHandler actionHandler = new ActionHandler();
    RecyclerAdapter recyclerAdapter;
    List<String> hostList;

    public static void start(Context context) {
        context.startActivity(new Intent(context, HostListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = HostsActivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.hostsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        hostList = hostSetPref.getHostSet().stream().sorted().collect(Collectors.toList());
        recyclerAdapter = new RecyclerAdapter();
        binding.hostsRecyclerView.setAdapter(recyclerAdapter);
        binding.fab.setOnClickListener(v-> actionHandler.onAdd());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_baseline_close_24);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // обработка кнопки Up
        if (item.getItemId() == android.R.id.home) {
            getOnBackPressedDispatcher().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /** @noinspection unused*/
    interface Events {
        void onClick(String host);
        void onEdit(String host, int index);
        void onDelete(int index);
        void onAdd();
    }

    private class ActionHandler implements Events {

        @Override
        public void onClick(String host) {
            setHostSelected(host);
            //noinspection DataFlowIssue
            new Handler(Looper.myLooper()).postDelayed(HostListActivity.this::finish, ANIMATE_DURATION); // завершаем активность после паузы
        }

        @Override
        public void onEdit(String oldHost, int index) {
            createDialog(oldHost, newHost -> {
                hostList.set(index, newHost);
                hostSetPref.changeHost(oldHost, newHost);
                recyclerAdapter.notifyItemChanged(index);
                setHostSelected(newHost);
            });
        }

        @Override
        public void onDelete(int index) {
            hostSetPref.removeHost(hostList.get(index));
            hostList.remove(index);
            recyclerAdapter.notifyItemRemoved(index);
            recyclerAdapter.notifyItemChanged(getSelectedPosition());       // если удалился выбранный хост, надо обновить selectedItem
        }

        @Override
        public void onAdd() {
            createDialog(null, host -> {
                hostSetPref.addHost(host);
                hostList.add(host);
                recyclerAdapter.notifyItemInserted(hostList.size() - 1);
                binding.hostsRecyclerView.smoothScrollToPosition(hostList.size() - 1);
                setHostSelected(host);                                      // новый станет выбранным
            });
        }

        private void createDialog(String oldHost, HostEditDialogController.DialogListener listener) {
            Validator<String> validator = new ValidatorImpl(oldHost);
            new HostEditDialogController(HostListActivity.this, validator, oldHost, newHost -> {
                if(validator.test(newHost))
                    listener.onOk(newHost);
            });
        }

        private void setHostSelected(String host) {
            int prevSelected = getSelectedPosition();
            hostSetPref.setSelectedHost(host);
            recyclerAdapter.notifyItemChanged(prevSelected);
            recyclerAdapter.notifyItemChanged(getSelectedPosition());
        }

        private int getSelectedPosition() {
            return hostList.lastIndexOf(hostSetPref.getSelectedHost());
        }
    }

    private class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_host, parent, false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            class MenuHandler {
                private void onCreateMenu() {
                    final PopupMenu menu = new PopupMenu(holder.menu.getContext(), holder.menu);
                    menu.inflate(R.menu.edit_remove_menu);
                    menu.setOnMenuItemClickListener(this::onAction);
                    menu.show();
                }

                private boolean onAction(MenuItem item) {
                    int index = holder.getAdapterPosition();
                    if(index == NO_POSITION)
                        return false;

                    if(item.getItemId() == R.id.edit)
                        actionHandler.onEdit(holder.getHost(), index);

                    if(item.getItemId() == R.id.delete)
                        actionHandler.onDelete(index);

                    return true;
                }
            }

            String host = hostList.get(position);
            holder.hostView.setText(host);
            holder.root.setOnClickListener(v-> actionHandler.onClick(holder.getHost()));
            holder.setSelected(hostSetPref.getSelectedHost().equals(host));

            holder.menu.setOnClickListener(v-> new MenuHandler().onCreateMenu());
        }

        @Override
        public int getItemCount() {
            return hostList.size();
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView hostView;
        final View menu;
        final View root;
        private boolean selected = false;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            root = itemView;
            hostView = itemView.findViewById(R.id.host_name);
            menu = itemView.findViewById(R.id.menu_layout);
        }

        String getHost() {
            return hostView.getText().toString();
        }

        public void setSelected(boolean isSelected) {
            if(isSelected != selected) {
                selected = isSelected;
                changeAnimatedBackground(root, selected);
            }
        }

    }

    private class ValidatorImpl implements Validator<String> {
        final HostNameValidator hostNameValidator = new HostNameValidator();
        final String originalHost;
        String reason;

        public ValidatorImpl(String originalHost) {
            this.originalHost = originalHost;
        }

        @Override
        public String getReasonOfInvalidity() {
            return reason;
        }

        @Override
        public boolean isValid() {
            return reason == null;
        }

        @Override
        public boolean test(String checked) {
            reason = null;

            if(checked == null || checked.trim().isEmpty() )
                reason = getString(R.string.required);

            else if(!hostNameValidator.test(checked))
                reason = getString(R.string.invalid_format);

            else if(!checked.equalsIgnoreCase(originalHost) &&
                hostList.stream().anyMatch(h->h.equalsIgnoreCase(checked))
            )
                reason = getString(R.string.already_exists);

            return isValid();
        }
    }
}