package com.mishuto.pingtest.settings.preferences;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceViewHolder;
import androidx.preference.SeekBarPreference;

import java.util.function.Consumer;

/**
 * Врапер для SeekBarPreference. Добавляет лисенер на событие onBindView
 * Created by Michael Shishkin on 26.06.2020
 */
public class SeekBarPref extends SeekBarPreference {
    private Consumer<PreferenceViewHolder> mOnBindViewListener;

    public SeekBarPref(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setOnBindViewListener(Consumer<PreferenceViewHolder> onBindViewListener) {
        this.mOnBindViewListener = onBindViewListener;
    }

    @Override
    public void onBindViewHolder(@NonNull PreferenceViewHolder view) {
        super.onBindViewHolder(view);
        if (mOnBindViewListener != null)
            mOnBindViewListener.accept(view);
    }
}
