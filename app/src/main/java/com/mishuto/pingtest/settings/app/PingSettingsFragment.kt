package com.mishuto.pingtest.settings.app

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Const.APPSettings.MOVING_PINGS_NUMBER
import com.mishuto.pingtest.common.Const.APPSettings.PING_INTERVAL
import com.mishuto.pingtest.common.Const.APPSettings.PING_TIMEOUT
import com.mishuto.pingtest.common.Const.APPSettings.TEST_DURATION
import com.mishuto.pingtest.common.Const.INFINITY
import com.mishuto.pingtest.common.msToTimeUnits
import com.mishuto.pingtest.settings.preferences.SequenceSlider

/**
 * Фрагмент настроек теста пинга
 * Created by Michael Shishkin on 14.02.2024
 */
class PingSettingsFragment : PreferenceFragmentCompat() {
    
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.ping_preferences, rootKey)
        (activity as SettingsActivity).supportActionBar?.title = getString(R.string.ping_test_settings)
        
        SequenceSlider(findPreference(R.string.ping_interval_key), PING_INTERVAL).setToTextFunc { msToTimeUnits(it) }
        SequenceSlider(findPreference(R.string.ping_timeout_key), PING_TIMEOUT).setToTextFunc { msToTimeUnits(it) }
        SequenceSlider(findPreference(R.string.ping_test_duration_key), TEST_DURATION).setToTextFunc { msToTimeUnits(it) }
        SequenceSlider(findPreference(R.string.ping_moving_number_key), MOVING_PINGS_NUMBER)
            .setToTextFunc { if (it == INFINITY) getString(R.string.all) else it.toString() }
    }
}