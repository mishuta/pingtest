package com.mishuto.pingtest.controller.services

import android.annotation.SuppressLint
import android.app.Activity.RESULT_CANCELED
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts.StartIntentSenderForResult
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.appupdate.AppUpdateOptions
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType.FLEXIBLE
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.InstallStatus.*
import com.google.android.play.core.install.model.UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
import com.google.android.play.core.install.model.UpdateAvailability.UPDATE_AVAILABLE
import com.google.android.play.core.ktx.clientVersionStalenessDays
import com.google.android.play.core.ktx.installStatus
import com.mishuto.pingtest.App.Companion.appContext
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Const.HOUR
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.e
import com.mishuto.pingtest.common.Logger.i
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.Resources.getString
import com.mishuto.pingtest.common.SharedPref
import com.mishuto.pingtest.common.SharedPref.putLong
import com.mishuto.pingtest.common.Utils.prepareSnack
import com.mishuto.pingtest.common.asDurationFormat
import com.mishuto.pingtest.common.pref.Version
import java.time.Duration
import java.time.Instant

/**
 * Управление апдейтом приложения.
 * Если пользователь отклонил запрос на обновление, ждем MILLI_BEFORE_NEXT_REQUEST часов до следующего запроса.
 * Если не отклонял, то приглашение появится при первом обнаружении обновления.
 * Если пользователь не обновлялся больше чем DAYS_FOR_IMMEDIATE_UPDATE дней назад, то обновляем принудительно (IMMEDIATE) иначе предлагаем возможность обновиться (FLEXIBLE)
 *
 * Кейсы для ручного тестирования см. в конце фалйа
 * Originally created by Michael Shishkin on 29.09.2020, refactored on Kotlin on 13.01.2024
 */

object AppUpdater {
    private const val LAST_REQUEST_KEY: String = "LastRequestForUpdate"
    private const val MILLI_BEFORE_NEXT_REQUEST = 12 * HOUR
    internal const val DAYS_FOR_IMMEDIATE_UPDATE = 30

    private val appUpdateManager
        get() = AppUpdateManagerFactory.create(appContext)

    private var lastDeclinedRequest     // по умолчанию - 1.1.1970
        get() = Instant.ofEpochMilli(SharedPref.getLong(LAST_REQUEST_KEY, 0))
        set(value) { putLong(LAST_REQUEST_KEY, value.toEpochMilli()) }

    private val milliSinceLastDeclinedRequest
        get() = Duration.between(lastDeclinedRequest, Instant.now()).toMillis()

    private var isFlexibleFlowInProgress = false

    lateinit var activityResultLauncher: ActivityResultLauncher<IntentSenderRequest>

    // вызывается из Activity.onCreate
    fun init(activity: AppCompatActivity) {
        tag()
        activityResultLauncher = activity.registerForActivityResult(StartIntentSenderForResult()) { onSystemConfirmationDialogDismiss(it) }
    }

    fun checkForUpdate(activity: AppCompatActivity) {
        tag()
        appUpdateManager.appUpdateInfo.addOnSuccessListener {// апдейт проверяется при каждом старте
            if (!activity.isFinishing)                  // колбэк может прийти после закрытия активности
                UpdateSession(activity, it).tryUpdate()
        }
        appUpdateManager.appUpdateInfo.addOnFailureListener { error -> e(error.toString()) }
    }

    private fun onSystemConfirmationDialogDismiss(activityResult: ActivityResult) {
        if (activityResult.resultCode == RESULT_CANCELED) {
            d("user declined update confirmation ")
            lastDeclinedRequest = Instant.now()
            isFlexibleFlowInProgress = false
        }
    }

    private class UpdateSession(private val activity: AppCompatActivity, private val appUpdateInfo: AppUpdateInfo) {

        fun tryUpdate() {
            with(appUpdateInfo) {
                d("availability: ${updateAvailability()}. status: ${installStatus()}. flexible in progress: $isFlexibleFlowInProgress")

                when (updateAvailability()) {
                    UPDATE_AVAILABLE -> {
                        val daysSinceLastRelease = clientVersionStalenessDays ?: -1
                        i("Update ${Version.CURRENT} -> ${availableVersionCode()} is available.")
                        d("$daysSinceLastRelease days since last release. ${milliSinceLastDeclinedRequest.asDurationFormat()} since last declined request.")

                        if (daysSinceLastRelease >= DAYS_FOR_IMMEDIATE_UPDATE && isUpdateTypeAllowed(IMMEDIATE))
                            startImmediateFlow()
                        else if (milliSinceLastDeclinedRequest >= MILLI_BEFORE_NEXT_REQUEST && isUpdateTypeAllowed(FLEXIBLE))
                            startFlexibleFlow()
                        else
                            i("update is deferred")
                    }

                    DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS ->   // активность или приложение было перезапущено во время апдейта
                        if (isFlexibleFlowInProgress)
                            startFlexibleFlow() // если была пересоздана активность, породившая FlexibleFlow, то Flow возобновляется
                        else
                            startImmediateFlow()

                    else -> d("update is not available")
                }
            }
        }

        private fun startFlexibleFlow() {
            tag()
            if (appUpdateInfo.installStatus().isFinal()) {               // если процесс перезапущен (вернулись в активность) уже после завершения загрузки или ошибки
                showFinishUpdateSnack(appUpdateInfo.installStatus())     // сразу показываем снек и уходим
                isFlexibleFlowInProgress = false
                return
            }

            if (!isFlexibleFlowInProgress) {
                isFlexibleFlowInProgress = true
                startUpdateFlow(FLEXIBLE)
            }

            val changeStateHandler = object : InstallStateUpdatedListener {    // мониторинг статуса FLEXIBLE update
                @SuppressLint("SwitchIntDef")
                override fun onStateUpdate(state: InstallState) {
                    d("state: ${state.installStatus()} errCode: ${state.installErrorCode()}")
                    if (state.installStatus.isFinal()) {
                        appUpdateManager.unregisterListener(this)
                        showFinishUpdateSnack(state.installStatus())
                        isFlexibleFlowInProgress = false
                    }
                }
            }
            appUpdateManager.registerListener(changeStateHandler)

            // отсоединяем лисенер при стопе активности, иначе он сработает при пересоздании активности с некорректной ссылкой на активность
            activity.lifecycle.addObserver(
                object : DefaultLifecycleObserver {
                    override fun onStop(owner: LifecycleOwner) {
                        appUpdateManager.unregisterListener(changeStateHandler)
                    }
                })
        }

        private fun Int.isFinal() = this in listOf(DOWNLOADED, INSTALLED, FAILED, CANCELED)

        private fun startImmediateFlow() {
            tag()
            startUpdateFlow(IMMEDIATE)
        }

        private fun startUpdateFlow(type: Int) {
            appUpdateManager.startUpdateFlowForResult(
                appUpdateInfo,
                activityResultLauncher,
                AppUpdateOptions.newBuilder(type).build()
            )
        }

        private fun showFinishUpdateSnack(state: Int) {
            d("state: $state")
            val messages = mapOf<Int, String>(
                DOWNLOADED to getString(R.string.update_downloaded),
                INSTALLED to getString(R.string.update_installed),
                CANCELED to getString(R.string.update_installed),
                FAILED to getString(R.string.update_failed),
            )

            val snack = prepareSnack(activity.findViewById(R.id.main_fragment), messages[state]!!)

            if (state == DOWNLOADED)
                snack.setDuration(BaseTransientBottomBar.LENGTH_INDEFINITE)
                    .setAction(R.string.restart) { appUpdateManager.completeUpdate() }

            snack.show()
        }
    }
}

/**
 * Кейсы для ручного тестирования
 *   Good path: запустил, появилось предложение. Принял. Подождал. Появился снек. Обновил.
 *   пользователь не стал подтверждать/отклонять загрузку, а переоткрыл приложение -> После открытия активности предложение опять появилось
 *   появилось предложение загрузки, пользователь сменил ориентацию экрана, подтвердил загрузку -> Загрузка выполнилась, снек появился.
 *   пользователь подтвердил загрузку:
 *      пользователь согласился на загрузку, но не стал активировать установку и закрыл аппу. -> Установка должна произойти при старте аппы
 *      скрыть активность, например settings активностью до конца загрузки -> После открытия активности снек появился.
 *      кратковременно скрыть активность, например settings активностью во время загрузки -> После открытия активности загрузка продолжится, снек появляется.
 *      перевести активность в бэкграунд -> После открытия активности загрзука завершается, снек появляется.
 *      запустить тест, сервис работает в бэке, закрыть активность -> После открытия активности версия установлена/устанавливается без запросов
 */