package com.mishuto.pingtest.controller.services

import android.content.Intent
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import com.mishuto.pingtest.common.CrashlyticsKeys.setKey
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.common.dispatchers.IntentDispatcher

/**
 * Фиксирует события уровня процесса.
 * Created by Michael Shishkin on 11.01.2022, refactored on Kotlin 26.06.24
 */
class ProcessEventsObserver : DefaultLifecycleObserver {
    private var isInForeground = false
    override fun onStart(owner: LifecycleOwner) {
        EventDispatcher.post(APP_IN_FOREGROUND, null)
        isInForeground = true
        setKey(APP_FOREGROUND, true)
    }
    
    override fun onStop(owner: LifecycleOwner) {
        EventDispatcher.post(APP_IN_BACKGROUND, null)
        isInForeground = false
        setKey(APP_FOREGROUND, false)
    }
    
    companion object {
        const val APP_IN_FOREGROUND = "ProcessEventsObserver.APP_IN_FOREGROUND"
        const val APP_IN_BACKGROUND = "ProcessEventsObserver.APP_IN_BACKGROUND"
        private const val APP_SCREEN = "app_screen"
        const val APP_FOREGROUND = "app_foreground"
        
        private val processEventsObserver = ProcessEventsObserver()
        fun registerObserver() {
            ProcessLifecycleOwner.get().lifecycle.addObserver(processEventsObserver)
            
            IntentDispatcher.register(Intent.ACTION_SCREEN_ON, "ProcessEventsObserver") { setKey(APP_SCREEN, "on") }
            IntentDispatcher.register(Intent.ACTION_SCREEN_OFF, "ProcessEventsObserver") { setKey(APP_SCREEN, "off") }
        }
        
        val isInForeground: Boolean
            get() = processEventsObserver.isInForeground
    }
}
