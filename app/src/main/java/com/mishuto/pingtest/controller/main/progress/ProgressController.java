package com.mishuto.pingtest.controller.main.progress;

import static com.mishuto.pingtest.common.Utils.percent;
import static com.mishuto.pingtest.view.SegmentedProgressBarKt.PROGRESS_FULL_ANIMATION_TIME;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.mishuto.pingtest.model.indicators.GroupIndicators;
import com.mishuto.pingtest.model.indicators.Indicator;
import com.mishuto.pingtest.view.SegmentedProgressBar;

/**
 * Блок индикатора, состоящий из подписи, иконки, прогрессбара и значения
 * Created by Michael Shishkin on 18.07.2020
 */
public abstract class ProgressController {
    final protected TextView mValue;
    final protected SegmentedProgressBar mProgressBar;
    final protected ImageView mIco;
    final protected GroupIndicators mGroupIndicator;

    public ProgressController(TextView value, SegmentedProgressBar progressBar, ImageView ico, GroupIndicators indicator) {
        mValue = value;
        mProgressBar = progressBar;
        mIco = ico;
        mGroupIndicator = indicator;
    }

    // Установка значения и отрисовка индикатора
    public void setController(GroupIndicators.GroupValue groupValue) {
        mGroupIndicator.setGroupValue(groupValue);
        Indicator indicator = mGroupIndicator.getTotalIndicator();

        mProgressBar.setProgress(percent(indicator.computeIndicatorValue()));
        updateIndicatorValue(indicator);
    }

    abstract void updateIndicatorValue(Indicator indicator);

    public void resetProgressBar() {
        mValue.setText(null);
        mProgressBar.setProgress(0);
    }

    // имя индикатора для сохранения в бандле
    @NonNull
    @Override
    public String toString() {
        return mGroupIndicator.getGroupThresholds().name();
    }

    public GroupIndicators.GroupValue getGroupValue() {
        return mGroupIndicator.getGroupValue();
    }

    // пробегание прогрессбара от 0 до 100% и обратно
    public void splash() {
        mProgressBar.setProgress(mProgressBar.getMaxProgress());
        mProgressBar.postDelayed(() -> mProgressBar.setProgress(0), PROGRESS_FULL_ANIMATION_TIME);
    }
}
