package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.controller.features.premium.alternative.fragments.base.AbstractServerRequestFragment
import com.mishuto.pingtest.controller.features.premium.billing.Billing.license

class ActivateRequestFragment : AbstractServerRequestFragment() {
    override val errorMessageResId: Int = R.string.error_activation_request
    
    override suspend fun callRestApi() {
        Logger.d("requesting activation...")
        license.requestActivation()
        doNextState()
    }
}