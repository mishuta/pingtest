package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.controller.features.premium.alternative.fragments.base.AbstractToolbarFragment
import com.mishuto.pingtest.databinding.FragmentWithMessageBinding

/**
 * Результат деактивации лицензии
 * Created by Michael Shishkin on 21.06.2024
 */
class InactivatedFragment: AbstractToolbarFragment(R.layout.fragment_with_message) {
    override val menuLayout = R.menu.activate_menu
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentWithMessageBinding.inflate(inflater, container, false)
        binding.title.text = getString(R.string.license_deactivation_title)
        binding.message.text = getString(R.string.license_deactivation_message)
        setupToolbarMenu(true)
        return binding.root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.activate) {
            Logger.d("removing activation")
            billingStateViewModel.nextState()
            setupToolbarMenu(false)
            true
        }
        else
            super.onOptionsItemSelected(item)
    }
}