package com.mishuto.pingtest.controller.features.results.list

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.controller.features.export.Saver
import com.mishuto.pingtest.controller.features.results.ResultsListViewModel
import com.mishuto.pingtest.controller.features.export.Sharer
import com.mishuto.pingtest.data.ResultEntity
import kotlinx.coroutines.launch
import java.util.TreeSet

//реализация операций над выбранной группой результатов

const val SELECTION_CHANGED = "SelectedGroup: Item selection changed"

class SelectedGroup(
    private val selectedGroupModel: SelectedGroupModel,
    private val fragment: Fragment,
    private val resultsListData: ResultsListData,
    private val activityViewModel: ResultsListViewModel
) {

    fun onMenuItemSelected(itemId: Int) : Boolean {
        when(itemId) {
            android.R.id.home,
            R.id.action_discard_all -> clearSelectedGroup()
            R.id.action_check_all -> selectAll()
            R.id.action_share -> share()
            R.id.action_save -> save()
            R.id.action_delete -> onDeleteGroup()
            else -> return false
        }
        return true
    }

    private fun clearSelectedGroup() {
        d("Clear selections")
        selectedGroupModel.clear()
    }

    private fun selectAll() {
        d("Select all items")
        selectedGroupModel.addAll(resultsListData.resultsList)
    }

    private fun share() {
        d("Launch share results")
        fragment.viewLifecycleOwner.lifecycleScope.launch {
            Sharer(fragment.requireActivity() as AppCompatActivity, selectedGroupModel.items.toList()).processFile()
        }
    }

    private fun save() {
        d("Launch save results")
        fragment.viewLifecycleOwner.lifecycleScope.launch {
            Saver(fragment.requireActivity() as AppCompatActivity, selectedGroupModel.items.toList()).processFile()
        }
    }

    private fun onDeleteGroup()  {
        d("Delete selections")
        activityViewModel.deletedEntities.markAsDeleted(selectedGroupModel.items)
        selectedGroupModel.clear()
    }
}

class SelectedGroupModel : ViewModel() {
    private val mutableItems: MutableSet<ResultEntity> = TreeSet()
    val items: Set<ResultEntity>
        get() = mutableItems

    fun isItemSelected(entity: ResultEntity) = items.contains(entity)
    fun isGroupSelected() = items.isNotEmpty()
    fun changeSelection(entity: ResultEntity) = if (isItemSelected(entity)) remove(entity) else add(entity)
    fun count() = items.size

    fun add(entity: ResultEntity) = updateGroupState(setOf(entity)) { mutableItems.add(entity) }
    fun remove(entity: ResultEntity) = updateGroupState(setOf(entity)) { mutableItems.remove(entity) }
    fun addAll(entities: Collection<ResultEntity>) = updateGroupState(entities) { mutableItems.addAll(entities) }
    fun clear() = updateGroupState(mutableItems.toSet()) { mutableItems.clear() }

    private fun updateGroupState(changedEntities: Collection<ResultEntity>, action: ()->Unit) {
        action.invoke()
        EventDispatcher.post(SELECTION_CHANGED, changedEntities)
    }
}
