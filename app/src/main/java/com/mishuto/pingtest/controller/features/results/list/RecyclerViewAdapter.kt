package com.mishuto.pingtest.controller.features.results.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Utils
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.common.shortDateFormat
import com.mishuto.pingtest.common.shortTimeFormat
import com.mishuto.pingtest.controller.features.results.ResultsListViewModel
import com.mishuto.pingtest.data.ResultEntity
import com.mishuto.pingtest.databinding.ResultListItemBinding
import com.mishuto.pingtest.model.stat.PingStatistic.Companion.overallQuality
import com.mishuto.pingtest.view.Utils.changeAnimatedBackground

class RecyclerViewAdapter(
    private val resultsListData: ResultsListData,
    private val selectedGroup: SelectedGroupModel,
    private val activityViewModel: ResultsListViewModel,
    fragment: Fragment
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    
    init {
        EventDispatcher.bind<Collection<ResultEntity>>(SELECTION_CHANGED, "ResultList:Adapter", fragment.lifecycle) { onSelectionChanged(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ResultListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(resultsListData.getItem(position))
    }

    override fun getItemCount(): Int {
        return resultsListData.count()
    }

    private fun onSelectionChanged(entities: Collection<ResultEntity>) = entities
        .filter { resultsListData.contains(it) }    // не является удаленным
        .forEach{ notifyItemChanged(resultsListData.getPosition(it)) }

    inner class ViewHolder(private val item: ResultListItemBinding) : RecyclerView.ViewHolder(item.root) {
        private var selected = false
        lateinit var entity: ResultEntity

        fun onBind(entity: ResultEntity) {
            this.entity = entity
            fillDataFromEntity()
            drawSelected(entity)

            item.root.setOnClickListener {
                if (selectedGroup.isGroupSelected())
                    changeSelection()
                else
                    activityViewModel.selectedItem.value = entity
            }
            item.root.setOnLongClickListener { changeSelection(); true}
        }

        private fun fillDataFromEntity() {
            item.netType.setImageResource(entity.mainNetType.imageId)
            item.dateVal.text = entity.startTime.shortDateFormat()
            item.timeVal.text = entity.startTime.shortTimeFormat()
            item.host.text = entity.host
            item.netQualityValue.text = Utils.getString(R.string.percent, entity.statistics.overallQuality())
        }

        private fun changeSelection() {
            d("select position $adapterPosition")
            selectedGroup.changeSelection(entity)
        }

        private fun drawSelected(entity: ResultEntity) {
            val isSelected = selectedGroup.isItemSelected(entity)
            if (isSelected != selected) {
                changeAnimatedBackground(item.root, isSelected)
                selected = isSelected
            }
        }
    }
}

class ResultsDiffUtil(private val oldList: List<ResultEntity>, val newList: List<ResultEntity>): DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldList.size
    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].testId == newList[newItemPosition].testId

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition] == newList[newItemPosition]
}