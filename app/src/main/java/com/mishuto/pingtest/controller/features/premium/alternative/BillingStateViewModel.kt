package com.mishuto.pingtest.controller.features.premium.alternative

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mishuto.pingtest.controller.features.premium.alternative.AltBillingState.*
import com.mishuto.pingtest.controller.features.premium.billing.Billing
import com.mishuto.pingtest.controller.features.premium.billing.alternative.License.LicenseStatus.*

/**
 * ViewModel для процесса покупки альтернативного биллинга
 * Created by Michael Shishkin on 16.05.2024
 */
class BillingStateViewModel : ViewModel() {
    val state: MutableLiveData<AltBillingState> = MutableLiveData(getInitialState())
    
    private fun getInitialState() =
        when (Billing.license.licenseStatus) {
            NONE -> DISCLAIMER
            GOT_KEY -> LICENSE_CHECK
            PURCHASED -> PURCHASE_CONFIRMED
            ACTIVATED -> LICENSE_INFO
        }
    
    fun nextState() {
        if (state.value!! < INACTIVATED)
            state.value = AltBillingState.entries.toTypedArray()[state.value!!.ordinal + 1]
        else
            state.value = ACTIVATE
    }
}

enum class AltBillingState {
    DISCLAIMER,                 // дисклеймер не подтвержден
    LICENSE_REQUEST,            // дисклеймер подтвержден, нет лицензии
    LICENSE_CHECK,              // есть лицензия, проверить статус
    PURCHASE_FORM,              // есть лицензия, но еще не куплена. Показать форму
    PURCHASE_COMPLETED,         // покупка выполнена, диплинк после покупки, ждем подтверждения
    PURCHASE_CONFIRMED,         // покупка подтверждена сервером
    ACTIVATE,                   // лицензия куплена, но не активирована
    ACTIVATION_REQUEST,         // активация лицензии
    LICENSE_INFO,               // лицензия куплена и активирована
    INACTIVATION_REQUEST,       // деактивация лицензии
    INACTIVATED                 // лицензия деактивирована
}