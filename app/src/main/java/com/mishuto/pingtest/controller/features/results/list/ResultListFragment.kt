package com.mishuto.pingtest.controller.features.results.list

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.pressBack
import com.mishuto.pingtest.controller.features.results.DeletedEntities.DeletedState.*
import com.mishuto.pingtest.controller.features.results.ResultsListViewModel

/**
 * Список результатов теста
 * Created by Michael Shishkin on 14.08.2022
 */

class ResultListFragment : Fragment(R.layout.results_list_fragment) {
    
    private lateinit var adapter: RecyclerViewAdapter
    private val activityViewModel: ResultsListViewModel by activityViewModels()
    private lateinit var selectedGroup: SelectedGroup
    private lateinit var resultsListData: ResultsListData
    val activity by lazy { getActivity() as AppCompatActivity }
    private lateinit var toolbarController: ToolbarControl
    private lateinit var emptyStateController: EmptyStateController
    private val selectedGroupModel: SelectedGroupModel by viewModels()
    private val itemsGenerator by lazy { ItemsGenerator(this, adapter, emptyStateController, resultsListData) }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        tag()
        val root = super.onCreateView(inflater, container, savedInstanceState)!!
        resultsListData = ResultsListData(this, activityViewModel)
        toolbarController = ToolbarControl(root, this, selectedGroupModel, resultsListData)
        adapter = RecyclerViewAdapter(resultsListData, selectedGroupModel, activityViewModel, this)
        selectedGroup = SelectedGroup(selectedGroupModel, this, resultsListData, activityViewModel)
        initRecyclerView(root.findViewById(R.id.result_list))
        emptyStateController = EmptyStateController(root, adapter)
        setOnDeleteItemBehavior()
        initActionBar()
        return root
    }
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                toolbarController.onCreateMenu(menu, menuInflater)
            }
            
            override fun onMenuItemSelected(item: MenuItem): Boolean =
                if (selectedGroupModel.isGroupSelected())
                    selectedGroup.onMenuItemSelected(item.itemId)
                else
                    when (item.itemId) {
                        R.id.wand_action -> {
                            itemsGenerator.generateResults(); true
                        }
                        
                        android.R.id.home -> {
                            activity.pressBack(); true
                        }
                        
                        else -> false
                    }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }
    
    private fun initRecyclerView(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }
    
    private fun initActionBar() {
        activity.setSupportActionBar(toolbarController.toolbar)
        toolbarController.update()
    }
    
    @SuppressLint("NotifyDataSetChanged")
    private fun setOnDeleteItemBehavior() = activityViewModel.deletedEntities.markedState.observe(viewLifecycleOwner) { state ->
        d("deleted state is $state")
        if (state in MARKED..UNMARKED) {
            val resultsDiffUtil: ResultsDiffUtil =
                if (state == MARKED) {
                    resultsListData.onMarked()
                    ResultsDiffUtil(oldList = resultsListData.resultsList, newList = resultsListData.resultsFilteredList)
                }
                else
                    ResultsDiffUtil(oldList = resultsListData.resultsFilteredList, newList = resultsListData.resultsList)
            
            DiffUtil.calculateDiff(resultsDiffUtil).dispatchUpdatesTo(adapter)
        }
        if (state == DELETED)
            resultsListData.updateResultList()
    }
}