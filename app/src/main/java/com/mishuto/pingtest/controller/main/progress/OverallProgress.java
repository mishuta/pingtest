package com.mishuto.pingtest.controller.main.progress;

import static com.mishuto.pingtest.common.Utils.getString;
import static com.mishuto.pingtest.common.Utils.percent;

import android.widget.ImageView;
import android.widget.TextView;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.model.indicators.GroupIndicators;
import com.mishuto.pingtest.model.indicators.Indicator;
import com.mishuto.pingtest.view.SegmentedProgressBar;

/**
 * Контроллер Overall - индикатора
 * Created by Michael Shishkin on 03.11.2021
 */
public class OverallProgress extends ProgressController {

    public OverallProgress(TextView value, SegmentedProgressBar progressBar, ImageView ico, GroupIndicators indicator) {
        super(value, progressBar, ico, indicator);
    }

    @Override
    void updateIndicatorValue(Indicator indicator) {
        mValue.setText(getString(R.string.percent, percent(indicator.computeIndicatorValue())));
    }
}
