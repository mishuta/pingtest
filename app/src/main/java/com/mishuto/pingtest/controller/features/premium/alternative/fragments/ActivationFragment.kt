package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.controller.features.premium.alternative.AltBillingState
import com.mishuto.pingtest.controller.features.premium.alternative.BillingStateViewModel
import com.mishuto.pingtest.controller.features.premium.billing.Billing.license
import com.mishuto.pingtest.databinding.AltBillingActivationBinding
import com.mishuto.pingtest.view.setBackgroundTintColor

/**
 * Активация лицензии
 * Created by Michael Shishkin on 27.05.2024
 */
class ActivationFragment : Fragment() {
    lateinit var binding: AltBillingActivationBinding
    private val billingStateViewModel: BillingStateViewModel by activityViewModels()
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        d("state: ${billingStateViewModel.state.value}")
        binding = AltBillingActivationBinding.inflate(inflater, container, false)
        binding.activate.setOnClickListener { onActivate() }
        binding.part1.addTextChangedListener(Watcher(binding.part1))
        binding.part2.addTextChangedListener(Watcher(binding.part2))
        initLicenseCode()
        
        updateViews()
        return binding.root
    }
    
    private fun onActivate() {
        d("Click Activate for ${binding.part1.text}-${binding.part2.text}")
        if (isCodeValid()) {
            license.key = "${binding.part1.text}${binding.part2.text}"
            billingStateViewModel.nextState()
        }
    }
    
    private fun initLicenseCode() {
        val key = license.key
        if (key.length == 8) {
            binding.part1.setText(key.substring(0..3))
            binding.part2.setText(key.substring(4..7))
        }
    }
    
    private fun updateViews() {
        binding.activate.visibility = if (billingStateViewModel.state.value == AltBillingState.ACTIVATE) VISIBLE else INVISIBLE
        binding.activate.isEnabled = isCodeValid()
        
        listOf(binding.part1, binding.part2).forEach {
            it.setBackgroundTintColor(if (it.isPartValid()) R.color.colorGood else R.color.colorError)
        }
    }
    
    private fun isCodeValid(): Boolean = binding.part1.isPartValid() && binding.part2.isPartValid()
    private fun EditText.isPartValid() = "[A-Z1-9]{4}".toRegex().matches(text.toString())
    
    inner class Watcher(val view: EditText) : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
        override fun afterTextChanged(s: Editable?) = Unit
        
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (view == binding.part1 && view.isPartValid()) binding.part2.requestFocus()
            
            updateViews()
        }
    }
}

