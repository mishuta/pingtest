package com.mishuto.pingtest.controller.ui;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.Utils;
import com.mishuto.pingtest.controller.features.help.Article;

/**
 * Фрагмент для вывода статей header/content
 * Created by Michael Shishkin on 13.01.2022
 */
public class ArticleFragment extends Fragment {
    private final static String TITLE = "title";
    private final static String CONTENT = "content";

    private String title;
    private String content;

    public ArticleFragment(int titleId, int contentId) {
        this();
        title = Utils.getString(titleId);
        content = Utils.getString(contentId);
    }

    public ArticleFragment(Article article) {
        this(article.getTitleId(), article.getContentId());
    }

    public ArticleFragment() {
        super(R.layout.article_fragment);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        restoreState(savedInstanceState);
        TextView titleView = view.findViewById(R.id.header);
        TextView contentView = view.findViewById(R.id.content);

        titleView.setText(title);
        contentView.setText(getSpanned(content));
        contentView.setMovementMethod(LinkMovementMethod.getInstance());
        return view;
    }

    private void restoreState(Bundle inState) {
        if (inState != null) {
            title = inState.getString(TITLE);
            content = inState.getString(CONTENT);
        }
    }

    private Spanned getSpanned(String formattedText) {
        return Html.fromHtml(formattedText, Html.FROM_HTML_MODE_COMPACT);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TITLE, title);
        outState.putString(CONTENT, content);
    }
}
