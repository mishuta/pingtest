package com.mishuto.pingtest.controller.features.export

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Utils.getString
import com.mishuto.pingtest.common.asDurationFormat
import com.mishuto.pingtest.common.msToTimeUnits
import com.mishuto.pingtest.common.shortDateFormat
import com.mishuto.pingtest.controller.ui.ComputingProgressListener
import com.mishuto.pingtest.controller.ui.log.LogFormatter.format
import com.mishuto.pingtest.data.*
import kotlinx.coroutines.yield
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.time.LocalDateTime
import java.util.*

/**
 * Сохранение лога пингов в файл
 * Created by Michael Shishkin on 26.06.2022
 */

object PingLogFile {

    suspend fun OutputStream.saveLog(resultEntities: List<ResultEntity>, computingProgressListener: ComputingProgressListener) =
        OutputStreamWriter(this).use { out ->
            val lines = countEvents(resultEntities).toFloat()
            var line = 0f
            for (entity in resultEntities) {
                out.append(header(entity))
                createLogEventsMap(entity).forEach {
                    out.appendLine(it.value)
                    computingProgressListener.onUpdate(++line/lines)
                    yield()
                }
            }
            d("completed ${line.toInt()} lines")
        }

    private fun countEvents(entities: Collection<ResultEntity>) : Int {
        val testIDs = entities.map { it.testId }.toList()
        return EventDao.events.sumOf { it.getCountForTests(testIDs) }
    }

    private fun header(entity: ResultEntity) : String {
        return getString(
            R.string.ping_log_header,
            entity.startTime.shortDateFormat(),
            entity.host,
            entity.ip,
            msToTimeUnits(entity.pingTimeout),
            msToTimeUnits(entity.pingInterval),
            entity.testDuration().asDurationFormat(),
            entity.startNetType.description
        )
    }

    private fun createLogEventsMap(entity: ResultEntity): Map<LocalDateTime, String> {
        val stringMap = TreeMap<LocalDateTime, String>()

        fun toMap(eventDao: EventDao<*>, formatter: (EventEntity)->String ) =
            eventDao.getTest(entity.testId).associateTo(stringMap) { Pair(it.timestamp, formatter(it)) }

        toMap(pingDAO) { (it as PingEntity).format().toString() }
        toMap(netChangeDAO) { (it as NetTypeEventEntity).format().toString() }
        toMap(errMessageDAO) { (it as ErrMessageEntity).format().toString() }

        return stringMap
    }
}