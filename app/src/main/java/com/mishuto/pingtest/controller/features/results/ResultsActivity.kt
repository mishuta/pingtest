package com.mishuto.pingtest.controller.features.results

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG
import com.google.android.material.snackbar.Snackbar
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.AnalyticEvents
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.Utils.prepareSnack
import com.mishuto.pingtest.common.replaceFragment
import com.mishuto.pingtest.controller.features.results.list.ResultListFragment
import com.mishuto.pingtest.controller.features.premium.billing.Billing
import com.mishuto.pingtest.data.resultDAO

/**
 * Активность для отображения и управления фрагментами: ResultListFragment, ResultDetailsFragment
 */

class ResultsActivity : AppCompatActivity() {
    private val resultsListViewModel: ResultsListViewModel by viewModels()
    private val deletingSnack = DeletingSnack()
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tag()
        setContentView(R.layout.result_activity)
        
        if (savedInstanceState == null)
            replaceFragment(ResultListFragment())
        
        resultsListViewModel.deletedEntities.markedState.observe(this) {
            if (it == DeletedEntities.DeletedState.MARKED) onDelete()
        }
        
        resultsListViewModel.selectedItem.observe(this) {
            if (resultsListViewModel.selectedItem.value != null) onItemSelected()
        }
        
        checkAccess()
    }
    
    private fun checkAccess() {
        if (!Billing.isPremium) {
            prepareSnack(findViewById(R.id.fragment), R.string.premium_access_violation)
                .addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        AnalyticEvents.logPremiumAccessViolation("result_activity")
                        this@ResultsActivity.finish()
                    }
                })
                .show()
        }
    }
    
    private fun onDelete() {
        val deletedEntities = resultsListViewModel.deletedEntities
        val entities = deletedEntities.markedEntities.value!!.toList()
        
        deletingSnack.show(object : DeletingSnackCallback {
            override fun onUndo() {
                deletedEntities.unMarkAsDeleted()
            }
            
            override fun onFinalDelete() {
                resultDAO.deleteEntries(entities)
                deletedEntities.makeDeleted()
            }
        })
    }
    
    private fun onItemSelected() {
        deletingSnack.dismiss()
        replaceFragment(ResultDetailsFragment(), true)
    }
    
    interface DeletingSnackCallback {
        fun onUndo()
        fun onFinalDelete()
    }
    
    private inner class DeletingSnack : BaseTransientBottomBar.BaseCallback<Snackbar>() {
        lateinit var deletingSnackCallback: DeletingSnackCallback
        
        private val snack: Snackbar by lazy {
            Snackbar.make(this@ResultsActivity, findViewById(R.id.fragment), getString(R.string.deleted), LENGTH_LONG)
                .setAction(R.string.undo) { deletingSnackCallback.onUndo() }
                .addCallback(this)
        }
        
        fun show(deletingSnackCallback: DeletingSnackCallback) {
            this.deletingSnackCallback = deletingSnackCallback
            snack.show()
        }
        
        fun dismiss() = snack.dismiss()
        
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            if (event != DISMISS_EVENT_ACTION) deletingSnackCallback.onFinalDelete()  // если не было нажато UNDO, то удаляем навсегда
        }
    }
}