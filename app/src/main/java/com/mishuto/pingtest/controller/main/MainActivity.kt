package com.mishuto.pingtest.controller.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.CrashlyticsKeys
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.Utils.prepareSnack
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.common.getToolbar
import com.mishuto.pingtest.common.startActivity
import com.mishuto.pingtest.controller.features.help.HelpActivity
import com.mishuto.pingtest.controller.features.premium.PremiumDialog
import com.mishuto.pingtest.controller.features.results.ResultsActivity
import com.mishuto.pingtest.controller.services.AppUpdater
import com.mishuto.pingtest.controller.features.premium.billing.Billing
import com.mishuto.pingtest.controller.features.premium.billing.Billing.CHANGE_PREMIUM_STATE
import com.mishuto.pingtest.controller.services.permissions.PermissionFactory
import com.mishuto.pingtest.settings.app.SettingsActivity

/**
 * Главная активность
 * Originally created by Michael Shishkin on 03.06.2020, refactored on Kotlin on 24.10.2023
 */

class MainActivity : AppCompatActivity() {
    
    private val toolbar by lazy { getToolbar() }
    private val logExporter = LogExporter(this)
    
    override fun onCreate(savedInstanceState: Bundle?) {
        tag()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        
        AppUpdater.init(this)
        PermissionFactory.createPermissions(this)
        CrashlyticsKeys.update(CrashlyticsKeys.Tag.DISPLAY, CrashlyticsKeys.Tag.BILLING)
        
        EventDispatcher.bind<Boolean>(CHANGE_PREMIUM_STATE, "MainActivity", lifecycle) { runOnUiThread { onPremiumUpdate(it) } }
    }
    
    override fun onStart() {
        tag()
        super.onStart()
        onPremiumUpdate(Billing.isPremium)
        AppUpdater.checkForUpdate(this)
    }
    
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }
    
    // обработчик меню
    @SuppressLint("NonConstantResourceId")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> startActivity(SettingsActivity::class.java)
            R.id.action_help -> startActivity(HelpActivity::class.java)
            R.id.action_share -> doIfPremium(R.string.premium_feature_export) { logExporter.shareLog() }
            R.id.action_save -> doIfPremium(R.string.premium_feature_save_log) { logExporter.saveLog() }
            R.id.action_results -> doIfPremium(R.string.premium_feature_history) { startActivity(ResultsActivity::class.java) }
            R.id.action_premium -> PremiumDialog(this).showBillingDialog()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }
    
    private fun doIfPremium(featureDescriptionId: Int, action: () -> Unit) {
        if (Billing.isPremium)
            action.invoke()
        else
            prepareSnack(findViewById(R.id.main_root), featureDescriptionId).show()
    }
    
    private fun onPremiumUpdate(isPremium: Boolean) {
        toolbar.subtitle = if (isPremium) getString(R.string.premium) else null
        invalidateOptionsMenu()
    }
    
    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val premiumActions = listOf(R.id.action_share, R.id.action_save, R.id.action_results)
        if (!Billing.isPremium)
            menu.getItem(0).subMenu!!.apply {
                premiumActions.forEach { findItem(it).setIcon(R.drawable.outline_lock_24) }
            }
        
        return super.onPrepareOptionsMenu(menu)
    }
    
    companion object {
        fun startMainActivity(context: Context) = Intent(context, MainActivity::class.java).also {
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(it)
            d("starting Main activity")
        }
    }
}