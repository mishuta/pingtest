package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.mishuto.pingtest.R
import com.mishuto.pingtest.backend.rest.ServerErrorHandler.tryRetrieveServerErrorResponse
import com.mishuto.pingtest.common.Const.SEC
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.controller.features.premium.alternative.BillingStateViewModel
import com.mishuto.pingtest.controller.features.premium.billing.Billing
import com.mishuto.pingtest.controller.features.premium.billing.alternative.License
import com.mishuto.pingtest.databinding.AltBillingConfirmationProcessBinding
import kotlinx.coroutines.*

/**
 * Ожидание подтверждения покупки
 * Created by Michael Shishkin on 17.06.2024
 */
class PurchaseConfirmationRequestFragment : Fragment() {
    companion object {
        const val TOTAL_FRAGMENT_TIMEOUT = 20L * SEC
        const val INTERVAL_BETWEEN_REQUESTS = 3L * SEC
    }
    
    private lateinit var binding: AltBillingConfirmationProcessBinding
    private val billingStateViewModel: BillingStateViewModel by activityViewModels()
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        Logger.tag()
        binding = AltBillingConfirmationProcessBinding.inflate(inflater, container, false)
        binding.button.setOnClickListener{ doRequest() }
        doRequest()
        return binding.root
    }
    
    private fun doRequest() {
        makeErrorVisible(false)
        runProgressIndicator()
        pollRequests()
    }
    
    private fun runProgressIndicator() {
        with(ValueAnimator()) {
            setIntValues(binding.progress.min, binding.progress.max)
            duration = TOTAL_FRAGMENT_TIMEOUT
            addUpdateListener { animator -> binding.progress.progress = animator.animatedValue as Int }
            start()
        }
    }
    
    private fun pollRequests() {
        lifecycleScope.launch(CoroutineExceptionHandler { _, e -> onRequestError(e.tryRetrieveServerErrorResponse()) }) {
            try {
                withTimeout(TOTAL_FRAGMENT_TIMEOUT) {
                    Logger.d("start series of requesting license status")
                    var status = Billing.license.requestLicenseInfo()
                    while (status < License.LicenseStatus.PURCHASED) {
                        delay(INTERVAL_BETWEEN_REQUESTS)
                        status = Billing.license.requestLicenseInfo()
                    }
                    billingStateViewModel.nextState()
                }
            }
            catch (e: TimeoutCancellationException) {
                Logger.e("timeout of purchase info request")
                binding.err.errorTitle.text = getString(R.string.error_purchase_confirm)
                binding.err.errorMessage.text = getString(R.string.error_purchase_confirm_timeout)
                binding.err.errorFooter.setText(getString(R.string.error_purchase_confirm_timeout_footer))
                makeErrorVisible(true)
            }
        }
    }
    
    private fun onRequestError(e: Throwable) {
        FirebaseCrashlytics.getInstance().recordException(e)
        Logger.e(e)
        binding.err.errorTitle.text = getString(R.string.error_purchase_confirm)
        binding.err.errorMessage.text = e.message ?: "undefined error"
        binding.err.errorFooter.setText(getString(R.string.error_retry))
        makeErrorVisible(true)
    }
    
    private fun makeErrorVisible(show: Boolean) {
        fun View.makeVisible(isVisible: Boolean) {
            visibility = if (isVisible) VISIBLE else INVISIBLE
        }
        
        with(binding) {
            progressMessage.makeVisible(!show)
            progress.makeVisible(!show)
            err.errorLayout.makeVisible(show)
            button.makeVisible(show)
        }
    }
}