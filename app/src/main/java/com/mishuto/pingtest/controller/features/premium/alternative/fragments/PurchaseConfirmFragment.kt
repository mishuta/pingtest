package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import com.mishuto.pingtest.R
import com.mishuto.pingtest.controller.features.premium.alternative.AltBillingState
import com.mishuto.pingtest.controller.features.premium.alternative.fragments.base.AbstractInfoFragment
import com.mishuto.pingtest.controller.features.premium.billing.Billing

/**
 * Показ сообщения после подтверждения покупки
 * Created by Michael Shishkin on 22.06.2024
 */
class PurchaseConfirmFragment: AbstractInfoFragment() {

    override fun execute() {
        binding.message.setText(getString(R.string.alt_after_purchase_message, Billing.license.formattedKey))
        binding.next.setOnClickListener { billingStateViewModel.state.value =
            AltBillingState.ACTIVATE
        }
    }
}