package com.mishuto.pingtest.controller.features.export

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.ContentValues
import android.os.Build
import android.os.Environment
import android.os.Environment.DIRECTORY_DOWNLOADS
import android.provider.MediaStore
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.controller.services.permissions.PermissionNotGranted
import com.mishuto.pingtest.controller.services.permissions.getPermissionOrThrow
import com.mishuto.pingtest.data.ResultEntity
import java.io.FileOutputStream
import java.io.OutputStream


/**
 * Сохранение фалйа в папку Download
 * Created by Michael Shishkin on 28.01.2024
 */

class Saver(activity: AppCompatActivity, entities: List<ResultEntity>) : AbstractSaver(activity, entities) {
    
    private val fullName = Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS).absolutePath.plus("/${getFileName()}")
    
    override suspend fun processFile() {
        try {
            val out: OutputStream =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                    metastoreOutputStream()
                else {
                    getPermissionOrThrow(WRITE_EXTERNAL_STORAGE)
                    FileOutputStream(fullName)
                }
            
            createFile(out)
            d("file ${getFileName()} was processed")
            showSnack(R.string.file_saved)
        }
        catch (e: PermissionNotGranted) {
            showSnack(R.string.no_write_permission)
        }
    }
    
    @RequiresApi(Build.VERSION_CODES.Q)
    fun metastoreOutputStream(): OutputStream {
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, getFileName())
            put(MediaStore.MediaColumns.MIME_TYPE, "text/plain")
            put(MediaStore.MediaColumns.RELATIVE_PATH, DIRECTORY_DOWNLOADS)
        }
        
        val contentUri = MediaStore.Downloads.EXTERNAL_CONTENT_URI
        val resolver = activity.contentResolver
        val uri = resolver.insert(contentUri, contentValues)!!
        
        return resolver.openOutputStream(uri)!!
    }
}