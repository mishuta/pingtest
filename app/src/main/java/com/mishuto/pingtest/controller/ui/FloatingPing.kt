package com.mishuto.pingtest.controller.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.graphics.PixelFormat
import android.graphics.Point
import android.provider.Settings
import android.view.*
import android.view.WindowManager.LayoutParams.*
import android.widget.TextView
import androidx.core.graphics.minus
import androidx.core.graphics.plus
import com.mishuto.pingtest.App.Companion.appContext
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.SharedPref
import com.mishuto.pingtest.controller.main.MainActivity.Companion.startMainActivity
import com.mishuto.pingtest.controller.features.premium.billing.Billing
import com.mishuto.pingtest.settings.SettingsPrefManager
import com.mishuto.pingtest.settings.SettingsPrefManager.showFloatingWindowKey
import com.mishuto.pingtest.view.Utils.dp2pix
import com.mishuto.pingtest.view.Utils.getScreenSize
import kotlin.math.abs


/**
 * Плавающее окно с пингом
 * Created by Michael Shishkin on 10.02.2024
 */
class FloatingPing(val context: Context) {
    
    private var session: Session? = null
    
    companion object {
        const val WINDOW_X_COORDINATE = "floating_window_x_coordinate"
        const val WINDOW_Y_COORDINATE = "floating_window_y_coordinate"
        const val MOVING_THRESHOLD = 3  // изменение координат больше чем на MOVING_THRESHOLD px считается перемещением, иначе - дребезгом
        val hasPermission: Boolean get() = Settings.canDrawOverlays(appContext)
    }
    
    fun start() {
        SettingsPrefManager.registerOnSPChangeListener(javaClass.simpleName) { _, key ->
            if (key == showFloatingWindowKey)
                if (SettingsPrefManager.preferences.showFloatingWindow) show() else dismiss()
        }
        show()
    }
    
    fun stop() {
        SettingsPrefManager.unregisterOnSPChangeListener(javaClass.simpleName)
        dismiss()
    }
    
    private fun show() {
        if (hasPermission && Billing.isPremium && SettingsPrefManager.preferences.showFloatingWindow)
            if (session == null)
                session = Session(context)
    }
    
    fun update(latency: Long) {
        if (session != null && hasPermission)
            session!!.update(latency)
    }
    
    private fun dismiss() {
        if (session != null)
            session!!.dismiss()
        session = null
    }
    
    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    private class Session(private val context: Context) {
        
        @SuppressLint("InflateParams")
        private val pingWindow = LayoutInflater.from(context).inflate(R.layout.overlay_ping_window, null)
        private val windowManager = context.getSystemService(WINDOW_SERVICE) as WindowManager
        private val windowParams = initializeWindowParams()
        private val latencyView = pingWindow.findViewById<TextView>(R.id.latency)
        
        init {
            pingWindow.setOnTouchListener(TouchListener())
            pingWindow.setOnClickListener { d("clicked") }  // регистрация лисенера нужен для того чтобы кнопка "нажималась"
            windowManager.addView(pingWindow, windowParams)
            latencyView.text = "   0"
        }
        
        private fun initializeWindowParams() = WindowManager.LayoutParams(
            WRAP_CONTENT, WRAP_CONTENT,
            SharedPref.getInt(WINDOW_X_COORDINATE,getScreenSize(context).x - dp2pix(72)),
            SharedPref.getInt(WINDOW_Y_COORDINATE, dp2pix(72)),
            TYPE_APPLICATION_OVERLAY,
            FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT)
            .apply { gravity = Gravity.TOP or Gravity.START }
            .apply {
                pingWindow.post {   // после инициализации окна устанавливаем фиксированную длину и начальную позиции, зависящие от реальных размеров окна
                    width = pingWindow.width
                    windowManager.updateViewLayout(pingWindow, this)
                }
            }
        
        fun update(latency: Long) = latencyView.post { latencyView.text = latency.toString() }
        
        fun dismiss() = windowManager.removeView(pingWindow)
        
        // трекер перемещения окна за пальцем
        private inner class TouchListener : View.OnTouchListener {
            var startWindowPoint = Point()
            var touchPoint = Point()
            
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                return when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        startWindowPoint = Point(windowParams.x, windowParams.y)
                        touchPoint = event.getPoint()
                        false   // в ACTION_DOWN и ACTION_UP необходимо вернуть false чтобы кнопка визуально "нажалась" и "отжалась"
                    }
                    
                    MotionEvent.ACTION_MOVE -> {
                        val finishWindowPoint = startWindowPoint + (event.getPoint() - touchPoint)
                        windowParams.x = finishWindowPoint.x
                        windowParams.y = finishWindowPoint.y
                        windowManager.updateViewLayout(v, windowParams)
                        true
                    }
                    
                    MotionEvent.ACTION_UP -> {
                        if (wasMoving()) {
                            d("save coordinates ${windowParams.x} ${windowParams.y}")
                            SharedPref.putInt(WINDOW_X_COORDINATE, windowParams.x)
                            SharedPref.putInt(WINDOW_Y_COORDINATE, windowParams.y)
                        }
                        else
                            startMainActivity(context)  // action здесь, а не в onClick(), чтобы избежать срабатывания action после перемещения
                        
                        false
                    }
                    
                    else -> false
                }
            }
            
            private fun MotionEvent.getPoint() = Point(rawX.toInt(), rawY.toInt())
            
            private fun wasMoving() =
                abs(windowParams.x - startWindowPoint.x) > MOVING_THRESHOLD || abs(windowParams.y - startWindowPoint.y) > MOVING_THRESHOLD
        }
    }
}