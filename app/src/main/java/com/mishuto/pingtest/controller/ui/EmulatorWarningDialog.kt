package com.mishuto.pingtest.controller.ui

import androidx.appcompat.app.AppCompatActivity
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.AlertQueManager
import com.mishuto.pingtest.common.Environment.isDebug
import com.mishuto.pingtest.common.Environment.isEmulator
import com.mishuto.pingtest.common.Utils.createDialogBuilder

/**
 * Предупреждение, выдаваемое при запуске релизной версии на эмуляторе
 * Created by Michael Shishkin on 02.12.2023
 */
object EmulatorWarningDialog {
    private var userCancelled = false

    fun init(alertQueManager: AlertQueManager, activity: AppCompatActivity) {
        if (isEmulator && !isDebug && !userCancelled ) {
            val dialog = createDialogBuilder(activity)
                    .setTitle(R.string.attention)
                    .setMessage(R.string.emulator_warning)
                    .setIcon(R.drawable.ic_error)
                    .create()

            alertQueManager.enqueue("EmulatorWarningDialog", dialog) { userCancelled = true}
        }
    }
}