package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.controller.features.premium.alternative.BillingStateViewModel
import com.mishuto.pingtest.databinding.AltBillingDisclaimerBinding

/**
 * Отображение дисклеймера перед покупкой в юмани
 * Created by Michael Shishkin on 15.05.2024
 */
class DisclaimerFragment : Fragment() {
    private lateinit var binding: AltBillingDisclaimerBinding
    private val billingStateViewModel: BillingStateViewModel by activityViewModels()
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        tag()
        binding = AltBillingDisclaimerBinding.inflate(inflater, container, false)
        binding.disclaimer.movementMethod = ScrollingMovementMethod()
        binding.checkBox.setOnCheckedChangeListener { _, _ -> updateButton() }
        binding.button.setOnClickListener { billingStateViewModel.nextState() }
        updateButton()
        
        return binding.root
    }
    
    private fun updateButton() {
        binding.button.isEnabled = binding.checkBox.isChecked
    }

}