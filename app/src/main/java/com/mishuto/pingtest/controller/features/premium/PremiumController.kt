package com.mishuto.pingtest.controller.features.premium

import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.AlertQueManager
import com.mishuto.pingtest.common.SharedPref
import com.mishuto.pingtest.common.Utils.getString
import com.mishuto.pingtest.common.Utils.prepareSnack
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.controller.features.premium.billing.Billing
import com.mishuto.pingtest.controller.features.premium.billing.google.PremiumPurchaseStatus
import com.mishuto.pingtest.controller.features.premium.billing.google.PremiumPurchaseStatus.Companion.ON_GP_PURCHASE_STATUS_CHANGED

/**
 * Управляет реакцией активности на интенты от Billing
 * Created by Michael Shishkin on 13.10.2023
 */
class PremiumController(private val alertQueManager: AlertQueManager, private val activity: AppCompatActivity) {
    
    init {
        EventDispatcher.bind<PremiumPurchaseStatus>(ON_GP_PURCHASE_STATUS_CHANGED, "PremiumController", activity.lifecycle) { showGooglePurchaseStatus(it) }
        alternativeBillingHint()
    }
    
    private fun showGooglePurchaseStatus(status: PremiumPurchaseStatus) {
        activity.runOnUiThread{
            var message = getString(status.mesId)
            if(status != PremiumPurchaseStatus.COMPLETED && Billing.googleBilling.errState.hasError())
                message += "\n${Billing.googleBilling.errState.errMessage}"
            
            prepareSnack(activity.findViewById(R.id.main_fragment), message).show()
        }
    }
    
    private fun alternativeBillingHint() {
        if (Billing.alternativeBillingConditionsMet() && !SharedPref.checkFlagAndSet("AlternateBillingDialogShowed"))
            alertQueManager.enqueue(
                "Alternative_billing_is_available",
                MaterialAlertDialogBuilder(activity, R.style.MaterialAlertDialogPrimary)
                    .setMessage(R.string.alternative_billing_is_avail)
                    .create()
            )
    }
}