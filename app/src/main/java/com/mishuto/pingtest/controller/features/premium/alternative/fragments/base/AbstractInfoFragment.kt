package com.mishuto.pingtest.controller.features.premium.alternative.fragments.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.controller.features.premium.alternative.BillingStateViewModel
import com.mishuto.pingtest.databinding.AltBillingPurchaseBinding

/**
 * Базовый фрагмент для информационных фрагментов
 * Created by Michael Shishkin on 22.06.2024
 */
abstract class AbstractInfoFragment : Fragment() {
    protected val billingStateViewModel: BillingStateViewModel by activityViewModels()
    protected lateinit var binding: AltBillingPurchaseBinding

    abstract fun execute()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = AltBillingPurchaseBinding.inflate(inflater, container, false)

        val state = billingStateViewModel.state.value
        Logger.d("state: $state")
        execute()

        return binding.root
    }
}