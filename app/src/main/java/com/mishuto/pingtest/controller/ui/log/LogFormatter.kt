package com.mishuto.pingtest.controller.ui.log

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
import android.text.style.ForegroundColorSpan
import androidx.core.text.BidiFormatter
import androidx.core.text.TextDirectionHeuristicsCompat
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Utils.getString
import com.mishuto.pingtest.common.mediumTimeFormat
import com.mishuto.pingtest.model.ErrorMessageEvent
import com.mishuto.pingtest.model.NetTypeChangeEvent
import com.mishuto.pingtest.model.ping.Ping

/**
 * Форматирование для вывода в лог
 * Используется как для вывода во LogView ( этом случае текст раскрашивается) так и для форматирования текста, например в файле
 * Created by Michael Shishkin on 06.07.2022 Refactored on kotlin on 25.01.2024
 */
object LogFormatter {
    private val errColor = R.color.colorError
    private val wrnColor = R.color.colorWarning

    fun Ping.format(viewContext: Context? = null): CharSequence {
        val line = SpannableStringBuilder()
        val latency: Long = if (!isLost && wasICMPFail()) effectiveLatency else latency

        line.append("${timeStamp.mediumTimeFormat()} ")
            .append("${getString(R.string.log_ping_num, ordinal)} ")
            .append("${getString(R.string.log_ping_latency, if (!isLost && wasICMPFail()) effectiveLatency else latency).toLtr()} ")

        if (isLost) {
            line.append(if (isError) getString(R.string.log_error) else getString(R.string.log_timeout))
            viewContext?.let { line.colorize(it, errColor) }
        }
        else if (wasICMPFail()) {
            line.append(getString(R.string.log_icmp_timeout))
            viewContext?.let { line.colorize(it, wrnColor) }
        }

        return line
    }

    fun NetTypeChangeEvent.format(): CharSequence =
        "${timestamp.mediumTimeFormat()} ${getString(R.string.log_net_type_changed, from.description, to.description)}"

    fun ErrorMessageEvent.format(viewContext: Context? = null): CharSequence =
        SpannableString("${timestamp.mediumTimeFormat()} $message")
            .also { line -> viewContext?.let { ctx -> line.colorize(ctx, errColor) } }

    //превращает строку в LTR в RTL-локали (RTL-символы останутся RTL)
    private fun CharSequence.toLtr(): CharSequence =
        BidiFormatter.getInstance().let {
            if (it.isRtlContext) it.unicodeWrap(this, TextDirectionHeuristicsCompat.ANYRTL_LTR) else this
        }

    private fun Spannable.colorize(context: Context, colorId: Int) =
        setSpan(ForegroundColorSpan(context.getColor(colorId)), 0, length, SPAN_EXCLUSIVE_EXCLUSIVE)
}