package com.mishuto.pingtest.controller.main;

import static com.mishuto.pingtest.view.Utils.isLtr;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mishuto.pingtest.R;
import com.mishuto.pingtest.view.Utils;

/**
 * Управление FAB.
 * Кнопка имеет состояние и меняет картинку в зависимости от него
 * Created by Michael Shishkin on 03.06.2020
 */
public class FabController  {
    // состояние
    public enum State {IN_PROGRESS, STOPPED}

    private final FloatingActionButton mFloatingActionButton;
    private State mState = State.STOPPED;

    public void setState(State state) {
        if(state != mState) {
            mState = state;
            mFloatingActionButton.setImageResource(mState == State.STOPPED ?
                    android.R.drawable.ic_media_play :
                    R.drawable.ic_baseline_stop_24 );
        }
    }

    public State getState() {
        return mState;
    }

    public FabController(FloatingActionButton fab) {
        mFloatingActionButton = fab;
    }

    public void setOnClickListener(@Nullable View.OnClickListener l) {
        mFloatingActionButton.setOnClickListener((s)-> {
            setState(mState == State.STOPPED ? State.IN_PROGRESS : State.STOPPED);
            if (l != null)
                l.onClick(mFloatingActionButton);
        });
    }

    public void enable() {
        mFloatingActionButton.setEnabled(true);
    }

    public void disable() {
        mFloatingActionButton.setEnabled(false);
    }

    /**
     * Перемещает кнопку при скролле
     * Вызывается из layout\comp_fab.xml
     * @noinspection unused
     */
    public static class FabMovingByScrollBehavior extends FloatingActionButton.Behavior {
        boolean isPositionInCenter = false;

        public FabMovingByScrollBehavior(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull FloatingActionButton child, @NonNull View directTargetChild, @NonNull View target, int axes, int type) {
            return axes == ViewCompat.SCROLL_AXIS_VERTICAL;
        }

        @Override
        public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull FloatingActionButton fab, @NonNull View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int type, @NonNull int[] consumed) {
            super.onNestedScroll(coordinatorLayout, fab, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, type, consumed);

            if(dyUnconsumed > 0 && !isPositionInCenter) {
                animationMoveByX(fab, computeBiasForFAB(coordinatorLayout, fab));
                isPositionInCenter = true;
            }
            else if(dyUnconsumed <= 0 && isPositionInCenter) {
                animationMoveByX(fab, 0);
                isPositionInCenter = false;
            }
        }

        private int computeBiasForFAB(ViewGroup layout, View fab) {
            int centerPosX = layout.getWidth()/2;
            int endPosX = layout.getWidth() - Utils.dimen2pix(R.dimen.fab_margin) - fab.getWidth()/2;
            int bias = endPosX - centerPosX;
            return isLtr(layout) ? -bias : bias ;
        }

        private void animationMoveByX(View view, int x) {
            view.animate().translationX(x).setInterpolator(new LinearInterpolator()).start();
        }
    }
}
