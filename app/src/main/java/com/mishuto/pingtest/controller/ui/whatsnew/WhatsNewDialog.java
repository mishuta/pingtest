package com.mishuto.pingtest.controller.ui.whatsnew;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static com.mishuto.pingtest.common.Utils.createDialogBuilder;
import static com.mishuto.pingtest.view.Utils.dp2pix;

import android.content.Context;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.DefaultLifecycleObserver;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.AlertQueManager;
import com.mishuto.pingtest.common.pref.Version;

/**
 * Диалог, отображающий What's New - список фич, появившихся после последнего апдейта
 * Created by Michael Shishkin on 16.09.2021
 */
public class WhatsNewDialog implements DefaultLifecycleObserver {

    private final static String VERSION_KEY = "VER_Main_Activity";          // ключ, в котором хранится версия
    private final Version version = Version.getVersion(VERSION_KEY);
    private final AlertQueManager alertQueManager;

    public WhatsNewDialog(AlertQueManager alertQueManager, Context context) {
        this.alertQueManager = alertQueManager;

        if(version.isVersionChanged() ) {
            FeatureHistory history = new FeatureHistory();
            history.setFilter(f -> f.versionCode > version.getOld() && f.versionCode <= Version.CURRENT);

            if(history.getFeatureStream().findAny().isPresent() )
                showWhatsNewAlert(context, history);
            else
                version.accept();   // если версия изменилась, а показывать нечего, просто коммитим новую версию
        }
    }

    private void showWhatsNewAlert(Context context, FeatureHistory history) {
        ViewGroup root = createScrollView(context);
        root.addView(history.createHistoryListView(context));

        AlertDialog dialog = createDialogBuilder(context)
                .setTitle(R.string.whats_new_title)
                .setOnCancelListener(d -> version.accept())     // в случае, когда диалог был отменен кнопкой back - пересохраняем номер версии, чтобы больше не отображался диалог
                .setView(root)
                .create();

        alertQueManager.enqueue("WhatsNewDialog", dialog, null);
    }

    private NestedScrollView createScrollView(Context context) {
        final int PADDING = dp2pix(context, 24);
        final int TOP_PADDING = dp2pix(context, 8);

        NestedScrollView scrollView = new NestedScrollView(context);
        scrollView.setLayoutParams(new ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        scrollView.setPadding(PADDING, TOP_PADDING, PADDING, PADDING);
        return scrollView;
    }
}
