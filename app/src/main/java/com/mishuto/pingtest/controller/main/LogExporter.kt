package com.mishuto.pingtest.controller.main

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.AnalyticEvents
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.Resources.getString
import com.mishuto.pingtest.common.Utils.prepareSnack
import com.mishuto.pingtest.controller.features.export.Saver
import com.mishuto.pingtest.controller.features.export.Sharer
import com.mishuto.pingtest.controller.features.premium.billing.Billing
import com.mishuto.pingtest.data.ResultEntity
import com.mishuto.pingtest.data.resultDAO
import com.mishuto.pingtest.service.ServiceManager
import kotlinx.coroutines.launch

/**
 * Реализация экспорта последнего теста
 * Created by Michael Shishkin on 13.04.2024
 */

class LogExporter(private val activity: AppCompatActivity) {
    
    fun saveLog() {
        tag()
        checkAndDoExport { result ->
            d("save result for ${result.statistics.quantity} pings")
            Saver(activity, listOf(result)).processFile()
        }
        
    }
    
    fun shareLog() {
        tag()
        checkAndDoExport { result ->
            d("sharing result for ${result.statistics.quantity} pings")
            Sharer(activity, listOf(result)).processFile()
        }
    }
    
    private fun checkAndDoExport(exporter: suspend (ResultEntity) -> Unit) {
        ServiceManager.bind("LogExporter") {
            val isServiceStarted = ServiceManager.isStarted()
            ServiceManager.unbind("LogExporter")
            
            if (activity.lifecycle.currentState != Lifecycle.State.RESUMED)
                return@bind
            
            if (!Billing.isPremium) {
                snack(R.string.premium_access_violation)
                AnalyticEvents.logPremiumAccessViolation("main_menu")
                return@bind
            }
            
            if (isServiceStarted) {
                snack(R.string.stop_test_to_export)
                return@bind
            }
            
            val lastResult = resultDAO.getLast()
            if (lastResult == null) {
                snack(R.string.no_data_to_share)
                return@bind
            }
            
            activity.lifecycleScope.launch {
                exporter(lastResult)
            }
        }
    }
    
    private fun snack(stringId: Int) {
        val rootView = activity.findViewById<View>(R.id.main_root)
        prepareSnack(rootView, getString(stringId)).show()
    }
}