package com.mishuto.pingtest.controller.main;

import static com.mishuto.pingtest.common.Const.SEC;
import static com.mishuto.pingtest.common.TimeFormatterKt.minus;
import static com.mishuto.pingtest.common.Utils.eventDispatcher;
import static com.mishuto.pingtest.common.Utils.logger;
import static com.mishuto.pingtest.common.Utils.prepareSnack;
import static com.mishuto.pingtest.common.net.NetworkTypeKt.NET_TYPE_CHANGED;
import static com.mishuto.pingtest.controller.main.FabController.State.IN_PROGRESS;
import static com.mishuto.pingtest.controller.main.FabController.State.STOPPED;
import static com.mishuto.pingtest.service.PingService.HOST_UPDATE_EVENT;
import static com.mishuto.pingtest.service.PingService.PING_EVENT;
import static com.mishuto.pingtest.service.PingService.SERVICE_FAULT_EVENT;
import static com.mishuto.pingtest.service.PingService.SERVICE_STOP_EVENT;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.AlertQueManager;
import com.mishuto.pingtest.common.tickers.Ticker;
import com.mishuto.pingtest.controller.features.premium.PremiumController;
import com.mishuto.pingtest.controller.services.InAppReview;
import com.mishuto.pingtest.controller.services.permissions.NotificationPermissionController;
import com.mishuto.pingtest.controller.ui.EmulatorWarningDialog;
import com.mishuto.pingtest.controller.ui.whatsnew.WhatsNewDialog;
import com.mishuto.pingtest.databinding.ContentMainBinding;
import com.mishuto.pingtest.model.ErrorMessageEventImp;
import com.mishuto.pingtest.model.HostInfo;
import com.mishuto.pingtest.model.NetTypeChangeEvent;
import com.mishuto.pingtest.model.PingTest;
import com.mishuto.pingtest.model.ping.Ping;
import com.mishuto.pingtest.service.ServiceManager;
import com.mishuto.pingtest.settings.Settings;
import com.mishuto.pingtest.settings.SettingsPrefManager;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Контроллер основного окна. Управляет всеми виджетами активности для работы пинга
 * Сам процесс пинга выполняется в отдельном сервисе
 * <p>
 * Created by Michael Shishkin on 14.06.2020
 */
@SuppressWarnings("ConstantConditions")
public class MainFragment extends Fragment {

    private ContentMainBinding mBinding;    // виджеты активности
    private final ServiceManager serviceManager = ServiceManager.INSTANCE;
    FabController mFabController;           // FAB
    Ticker mElapsedTicker;                  // секундный тикер для вывода времени
    ResolveProgress mResolveProgress;       // контроллер диалога долгого резолва
    InAppReview mInAppReview;               // контроллер in-app оценки приложения
    MainFragmentWidgets mWidgets;           // Виджеты главного окна
    private boolean isFirstStart = true;    // Фрагмент впервые создан при первом запуске активности
    private Instant mLastRenewedService;
    private LocalDateTime startTestTime;
    private NotificationPermissionController notificationPermissionController;
    Settings pref = SettingsPrefManager.INSTANCE.getPreferences();

    private final static String TAG = "MainFragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        logger.tag();
        mBinding = ContentMainBinding.inflate(inflater, container, false);
        isFirstStart &= savedInstanceState == null;
        initWidgets();
        initControllers();
        initInformationalDialogs();
        bindToEventDispatcher();

        return mBinding.root;
    }

    private void initWidgets() {
        mWidgets = new MainFragmentWidgets(mBinding);
        mElapsedTicker = new Ticker(this::updateElapsedWidget,true, SEC, "--elapsedTicker");
    }

    private void initControllers() {
        mResolveProgress = new ResolveProgress(getActivity(), serviceManager::stopService);
        mInAppReview = new InAppReview();
        mFabController = new FabController(getActivity().findViewById(R.id.fab));
        mFabController.setOnClickListener(view -> onFABClick());
    }

    private void initInformationalDialogs() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        AlertQueManager alertQueManager = new AlertQueManager(activity);
        EmulatorWarningDialog.INSTANCE.init(alertQueManager, activity);
        new NetworkTypeIconController(mBinding.overallLayout.netCheckIco, alertQueManager, activity);
        notificationPermissionController = new NotificationPermissionController(alertQueManager, activity);
        new WhatsNewDialog(alertQueManager, activity);
        new PremiumController(alertQueManager, activity);
    }

    private void bindToEventDispatcher() {
        Map.of(
                        HOST_UPDATE_EVENT, (Consumer<HostInfo>) this::onHostInfoUpdate,
                        PING_EVENT, (Consumer<Ping>) this::onPing,
                        SERVICE_FAULT_EVENT, (Consumer<String>) this::onFault,
                        SERVICE_STOP_EVENT, p -> onStopTest(),
                        NET_TYPE_CHANGED, (Consumer<NetTypeChangeEvent>) this::onNetTypeChanged
                )
                .forEach((action, consumer) -> eventDispatcher.bindTyped(action, TAG, getLifecycle(), consumer));
    }

    private void onFABClick() {
        logger.tag();
        logger.flushLog();
        if (!serviceManager.isStarted())
            startTest();
        else {
            serviceManager.stopService();
            mInAppReview.requestRateIfNeed(getActivity());   // просим оценить, если надо (и только при "ручной" остановке)
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        logger.tag();
        mFabController.disable();   // чтобы избежать гонок между onFabClick() и onBindService()
        serviceManager.bind(TAG, this::onBindService);
    }

    void onBindService() {
        logger.tag();
        if (serviceManager.isStarted())
            startOnWorkingService(serviceManager.getPingTest());
        else
            startOnIdleService();

        mFabController.enable();
    }

    private void startOnWorkingService(PingTest test) {
        logger.tag();
        if(isRenewed(test.getLastRenewed()))      //был ли восстановлен сервис после останова
            snack(R.string.restart_service_warn);

        // инциализируем виджеты значениями из сервиса
        startTestTime = test.getStarted();

        mWidgets.loadHistoricalWidgets(test.getTestId());
        mWidgets.updateStatWidgets(test.getStatistic());
        mWidgets.updateHost(test.getHostInfo());
        updateElapsedWidget();
        mFabController.setState(IN_PROGRESS);

        mElapsedTicker.start();
        preventScreenOff(true);                            // экран всегда включен (если установлено)
    }

    private void startOnIdleService() {
        logger.tag();
        mFabController.setState(FabController.State.STOPPED);   // если приложение было на заднем плане, а сервис был убит системой, нужно обновить кнопку
        if (isFirstStart) {
            isFirstStart = false;
            mWidgets.initializeWidgets();                       // инициируем их начальными значениями
            mWidgets.splashProgress();

            if (pref.isAutostart()) {
                getView().postDelayed(() -> {
                    logger.d("test started due to autostart is on");
                    startTest();
                }, 500);
            }
        }

    }

    private boolean isRenewed(Instant lastRenewed) {
        boolean isRenewed = (lastRenewed != null) && (mLastRenewedService == null || mLastRenewedService.isBefore(lastRenewed));
        mLastRenewedService = lastRenewed;
        return isRenewed;
    }

    @Override
    public void onStop() {
        logger.tag();              // при останове фрагмента
        mElapsedTicker.stop();              // останавливаем тикер, если был запущен
        mResolveProgress.stop();            // закрывем диалог, если был открыт (все равно будет убит, зато корректно)
        serviceManager.unbind(TAG);         // отвязываем фрагмент от сервиса
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        logger.tag();
        mWidgets.store(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        logger.tag();
        mWidgets.recall(savedInstanceState);
    }

    // запуск теста
    private void startTest() {
        if (!serviceManager.isBound() || serviceManager.isStarted() || getActivity() == null)
            return;

        logger.tag();
        serviceManager.startService();      // стартует сервис пинга
        mElapsedTicker.start();             // стартует тикер
        mWidgets.initializeWidgets();       // очищаем виджеты. Они и так перезатрутся новыми, но если будет ошибка резолва, то нет.
        preventScreenOff(true);             // экран всегда включен
        mResolveProgress.start();           // стартуем контроллер диалога резолва
        mFabController.setState(IN_PROGRESS);
        startTestTime = LocalDateTime.now();
        notificationPermissionController.getPermission();
    }

    /* обработчики событий */

    private void onHostInfoUpdate(HostInfo hostInfo) {
        getActivity().runOnUiThread(()-> {
            logger.d(hostInfo.toString());
            mResolveProgress.stop();
            mWidgets.updateHost(hostInfo);
        }) ;
    }

    private void onPing(Ping ping) {
        if (!serviceManager.isBound() || serviceManager.getPingTest().getStatistic().isEmpty() || getActivity() == null)    // фильтруем ошибочные ситауции
            return;

        var test = serviceManager.getPingTest();

        getActivity().runOnUiThread(()-> {
            if(isRenewed(test.getLastRenewed())) {        // проверка был ли восстановлен сервис после останова
                snack(R.string.restart_service_warn);
                mWidgets.clearHistoricalWidgets();
            }

            mWidgets.addPingToHistoricalWidgets(ping);
            mWidgets.updateStatWidgets(test.getStatistic());               // обновляем виджеты статистикой
            startTestTime = test.getStarted();
        });
    }

    public void onFault(String errMsg) {
        getActivity().runOnUiThread(() -> {
            logger.tag();
            mResolveProgress.stop();
            if(errMsg != null) {
                prepareSnack(mBinding.getRoot(), errMsg).show();
                mWidgets.getLogController().log(new ErrorMessageEventImp(errMsg));
            }

            mFabController.setState(FabController.State.STOPPED);

        });
    }

    public void onStopTest() {
        getActivity().runOnUiThread(() -> {
            logger.tag();
            mElapsedTicker.stop();
            mResolveProgress.stop();
            mFabController.setState(STOPPED);
            preventScreenOff(false);

            if (serviceManager.isBound() && serviceManager.getPingTest().isTestTimeExpired())   // onStopTest может прийти после onStop(), но до onDestroy()
                snack(R.string.test_completed);
        });
    }

    public void onNetTypeChanged(NetTypeChangeEvent event) {
        getActivity().runOnUiThread(()->
                mWidgets.getLogController().log(event));
    }

    private void preventScreenOff(boolean isOn) {
        if(isOn) {
            if (pref.isKeepScreenOn())
                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        else if (getActivity() != null) // вызывается при изменении настроек в SettingsActivity. В этот момент MainActivity может быть утилизирована
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    // при тиканьи тикера выводим прошедшее с начала теста время
    private void updateElapsedWidget() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                if (serviceManager.isBound() && serviceManager.isStarted()) {
                    long ms = minus(LocalDateTime.now(), startTestTime); // длительность теста в мс
                    mWidgets.setElapsedTime(ms);
                }
            });
        }
    }

    private void snack(int stringId) {
        prepareSnack(mBinding.getRoot(), getString(stringId)).show();
    }
}