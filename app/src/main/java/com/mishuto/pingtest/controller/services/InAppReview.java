package com.mishuto.pingtest.controller.services;

import static com.mishuto.pingtest.common.AnalyticEvents.IN_APP_REVIEW;
import static com.mishuto.pingtest.common.Utils.fbAnalytics;
import static com.mishuto.pingtest.common.Utils.logger;

import android.app.Activity;

import com.google.android.play.core.review.ReviewException;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.gms.tasks.Task;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.mishuto.pingtest.common.SharedPref;


/**
 * Контроллер для in-app Review (оценки приложения)
 * Запрос на оценку вызывается из удобной точки основной активности (фрагмента)
 * Запрос показывает окно не каждый раз, а через COUNT_TO_RATE повторений
 * Если пользователь раньше оценивал аппу, то окно Google Play не отобразится. Контролируется GPS. Аппа не может получить информацию: оценил юзер аппу или нет
 * Даже если пользователь не оценивал аппу, окно GP может не отобразиться из-за лимитов GPS или других причин. Узнать невозможно, нужно забить.
 * Created by Michael Shishkin on 13.09.2020
 */
public class InAppReview {
    private final static int COUNT_TO_RATE = 25;    // количество вызовов requestRateIfNeed() чтобы было запущено окно GP. Только опытные пользователи допускаются к оценке
    private static final String RATE_COUNTER = "rate_counter";

    private int mCounter;                           // счетчик вызовов requestRateIfNeed()

    public InAppReview() {
        mCounter = SharedPref.INSTANCE.getInt(RATE_COUNTER, 0);    // инициализируем счетчик
    }

    /**
     * Запрос пользователя оценить приложение.
     * @param activity - основная активность приложения
     */
    public void requestRateIfNeed(Activity activity) {
        if(activity != null && testCounter() ) {                                    // если счетчик достиг пороговой величины - запускаем окно GP
            logger.d("trying rate");

            ReviewManager reviewManager = ReviewManagerFactory.create(activity);    // создается ReviewManager из play.core
            Task<ReviewInfo> request = reviewManager.requestReviewFlow();           // у него запрашивается flow
            request.addOnCompleteListener(task -> {
                if (task.isSuccessful()) {                                          // в случае успешного запуска flow
                    logger.d("starting flow");
                    ReviewInfo reviewInfo = task.getResult();
                    Task<Void> flow = reviewManager.launchReviewFlow(activity, reviewInfo); // запускаем ревью GP. Вызов ревью не гарантирует реального показа окна GP
                    flow.addOnCompleteListener(t -> logger.d("review complete ") );
                    flow.addOnFailureListener(t -> logger.d("failure: " + t.getMessage()));
                    flow.addOnCanceledListener(() -> logger.d("cancel"));
                    flow.addOnSuccessListener(t -> {
                        logger.d("success");
                        fbAnalytics.log(IN_APP_REVIEW);
                    });
                }
                else {
                    var e = task.getException();
                    if (e instanceof ReviewException)
                        logger.e("error code is: " + ((ReviewException) e).getErrorCode());
                    if (e != null)
                        FirebaseCrashlytics.getInstance().recordException(e);
                }
            });
        }
    }

    // увеличение счетчика вызовов requestRateIfNeed() со сбросом при достижении COUNT_TO_RATE
    private boolean testCounter() {
        if(++mCounter >= COUNT_TO_RATE )
            mCounter = 0;

        SharedPref.INSTANCE.putInt(RATE_COUNTER, mCounter);
        return mCounter == 0;
    }
}
