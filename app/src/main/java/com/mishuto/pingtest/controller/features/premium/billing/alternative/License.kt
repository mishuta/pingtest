package com.mishuto.pingtest.controller.features.premium.billing.alternative

import com.mishuto.pingtest.App.Companion.appGuid
import com.mishuto.pingtest.App.Companion.applicationScope
import com.mishuto.pingtest.backend.rest.RetrofitConfig.restService
import com.mishuto.pingtest.backend.rest.dto.ActivationRequest
import com.mishuto.pingtest.backend.rest.dto.LicenseInfoResponse
import com.mishuto.pingtest.common.AnalyticEvents.LICENSE_ACTIVATION
import com.mishuto.pingtest.common.AnalyticEvents.LICENSE_CREATION
import com.mishuto.pingtest.common.AnalyticEvents.LICENSE_DEACTIVATION
import com.mishuto.pingtest.common.Assert
import com.mishuto.pingtest.common.Environment.deviceModel
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.e
import com.mishuto.pingtest.common.Logger.i
import com.mishuto.pingtest.common.SharedPref
import com.mishuto.pingtest.common.Utils.fbAnalytics
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.common.locales
import com.mishuto.pingtest.controller.features.premium.billing.alternative.License.LicenseStatus.*
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

/**
 * Модель объекта Лицензия
 * Created by Michael Shishkin on 14.05.2024
 */

const val ALT_BILLING_ACTIVATION_EVENT = "License.ActivationStateChanged"
private const val licenseKeyPrefName = "license_key"
private const val licenseStatePrefName = "license_state"

class License {
    enum class LicenseStatus { NONE, GOT_KEY, PURCHASED, ACTIVATED }
    
    var licenseStatus: LicenseStatus = LicenseStatus.entries[SharedPref.getInt(licenseStatePrefName, 0)]
        private set(value) {
            if (field != value) {
                i("license change state $field -> $value ")
                SharedPref.putInt(licenseStatePrefName, value.ordinal)
                if(field == ACTIVATED || value == ACTIVATED)
                    EventDispatcher.post(ALT_BILLING_ACTIVATION_EVENT, value == ACTIVATED)
                field = value
            }
        }
    
    val isActivated get() = licenseStatus == ACTIVATED
    
    var key: String = SharedPref.getString(licenseKeyPrefName, "")
        set(value) {
            i("new license key: $value")
            field = value
            SharedPref.putString(licenseKeyPrefName, value)
        }
    
    val formattedKey get() = formatKey(key)
    
    var licenseInfo: LicenseInfoResponse = LicenseInfoResponse(licenseStatus >= GOT_KEY, licenseStatus >= PURCHASED, 0, mutableListOf())
        private set(value) {
            if (value != field) {
                i("new license info: $value")
                field = value
                licenseStatus = when {
                    value.activatedDevices.any { it.appGuid == appGuid } -> ACTIVATED
                    value.paid -> PURCHASED
                    value.exist -> GOT_KEY
                    else -> NONE
                }
            }
        }
    
    init {
        d("license initial state: $licenseStatus")
        
        if (licenseStatus >= PURCHASED) // перепроверяем лицензию при каждом старте
            applicationScope.launch(CoroutineExceptionHandler { _, t -> e(t.message ?: "") }) {
                requestLicenseInfo()
                d("license got state: $licenseStatus")
            }
    }
    
    suspend fun requestLicense(): String {
        d("createLicense")
        val deviceInfo = "$appGuid, $deviceModel, [${locales().joinToString()}]"
        key = restService.createLicense(deviceInfo).licenseKey
        licenseStatus = GOT_KEY
        fbAnalytics.log(LICENSE_CREATION)
        return key
    }
    
    suspend fun requestLicenseInfo(): LicenseStatus {
        d("getLicenseStatus($key)")
        licenseInfo = restService.getLicenseStatus(key)
        return licenseStatus
    }
    
    suspend fun requestActivation() {
        d("createActivation $key, $appGuid, $deviceModel")
        licenseInfo = restService.createActivation(ActivationRequest(key, appGuid, deviceModel))
        fbAnalytics.log(LICENSE_ACTIVATION)
    }
    
    suspend fun requestActivationDeletion() {
        d("remove activation for $appGuid")
        restService.deleteActivation(appGuid)
        licenseStatus = PURCHASED
        licenseInfo.activatedDevices -= licenseInfo.activatedDevices.first { it.appGuid == appGuid }
        fbAnalytics.log(LICENSE_DEACTIVATION)
    }
    
    companion object {
        fun formatKey(key: String): String {
            Assert.that(key.length == 8)
            return "${key.substring(0..3)}-${key.substring(4..7)}"
        }
    }
}