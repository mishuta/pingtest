package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.TextView
import com.mishuto.pingtest.R
import com.mishuto.pingtest.backend.rest.dto.ActivatedDevice
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.shortDateFormat
import com.mishuto.pingtest.controller.features.premium.alternative.fragments.base.AbstractToolbarFragment
import com.mishuto.pingtest.controller.features.premium.billing.Billing.license
import com.mishuto.pingtest.databinding.AltBillingLicenseInfoBinding

/**
 * Отображение информации о лицензии
 * Created by Michael Shishkin on 29.05.2024
 */
class LicenseInfoFragment : AbstractToolbarFragment() {
    private lateinit var binding: AltBillingLicenseInfoBinding
    override val menuLayout: Int = R.menu.inactivate_menu
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        tag()
        val devices = license.licenseInfo.activatedDevices
        
        binding = AltBillingLicenseInfoBinding.inflate(inflater, container, false)
        
        binding.licenseCode.text = license.formattedKey
        binding.quantity.text = getString(R.string.out_of, devices.count(), license.licenseInfo.permittedDevices)
        binding.activationsList.adapter = ListViewAdapter(requireContext(), devices)
        setupToolbarMenu(true)
        return binding.root
    }
    
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.inactivate) {
            d("removing activation")
            billingStateViewModel.nextState()
            setupToolbarMenu(false)
            true
        }
        else
            super.onOptionsItemSelected(item)
    }
    
    class ListViewAdapter(context: Context, devices: List<ActivatedDevice>) :
        ArrayAdapter<ActivatedDevice>(context, R.layout.alt_billing_activation_item, devices) {
        
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view: View = convertView ?: LayoutInflater.from(context).inflate(R.layout.alt_billing_activation_item, parent, false)
            val item = getItem(position)
            
            view.findViewById<TextView>(R.id.title).text = item?.model
            view.findViewById<TextView>(R.id.sub_title).text = item?.activationDate?.shortDateFormat()
            return view
        }
    }
}