package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.controller.features.premium.alternative.fragments.base.AbstractServerRequestFragment
import com.mishuto.pingtest.controller.features.premium.billing.Billing

/**
 * Запрос лицензии на сервере
 * Created by Michael Shishkin on 24.05.2024
 */
class LicenseRequestFragment : AbstractServerRequestFragment() {
    override val errorMessageResId: Int = R.string.error_license_request
    
    override suspend fun callRestApi() {
        Logger.d("requesting license...")
        Billing.license.requestLicense()
        doNextState()
    }
}