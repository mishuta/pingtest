package com.mishuto.pingtest.controller.features.results.list

import android.annotation.SuppressLint
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.mishuto.pingtest.common.Assert
import com.mishuto.pingtest.common.Environment.isDebugOnEmulator
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.controller.ui.ComputationProgress
import com.mishuto.pingtest.controller.ui.ComputingProgressListener
import com.mishuto.pingtest.controller.ui.ProgressDialog
import com.mishuto.pingtest.data.PersistData
import com.mishuto.pingtest.model.HostInfo
import com.mishuto.pingtest.model.PingTest
import com.mishuto.pingtest.model.ping.PingMock
import com.mishuto.pingtest.model.stat.PingStatData
import com.mishuto.pingtest.settings.SettingsPrefManager
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield

/**
 * Генератор результатов. Используется для тестовых целей и отладки
 * Created by Michael Shishkin on 06.01.2024
 */

private const val ITEMS = 800
private const val PINGS = 25

class ItemsGenerator(
    private val fragment: Fragment,
    private val adapter: RecyclerViewAdapter,
    private val emptyStateController: EmptyStateController,
    private val listData: ResultsListData
) {

    val activity = fragment.requireActivity()

    init {
        Assert.that(isDebugOnEmulator)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun generateResults(items: Int = ITEMS, pings: Int = PINGS) {
        fragment.viewLifecycleOwner.lifecycleScope.launch {
            val dialog = ProgressDialog(activity, "Generating results")
            try {
                ComputationProgress(activity.lifecycleScope, dialog).run { l -> generate(items, pings, l) }
            }
            finally {
                listData.updateResultList()
                adapter.notifyDataSetChanged()
                emptyStateController.changeVisibility()
            }
        }
    }

    companion object {
        suspend fun generate(results: Int, pings: Int, computingProgressListener: ComputingProgressListener) {
            for (i in 0 until results) {
                d("pingtest: $i")
                val pingTest = PingTest().also {
                    it.statistic = PingStatData(1, 0, 1, 1, 1, 1, 1,)
                    it.hostInfo = HostInfo("1.1.1.1", "host")
                    it.finish()
                }
                val persistData = PersistData(pingTest, SettingsPrefManager.preferences)
                for (j in 0 until pings) {
                    val ping = PingMock(10)
                    persistData.add(ping)
                    computingProgressListener.onUpdate((i * pings + j).toFloat() / (results * pings))
                    yield()
                }
                persistData.saveResult()
            }
        }
    }
}