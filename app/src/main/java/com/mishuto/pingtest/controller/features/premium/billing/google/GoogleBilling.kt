package com.mishuto.pingtest.controller.features.premium.billing.google

import android.app.Activity
import com.android.billingclient.api.BillingClient.BillingResponseCode.*
import com.android.billingclient.api.BillingResult
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Resources.getString
import com.mishuto.pingtest.common.dispatchers.EventDispatcher

interface GoogleBilling {
    val isPremiumPaid: Boolean                          // премиум версия куплена
    val errState: BillingState                          // текущее состояние биллинга
    val premiumPurchaseStatus: PremiumPurchaseStatus?   // статус покупки или null, если попыток купить не было
    val gpCountry: String?                              // страна GooglePlay
    
    fun showPremiumProductOffer(activity: Activity)     // показать предложение о покупке премиума
    fun refreshPurchaseStatus()                         // обновить текущий список покупок
}

class BillingState(result: BillingResult) {
    constructor(errorCode: Int) : this(buildBillingResult(errorCode))
    
    val errCode = result.responseCode
    val errMessage = result.debugMessage.ifEmpty { mesEnrichment(result.responseCode) }
    fun hasError(): Boolean = errCode != OK
    
    companion object {
        private val messages = mapOf(
            FEATURE_NOT_SUPPORTED   to R.string.billing_unsupported_feature,
            SERVICE_DISCONNECTED    to R.string.billing_cannot_connect,
            USER_CANCELED           to R.string.billing_user_canceled,
            SERVICE_UNAVAILABLE     to R.string.billing_service_unavailable,
            BILLING_UNAVAILABLE     to R.string.billing_unavailable,
            ITEM_UNAVAILABLE        to R.string.product_not_available,
            DEVELOPER_ERROR         to R.string.billing_dev_err,
            ERROR                   to R.string.billing_internal_error,
            ITEM_ALREADY_OWNED      to R.string.billing_already_purchased_premium,
            ITEM_NOT_OWNED          to R.string.billing_not_purchased_premium,
            NETWORK_ERROR           to R.string.billing_network_error,
        )
        
        fun mesEnrichment(code: Int): String =
            when (code) {
                in messages.keys -> getString(messages[code]!!)
                OK -> ""
                else -> getString(R.string.billing_undefined_error)
            }
    }
}

class BillingException(val errState: BillingState) : Exception(errState.errMessage)


enum class PremiumPurchaseStatus(val mesId: Int) {
    
    FAILED(R.string.purchase_failed),
    NOT_COMPLETED(R.string.purchase_not_completed),
    COMPLETED(R.string.billing_premium_is_purchased);
    
    companion object {
        const val ON_GP_PURCHASE_STATUS_CHANGED = "GoogleBilling.OnPremiumPurchaseStatusChanged"
    }
    
    fun postEvent() = EventDispatcher.post(ON_GP_PURCHASE_STATUS_CHANGED, this)
}

internal fun buildBillingResult(code: Int): BillingResult =
    BillingResult.newBuilder()
        .setResponseCode(code)
        .setDebugMessage(BillingState.mesEnrichment(code))
        .build()