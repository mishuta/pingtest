package com.mishuto.pingtest.controller.ui.whatsnew;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.mishuto.pingtest.common.Utils.getJsonFromRawResource;
import static com.mishuto.pingtest.view.Utils.dp2pix;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.divider.MaterialDivider;
import com.mishuto.pingtest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Подготовка списка фич для What's new и Version history
 */
public class FeatureHistory {

    private final Feature[] features;
    private Predicate<Feature> filter = p->true;

    public FeatureHistory() {
        try {
            JSONObject jsonRootObject = getJsonFromRawResource(R.raw.features);
            JSONArray versions = jsonRootObject.getJSONArray("versions");
            features = new Feature[versions.length()];
            parseVersionsArray(versions);
        }
        catch (Exception e) {
            throw new Error(e);
        }
    }

    public void setFilter(Predicate<Feature> filter) {
        this.filter = filter;
    }

    public Stream<Feature> getFeatureStream() {
        return Stream.of(features)
                .filter(filter);
    }

    private void parseVersionsArray(JSONArray versions) throws JSONException {
        for (int i = 0; i < versions.length(); i++) {
            JSONObject f = versions.getJSONObject(i);
            features[i] = new Feature(getVersionCode(f), getVersionName(f), getReleased(f), getDescriptionArray(f));
        }
    }

    int getVersionCode(JSONObject feature) throws JSONException {
        return feature.getInt("versionCode");
    }

    String getVersionName(JSONObject feature) throws JSONException {
        return feature.getString("versionName");
    }

    LocalDate getReleased(JSONObject feature) throws JSONException {
        String released = feature.getString("released");
        return LocalDate.parse(released, DateTimeFormatter.ofPattern("dd.MM.yy"));
    }

    String[] getDescriptionArray(JSONObject feature) throws JSONException {
        JSONArray jsonDesc = feature.getJSONArray("features");
        String[] descArr = new String[jsonDesc.length()];
        for (int j = 0; j < jsonDesc.length(); j++)
            descArr[j] = jsonDesc.getString(j);

        return descArr;
    }


    public View createHistoryListView(Context context) {
        HistoryViewBuilder viewBuilder = new HistoryViewBuilder(context);

        getFeatureStream()
                .sorted((a,b)->b.versionCode - a.versionCode)
                .forEach(viewBuilder::addFeature);

        return viewBuilder.root;
    }

    public static class Feature {
        final int versionCode;
        final String versionName;
        final LocalDate released;
        final String[] description;

        public Feature(int number, String version, LocalDate released, String[] description) {
            versionCode = number;
            versionName = version;
            this.released = released;
            this.description = description;
        }
    }

    /**
     * Хелпер для построения вьюхи со списком фич
     */
    private static class HistoryViewBuilder {
        private final Context context;
        private final ViewGroup root;

        HistoryViewBuilder(Context context) {
            this.context = context;
            root = createParentLayout();
        }

        void addFeature(Feature feature) {
            if(root.getChildCount() != 0)
                addDivider();

            String versionDate = feature.released.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT));
            addVersion(feature.versionName, versionDate);
            addDescriptions(feature.description);
        }

        void addVersion(String version, String date) {
            LinearLayout versionLayout = createVersionLayout();
            root.addView(versionLayout);
            versionLayout.addView(createVersionTextView(version));
            versionLayout.addView(createDateTextView(date));
        }

        void addDescriptions(String[] descriptions) {
            for (String desc : descriptions)
                root.addView(createDescriptionTextView(desc));
        }

        void addDivider() {
            root.addView(createDivider());
        }

        private LinearLayout createParentLayout() {
            LinearLayout layout = new LinearLayout(context);
            layout.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT));
            layout.setOrientation(LinearLayout.VERTICAL);

            return layout;
        }

        private LinearLayout createVersionLayout() {
            LinearLayout layout = new ViewBuilder<>(new LinearLayout(context))
                    .setWidthAndHigh(MATCH_PARENT, WRAP_CONTENT)
                    .setTopMargin(8)
                    .setBottomMargin(8)
                    .make();

            layout.setOrientation(LinearLayout.HORIZONTAL);
            return layout;
        }

        private TextView createVersionTextView(String value) {
            TextView textView = new ViewBuilder<>(new TextView(context, null, 0, R.style.SecondaryText))
                    .setWeight(1)
                    .make();

            textView.setText(value);
            textView.setTypeface(null, Typeface.BOLD);

            return textView;
        }

        private TextView createDateTextView(String value) {
            TextView textView = new ViewBuilder<>(new TextView(context, null, 0, R.style.Caption))
                    .make();

            textView.setText(value);
            return textView;
        }

        private TextView createDescriptionTextView(String value) {
            TextView textView = new ViewBuilder<>(new TextView(context, null, 0, R.style.PrimaryText))
                    .setBottomMargin(8)
                    .make();

            textView.setText(value);
            return textView;
        }

        private MaterialDivider createDivider() {
            return new ViewBuilder<>(new MaterialDivider(context))
                    .setWidthAndHigh(MATCH_PARENT, WRAP_CONTENT)
                    .setTopMargin(8)
                    .setBottomMargin(8)
                    .make();
        }

        @SuppressWarnings("SameParameterValue")
        private class ViewBuilder<T extends View> {
            T view;
            LinearLayout.LayoutParams params;


            ViewBuilder(T view) {
                this.view = view;
                params = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
            }

            T make() {
                view.setLayoutParams(params);
                return view;
            }

            ViewBuilder<T> setWidthAndHigh(int width, int high) {
                params.width = width > 0 ? dp(width) : width;
                params.height = high > 0 ? dp(high) : high;
                return this;
            }

            ViewBuilder<T> setTopMargin(int topMargin) {
                params.topMargin = dp(topMargin);
                return this;
            }

            ViewBuilder<T> setBottomMargin(int bottomMargin) {
                params.bottomMargin = dp(bottomMargin);
                return this;
            }

            ViewBuilder<T> setWeight(int w) {
                params.weight = (float) w;
                return this;
            }
        }

        private int dp(int dp) {
            return dp2pix(context, dp);
        }
    }
}
