package com.mishuto.pingtest.controller.services.permissions

import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import com.mishuto.pingtest.App.Companion.appContext

/**
 * Управление пермишеннами
 * Типовой Алгоритм работы с пермишенами:
 * 0. Прописать пермишен в PermissionFactory
 * 1. Вызвать tryGetPermission(). Если вернулось true - продолжить good path. Иначе объект Permission запрашивает его у ОС, п.2
 * 2. После получения колбэка needShowRequestPermissionRational опционально показать пользователю окно с пояснением и предложением принять пермишн.
 *    обязательно Вызвать onRequestPermissionRational() в параметре передать решение пользователя хочет/не хочет
 * 3. После получения колбэка onRequestPermissionResult продолжить goodPath или канселится в зависимости от ответа
 *
 * Created by Michael Shishkin on 25.10.2023
 */
interface Permission {
    // Идентификатор запрашиваемого пермишена
    val permissionID: String
    
    // Наличие пермишена
    val hasPermission: Boolean
    
    // Если wasDeclinedEarlier=true - пользователь ранее отклонял запрос.
    val wasDeclinedEarlier: Boolean
    
    // Запрос пермишена разрешен пользователем.
    // Если requestPermissionEnabled = false - пользователь отклонял RequestPermissionRational или RequestPermission в этой сессии
    val requestPermissionEnabled: Boolean
    
    // Запрос пермишена. Если пермишн есть, сразу возвращается true. Иначе возвращается false и менеджер делает попытку его получить
    fun tryGetPermission(): Boolean
    
    // Вызывается после отображения клиентом запроса на пермишен пользователю. В accepted передается решение пользователя (подтвердил/отклонил)
    // если accepted=true, менеджер отображает пользователю системный запрос на пермишен.
    // Иначе запоминает состояние и не вызывает needShowRequestPermissionRational() в этой сессии
    fun onRequestPermissionRational(accepted: Boolean)
    
    // Коллбэки, реализуемые клиентом
    interface PermissionManagerListener {
        // Требуется показать пользователю запрос на получение пермишена.
        fun needShowRequestPermissionRational()
        
        // Результат запроса пермишена
        fun onRequestPermissionResult(granted: Boolean)
    }
    
    companion object {
        fun hasPermission(permissionID: String): Boolean = checkSelfPermission(appContext, permissionID) == PERMISSION_GRANTED
    }
}
