package com.mishuto.pingtest.controller.features.results

import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.asDurationFormat
import com.mishuto.pingtest.common.msToTimeUnits
import com.mishuto.pingtest.common.pressBack
import com.mishuto.pingtest.controller.features.export.Saver
import com.mishuto.pingtest.controller.features.export.Sharer
import com.mishuto.pingtest.data.ResultEntity
import com.mishuto.pingtest.databinding.ResultDetailFragmentBinding
import com.mishuto.pingtest.model.stat.PingStatistic.Companion.overallQuality
import kotlinx.coroutines.launch
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle.MEDIUM
import java.time.format.FormatStyle.SHORT

/**
 * Детали результата теста
 * Created by Michael Shishkin on 15.08.2022
 */
class ResultDetailsFragment : Fragment() {
    
    private lateinit var entity: ResultEntity
    private val viewModel: ResultsListViewModel by activityViewModels()
    lateinit var binding: ResultDetailFragmentBinding
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        entity = viewModel.selectedItem.value!!
        d("opened entity ${entity.startTime}")
    }
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        tag()
        binding = ResultDetailFragmentBinding.inflate(inflater, container, false)
        registerBackPressBehaviour()
        
        with(binding) {
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
            startTest.setText(DateTimeFormatter.ofLocalizedDateTime(SHORT, MEDIUM).format(entity.startTime))
            pingTimeout.setText(msToTimeUnits(entity.pingTimeout))
            pingInterval.setText(msToTimeUnits(entity.pingInterval))
            host.setText(entity.host)
            ip.setText(entity.ip)
            statDuration.setText(entity.statistics.duration.asDurationFormat())
            quantity.setText("%d".format(entity.statistics.quantity))
            netType.setText(entity.mainNetType.description)
            ping.setText(getString(R.string.num_ms, entity.statistics.average))
            quality.setText(getString(R.string.percent, entity.statistics.overallQuality()))
            jitter.setText(getString(R.string.num_ms, entity.statistics.jitter))
            lost.setText(getString(R.string.percent1, entity.statistics.getLostPermille() / 10f))
            best.setText(getString(R.string.num_ms, entity.statistics.best))
            worst.setText(getString(R.string.num_ms, entity.statistics.worst))
        }
        setHasOptionsMenu(true)
        
        return binding.root
    }
    
    private fun registerBackPressBehaviour() {
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    d("back is pressed")
                    viewModel.selectedItem.value = null // сбрасываем селекшен чтобы активность не открыла потом этот фрагмент
                    isEnabled = false                   // запрещаем дальнейшую обработку handleOnBackPressed чтобы не уйти в рекурсию
                    requireActivity().onBackPressedDispatcher.onBackPressed()
                }
            })
    }
    
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.result_details_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
    
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.delete_action -> {
                d("delete action")
                viewModel.deletedEntities.markAsDeleted(entity)
                (activity as AppCompatActivity).pressBack()
                true
            }
            
            R.id.share_action -> {
                d("share action")
                viewLifecycleOwner.lifecycleScope.launch {
                    Sharer(requireActivity() as AppCompatActivity, listOf(entity)).processFile()
                }
                true
            }
            
            R.id.save_action -> {
                d("save action")
                viewLifecycleOwner.lifecycleScope.launch {
                    Saver(requireActivity() as AppCompatActivity, listOf(entity)).processFile()
                }
                true
            }
            
            android.R.id.home -> {
                d("home action")
                (activity as AppCompatActivity).pressBack()
                true
            }
            
            else -> super.onOptionsItemSelected(item)
        }
    }
}