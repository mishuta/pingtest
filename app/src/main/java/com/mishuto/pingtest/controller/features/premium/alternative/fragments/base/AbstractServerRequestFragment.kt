package com.mishuto.pingtest.controller.features.premium.alternative.fragments.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.mishuto.pingtest.backend.rest.ServerErrorHandler.tryRetrieveServerErrorResponse
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.e
import com.mishuto.pingtest.controller.features.premium.alternative.BillingStateViewModel
import com.mishuto.pingtest.databinding.AltBillingServerRequestBinding
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

/**
 * Базовый фрагмент для обращений к серверу.
 * Created by Michael Shishkin on 24.05.2024
 */

abstract class AbstractServerRequestFragment : Fragment() {
    private lateinit var binding: AltBillingServerRequestBinding
    protected val billingStateViewModel: BillingStateViewModel by activityViewModels()
    private var exceptionMessage: String? = null
    private val hasError: Boolean get() = exceptionMessage != null
    protected abstract val errorMessageResId: Int
    
    abstract suspend fun callRestApi()
    
    protected fun doNextState() = billingStateViewModel.nextState()
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        d(javaClass.simpleName)
        binding = AltBillingServerRequestBinding.inflate(inflater, container, false)
        binding.button.setOnClickListener { onRetry() }
        binding.err.errorTitle.text = getString(errorMessageResId)
        updateView()
        doRequest()
        return binding.root
    }
    
    private fun doRequest() {
        lifecycleScope.launch(CoroutineExceptionHandler { _, e -> onRequestError(e.tryRetrieveServerErrorResponse()) }) {
            callRestApi()
        }
    }
    
    private fun onRequestError(e: Throwable) {
        FirebaseCrashlytics.getInstance().recordException(e)
        e(e)
        exceptionMessage = e.message ?: "undefined error"
        updateView()
    }
    
    private fun updateView() {
        with(binding) {
            if (hasError) {
                indicator.indicatorLayout.visibility = INVISIBLE
                button.visibility = VISIBLE
                err.errorLayout.visibility = VISIBLE
                err.errorMessage.text = exceptionMessage
            }
            else {
                indicator.indicatorLayout.visibility = VISIBLE
                button.visibility = INVISIBLE
                err.errorLayout.visibility = INVISIBLE
            }
        }
    }
    
    private fun onRetry() {
        exceptionMessage = null
        updateView()
        doRequest()
    }
}

