package com.mishuto.pingtest.controller.features.export

import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.mishuto.pingtest.App.Companion.appContext
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Assert
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.data.ResultEntity
import java.io.File
import java.io.FileOutputStream

/**
 * Шаринг лога в другие приложения
 * Created by Michael Shishkin on 31.01.2024
 */
class Sharer(activity: AppCompatActivity, entities: List<ResultEntity>) : AbstractSaver(activity, entities) {

    private val fullName: String = "${appContext.filesDir}/${getFileName()}"
    private val fileType: String = "text/plain"

    override suspend fun processFile() {
        val file = File(fullName)
        createFile(FileOutputStream(file))
        share(file)
    }

    private fun share(file: File) {
        Logger.tag()
        val uri: Uri = FileProvider.getUriForFile(activity, "${appContext.packageName}.fileprovider", file)
        val intent = createIntent(uri)
        val chooserIntent = Intent.createChooser(intent, activity.getString(R.string.export_file_chooser))

        if (intent.resolveActivity(activity.packageManager) != null) {
            resolveActivities(uri, chooserIntent)
            activity.startActivity(chooserIntent)
        }
        else
            showSnack(R.string.no_activity_for_sharing)
    }

    private fun createIntent(uri: Uri) : Intent = Intent(Intent.ACTION_SEND).apply {
        type = fileType
        putExtra(Intent.EXTRA_STREAM, uri)
        addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    }

    private fun resolveActivities(uri: Uri, chooser: Intent) {
        val resolvedActivities: List<ResolveInfo> = activity.packageManager.queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY)
        Assert.that(resolvedActivities.isNotEmpty(), "activity not resolved")

        resolvedActivities.map { it.activityInfo.packageName }
            .forEach { activity.grantUriPermission(it, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION) }
    }
}

class LogFileProvider : FileProvider(R.xml.file_paths)