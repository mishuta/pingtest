package com.mishuto.pingtest.controller.features.premium.alternative.fragments.base

import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.mishuto.pingtest.R
import com.mishuto.pingtest.controller.features.premium.alternative.BillingStateViewModel

/**
 * Базовый фрагмент для фрагментов с тулбаарм
 * Created by Michael Shishkin on 21.06.2024
 */
abstract class AbstractToolbarFragment : Fragment {
    protected val billingStateViewModel: BillingStateViewModel by activityViewModels()

    constructor() : super()
    constructor(id: Int) : super(id)
    
    protected val activity: AppCompatActivity by lazy { (requireActivity() as AppCompatActivity) }
    abstract val menuLayout: Int
    
    protected fun setupToolbarMenu(enabled: Boolean) {
        activity.supportActionBar?.let {
            if (enabled) it.setHomeAsUpIndicator(R.drawable.ic_baseline_close_24) else it.setHomeAsUpIndicator(null)
            it.setDisplayHomeAsUpEnabled(enabled)
        }
        setHasOptionsMenu(enabled)
    }
    
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(menuLayout, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
    
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity.finish()
                true
            }
            
            else -> super.onOptionsItemSelected(item)
        }
    }
}