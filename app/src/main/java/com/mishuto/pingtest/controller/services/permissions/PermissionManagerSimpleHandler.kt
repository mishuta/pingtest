package com.mishuto.pingtest.controller.services.permissions

import com.mishuto.pingtest.common.Logger.d
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

/**
 * Реализация простого сценария запроса пермишена у пользователя без запроса показа Rational диалога
 * Клиенту достаточно вызвать getPermission и обработать возвращаемое значение
 * Created by Michael Shishkin on 10.02.2024
 */

suspend fun getPermission(permissionID: String): Boolean =
    suspendCoroutine { continuation ->
        // создаем объект permission handler и передаем ему обработчик onRequestPermissionResult
        val handler = PermissionManagerSimpleHandler(permissionID) { granted ->
            d("onRequestPermissionResult: $granted")
            continuation.resume(granted)
        }
        // пытаемся получить пермишн, в случае если он есть, сразу возвращаем положительный результат
        if (handler.permission.tryGetPermission()) {
            d("permission granted")
            continuation.resume(true)
        }
        // если пермишена нет и запрос пермишена не разрешен, сразу возвращаем отрицательный результат
        else if (!handler.permission.requestPermissionEnabled) {
            d("permission disabled")
            continuation.resume(false)
        }
        // в остальных случаях результат функции будет возвращен после выполнения onRequestPermissionResult
    }

suspend fun getPermissionOrThrow(permissionID: String) {
    if (!getPermission(permissionID)) throw PermissionNotGranted()
}

class PermissionManagerSimpleHandler(permissionID: String, private  val onRequestPermission: (Boolean) -> Unit) : Permission.PermissionManagerListener {
    val permission = PermissionFactory.getPermission(permissionID, this)
    
    override fun needShowRequestPermissionRational() = permission.onRequestPermissionRational(true)
    override fun onRequestPermissionResult(granted: Boolean) = onRequestPermission(granted)
}

class PermissionNotGranted: Exception()