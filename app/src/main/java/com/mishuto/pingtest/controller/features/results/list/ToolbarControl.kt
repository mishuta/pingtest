package com.mishuto.pingtest.controller.features.results.list

import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Environment.isDebugOnEmulator
import com.mishuto.pingtest.common.Resources.getString
import com.mishuto.pingtest.common.dispatchers.EventDispatcher

class ToolbarControl(root: View, private val fragment: Fragment, private val selectedGroupModel: SelectedGroupModel, private val resultsListData: ResultsListData) {

    val toolbar: Toolbar = root.findViewById(R.id.toolbar)
    private var lastIsGroupSelectedState = selectedGroupModel.isGroupSelected()
    private var lastIsAllSelectedState = isAllSelected()

    init {
        EventDispatcher.bind(SELECTION_CHANGED, "ResultList:Toolbar", fragment.lifecycle) { update() }
    }

    fun update() {
        if( selectedGroupModel.isGroupSelected()) {
            toolbar.title = selectedGroupModel.count().toString()
            toolbar.navigationIcon = ResourcesCompat.getDrawable(fragment.resources, R.drawable.ic_baseline_close_24, null )
            if(lastIsAllSelectedState != isAllSelected()) {
                fragment.requireActivity().invalidateOptionsMenu()
                lastIsAllSelectedState = isAllSelected()
            }
        }
        else {
            toolbar.title = getString(R.string.results)
            toolbar.navigationIcon = ResourcesCompat.getDrawable(fragment.resources, R.drawable.ic_baseline_arrow_back_24, null )
        }
        if( selectedGroupModel.isGroupSelected() != lastIsGroupSelectedState) {
            fragment.requireActivity().invalidateOptionsMenu()
            lastIsGroupSelectedState = selectedGroupModel.isGroupSelected()
        }
    }

    fun onCreateMenu(menu: Menu, inflater: MenuInflater) {
        val menuId = if(selectedGroupModel.isGroupSelected()) R.menu.result_group_menu
                        else if(isDebugOnEmulator) R.menu.result_debug_menu
                        else R.menu.empty

        inflater.inflate(menuId, menu)

        if(selectedGroupModel.isGroupSelected() && isAllSelected()) {
            menu.findItem(R.id.action_check_all).setVisible(false)
            menu.findItem(R.id.action_discard_all).setVisible(true)
        }
    }

    private fun isAllSelected() = selectedGroupModel.count() == resultsListData.count()
}