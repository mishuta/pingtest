package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.controller.features.premium.alternative.fragments.base.AbstractServerRequestFragment
import com.mishuto.pingtest.controller.features.premium.billing.Billing.license

/**
 * Запрос деактивации лицензии
 * Created by Michael Shishkin on 06.06.2024
 */
class RemoveActivationRequestFragment : AbstractServerRequestFragment() {
    override val errorMessageResId: Int = R.string.error_activation_remove
    
    override suspend fun callRestApi() {
        Logger.tag()
        license.requestActivationDeletion()
        billingStateViewModel.nextState()
    }
}