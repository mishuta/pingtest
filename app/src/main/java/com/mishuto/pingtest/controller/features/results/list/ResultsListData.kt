package com.mishuto.pingtest.controller.features.results.list

import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.mishuto.pingtest.common.Assert
import com.mishuto.pingtest.controller.features.results.ResultsListViewModel
import com.mishuto.pingtest.data.ResultEntity
import com.mishuto.pingtest.data.resultDAO

// данные для списка объектов с учетом элементов, помеченных удаленными
class ResultsListData(fragment: Fragment, activityViewModel: ResultsListViewModel) {
    private val deletedEntities = activityViewModel.deletedEntities
    var resultsList : List<ResultEntity> = resultDAO.getResultsInReverseOrder()
    lateinit var resultsFilteredList : MutableList<ResultEntity>
    private val currentSource: List<ResultEntity>
        get() = if(deletedEntities.markedAsDeleted) resultsFilteredList else resultsList

    init {
        fragment.lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onResume(owner: LifecycleOwner) { updateResultList() }})
        if(deletedEntities.markedAsDeleted)
            onMarked()
    }

    fun onMarked() {
        resultsFilteredList = resultsList.toMutableList()
        for(entity in deletedEntities.markedEntities.value!!)
            resultsFilteredList.remove(entity)
    }

    fun updateResultList() {
        if(resultDAO.getCount() != resultsList.size)
            resultsList = resultDAO.getResultsInReverseOrder()
    }

    fun contains(entity: ResultEntity) = currentSource.contains(entity)
    fun count() : Int  = currentSource.size
    fun getItem(position: Int): ResultEntity = currentSource[position]
    fun getPosition(entity: ResultEntity) = currentSource.indexOf(entity)
        .also {Assert.that(it >= 0, "entity ${entity.testId} not found") }

}