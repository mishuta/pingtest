package com.mishuto.pingtest.controller.ui.log;

import android.view.View;
import android.widget.ImageView;

import com.mishuto.pingtest.common.DeferredExecutor;
import com.mishuto.pingtest.model.ErrorMessageEvent;
import com.mishuto.pingtest.model.NetTypeChangeEvent;
import com.mishuto.pingtest.data.AppDatabaseKt;
import com.mishuto.pingtest.data.PingEntity;
import com.mishuto.pingtest.model.ping.Ping;
import com.mishuto.pingtest.view.LogView;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Контроллер виджета лога
 * Created by Michael Shishkin on 05.08.2020
 */
public class LogController {
    private final LogView mLogView;
    private final ImageView mEmptyStateView;
    private boolean isEmpty = true;
    private final DeferredExecutor mDeferredExecutor = new DeferredExecutor();

    // когда лог превышает MAX_LINES_WRAP строк, он обрезается до MIN_LINES_WRAP
    private final static int MIN_LINES_WRAP = 200; // количество строк до которого заворачивается лог
    private final static int MAX_LINES_WRAP = 300;  // количество строк после которого выполнгяется заворачивание лог

    public LogController(LogView view, ImageView emptyStateView) {
        mLogView = view;
        mEmptyStateView = emptyStateView;
        mLogView.setOnInitCompleteListener(mDeferredExecutor::onDeferredEvent);
    }

    public void show(boolean visibility) {
        mLogView.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
        mEmptyStateView.setVisibility(visibility && isEmpty ? View.VISIBLE : View.INVISIBLE);
    }

    public void clear() {
        showEmptyStateView(true);
        mLogView.clear();
    }

    public void log(Ping ping) {
        addLine(LogFormatter.INSTANCE.format(ping, mLogView.getContext()));
    }

    public void log(NetTypeChangeEvent event) {
        addLine(LogFormatter.INSTANCE.format(event));
    }

    public void log(ErrorMessageEvent event) {
        addLine(LogFormatter.INSTANCE.format(event, mLogView.getContext()));
    }

    public void addLine(CharSequence line) {
        showEmptyStateView(false);

        if(mLogView.getPrintedLineCount() > MAX_LINES_WRAP)                // заворачиваем лог, если надо
            mLogView.removeFirstLines(MAX_LINES_WRAP - MIN_LINES_WRAP);

        mLogView.addLine(line);
    }

    public void loadFromDb(long testId) {
        mDeferredExecutor.execute(() -> {
            mLogView.clear();
            getLogEventStrings(testId).forEach(this::addLine);
        });
    }

    private Collection<CharSequence> getLogEventStrings(long testId) {
        TreeMap<LocalDateTime, CharSequence> eventStringMap = loadPingEventStrings(testId);

        if(!eventStringMap.isEmpty())
            eventStringMap.putAll(loadNetChangeEventStringsAfter(testId, eventStringMap.firstKey()));

        eventStringMap.putAll(loadErrMessageEventStrings(testId));

        return eventStringMap.values();
    }

    private TreeMap<LocalDateTime, CharSequence> loadPingEventStrings(long testId) {
        return AppDatabaseKt.getPingDAO().getLastElementsForTest(testId, MAX_LINES_WRAP)
                .stream().collect(Collectors.toMap(
                        PingEntity::getTimestamp,
                        ping -> LogFormatter.INSTANCE.format(ping, mLogView.getContext()),
                        (v1, v2) -> v1,
                        TreeMap::new));
    }

    private Map<LocalDateTime, CharSequence> loadNetChangeEventStringsAfter(long testId, LocalDateTime startTime) {
        return AppDatabaseKt.getNetChangeDAO().getTest(testId).stream()
                .filter(event -> event.getTimestamp().isAfter(startTime))
                .collect(Collectors.toMap(NetTypeChangeEvent::getTimestamp, LogFormatter.INSTANCE::format)); //todo crash если два события имеют одинаковый timestamp (очень редко)
    }

    private Map<LocalDateTime, CharSequence> loadErrMessageEventStrings(long testId) {
        return AppDatabaseKt.getErrMessageDAO().getTest(testId).stream()
                .collect(Collectors.toMap(ErrorMessageEvent::getTimestamp, event -> LogFormatter.INSTANCE.format(event, mLogView.getContext())));
    }

    private void showEmptyStateView(boolean isEmptyVisible) {
        isEmpty = isEmptyVisible;
        mEmptyStateView.setVisibility(mLogView.getVisibility() == View.VISIBLE && isEmpty ? View.VISIBLE : View.INVISIBLE);
    }
}
