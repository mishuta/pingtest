package com.mishuto.pingtest.controller.main;

import static com.mishuto.pingtest.common.Utils.createDialogBuilder;
import static com.mishuto.pingtest.common.Utils.logger;
import static com.mishuto.pingtest.model.PingHolder.RESOLVE_TIMEOUT;

import android.app.Activity;
import android.content.DialogInterface;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.tickers.Ticker;
import com.mishuto.pingtest.settings.SettingsPrefManager;

/**
 * Контроллер прогресс-бара долгого резолва
 * Если резолвинг длится больше чем START_DELAY мс, появляется диалог с прогрессбаром.
 * Прогрессбар обновляется каждые INTERVAL мс, до тех пор пока не произойдет резолв или таймаут резолва
 * Created by Michael Shishkin on 27.08.2020
 */
public class ResolveProgress {

    private final static int START_DELAY = 1000;
    private final static int INTERVAL = 100;

    private final AlertDialog mDialog;
    private final Activity mActivity;
    private Ticker mTicker;
    private ProgressBar mProgressBar;
    private int elapsedTime;
    private final Runnable mCancelListener;
    private boolean resolveInProgress = false;

    public ResolveProgress(Activity activity, Runnable cancelListener) {
        mActivity = activity;
        mCancelListener = cancelListener;
        mDialog = createDialogBuilder(activity)
                .setView(R.layout.alert_progress)
                .setOnDismissListener(this::onDismiss)
                .create();
    }

    public void start() {
        elapsedTime = START_DELAY;
        mTicker = new Ticker(this::showProgress, false, START_DELAY, "--showResolveDialogTicker");
        mTicker.start();
        resolveInProgress = true;
    }

    public void stop() {
        resolveInProgress = false;
        if(mTicker != null)
            mTicker.stop();
        if(mDialog.isShowing() && !mActivity.isDestroyed())
            mDialog.dismiss();

    }

    private void onDismiss(DialogInterface dialogInterface) {
        if(mTicker.stop())
            mCancelListener.run();
    }

    private void showProgress() {
        mActivity.runOnUiThread(()-> {
            if (!resolveInProgress || mActivity.isFinishing())
                return;

            logger.d("showing resolve dialog");
            mDialog.setTitle(mActivity.getString(R.string.resolving, SettingsPrefManager.INSTANCE.getPreferences().getHost()));
            mDialog.show();
            mProgressBar = mDialog.findViewById(R.id.progressBar);
            //noinspection ConstantConditions
            mProgressBar.setMax(RESOLVE_TIMEOUT);
            mProgressBar.setProgress(elapsedTime);
            mTicker = new Ticker(this::accelerate, true, INTERVAL, "--accelerateProgressResolveDialogTicker");
            mTicker.start();
        });

    }

    private void accelerate() {
        mActivity.runOnUiThread(()-> {
            elapsedTime += INTERVAL;
            mProgressBar.setProgress(elapsedTime, true);

            if (elapsedTime > RESOLVE_TIMEOUT)
                stop();
        });
    }
}
