package com.mishuto.pingtest.controller.features.premium.billing.google

import android.app.Activity
import com.android.billingclient.api.*
import com.android.billingclient.api.BillingClient.BillingResponseCode.*
import com.android.billingclient.api.Purchase.PurchaseState.PENDING
import com.android.billingclient.api.Purchase.PurchaseState.PURCHASED
import com.mishuto.pingtest.App
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.CrashlyticsKeys
import com.mishuto.pingtest.common.CrashlyticsKeys.Tag.BILLING
import com.mishuto.pingtest.common.Environment.isDebug
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.Resources.getBoolean
import com.mishuto.pingtest.common.SharedPref
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.controller.features.premium.billing.google.PremiumPurchaseStatus.COMPLETED
import com.mishuto.pingtest.controller.features.premium.billing.google.PremiumPurchaseStatus.FAILED
import com.mishuto.pingtest.controller.features.premium.billing.google.PremiumPurchaseStatus.NOT_COMPLETED

/**
 * Управление покупкой премиальной версии
 * Created by Michael Shishkin on 13.09.2023
 *
 * При запуске аппы устанавливается связь с GP и скачивается список продуктов для покупки queryProductDetailsFromGP() и список покупок queryPurchasesFromGP().
 *  Если премиум куплен (isPremiumPaid=true), то аппа это понимает и становится премиальной
 * Если пользователь хочет купить, должен быть вызван showPremiumProductOffer(). После завершения покупки вызывается колбэк purchaseUpdate после чего
 * устанавливается флаг isPremiumPaid
 * До вызова showPremiumProductOffer() желательно вызвать refreshPurchaseStatus() (асинхронный метод) чтобы обновить статус покупки.
 */

internal const val PREMIUM_PRODUCT_ID = "pingmon_premium"   //ID продукта в GPC. НЕ МЕНЯТЬ!
private const val PREMIUM_PURCHASED = "premiumPurchased"
internal const val MAX_CONNECTION_ATTEMPTS = 3
const val ON_GP_PAID_STATE_CHANGED = "GoogleBilling.OnPremiumPaidStateChanged"

object GoogleBillingImpl : GoogleBilling, BillingClientStateListener {
    // начальное значение факта покупки берется из SP, чтобы избежать ситуации, когда аппа не знает что она премиум, если GP недоступен
    override var isPremiumPaid = SharedPref.getBoolean(PREMIUM_PURCHASED, false)
        // В дебаг версии премиум определяется настройкой. (прим. в эмуляторе используется другая имплементация)
        get() = field || (isDebug && getBoolean(R.bool.debug_premium_on_start))
        private set(value) {
            d("isPremiumPaid=$value")
            if (value != field) {
                field = value
                SharedPref.putBoolean(PREMIUM_PURCHASED, field)
                CrashlyticsKeys.update(BILLING)
                EventDispatcher.post(ON_GP_PAID_STATE_CHANGED, field)
            }
        }
    
    override val errState: BillingState
        get() = BillingState(lastResult)
    
    override var premiumPurchaseStatus: PremiumPurchaseStatus? = null
        private set(value) {
            d("purchaseState=$value")
            field = value
            CrashlyticsKeys.update(BILLING)
        }
    
    override var gpCountry: String? = null
    
    
    private val client: BillingClient
    private var lastResult: BillingResult = buildBillingResult(OK)
    private lateinit var gpProductsDetails: List<ProductDetails>
    private var connectionAttempts = 0
    
    @Throws(BillingException::class)
    override fun showPremiumProductOffer(activity: Activity) {
        tag()
        validateOfferConditions()
        
        val productDetailsParamsList = listOf(
            BillingFlowParams.ProductDetailsParams.newBuilder()
                .setProductDetails(getPremiumDetails())
                .build()
        )
        
        val billingFlowParams = BillingFlowParams.newBuilder()
            .setProductDetailsParamsList(productDetailsParamsList)
            .build()
        
        lastResult = client.launchBillingFlow(activity, billingFlowParams)
        d("launchBillingFlow lastResult $lastResult")
        
        if (lastResult.responseCode != OK)
            throw BillingException(errState)
    }
    
    private fun validateOfferConditions() {
        tag()
        if (lastResult.responseCode != OK)
            throw BillingException(errState)
        
        if (!client.isReady)
            throw BillingException(BillingState(SERVICE_DISCONNECTED))
        
        if (isPremiumPaid)
            throw BillingException(BillingState(ITEM_ALREADY_OWNED))
        
        if (gpProductsDetails.isEmpty())
            throw BillingException(BillingState(ITEM_UNAVAILABLE))
        
        d("validation is successfully")
    }
    
    private fun getPremiumDetails(): ProductDetails =
        gpProductsDetails.find { it.productId == PREMIUM_PRODUCT_ID } ?: throw BillingException(BillingState(ITEM_UNAVAILABLE))
    
    init {
        tag()
        client = BillingClient.newBuilder(App.appContext)
            .setListener { result, purchases -> purchasesUpdate(result, purchases) }
            .enablePendingPurchases(
                PendingPurchasesParams.newBuilder()
                .enableOneTimeProducts()
                .build())
            .build()
        
        startConnection()
        d("Billing initialized")
    }
    
    override fun refreshPurchaseStatus() {
        if (client.isReady)
            queryPurchases()
        else {
            connectionAttempts = 0
            startConnection()
        }
    }
    
    private fun startConnection() {
        d("ready: ${client.isReady}")
        if (!client.isReady) {
            gpProductsDetails = listOf()
            lastResult = BillingResult.newBuilder().setResponseCode(OK).build()
            client.startConnection(this)
            d("connection starting")
        }
    }
    
    override fun onBillingSetupFinished(billingResult: BillingResult) {
        d("result: $billingResult")
        lastResult = billingResult
        if (billingResult.responseCode == OK) {
            queryProductDetails()
            queryPurchases()
            queryConfig()
        }
    }
    
    override fun onBillingServiceDisconnected() {
        d("attempt: $connectionAttempts")
        lastResult = buildBillingResult(SERVICE_DISCONNECTED)
        if (connectionAttempts++ < MAX_CONNECTION_ATTEMPTS)
            startConnection()
    }
    
    private fun purchasesUpdate(result: BillingResult, purchases: MutableList<Purchase>?) {
        d("result: $result. Purchases: ${purchases?.joinToString { it.originalJson }}")
        lastResult = result
        isPremiumPaid = false
        
        if (result.responseCode == OK)
            purchases?.find { it.isPremiumPurchaseCompleted() }
                ?.let {
                    isPremiumPaid = true
                    premiumPurchaseStatus = COMPLETED
                    it.acknowledgePurchase()
                }
                ?: run { premiumPurchaseStatus = NOT_COMPLETED }
        else
            premiumPurchaseStatus = FAILED
        
        premiumPurchaseStatus?.postEvent()
    }
    
    private fun Purchase.acknowledgePurchase() {
        d("isAcknowledged=$isAcknowledged")
        if (!isAcknowledged) {
            val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                .setPurchaseToken(purchaseToken)
                .build()
            
            client.acknowledgePurchase(acknowledgePurchaseParams) { result ->
                lastResult = result
                d("Acknowledge result is $result")
            }
        }
    }
    
    private fun queryProductDetails() {
        tag()
        val queryPremium = QueryProductDetailsParams.Product.newBuilder()
            .setProductId(PREMIUM_PRODUCT_ID)
            .setProductType(BillingClient.ProductType.INAPP)
            .build()
        
        val queryProductDetailsParams = QueryProductDetailsParams.newBuilder()
            .setProductList(listOf(queryPremium))
            .build()
        
        client.queryProductDetailsAsync(queryProductDetailsParams) { billingResult, productDetailsList ->
            lastResult = billingResult
            d("queryProductDetailsAsync result: $lastResult")
            if (billingResult.responseCode == OK) {
                gpProductsDetails = productDetailsList
            }
            d("gpProductDetails: ${gpProductsDetails.joinToString { it.name }}")
        }
    }
    
    private fun queryPurchases() {
        tag()
        val purchasesParams = QueryPurchasesParams.newBuilder()
            .setProductType(BillingClient.ProductType.INAPP)
            .build()
        
        client.queryPurchasesAsync(purchasesParams) { result, purchases ->
            lastResult = result
            d("queryPurchasesAsync result: $result. Purchases: ${purchases.joinToString { it.originalJson }}")
            
            val premiumPurchase = purchases.find { it.products.contains(PREMIUM_PRODUCT_ID) }
            premiumPurchaseStatus = when (premiumPurchase?.purchaseState) {
                PURCHASED -> COMPLETED
                PENDING, 4 -> NOT_COMPLETED
                else -> null
            }
            
            if (result.responseCode == OK)
                isPremiumPaid = purchases.any { it.isPremiumPurchaseCompleted() }
        }
    }
    
    private fun queryConfig() {
        tag()
        client.getBillingConfigAsync(GetBillingConfigParams.newBuilder().build()) { billingResult, config ->
            d("getBillingConfigAsync result: $billingResult")
            if (billingResult.responseCode == OK) {
                gpCountry = config?.countryCode
                d("countryCode is: $gpCountry")
            }
        }
    }
    
    private fun Purchase.isPremiumPurchaseCompleted() = products.contains(PREMIUM_PRODUCT_ID) && purchaseState == PURCHASED
}