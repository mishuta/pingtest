package com.mishuto.pingtest.controller.services.permissions

import android.Manifest.permission.POST_NOTIFICATIONS
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.TIRAMISU
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.AlertQueManager
import com.mishuto.pingtest.common.Logger.i
import com.mishuto.pingtest.common.Utils.createDialogBuilder
import com.mishuto.pingtest.service.ServiceManager

/**
 * Включает POST_NOTIFICATIONS permission для Android 33+ (Tiramisu+)
 * Created by Michael Shishkin on 26.10.2023
 */
class NotificationPermissionController(private val alertQueManager: AlertQueManager, private val activity: AppCompatActivity) :
    Permission.PermissionManagerListener {
    
    private lateinit var permission: Permission
    
    init {
        if (SDK_INT >= TIRAMISU) {
            permission = PermissionFactory.getPermission(POST_NOTIFICATIONS, this)
            
            if (!permission.hasPermission) {
                ServiceManager.bind("NotificationPermissionController") {
                    if (ServiceManager.isStarted())
                        getPermission()
                    ServiceManager.unbind("NotificationPermissionController")
                }
            }
        }
    }
    
    fun getPermission() {
        if (SDK_INT >= TIRAMISU)
            permission.tryGetPermission()
    }
    
    override fun needShowRequestPermissionRational() {
        val dialog = createDialogBuilder(activity)
            .setTitle(R.string.permission_req_title)
            .setPositiveButton(R.string.proceed) { _, _ -> permission.onRequestPermissionRational(true) }
            .setMessage(R.string.permission_notification_rational)
            .create()
        
        alertQueManager.enqueue("NotificationPermissionRationalDialog", dialog) { permission.onRequestPermissionRational(false) }
    }
    
    @RequiresApi(TIRAMISU)
    override fun onRequestPermissionResult(granted: Boolean) {
        i("$POST_NOTIFICATIONS granted: $granted")
    }
}