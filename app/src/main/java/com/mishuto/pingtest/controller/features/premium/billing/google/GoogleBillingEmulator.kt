@file:Suppress("SENSELESS_COMPARISON")

package com.mishuto.pingtest.controller.features.premium.billing.google

import android.app.Activity
import com.android.billingclient.api.BillingClient.BillingResponseCode.OK
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.CrashlyticsKeys
import com.mishuto.pingtest.common.Resources.getBoolean
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.common.tickers.Ticker

/**
 * Реализация биллинга для отладки GUI на эмуляторе
 * Created by Michael Shishkin on 20.10.2023
 */

@Suppress("MemberVisibilityCanBePrivate")
object GoogleBillingEmulator : GoogleBilling {
    // настройки поведения
    var premiumOnStart = getBoolean(R.bool.emulator_premium_on_start)
    var errOn_init = OK
    var makePremiumOn_init_WithDelay = false
    var purchaseStateOn_init_WithDelay: PremiumPurchaseStatus? = null
    
    var errAfter_showPremiumProductOffer = OK
    
    var purchaseStateAfter_onPurchase = PremiumPurchaseStatus.COMPLETED
    var errAfter_onPurchase = OK
    
    var delay = 500
    
    /* errors:
        OK,
        FEATURE_NOT_SUPPORTED,
        SERVICE_DISCONNECTED,
        USER_CANCELED,
        SERVICE_UNAVAILABLE,
        BILLING_UNAVAILABLE,
        ITEM_UNAVAILABLE,
        DEVELOPER_ERROR,
        ERROR,
        ITEM_ALREADY_OWNED,
        ITEM_NOT_OWNED,
        NETWORK_ERROR,
        100500
    */
    override var gpCountry: String? = null
    
    
    override var isPremiumPaid: Boolean = premiumOnStart
        set(value) {
            field = value
            CrashlyticsKeys.update(CrashlyticsKeys.Tag.BILLING)
            EventDispatcher.post(ON_GP_PAID_STATE_CHANGED, field)
        }
    override var errState: BillingState = BillingState(OK)
    override var premiumPurchaseStatus: PremiumPurchaseStatus? = null
    
    init {
        errState = BillingState(errOn_init)
        if (makePremiumOn_init_WithDelay)
            Ticker({ isPremiumPaid = true }, false, delay, "Billing").start()
        
        if (purchaseStateOn_init_WithDelay != null)
            Ticker(
                {
                    premiumPurchaseStatus = purchaseStateOn_init_WithDelay
                    premiumPurchaseStatus!!.postEvent()
                }, false, delay, "Billing").start()
    }
    
    override fun refreshPurchaseStatus() {
        errState = BillingState(OK)
    }
    
    override fun showPremiumProductOffer(activity: Activity) {
        errState = BillingState(errAfter_showPremiumProductOffer)
        if (errState.hasError())
            throw BillingException(errState)
        
        if (purchaseStateAfter_onPurchase != null) {
            
            Ticker({
                premiumPurchaseStatus = purchaseStateAfter_onPurchase
                premiumPurchaseStatus!!.postEvent()
                errState = BillingState(errAfter_onPurchase)
                isPremiumPaid = premiumPurchaseStatus == PremiumPurchaseStatus.COMPLETED
            }, false, delay, "Billing").start()
        }
    }
}