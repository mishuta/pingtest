package com.mishuto.pingtest.controller.features.results.list

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mishuto.pingtest.R

class EmptyStateController(root: View, private val adapter: RecyclerViewAdapter) : RecyclerView.AdapterDataObserver() {
    private val text: TextView = root.findViewById(R.id.empty_state_mes)
    private val image : ImageView = root.findViewById(R.id.empty_state_ico)

    init {
        adapter.registerAdapterDataObserver(this)
        changeVisibility()
    }

    override fun onItemRangeInserted(positionStart: Int, itemCount: Int) = changeVisibility()
    override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) =  changeVisibility()

    fun changeVisibility() {
        val visibility = if(adapter.itemCount == 0) View.VISIBLE else View.INVISIBLE
        text.visibility = visibility
        image.visibility = visibility
    }
}