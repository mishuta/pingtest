package com.mishuto.pingtest.controller.features.premium.alternative

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.mishuto.pingtest.R
import com.mishuto.pingtest.backend.rest.ServerConfig.purchaseCompleteDeepLink
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.replaceFragment
import com.mishuto.pingtest.controller.features.premium.alternative.AltBillingState.*
import com.mishuto.pingtest.controller.features.premium.alternative.fragments.*

/**
 * Активность, управляет процессом покупки в юмани
 * Created by Michael Shishkin on 14.05.2024
 */

class AltBillingActivity : AppCompatActivity(R.layout.alt_billing_activity) {
    private val billingStateViewModel: BillingStateViewModel by viewModels()
    private var currentState: AltBillingState? = null
    private val state get() = billingStateViewModel.state
    private var isFirstLaunch: Boolean = true
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isFirstLaunch = savedInstanceState == null
        state.value = computeInitialState()
        d("init state: ${state.value!!.name}")
        
        state.observe(this) { onStatusChange(it) }
    }
    
    private fun computeInitialState(): AltBillingState {
        d("Intent data=${intent.data}]")
        return when {
            intent.getBooleanExtra(FORCE_ACTIVATION, false) -> ACTIVATE     // запуск активации
            intent.data != null
                    && intent.data!!.toString() == purchaseCompleteDeepLink
                    && state.value!! <= PURCHASE_FORM -> PURCHASE_COMPLETED    // deep link из успешной покупки
            isFirstLaunch && state.value!! > DISCLAIMER -> LICENSE_CHECK    // обновляем статус лицензии при запуске активности
            else -> state.value!!
        }
    }
    
    private fun onStatusChange(newState: AltBillingState) {
        d("$currentState -> $newState")
        if (currentState != newState) {
            currentState = newState
            
            when (currentState!!) {
                DISCLAIMER -> setFragment(DisclaimerFragment(), R.string.agreement)
                LICENSE_REQUEST -> setFragment(LicenseRequestFragment(), R.string.license_request)
                LICENSE_CHECK -> setFragment(CheckLicenseRequestFragment(), R.string.license_check)
                PURCHASE_FORM -> setFragment(PurchaseFormFragment(), R.string.license_purchase)
                PURCHASE_COMPLETED -> setFragment(PurchaseConfirmationRequestFragment(), R.string.purchase_confirmation)
                PURCHASE_CONFIRMED -> setFragment(PurchaseConfirmFragment(), R.string.purchase_confirmation)
                ACTIVATE -> setFragment(ActivationFragment(), R.string.activate_license)
                ACTIVATION_REQUEST -> setFragment(ActivateRequestFragment(), R.string.license_activation)
                LICENSE_INFO -> setFragment(LicenseInfoFragment(), R.string.license)
                INACTIVATION_REQUEST -> setFragment(RemoveActivationRequestFragment(), R.string.removing_activation)
                INACTIVATED -> setFragment(InactivatedFragment(), R.string.removing_activation)
            }
        }
    }
    
    private fun setFragment(fragment: Fragment, titleId: Int) {
        setTitle(titleId)
        replaceFragment(fragment)
    }
    
    companion object {
        private const val FORCE_ACTIVATION = "forceActivationKey"
        
        fun Activity.startAltBillingActivity(forceActivation: Boolean = false) {
            val intent = Intent(this, AltBillingActivity::class.java)
            intent.putExtra(FORCE_ACTIVATION, forceActivation)
            startActivity(intent)
        }
    }
}