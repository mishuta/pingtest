package com.mishuto.pingtest.controller.features.premium.billing

import com.mishuto.pingtest.common.Environment
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.common.locales
import com.mishuto.pingtest.controller.features.premium.billing.alternative.ALT_BILLING_ACTIVATION_EVENT
import com.mishuto.pingtest.controller.features.premium.billing.alternative.License
import com.mishuto.pingtest.controller.features.premium.billing.google.*

/**
 * Управление биллингом
 * Created by Michael Shishkin on 08.05.2024
 */
object Billing {
    const val CHANGE_PREMIUM_STATE = "Billing.changePremiumState"
    
    val googleBilling: GoogleBilling = if (Environment.isDebugOnEmulator) GoogleBillingEmulator else GoogleBillingImpl
    val license = License()
    
    val isPremium
        get() = license.isActivated || googleBilling.isPremiumPaid  // сначала проверяем license, чтобы не грузить google без необходимости
    
    val billingType: BillingType
        get() = when {
            license.isActivated -> BillingType.ALTERNATIVE
            googleBilling.isPremiumPaid -> BillingType.GOOGLE_PLAY
            else -> BillingType.NONE
        }
    
    init {
        fun sendPremiumMessage(isPremium: Boolean) = EventDispatcher.post(CHANGE_PREMIUM_STATE, isPremium)
        
        EventDispatcher.register<Boolean>(ON_GP_PAID_STATE_CHANGED, "Billing") { sendPremiumMessage(it) }
        EventDispatcher.register<Boolean>(ALT_BILLING_ACTIVATION_EVENT, "Billing") { sendPremiumMessage(it) }
    }
    
    fun alternativeBillingConditionsMet() =
        !isPremium &&
                googleBilling.premiumPurchaseStatus in listOf(null, PremiumPurchaseStatus.FAILED) &&
                (googleBilling.gpCountry in listOf("BY", "RU") || locales().any { it.country in listOf("BY", "RU") })
    
    enum class BillingType {
        NONE, GOOGLE_PLAY, ALTERNATIVE
    }
}