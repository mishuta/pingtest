package com.mishuto.pingtest.controller.features.help

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Utils
import com.mishuto.pingtest.controller.ui.ArticleFragment

/**
 * Компонент HelpFragment, реализующий классы для поддержки recyclerView со списком статей
 * Created by Michael Shishkin on 27.08.2022
 */

data class Article(val titleId : Int, val contentId : Int) {
    val title: String = Utils.getString(titleId)
}

class HelpArticleRecyclerViewController(private val helpFragment: HelpFragment, recyclerView: RecyclerView) {
    init {
        recyclerView.layoutManager = LinearLayoutManager(helpFragment.context)
        recyclerView.adapter = ArticleAdapter()
    }

    inner class ArticleAdapter : RecyclerView.Adapter<ArticleViewHolder>() {

        private val articles = listOf(
            Article(R.string.how_it_works_title, R.string.how_it_works),
            Article(R.string.why_measure_incorrect_title, R.string.why_measure_incorrect),
            Article(R.string.background_settings_title, R.string.background_settings),
            Article(R.string.permission_access_call_title, R.string.permission_access_call),
            Article(R.string.considerations_title, R.string.considerations),
        )

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
            val view: View = LayoutInflater.from(parent.context).inflate(R.layout.help_article_item, parent, false)
            return ArticleViewHolder(view)
        }

        override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
            holder.onBind(articles[position])
        }

        override fun getItemCount(): Int = articles.size
    }

    inner class ArticleViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun onBind(article: Article) {
            itemView.findViewById<TextView>(R.id.title).text = article.title
            itemView.setOnClickListener { helpFragment.showFragment(ArticleFragment(article)) }
        }
    }
}
