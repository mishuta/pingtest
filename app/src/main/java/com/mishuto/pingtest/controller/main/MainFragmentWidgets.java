package com.mishuto.pingtest.controller.main;

import static com.mishuto.pingtest.common.Utils.getParcelable;
import static com.mishuto.pingtest.common.Utils.getString;
import static com.mishuto.pingtest.model.indicators.GroupThresholds.GAMES;
import static com.mishuto.pingtest.model.indicators.GroupThresholds.OVERALL;
import static com.mishuto.pingtest.model.indicators.GroupThresholds.VIDEO;
import static com.mishuto.pingtest.model.indicators.GroupThresholds.VOIP;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.tabs.TabLayout;
import com.mishuto.pingtest.App;
import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.TextExtender;
import com.mishuto.pingtest.common.TimeFormatterKt;
import com.mishuto.pingtest.controller.ui.log.LogController;
import com.mishuto.pingtest.controller.main.progress.IndicatorProgress;
import com.mishuto.pingtest.controller.main.progress.OverallProgress;
import com.mishuto.pingtest.controller.main.progress.ProgressController;
import com.mishuto.pingtest.data.AppDatabaseKt;
import com.mishuto.pingtest.databinding.ContentMainBinding;
import com.mishuto.pingtest.model.HostInfo;
import com.mishuto.pingtest.model.indicators.GroupIndicators;
import com.mishuto.pingtest.model.ping.Ping;
import com.mishuto.pingtest.model.stat.PingStatistic;
import com.mishuto.pingtest.settings.preferences.host.HostListActivity;
import com.mishuto.pingtest.view.GraphView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Виджеты главного окна
 * Created by Michael Shishkin on 27.10.2020
 */
//todo нужно ослабить зависимость между фрагментом и виджетами
// идея в том, чтобы каждый виджет стал отдельным объектом, который сам подписывается и сам обрабатывает события изменения и сохранения; логикой изменения цветов
// события генерит фрагмент. События определяются в отдельном интерфейсе. апдейт д.б с дефолтными методами. Например onHostUpdate(hostInfo)
public class MainFragmentWidgets  {

    private final ContentMainBinding mBinding;              // виджеты активности
    private final List<ProgressController> mProgressControllers = new ArrayList<>(); // набор контроллеров прогрессбаров (4 шт)
    private final LogController mLogController;             // контроллер окна лога
    private final LostWidget lostWidget;

    public MainFragmentWidgets(ContentMainBinding binding) {
        mBinding = binding;

        mLogController = new LogController(mBinding.history.log, mBinding.history.icon);

        mProgressControllers.add(new OverallProgress(mBinding.overallLayout.netQualityValue, mBinding.overallLayout.overallProgressBar, mBinding.overallLayout.netCheckIco, new GroupIndicators(OVERALL)));
        mProgressControllers.add(new IndicatorProgress(mBinding.gameProgress.gameVal, mBinding.gameProgress.gameProgressbar, mBinding.gameProgress.gameIco, new GroupIndicators(GAMES)));
        mProgressControllers.add(new IndicatorProgress(mBinding.vcsProgress.vcsValue, mBinding.vcsProgress.vcsProgressbar, mBinding.vcsProgress.vcsImg, new GroupIndicators(VOIP)));
        mProgressControllers.add(new IndicatorProgress(mBinding.videoProgress.videoVal, mBinding.videoProgress.videoProgressbar, mBinding.videoProgress.videoImg, new GroupIndicators(VIDEO)));
        alignProgressLabels();

        mBinding.history.tabsHeader.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {    // поведение при смене вкладок гистограмма/лог
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                boolean isLogTab = tab.getPosition() == 1;
                mBinding.history.graph.setVisibility(isLogTab ? View.INVISIBLE : View.VISIBLE);
                mLogController.show(isLogTab);
            }

            public void onTabUnselected(TabLayout.Tab tab) { }
            public void onTabReselected(TabLayout.Tab tab) { }
        });

        mBinding.hostLayout.setOnClickListener(v-> HostListActivity.start(mBinding.getRoot().getContext()));
        lostWidget = new LostWidget();
    }

    private void alignProgressLabels() {
        String games = getString(R.string.games), vcs = getString(R.string.vcs), video = getString(R.string.video);
        TextExtender extender = new TextExtender(games, vcs, video);
        mBinding.gameProgress.gameLabel.setText(extender.expandString(games));
        mBinding.vcsProgress.vcsLabel.setText(extender.expandString(vcs));
        mBinding.videoProgress.videoLab.setText(video);
    }

    /* Сохранение/восстановление состояния */

    void store(@NonNull Bundle outState) {
        // сохранение статуса виджетов
        outState.putInt("TAB", mBinding.history.tabsHeader.getSelectedTabPosition());
        mProgressControllers.forEach(p -> outState.putParcelable(p.toString(), p.getGroupValue()));
        outState.putString("HOST", mBinding.host.value.getText().toString());
        outState.putString("IP", mBinding.ip.value.getText().toString());
        outState.putString("ELAPSED", mBinding.elapsed.elapsedVal.getText().toString());
        outState.putString("AVERAGE", mBinding.average.value.getText().toString());
        outState.putString("LOST", mBinding.lost.value.getText().toString());
        outState.putString("JITTER", mBinding.jitter.value.getText().toString());
        outState.putString("BEST", mBinding.best.value.getText().toString());
        outState.putString("WORST", mBinding.worst.value.getText().toString());
    }

    public void recall(@Nullable Bundle inState) {
        if(inState == null)
            return;

        Long lastTestId = AppDatabaseKt.getPingDAO().getLastTestKey();
        loadHistoricalWidgets( lastTestId == null ? 0: lastTestId);
        mBinding.history.tabsHeader.selectTab(mBinding.history.tabsHeader.getTabAt(inState.getInt("TAB")));

        mProgressControllers.forEach(p -> {
            GroupIndicators.GroupValue groupValue = getParcelable(inState, p.toString(), GroupIndicators.GroupValue.class);
            if(groupValue == null)
                p.resetProgressBar();
            else
                p.setController(groupValue);
        });

        mBinding.host.value.setText(inState.getString("HOST"));
        mBinding.ip.value.setText(inState.getString("IP"));
        mBinding.elapsed.elapsedVal.setText(inState.getString("ELAPSED"));
        mBinding.average.value.setText(inState.getString("AVERAGE"));
        mBinding.lost.value.setText(inState.getString("LOST"));
        mBinding.jitter.value.setText(inState.getString("JITTER"));
        mBinding.best.value.setText(inState.getString("BEST"));
        mBinding.worst.value.setText(inState.getString("WORST"));
    }

    // инициализация виджетов после рестарта фрагмента
    void loadHistoricalWidgets(long testId) {
        mBinding.history.graph.fill(loadGraphBuffer(testId));
        mLogController.loadFromDb(testId);
    }

    Collection<? extends Ping> loadGraphBuffer(long testId) {
        int bufSize = GraphView.getMaxBufferSize(App.getAppContext());
        return AppDatabaseKt.getPingDAO().getLastElementsForTest(testId, bufSize);
    }
    
    void clearHistoricalWidgets() {
        mBinding.history.graph.clear();
        mLogController.clear();
    }

    void addPingToHistoricalWidgets(Ping ping) {
        if(ping != null) {
            mBinding.history.graph.drawPing(ping);            // отрисовываем график
            mLogController.log(ping);                         // выводим в лог
        }
    }

    // апдейт виджетов на основе статистики
    void updateStatWidgets(PingStatistic stat) {
        final int[] COLOR_ARR = {R.color.colorError, R.color.DeepOrange400, R.color.colorWarning, R.color.colorMainText, R.color.colorMainText};

        if(stat != null && !stat.isEmpty()) {
            Context context = mBinding.getRoot().getContext();

            mBinding.average.value.setText(context.getString(R.string.num_ms, stat.getAverage()));
            mBinding.jitter.value.setText(context.getString(R.string.num_ms, stat.getJitter()));
            lostWidget.onUpdate(stat);
            mBinding.best.value.setText(context.getString(R.string.num_ms, stat.getBest()));
            mBinding.worst.value.setText(context.getString(R.string.num_ms, stat.getWorst()));

            GroupIndicators.GroupValue groupValue = new GroupIndicators.GroupValue(stat.getAverage(), stat.getJitter(), stat.getLostPermille());
            mProgressControllers.forEach(c -> c.setController(groupValue));
            GroupIndicators overall = GroupIndicators.overallFromStat(stat);

            mBinding.average.value.setTextColor(context.getColor(COLOR_ARR[overall.getPingIndicator().getPosition().ordinal()]));
            mBinding.jitter.value.setTextColor(context.getColor(COLOR_ARR[overall.getJitterIndicator().getPosition().ordinal()]));
            lostWidget.setColor(context.getColor(COLOR_ARR[overall.getLossIndicator().getPosition().ordinal()]));
        }
    }

    void updateHost(HostInfo hostInfo) {
        mBinding.host.value.setText(hostInfo.name);
        mBinding.ip.value.setText(hostInfo.ip);
    }

    // очистка виджетов перед стартом теста
    void initializeWidgets() {
        Context context = mBinding.getRoot().getContext();

        mBinding.history.graph.clear();
        mLogController.clear();
        mProgressControllers.forEach(ProgressController::resetProgressBar);
        mBinding.host.value.setText(R.string.dns);
        mBinding.ip.value.setText("0.0.0.0");
        setElapsedTime(0L);
        mBinding.average.value.setText(context.getString(R.string.num_ms, 0));
        lostWidget.onInit();
        mBinding.jitter.value.setText(context.getString(R.string.num_ms, 0));
        mBinding.best.value.setText(context.getString(R.string.num_ms, 0));
        mBinding.worst.value.setText(context.getString(R.string.num_ms, 0));
    }

    void setElapsedTime(Long durationMs) {
        mBinding.elapsed.elapsedVal.setText(TimeFormatterKt.asDurationFormat(durationMs));
    }

    LogController getLogController() {
        return mLogController;
    }

    void splashProgress() {
        mProgressControllers.forEach(ProgressController::splash);
    }

    private class LostWidget {
        boolean isShowPercent = true;
        long total = 0;
        int ppm = 0;


        public LostWidget() {
            mBinding.lostLayout.setOnClickListener(v->
            {
                isShowPercent = !isShowPercent;
                setText();
            });
        }

        void onInit() {
            total = ppm = 0;
            setText();
        }

        void onUpdate(PingStatistic statistic) {
            total = statistic.getLost();
            ppm = statistic.getLostPermille();
            setText();
        }

        void setColor(int color) {
            mBinding.lost.value.setTextColor(color);
        }

        private void setText() {
            mBinding.lost.value.setText(isShowPercent ? getString(R.string.percent1, ppm/10f) : String.valueOf(total));
        }
    }
}