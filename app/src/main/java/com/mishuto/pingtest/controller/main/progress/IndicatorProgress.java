package com.mishuto.pingtest.controller.main.progress;

import static com.mishuto.pingtest.common.Utils.getString;
import static com.mishuto.pingtest.model.indicators.Indicator.Position;
import static com.mishuto.pingtest.view.SegmentedProgressBarKt.PROGRESS_FULL_ANIMATION_TIME;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.mishuto.pingtest.R;
import com.mishuto.pingtest.common.TextExtender;
import com.mishuto.pingtest.model.indicators.GroupIndicators;
import com.mishuto.pingtest.model.indicators.Indicator;
import com.mishuto.pingtest.view.SegmentedProgressBar;

import java.util.stream.Stream;

/**
 * Контроллер индикаторов Game, VConf, Video
 * Created by Michael Shishkin on 03.11.2021
 */
public class IndicatorProgress extends ProgressController {

    private final String[] scores; // выровненные по ширине значения индикаторов
    private boolean firstReset = true;

    public IndicatorProgress(TextView value, SegmentedProgressBar progressBar, ImageView ico, GroupIndicators indicator) {
        super(value, progressBar, ico, indicator);
        scores = createAlignedScoresArray();
    }

    // преобразование значений индикаторов в выровненные по ширине
    private static String[] createAlignedScoresArray() {
        String[] original = Stream.of(Position.values())
                .map(p->getString(p.getScoreId()))
                .toArray(String[]::new);

        TextExtender textExtender = new TextExtender(original);

        return Stream.of(original)
                .map(textExtender::expandString)
                .toArray(String[]::new);
    }

    @Override
    void updateIndicatorValue(Indicator indicator) {
        Context context = mProgressBar.getContext();
        mValue.setText(scores[indicator.getPosition().ordinal()]);
        mIco.setColorFilter(context.getColor(indicator.computeIndicatorValue() == 0 ? R.color.colorIndicatorDash : R.color.colorPrimary));
    }

    @Override
    public void resetProgressBar() {
        super.resetProgressBar();
        if(firstReset) {
            mValue.postDelayed(() -> mValue.setText(R.string.ready), 2 * PROGRESS_FULL_ANIMATION_TIME);
            firstReset = false;
        }
    }
}
