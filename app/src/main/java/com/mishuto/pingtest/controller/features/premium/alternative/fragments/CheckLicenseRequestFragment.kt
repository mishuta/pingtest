package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.controller.features.premium.alternative.AltBillingState.*
import com.mishuto.pingtest.controller.features.premium.alternative.fragments.base.AbstractServerRequestFragment
import com.mishuto.pingtest.controller.features.premium.billing.Billing.license
import com.mishuto.pingtest.controller.features.premium.billing.alternative.License.LicenseStatus.ACTIVATED
import com.mishuto.pingtest.controller.features.premium.billing.alternative.License.LicenseStatus.GOT_KEY
import com.mishuto.pingtest.controller.features.premium.billing.alternative.License.LicenseStatus.PURCHASED

/**
 * Проверка статуса лицензии
 * Created by Michael Shishkin on 24.05.2024
 */
class CheckLicenseRequestFragment : AbstractServerRequestFragment() {
    override val errorMessageResId: Int = R.string.error_license_request
    
    override suspend fun callRestApi() {
        Logger.d("requesting license status...")
        val status = license.requestLicenseInfo()
        billingStateViewModel.state.value = when (status) {
            ACTIVATED -> LICENSE_INFO
            PURCHASED -> ACTIVATE
            GOT_KEY -> PURCHASE_FORM
            else -> LICENSE_REQUEST
        }
    }
}