package com.mishuto.pingtest.controller.features.premium

import android.app.Activity
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AlertDialog
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger.e
import com.mishuto.pingtest.common.Resources.getString
import com.mishuto.pingtest.common.Utils.createDialogBuilder
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.controller.features.premium.alternative.AltBillingActivity.Companion.startAltBillingActivity
import com.mishuto.pingtest.controller.features.premium.billing.Billing
import com.mishuto.pingtest.controller.features.premium.billing.Billing.isPremium
import com.mishuto.pingtest.controller.features.premium.billing.Billing.license
import com.mishuto.pingtest.controller.features.premium.billing.alternative.License.LicenseStatus.PURCHASED
import com.mishuto.pingtest.controller.features.premium.billing.google.BillingException
import com.mishuto.pingtest.controller.features.premium.billing.google.BillingState
import com.mishuto.pingtest.controller.features.premium.billing.google.PremiumPurchaseStatus
import com.mishuto.pingtest.controller.features.premium.billing.google.PremiumPurchaseStatus.COMPLETED
import com.mishuto.pingtest.controller.features.premium.billing.google.PremiumPurchaseStatus.Companion.ON_GP_PURCHASE_STATUS_CHANGED
import com.mishuto.pingtest.databinding.AlertBillingDialogBinding

/**
 * Отображение алерт-диалога со статусом покупки Премиум
 * Created by Michael Shishkin on 30.09.2023
 */

private const val TAG = "BillingDialog"

class PremiumDialog(private val activity: Activity) {
    
    private val binding: AlertBillingDialogBinding by lazy { AlertBillingDialogBinding.inflate(activity.layoutInflater) }
    private lateinit var dialog: AlertDialog
    
    fun showBillingDialog() {
        dialog = createDialogBuilder(activity)
            .setView(binding.root)
            .setTitle(R.string.purchase_state)
            .show()
        
        initDialog()
    }
    
    private fun initDialog() {
        updateWidgets()
        
        registerBillingEvent()
        dialog.setOnDismissListener { _ -> unregisterBillingEvent() }
        Billing.googleBilling.refreshPurchaseStatus()
        
        binding.getPremiumButton.setOnClickListener { onGetPremiumClick() }
        binding.licenseButton.setOnClickListener { onLicenseClick() }
    }
    
    private fun onGetPremiumClick() {
        dialog.dismiss()
        when(binding.alternativeBillingChoice.checkedRadioButtonId) {
            R.id.yoomoney -> activity.startAltBillingActivity(false)
            R.id.activate_license -> activity.startAltBillingActivity(true)
            else -> onGooglePlay()
        }
    }
    
    private fun onLicenseClick() {
        dialog.dismiss()
        activity.startAltBillingActivity()
    }
    
    private fun onGooglePlay() {
        try {
            Billing.googleBilling.showPremiumProductOffer(activity)
        }
        catch (e: BillingException) {
            e(e)
            e(Billing.googleBilling.errState.errMessage)
            createDialogBuilder(activity).setMessage(e.errState.message()).show()
        }
    }
    
    private fun updateWidgets() {
        with(Billing.googleBilling) {
            updatePremiumState(isPremium)
            updatePurchaseState(premiumPurchaseStatus)
            binding.alternativeBillingChoice.showIf(Billing.alternativeBillingConditionsMet())
            binding.billingErrorLabel.showIf(errState.hasError())
            binding.billingErrorLabel.text = errState.message()
            binding.premiumAdvantagesLayout.showIf(!isPremium && !errState.hasError() && premiumPurchaseStatus == null)
            binding.getPremiumButton.showIf(!isPremium)
            binding.licenseButton.showIf(license.isActivated)
            binding.yoomoney.showIf(license.licenseStatus < PURCHASED)
        }
    }
    
    private fun updatePremiumState(isPremium: Boolean) {
        val text = getString(when {
                isPremium -> R.string.billing_premium_is_purchased
                license.licenseStatus == PURCHASED -> R.string.billing_license_not_activated
                else -> R.string.billing_not_purchased_premium
            }
        )
        
        val icon = if (isPremium) R.drawable.ic_outline_lock_open_24 else R.drawable.outline_lock_24
        val color = if (isPremium) R.color.colorGood else R.color.colorError
        
        binding.premiumStateLabel.text = text
        binding.premiumStateLabel.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0)
        binding.premiumStateLabel.compoundDrawables.filterNotNull().forEach { it.setTint(activity.resources.getColor(color, null)) }
    }
    
    private fun updatePurchaseState(purchaseState: PremiumPurchaseStatus?) {
        activity.runOnUiThread {
            val text = purchaseState?.mesId?.let { getString(it) }
            binding.billingPurchaseStateLabel.showIf(purchaseState !in listOf(null, COMPLETED))
            binding.billingPurchaseStateLabel.text = text
            binding.billingPurchaseStateLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_error, 0, 0, 0)
        }
    }
    
    private fun onPremiumStatusChanged() {
        activity.runOnUiThread {
            if (dialog.isShowing)
                activity.runOnUiThread { updateWidgets() }
            else
                unregisterBillingEvent() // last chance to unreg
        }
    }
    
    private fun registerBillingEvent() {
        EventDispatcher.register(Billing.CHANGE_PREMIUM_STATE, TAG) { onPremiumStatusChanged() }
        EventDispatcher.register<PremiumPurchaseStatus>(ON_GP_PURCHASE_STATUS_CHANGED, TAG) { updatePurchaseState(it) }
    }
    
    private fun unregisterBillingEvent() {
        EventDispatcher.unregister(Billing.CHANGE_PREMIUM_STATE, TAG)
        EventDispatcher.unregister(ON_GP_PURCHASE_STATUS_CHANGED, TAG)
    }
    
    private fun View.showIf(visible: Boolean) {
        visibility = if (visible) VISIBLE else GONE
    }
    
    private fun BillingState.message() = "${getString(R.string.error)} #$errCode: $errMessage"
}
