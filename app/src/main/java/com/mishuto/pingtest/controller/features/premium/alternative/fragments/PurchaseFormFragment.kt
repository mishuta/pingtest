package com.mishuto.pingtest.controller.features.premium.alternative.fragments

import android.content.Intent
import android.net.Uri
import com.mishuto.pingtest.R
import com.mishuto.pingtest.backend.rest.ServerConfig.serverHost
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.controller.features.premium.alternative.AltBillingState
import com.mishuto.pingtest.controller.features.premium.alternative.fragments.base.AbstractInfoFragment
import com.mishuto.pingtest.controller.features.premium.billing.Billing

/**
 * Перенаправляет в браузер для покупки лицензии, поскольку Юмани не работает в WebView.
 * Отображает сообщение которое пользователь увидит, только если сам переключится в аппу из браузера
 * Created by Michael Shishkin on 22.06.2024
 */
class PurchaseFormFragment: AbstractInfoFragment() {
    override fun execute() {
        binding.message.setFromResource(R.string.alt_before_purchase_message)
        binding.next.setOnClickListener { billingStateViewModel.state.value =
            AltBillingState.LICENSE_CHECK
        }
        runPurchaseFormInBrowser()
    }

    private fun runPurchaseFormInBrowser() {
        val purchaseFormUrl = "${serverHost}/payments/form?licenseKey=${Billing.license.key}"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(purchaseFormUrl))
        Logger.d("run url: $purchaseFormUrl")
        startActivity(intent)
    }
}