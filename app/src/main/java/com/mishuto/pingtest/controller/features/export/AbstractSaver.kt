package com.mishuto.pingtest.controller.features.export

import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Logger
import com.mishuto.pingtest.common.Utils
import com.mishuto.pingtest.common.Utils.prepareSnack
import com.mishuto.pingtest.controller.ui.ComputationProgress
import com.mishuto.pingtest.controller.ui.ProgressDialog
import com.mishuto.pingtest.controller.features.export.PingLogFile.saveLog
import com.mishuto.pingtest.data.ResultEntity
import kotlinx.coroutines.future.future
import java.io.OutputStream
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Общие операции для Saver и Sharer по подготовке асинхронного создания файла
 * Created by Michael Shishkin on 31.01.2024
 */
abstract class AbstractSaver(val activity: AppCompatActivity, val entities: List<ResultEntity>) {
    private val activityView = (activity.findViewById(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup

    protected open fun onError(message: String?) = showSnack("${activity.getString(R.string.file_create_err)}: $message")

    protected fun showSnack(msg: String) = prepareSnack(activityView, msg).show()
    protected fun showSnack(msgId: Int) = prepareSnack(activityView, msgId).show()

    protected suspend fun createFile(out: OutputStream) {
        Logger.d("saving ${entities.count()} results")
        try {
            val progressDialog = ProgressDialog(activity, Utils.getString(R.string.generation_log))
            ComputationProgress(activity.lifecycleScope, progressDialog).run { l -> out.saveLog(entities, l) }
        }
        catch (e: Exception) {
            Logger.e(e)
            onError(e.message)
        }
    }

    abstract suspend fun processFile()

    // обертка для вызова из Java
    fun processFileAsync() {
        activity.lifecycleScope.future { processFile() }
    }

    fun getFileName(): String {
        val timePart = DateTimeFormatter.ofPattern("yyMMdd-HHmmss").format(LocalDateTime.now())
        return "pingmon_log_$timePart.txt"
    }
}