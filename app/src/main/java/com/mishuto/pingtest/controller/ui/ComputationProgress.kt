package com.mishuto.pingtest.controller.ui

import android.app.Activity
import android.widget.ProgressBar
import androidx.lifecycle.LifecycleCoroutineScope
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Assert
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.common.Logger.tag
import com.mishuto.pingtest.common.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlin.math.roundToInt

/**
 * Вынос выполняющегося длительное время кода в GUI активности из Main thread в отдельный поток с отображением прогрессбара
 * Created by Michael Shishkin on 25.12.2023
 */

//долгий метод должен вызывать передаваемый ему лисенер для обновления прогресса
fun interface ComputingProgressListener {
    fun onUpdate(progress: Float)   // прогресс меняется от 0 до 1
}

interface ComputingProgressRenderer : ComputingProgressListener {
    suspend fun start()
    fun stop()
    fun setOnCloseListener(listener: ()->Unit)
}

class ComputationProgress(private val scope: LifecycleCoroutineScope, private val renderer: ComputingProgressRenderer) {

    suspend fun <T> run(offLoadFun: suspend (listener: ComputingProgressListener) -> T): T {
        val job = scope.async {
            withContext(Dispatchers.Default) { offLoadFun(renderer) }
        }
        renderer.setOnCloseListener { job.cancel() }
        renderer.start()
        try {
            return job.await()
        } finally {
            renderer.stop()
        }
    }
}

 class ProgressDialog(private val activity: Activity,
                      title: String,
                      private val delayForDialogShow : Long = 350,
                      private val maxProgress: Int = 100) : ComputingProgressRenderer
 {
    private val dialog = Utils.createDialogBuilder(activity)
        .setView(R.layout.alert_progress)
        .setTitle(title)
        .create()
    private lateinit var progressBar: ProgressBar

    private var completed = false
    private var currentProgress: Int = 0 // 0..maxProgress

     override suspend fun start() {
         tag()
        completed = false
        delay(delayForDialogShow)
        if(!activity.isFinishing && !completed)
            activity.runOnUiThread { showDialog() }
    }
    private fun showDialog() {
        tag()
        dialog.show()
        progressBar = dialog.findViewById(R.id.progressBar)!!
        progressBar.max = maxProgress
    }

    override fun stop() {
        tag()
        completed = true
        if (dialog.isShowing && !activity.isDestroyed)
            dialog.dismiss()
    }

     override fun setOnCloseListener(listener: () -> Unit) {
         dialog.setOnCancelListener {
             _ -> listener.invoke()
             d("dialog cancelled")
         }
     }

    // т.к. приращения progress могут быть сильно меньше чем на 1/maxProgress,
    // то реально апдейт прогрессбара выполняется при изменении приращения на величину больше чем 1/maxProgress.
    // После достижения приращения до maxProgress диалог закрывается
    override fun onUpdate(progress: Float) {
        val intProgress = (progress * maxProgress).roundToInt()
        Assert.that(intProgress in 0..maxProgress, "progress is $progress")
        if(intProgress != currentProgress && !completed && dialog.isShowing) {
            activity.runOnUiThread {
                progressBar.progress = intProgress
                currentProgress = intProgress
            }
            if(intProgress == maxProgress)
                stop()
        }
    }
}
