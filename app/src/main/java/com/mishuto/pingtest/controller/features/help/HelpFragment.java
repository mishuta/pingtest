package com.mishuto.pingtest.controller.features.help;

import static android.os.Build.VERSION.SDK_INT;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.webkit.WebSettingsCompat;
import androidx.webkit.WebViewFeature;

import com.mishuto.pingtest.BuildConfig;
import com.mishuto.pingtest.R;
import com.mishuto.pingtest.backend.rest.ServerConfig;
import com.mishuto.pingtest.common.Environment;
import com.mishuto.pingtest.controller.features.premium.billing.Billing;
import com.mishuto.pingtest.controller.ui.whatsnew.FeatureHistory;
import com.mishuto.pingtest.databinding.HelpFragmentBinding;
import com.mishuto.pingtest.settings.preferences.theme.AppTheme;

import java.util.Locale;

@SuppressWarnings("ConstantConditions")
public class HelpFragment extends Fragment {

    HelpFragmentBinding mBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = HelpFragmentBinding.inflate(inflater, container, false);

        mBinding.versionValue.setText(BuildConfig.VERSION_NAME);
        mBinding.history.setOnClickListener(v-> showVersionHistory());
        mBinding.feedback.setOnClickListener(v -> sendFeedback());
        new HelpArticleRecyclerViewController(this, mBinding.articles);
        mBinding.legend.setOnClickListener(v->showLegend());
        mBinding.privacy.setOnClickListener(v -> showFragment(new WebViewFragment()));

        return mBinding.getRoot();
    }

    private void showVersionHistory() {
        showFragment(new VersionHistoryFragment());
    }

    private void showLegend() {
        showFragment(new Fragment(R.layout.help_legend_fragment));
    }

    void showFragment(Fragment fragment) {
        getParentFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_in, R.anim.fragment_fade_out, R.anim.fragment_fade_in, R.anim.fragment_slide_out)
                .replace(R.id.fragment, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void sendFeedback() {
        Intent sendIntent = new Intent(Intent.ACTION_SENDTO);

        String uriText = "mailto:" + getString(R.string.mail_address) +
                "?subject=" + Uri.encode(getString(R.string.feedback_subject)) +
                "&body=" + Uri.encode((getString(R.string.feedback_body,
                BuildConfig.VERSION_NAME,
                Build.VERSION.RELEASE,
                Environment.INSTANCE.getDeviceModel(),
                Locale.getDefault().getDisplayName(),
                Billing.INSTANCE.isPremium() ? "Premium (priority support)" : "Regular"
        )));

        Uri uri = Uri.parse(uriText);
        sendIntent.setData(uri);

        startActivity(Intent.createChooser(sendIntent, getString(R.string.mail_chooser)));
    }

    public static class WebViewFragment extends Fragment {
        public WebViewFragment() {
            super(R.layout.fragment_web_view_form);
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = super.onCreateView(inflater, container, savedInstanceState);
            WebView webView = view.findViewById(R.id.webview);
            View loadingView = view.findViewById(R.id.indicator);
            initWebView(webView, loadingView);

            return view;
        }

        @SuppressWarnings("deprecation")
        private void initWebView(WebView webView, View loading) {
            webView.loadUrl(ServerConfig.INSTANCE.getServerHost() + "/privacy_policy.html");
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    loading.setVisibility(View.INVISIBLE);
                }
            });

            if (SDK_INT >= Build.VERSION_CODES.TIRAMISU ) {
                if (WebViewFeature.isFeatureSupported(WebViewFeature.ALGORITHMIC_DARKENING)) {
                    WebSettingsCompat.setAlgorithmicDarkeningAllowed(webView.getSettings(), true);

                }
            }
            else if(AppTheme.isDarkThemeSet() && WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK))
                WebSettingsCompat.setForceDark(webView.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
        }
    }

    public static class VersionHistoryFragment extends Fragment {

        public VersionHistoryFragment() {
            super(R.layout.help_version_history_fragment);
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = super.onCreateView(inflater, container, savedInstanceState);
            FrameLayout layout = view.findViewById(R.id.history);

            FeatureHistory history = new FeatureHistory();
            layout.addView(history.createHistoryListView(getContext()));

            return view;
        }
    }
}