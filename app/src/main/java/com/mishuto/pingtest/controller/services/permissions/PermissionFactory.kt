package com.mishuto.pingtest.controller.services.permissions

import android.Manifest.permission.*
import android.annotation.SuppressLint
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.appcompat.app.AppCompatActivity
import com.mishuto.pingtest.common.SharedPref
import com.mishuto.pingtest.common.Logger.d
import com.mishuto.pingtest.controller.services.permissions.Permission.Companion.hasPermission

/**
 *  Фабрика объектов Permission
 *  Объекты Permissions должны создаваться в onCreate основной активности, т.к. при создании объекта вызывается registerForActivityResult.
 *  Следовательно, объекты должны быть созданы, даже если пермишены реально не потребуются.
 *
 * Created by Michael Shishkin on 02.02.2024
 */

object PermissionFactory {
    @SuppressLint("InlinedApi")
    val usedPermissions = listOf(POST_NOTIFICATIONS, READ_PHONE_STATE, WRITE_EXTERNAL_STORAGE)
    
    private lateinit var permissionMap: Map<String, Permission>
    
    fun createPermissions(activity: AppCompatActivity) {
        permissionMap = usedPermissions.map { PermissionImpl(it, activity) }
            .associateBy { it.permissionID }
    }
    
    fun getPermission(permissionId: String, listener: Permission.PermissionManagerListener): Permission =
        permissionMap[permissionId]!!
            .also { (it as PermissionImpl).listener = listener }
    
}

/**
 * Реализация Permission
 * Created by Michael Shishkin on 25.10.2023
 */
private const val PERMISSION_WAS_DECLINED_PREFIX = "PermissionDeclinedFor_"

private class PermissionImpl(override val permissionID: String, val activity: AppCompatActivity) : Permission {
    
    lateinit var listener: Permission.PermissionManagerListener
    private val activityResultLauncher = activity.registerForActivityResult(RequestPermission()) { granted -> onRequestPermissionResult(granted) }
    override var requestPermissionEnabled: Boolean
        get() = requestPermissionEnabledMap[permissionID] ?: true
        set(value) {
            requestPermissionEnabledMap[permissionID] = value
        }
    
    override var wasDeclinedEarlier
        get() = SharedPref.getBoolean("$PERMISSION_WAS_DECLINED_PREFIX$permissionID", false)
        set(value) {
            SharedPref.putBoolean("$PERMISSION_WAS_DECLINED_PREFIX$permissionID", value)
        }
    
    override val hasPermission: Boolean
        get() = hasPermission(permissionID)
    
    private var requestInProgress = false
    
    override fun tryGetPermission(): Boolean {
        if (!hasPermission && requestPermissionEnabled && !requestInProgress) {
            if (activity.shouldShowRequestPermissionRationale(permissionID) || !wasDeclinedEarlier)
                listener.needShowRequestPermissionRational()
            else
                activityResultLauncher.launch(permissionID)
            
            requestInProgress = true    // защита от повторного запроса
        }
        
        return hasPermission
    }
    
    override fun onRequestPermissionRational(accepted: Boolean) {
        d("accepted: $accepted")
        requestPermissionEnabled = accepted
        requestInProgress = accepted    // если пользователь отклонил запрос, то requestInProgress сбрасывается потому что процесс реквеста завершается
        
        if (accepted)
            activityResultLauncher.launch(permissionID)
    }
    
    private fun onRequestPermissionResult(granted: Boolean) {
        d("granting of permission $permissionID: $granted")
        requestInProgress = false
        requestPermissionEnabled = granted
        wasDeclinedEarlier = !granted
        listener.onRequestPermissionResult(granted)
    }
    
    companion object {
        // Мапа признаков разрешений для показа запросов пермишенов.
        // Если пользователь отклонил разрешение, оно не должно быть показано в текущей сессии до закрытия программы. Поэтому они хранятся в статической мапе.
        val requestPermissionEnabledMap = mutableMapOf<String, Boolean>()
    }
}