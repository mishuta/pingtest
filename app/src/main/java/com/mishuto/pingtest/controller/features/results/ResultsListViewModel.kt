package com.mishuto.pingtest.controller.features.results

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mishuto.pingtest.data.ResultEntity
import com.mishuto.pingtest.controller.features.results.DeletedEntities.DeletedState.*

/**
 * ViewModel классы для обмена данными между активностью и фрагментами
 * Created by Michael Shishkin on 02.06.2023
 */
class ResultsListViewModel : ViewModel() {
    val selectedItem = MutableLiveData<ResultEntity>()          // выбранный элемент для DetailsFragment
    val deletedEntities: DeletedEntities = DeletedEntities()    // список элементов, помеченных как удаленные
}

class DeletedEntities {
    enum class DeletedState { NOT_MARKED, MARKED, UNMARKED, DELETED }
    val markedEntities : MutableLiveData<Collection<ResultEntity>> = MutableLiveData(listOf())
    val markedState = MutableLiveData(NOT_MARKED)

    val markedAsDeleted: Boolean get() = markedState.value == MARKED

    fun markAsDeleted(entity: ResultEntity) = markAsDeleted(listOf(entity))

    fun markAsDeleted(entities: Collection<ResultEntity>) {
        markedEntities.value = entities.toList()
        markedState.value = MARKED
    }

    fun unMarkAsDeleted() { if(markedState.value == MARKED) markedState.value = UNMARKED } // защита от undo в залипшем снеке после удаления
    fun makeDeleted() { markedState.value = DELETED }
}