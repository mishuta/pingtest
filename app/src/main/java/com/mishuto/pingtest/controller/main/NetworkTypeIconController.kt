package com.mishuto.pingtest.controller.main

import android.Manifest.permission.READ_PHONE_STATE
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.AlertQueManager
import com.mishuto.pingtest.common.Logger.i
import com.mishuto.pingtest.common.Utils.createDialogBuilder
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.common.net.NET_TYPE_CHANGED
import com.mishuto.pingtest.common.net.NetTypeObject
import com.mishuto.pingtest.common.net.NetworkType
import com.mishuto.pingtest.common.net.NetworkType.UNKNOWN_CELL
import com.mishuto.pingtest.controller.services.permissions.Permission
import com.mishuto.pingtest.controller.services.permissions.PermissionFactory
import com.mishuto.pingtest.model.NetTypeChangeEvent

/**
 * Контроллер иконки типа сети.
 * Created by Michael Shishkin on 25.10.2023
 */
class NetworkTypeIconController(
    val icon: ImageView,
    val alertQueManager: AlertQueManager,
    val activity: AppCompatActivity) : DefaultLifecycleObserver {

    private val permissionDialog = PermissionRequestDialog()
    private val permission: Permission = PermissionFactory.getPermission(READ_PHONE_STATE, permissionDialog)

    init {
        EventDispatcher.bind<NetTypeChangeEvent>(NET_TYPE_CHANGED, "NetworkTypeIconController", activity.lifecycle) { updateIcon(it.to) }
        activity.lifecycle.addObserver(this)
    }

    private fun updateIcon(netType: NetTypeObject) {
        activity.runOnUiThread {
            icon.setImageResource(netType.imageId)
            if(netType.type == UNKNOWN_CELL)
                permission.tryGetPermission()
        }
    }

    override fun onStart(owner: LifecycleOwner) {
        updateIcon(NetworkType.getNetType())
    }

    inner class PermissionRequestDialog : Permission.PermissionManagerListener {
        override fun needShowRequestPermissionRational() {
            val alertDialog = createDialogBuilder(activity)
                .setTitle(R.string.permission_req_title)
                .setPositiveButton(R.string.proceed) { _,_ -> acceptRequest(true) }
                .apply {
                    if(!permission.wasDeclinedEarlier)
                        setView(R.layout.alert_before_first_permission_request)
                    else {
                        setView(R.layout.alert_permission_request)
                        setNegativeButton(R.string.not_now) { _,_ -> acceptRequest(false) }
                    }
                }
                .create()

            alertQueManager.enqueue("NetworkTypePermissionRationalDialog", alertDialog) { acceptRequest(false) }
        }

        override fun onRequestPermissionResult(granted: Boolean) {
            i("$READ_PHONE_STATE granted: $granted")
        }

        private fun acceptRequest(accepted: Boolean) = permission.onRequestPermissionRational(accepted)
    }
}