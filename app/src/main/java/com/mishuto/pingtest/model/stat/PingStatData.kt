package com.mishuto.pingtest.model.stat

/**
 * Хранение статистики
 * Created by Michael Shishkin 16.06.2021 (refactored on 22.02.2024)
 */
data class PingStatData(
    override val average: Long,
    override val lost: Long,
    override val jitter: Long,
    override val best: Long,
    override val worst: Long,
    override val quantity: Long,
    override val duration: Long
) : PingStatistic {
    
    constructor() : this(0, 0, 0, 0, 0, 0, 0)
    constructor(s: PingStatistic) : this(s.average, s.lost, s.jitter, s.best, s.worst, s.quantity, s.duration)
}