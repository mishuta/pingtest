package com.mishuto.pingtest.model.stat

import com.mishuto.pingtest.model.ping.Ping

/**
 *   Статистика, которая поддерживает добавление в нее пингов
 *   Created by Michael Shishkin on 27.01.2022 refactored on kotlin on 11.02.2024
 */
interface LivePingStatistic: PingStatistic {
    fun addNew(ping: Ping)
    fun clear()
}