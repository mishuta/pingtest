package com.mishuto.pingtest.model

import com.mishuto.pingtest.common.net.NetTypeObject
import java.time.LocalDateTime

/**
 * События, помимо пинга, для лога и контроллера истории
 * Created by Michael Shishkin on 05.09.2022
 */

interface LogEvent {
    val timestamp : LocalDateTime
        get() = LocalDateTime.now()
}

interface  NetTypeChangeEvent : LogEvent {
    val from: NetTypeObject
    val to: NetTypeObject
}

interface ErrorMessageEvent : LogEvent {
    val message: String
}

data class NetTypeChangeEventImp(
    override val from: NetTypeObject,
    override val to: NetTypeObject,
) : NetTypeChangeEvent

data class ErrorMessageEventImp (override val message: String) : ErrorMessageEvent