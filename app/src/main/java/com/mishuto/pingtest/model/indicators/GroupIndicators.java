package com.mishuto.pingtest.model.indicators;

import static com.mishuto.pingtest.model.indicators.GroupThresholds.OVERALL;

import android.os.Parcel;
import android.os.Parcelable;

import com.mishuto.pingtest.model.stat.PingStatistic;

import java.util.Comparator;
import java.util.stream.Stream;

/**
 * Хелпер для работы с групповыми индикаторами.
 * Групповой индикатор - интегральный по трем частным индикаторам пинг, джиттер, лост
 * Created by Michael Shishkin on 08.02.2021
 */
public class GroupIndicators {
    private final Indicator mPingIndicator, mJitterIndicator, mLossIndicator;   // индикаторы в группе
    final GroupThresholds mGroupThresholds;                                     // установленные предельные значения для всех групп
    GroupValue mGroupValue;                                                     // значения параметра каждого индикатора

    // создаем группу индикаторов на базе заданных трешолдов
    public GroupIndicators(GroupThresholds groupThresholds) {
        mGroupThresholds = groupThresholds;
        mPingIndicator = new Indicator(groupThresholds.ping);
        mJitterIndicator = new Indicator(groupThresholds.jitter);
        mLossIndicator = new Indicator(groupThresholds.loss);
    }

    public GroupValue getGroupValue() {
        return mGroupValue;
    }

    // устанавливаем значения группе индикаторов
    public void setGroupValue(GroupValue groupValue) {
        mGroupValue = groupValue;
        mPingIndicator.setIndicatorParameter(groupValue.getPing());
        mJitterIndicator.setIndicatorParameter(groupValue.getJitter());
        mLossIndicator.setIndicatorParameter(groupValue.getLost());
    }

    // групповой результат равен наихудшему из частных индикаторов
    public Indicator getTotalIndicator() {
        return Stream.of(mPingIndicator, mJitterIndicator, mLossIndicator)
                .min(Comparator.comparingDouble(Indicator::computeIndicatorValue))
                .get();
    }

    public Indicator getPingIndicator() {
        return mPingIndicator;
    }

    public Indicator getJitterIndicator() {
        return mJitterIndicator;
    }

    public Indicator getLossIndicator() {
        return mLossIndicator;
    }

    public GroupThresholds getGroupThresholds() {
        return mGroupThresholds;
    }

    public static GroupIndicators overallFromStat(PingStatistic stat) {
        GroupIndicators overall = new GroupIndicators(OVERALL);
        overall.setGroupValue(new GroupValue(stat));
        return overall;
    }

    // Значение группового индикатора.
    // Состоит из значений Ping, Jitter, Lost
   public static class GroupValue implements Parcelable {
        final private long mPing;
        final private long mJitter;
        final private long lost;

        public GroupValue(long ping, long jitter, int loss) {
            mPing = ping;
            mJitter = jitter;
            lost = loss;
        }

        public GroupValue(PingStatistic stat) {
            this(stat.getAverage(), stat.getJitter(), stat.getLostPermille());
        }

        public long getPing() {
            return mPing;
        }

        public long getJitter() {
            return mJitter;
        }

        public long getLost() {
            return lost;
        }

        /* Parcelable implementation */

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(mPing);
            dest.writeLong(mJitter);
            dest.writeLong(lost);
        }

        protected GroupValue(Parcel in) {
            mPing = in.readLong();
            mJitter = in.readLong();
            lost = in.readLong();
        }

        public static final Creator<GroupValue> CREATOR = new Creator<>() {
            @Override
            public GroupValue createFromParcel(Parcel in) {
                return new GroupValue(in);
            }

            @Override
            public GroupValue[] newArray(int size) {
                return new GroupValue[size];
            }
        };
    }
}
