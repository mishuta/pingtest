package com.mishuto.pingtest.model.stat;

import com.mishuto.pingtest.common.Assert;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * Билдер для ява-клиентов PingStatData. Должен уйти вместе с ява-клиентами
 * Created by Michael Shishkin on 22.02.2024
 */
public class PingStatDataBuilder {

    Long avg, best, worst, jitter, duration, quantity, lost;

    public PingStatDataBuilder setAvg(long avg) {
        this.avg = avg;
        return this;
    }

    public PingStatDataBuilder setBest(long best) {
        this.best = best;
        return this;
    }

    public PingStatDataBuilder setWorst(long worst) {
        this.worst = worst;
        return this;
    }

    public PingStatDataBuilder setJitter(long jitter) {
        this.jitter = jitter;
        return this;
    }

    public PingStatDataBuilder setQuantity(long quantity) {
        this.quantity = quantity;
        return this;
    }

    public PingStatDataBuilder setLost(long lost) {
        this.lost = lost;
        return this;
    }

    public PingStatDataBuilder setDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public PingStatData make() {
        Assert.that(Stream.of(avg, best, worst, jitter, duration, quantity, lost).noneMatch(Objects::isNull),
                "PingStatData. All parameters must be set before make() call");

        return new PingStatData(avg, lost, jitter, best, worst, quantity, duration);
    }
}

