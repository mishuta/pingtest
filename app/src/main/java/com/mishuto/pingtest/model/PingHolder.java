package com.mishuto.pingtest.model;

import static com.mishuto.pingtest.common.Utils.logger;
import static com.mishuto.pingtest.model.HostNameValidator.IP4_MATCHER;
import static java.util.regex.Pattern.matches;

import com.mishuto.pingtest.common.tickers.Ticker;
import com.mishuto.pingtest.model.ping.Ping;
import com.mishuto.pingtest.model.ping.PingFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Locale;
import java.util.function.Consumer;

/**
 *  Объект PingHolder.
 *  Содержит данные и подготовительные операции для создания объекта Ping
 *  Выполняет пинг в отдельном потоке
 *  Created by Michael Shishkin on 03.06.2020
 */

public class PingHolder {

    final public static int RESOLVE_TIMEOUT = 12_500;  // таймаут резолва хоста

    /**
     * интерфейс для получения результата асинхнронных методов
     */
    public interface PingListener {
        void onResolve(PingHolder pingHolder);

        void onLookUp(HostInfo hostInfo);

        void onPing(Ping ping);

        void onPingError(PingException exception);
    }

    private final SafePingListener safeListener;
    private final int mTimeout;                 // таймаут пинга
    volatile private InetAddress mHost;         // разрезолвенный хост
    private String mHostName;                   // Имя хоста. Если в конструкторе передавался ip, то hostName вычисляется как nslookup
    private String mHostIP;                     // IP. Вычисляется в процессе резолва
    private int mPingCount = 0;                 // количество выполненных пингов в этом объекте
    private Thread resolveThread;

    public InetAddress getHost() {
        return mHost;
    }

    public HostInfo getHostInfo() {
        return new HostInfo(mHostIP, mHostName);
    }

    public PingHolder(PingListener listener, int timeout) {
        safeListener = new SafePingListener(listener);
        mTimeout = timeout;
    }

    /**
     * резолвинг пинга
     * выполняется не дольше чем RESOLVE_TIMEOUT
     * @param address URL хоста
     */
    synchronized public void resolve(String address) {
        if (matches(IP4_MATCHER, address)) {
            resolveIP(address);     // если известен ip, то resolve вызывается сразу, не дожидаясь лукапа
            startLookUpThread();    // лукап приходит асинхронно, спустя время после резолва
        }
        else
            startResolveThread(address);
    }

    private void resolveIP(String ip) {
        try {
            mHost = InetAddress.getByName(ip); // отрабатывает мнгоновенно
            mHostIP = ip;
            mHostName = ip;
            safeListener.onResolve(this);
        }
        catch (UnknownHostException e) {
            onPingError(new PingException.GeneralException(e, ip));
        }
    }

    private void startLookUpThread() {
        Thread lookupThread = new Thread(() -> {
            //SystemClock.sleep(5_000); // имитация долгого лукапа
            mHostName = mHost.getHostName();
            safeListener.onLookUp(getHostInfo());
        });

        lookupThread.setName("--PingHolder.lookup");
        lookupThread.start();
    }

    private void startResolveThread(String hostname) {
        resolveThread = new Thread(() -> doResolve(hostname));
        resolveThread.setName("--PingHolder.resolve");
        resolveThread.start();
    }

    private void doResolve(String hostname) {
        logger.d(traceString());
        boolean successfully = false;   // успешное завершение операции

        // стартуем тикер на таймаут. Если тикер завершится раньше резолва, то выполнится onPingError(new TimeoutException())
        Ticker timeout = new Ticker( () -> onPingError(new PingException.TimeoutException()), false, RESOLVE_TIMEOUT, "--PingHolder.ResolveTimeoutTicker");
        timeout.start();

        try {
            mHost = null;
            InetAddress[] addresses = InetAddress.getAllByName(hostname);   // резолвинг
            //android.os.SystemClock.sleep( 10_000); // имитация долгого резолва
            mHost = Arrays.stream(addresses).findFirst()
                    .orElseThrow(() -> new UnknownHostException("no valid addresses found"));

            mHostName = hostname;
            mHostIP = mHost.getHostAddress();
            successfully = true;
            logger.d("completed. ip: %s", mHostIP);
        }
        catch (IOException e) {
            logger.e(e);
            if(timeout.stop())                       // останавливаем тикер. onPingError вызывается только если не вышел таймаут
                onPingError((e instanceof UnknownHostException) ? new PingException.GeneralException(e, hostname) : (PingException) e );
        }

        boolean noTimeout = timeout.stop();         // останавливаем таймер таймаута

        // onResolve вызывается только если операция успешно завершена, не вышел таймаут, поток не прервали
        if (successfully && noTimeout && !Thread.currentThread().isInterrupted())
            safeListener.onResolve(this);
    }

    private String traceString() {
        return String.format(Locale.ENGLISH,
                "object: %h. thread: %s:%d", hashCode(),
                Thread.currentThread().getName(),
                Thread.currentThread().getId());
    }

    synchronized public void doPing() {
        logger.d(traceString());

        if(mHost == null)
            throw new IllegalStateException("doPing() method was called before resolve(). " + traceString());

        new Thread(() -> {
            try {
                Thread.currentThread().setName("--PingHolder.doPing");
                Ping ping = PingFactory.getInstance().create(++mPingCount, mHost, mTimeout);
                safeListener.onPing(ping);
            }
            catch (IOException e) {
                onPingError(new PingException.GeneralException(e, mHostIP));
            }
        }).start();
    }

    public void unregister() {
        safeListener.clear();
    }

    private void onPingError(PingException exception) {
        logger.e(exception);
        if (exception instanceof PingException.TimeoutException) {
            if (resolveThread.isAlive())
                resolveThread.interrupt();
        }

        safeListener.onPingError(exception);
    }

    private static class SafePingListener implements PingListener {
        volatile PingListener client;

        SafePingListener(PingListener listener) {
            client = listener;
        }

        void clear() {
            client = null;
        }

        @Override
        public void onResolve(PingHolder pingHolder) {
            run("onResolve", l -> l.onResolve(pingHolder));
        }

        @Override
        public void onLookUp(HostInfo hostInfo) {
            run("onLookup", l -> l.onLookUp(hostInfo));
        }

        @Override
        public void onPing(Ping ping) {
            run("onPing", l -> l.onPing(ping));
        }

        @Override
        public void onPingError(PingException exception) {
            run("onPingError", l -> l.onPingError(exception));
        }

        private void run(String tag, Consumer<PingListener> consumer) {
            var listener = client;  //потокобезопасная копия
            if (listener != null) {
                logger.d("%s: %s@%h", tag, listener.getClass().getSimpleName(), listener.hashCode());
                consumer.accept(listener);
            }
        }
    }
}
