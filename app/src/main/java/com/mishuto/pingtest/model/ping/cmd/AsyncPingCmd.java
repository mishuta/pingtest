package com.mishuto.pingtest.model.ping.cmd;

import static com.mishuto.pingtest.common.Const.MIN;
import com.mishuto.pingtest.common.Logger;

import com.mishuto.pingtest.common.Environment;
import com.mishuto.pingtest.common.tickers.Ticker;
import com.mishuto.pingtest.model.stat.PingStatistic;

/**
 * Обертка над PingCmdImpl, делающая вызов команды асинхронным
 * Created by Michael Shishkin on 19.06.2021
 */
public class AsyncPingCmd  {

    public interface OnAsyncPingCmdListener {
        void onPingCmdSuccess(PingStatistic statistic); // событие корректного завершения
        void onPingCmdFault(Exception e);               // событие ошибки команды или парсинга
        default void onPingCmdInterrupted() {}          // событие прерывания команды
    }

    final static int TIMEOUT = 3*MIN;   // время через которое поток будет убит
    OnAsyncPingCmdListener mListener;
    volatile Thread workerThread;
    PingCmd mPingCmdSrv = Environment.INSTANCE.isEmulator() ? new EmulatorCmdImpl() : new PingCmdImpl();
    TimeoutWatcher timeoutWatcher = new TimeoutWatcher();

    // асинхронный вариант метода PingCmdImpl.execSeries
    synchronized public void execAsyncSeries(OnAsyncPingCmdListener listener, String address, int n, int interval, int deadline)  {
        if(inProgress())
            interrupt();
        mListener = listener;
        workerThread = new Thread(()-> worker(address, n, interval, deadline));
        workerThread.setName("--AsyncPingCmd.worker");
        workerThread.start();
        timeoutWatcher.start();
        Logger.INSTANCE.d("worker and timeout threads started");
    }

    // прерывание метода execAsyncSeries.
    // Блокирует вызывающий поток до успешного прерывания (и завершения вызова onPingCmdInterrupted)
    synchronized public void interrupt() {
        if(inProgress()) {
            workerThread.interrupt();
            mPingCmdSrv.interrupt();
            try {
                workerThread.join();
            }
            catch (InterruptedException e) {
                Logger.INSTANCE.e(e);
            }
        }
    }

    private void worker(String address, int n, int interval, int deadline) {
        try {
            PingStatistic statistic = mPingCmdSrv.execSeries(address, n, interval, deadline);
            mListener.onPingCmdSuccess(statistic);
            Logger.INSTANCE.d("worker successfully finished");
        }
        catch (Exception e) {
            if(workerThread.isInterrupted())
                mListener.onPingCmdInterrupted();
            else
                mListener.onPingCmdFault(e);
        }
        finally {
            timeoutWatcher.stop();
        }
    }

    private boolean inProgress() {
        return workerThread != null && workerThread.isAlive();
    }

    private class TimeoutWatcher {
        long currentWorkerId;
        Ticker timeoutTicker;
        boolean isInterrupting = false; // флаг устанавливается на время прерывания воркера, чтобы воркер завершаясь не прервал timeoutTicker

        void start() {
            isInterrupting = false;
            currentWorkerId = workerThread.getId();
            timeoutTicker = new Ticker(this::tick, false, TIMEOUT, "--AsyncPingCmd.timeoutWatcher");
            timeoutTicker.start();
        }

        void stop() {
            if(!isInterrupting)
                timeoutTicker.stop();
        }

        void tick() {
            isInterrupting = true;
            if (inProgress() && !workerThread.isInterrupted() && workerThread.getId() == currentWorkerId) {
                interrupt();
                isInterrupting = false;
                Logger.INSTANCE.d("interrupted by timeout");
            }
        }
    }
}
