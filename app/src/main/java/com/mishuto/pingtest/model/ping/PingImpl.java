package com.mishuto.pingtest.model.ping;

import androidx.annotation.NonNull;

import com.mishuto.pingtest.common.Timer;

import java.io.IOException;
import java.net.InetAddress;
import java.time.Instant;
import java.time.LocalDateTime;

/**
 * Объект, содержащий результат выполнения команды пинг
 * Created by Michael Shishkin on 04.08.2020
 */
public class PingImpl implements Ping {
    private final long mLatency;            // задержка пинга
    private final boolean isLost;           // флаг потерянного пинга. Потерянный пинг включает в себя таймаут и пинга и ошибку, чтобы такой пинг не попал в статистику
    private final boolean isError;          // флаг ошибки пинга.
    private final boolean wasICMPFail;      // флаг ICMP-таймаута
    private final int mTimeout;             // таймаут в мс
    private final int mOrdinal;             // порядковый номер пинга
    private final LocalDateTime mTimeStamp; // время создания пинга

    public PingImpl(int ordinal, InetAddress host, int timeout) throws IOException {
        mOrdinal = ordinal;
        mTimeout = timeout;
        mTimeStamp = LocalDateTime.now();

        Reach reach = reach(host);
        mLatency = reach.latency;

        // если сервис был выгружен ОС во время выполнения isReachable, latency может оказаться неадекватно большим, поэтому если задержка превысила двойной таймаут на > 0.5 сек,
        // устанавливаем lost и ошибку чтобы такой ping не портил статистику.
        // если во время пинга произошла ошибка соединения (напр, был установлен авиарежим), isReachable отрабатывет быстро и возвращает false. Это поведение так же считается ошибкой
        isError = mLatency > 2L * timeout + 500 || !reach.isReachable && mLatency < timeout;
        wasICMPFail = mLatency > timeout;       // если задержка больше чем 1 таймаут, значит по меньшей мере первая попытка пинга провалилась
        isLost = !reach.isReachable || isError; // ошибка или недоступность считаются за потерю пакета
    }

    @Override
    public long getLatency() {
        return mLatency;
    }

    @Override
    public long getEffectiveLatency() {
        return isLost ? 0 :
                wasICMPFail ? getLatency() - mTimeout : getLatency();
    }

    @Override
    public boolean isLost() {
        return isLost;
    }

    @Override
    public LocalDateTime getTimeStamp() {
        return mTimeStamp;
    }

    @Override
    public boolean isError() {
        return isError;
    }

    @Override
    public int getOrdinal() {
        return mOrdinal;
    }

    public int getTimeout() {
        return mTimeout;
    }

    @Override
    public boolean wasICMPFail() {
        return wasICMPFail;
    }

    @NonNull
    @Override
    public String toString() {
        return asString();
    }

    /**
     * Вычисление доступности и задержки.
     * Поскольку величины связанные, то вычисляются в одном методе
     */

    protected Reach reach(InetAddress host) throws IOException {
        Instant start = Instant.now();
        boolean isReachable = host.isReachable(mTimeout);
        long latency = Timer.millsSince(start);
        return new Reach(isReachable, latency);
    }

    // класс для хранения пары latency/isReachable
    final public static class Reach {
        final public long latency;
        final public boolean isReachable;

        public Reach(boolean isReachable, long latency) {
            this.isReachable = isReachable;
            this.latency = latency;
        }
    }
}
