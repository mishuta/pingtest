package com.mishuto.pingtest.model.ping.cmd;

import static com.mishuto.pingtest.common.Const.SEC;
import static com.mishuto.pingtest.common.Utils.logger;

import com.mishuto.pingtest.common.Logger;
import com.mishuto.pingtest.model.ping.EmulatorImpl;
import com.mishuto.pingtest.model.ping.Ping;
import com.mishuto.pingtest.model.stat.PingStatDataBuilder;
import com.mishuto.pingtest.model.stat.PingStatistic;

import java.util.Random;

/**
 * Реализация PingCmd для эмулятора. Статистика генерируется
 * Created by Michael Shishkin on 21.06.2021
 */
public class EmulatorCmdImpl implements PingCmd {

    Thread currentThread;

    @Override
    public int execPing(String address, int timeout) throws ParseNotFoundException {
        currentThread = Thread.currentThread();
        Ping ping = new EmulatorImpl(0, null, timeout);
        if (ping.isLost())
            throw new ParseNotFoundException("lost");
        return (int)ping.getLatency();
    }

    @Override
    public PingStatistic execSeries(String address, int n, int interval, int deadline) throws ParseNotFoundException {
        currentThread = Thread.currentThread();
        float intervalSec = (float) interval / SEC;
        logger.d("ping -c %d -i %.1f -w %d %s", n, intervalSec, deadline / SEC, address);
        try {
            Thread.sleep((n - 1L) * interval);
            Logger.INSTANCE.d("execSeries");
            return new PingStatDataBuilder()
                    .setAvg(randLong(10, 100))
                    .setJitter(randLong(0, 40))
                    .setLost(randLong(0, 5))
                    .setWorst(100)
                    .setBest(10)
                    .setQuantity(n)
                    .setDuration(deadline - 1)
                    .make();
        }
        catch (InterruptedException e) {
            throw new ParseNotFoundException("interrupted");
        }
    }

    @Override
    public void interrupt() {
        currentThread.interrupt();
    }

    Random mRandom = new Random();
    private long randLong(int from, int to) {
        float r = mRandom.nextFloat();
        return (long) (r * (to - from) + from);
    }
}
