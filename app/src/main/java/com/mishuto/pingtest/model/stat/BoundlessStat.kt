package com.mishuto.pingtest.model.stat

import com.mishuto.pingtest.model.ping.Ping
import java.time.Duration
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

/**
 * Рекуррентный расчет статистики, без буфера
 * Created by Michael Shishkin on 27.01.2022
 */
class BoundlessStat : LivePingStatistic {
    override val average: Long
        @Synchronized get() = if(quantityNotLost > 0) sumEffectiveLatency / quantityNotLost else 0

    override val lost: Long
        @Synchronized get() = quantity - quantityNotLost

    override val jitter: Long
        @Synchronized get() = jitterData.jitter

    override val best: Long
        @Synchronized get() = minLatency ?: 0L

    override val worst: Long
        @Synchronized get() = maxLatency

    override var quantity : Long = 0L
        @Synchronized get

    override val duration: Long
        @Synchronized get() = interval.duration

    private var sumEffectiveLatency  = 0L
    private var quantityNotLost = 0L
    private var minLatency : Long? = null
    private var maxLatency = 0L
    private val jitterData = JitterData()
    private val interval = Interval()

    private inner class JitterData {
        private var lastLatency = 0L
        private var sumDeltaLatencies = 0L
        val jitter
            get() = if(quantityNotLost > 1) sumDeltaLatencies/(quantityNotLost - 1) else 0

        fun addLatency(latency: Long) {
            if(quantityNotLost > 1)
                sumDeltaLatencies+= abs(latency - lastLatency)

            lastLatency = latency
        }

        fun clear() {
            lastLatency = 0L
            sumDeltaLatencies = 0L
        }
    }

    private inner class Interval {
        var start: LocalDateTime? = null
        var last: LocalDateTime? = null
        val duration
            get() = if(start == null) 0 else Duration.between(start, last).toMillis()

        fun update(ping: Ping) {
            start = start ?: ping.timeStamp
            last = ping.timeStamp.plus(ping.latency, ChronoUnit.MILLIS)
        }

        fun clear() {
            start = null
            last = null
        }
    }

    @Synchronized override fun addNew(ping: Ping) {
        quantity++
        interval.update(ping)

        if(!ping.isLost) {
            val effLatency = ping.effectiveLatency
            quantityNotLost++
            sumEffectiveLatency+=effLatency
            jitterData.addLatency(effLatency)
            minLatency = min(minLatency ?: effLatency, effLatency)
            maxLatency = max(maxLatency, effLatency)
        }
    }

    @Synchronized override fun clear() {
        sumEffectiveLatency  = 0L
        quantity = 0L
        quantityNotLost = 0L
        minLatency = null
        maxLatency = 0L
        jitterData.clear()
        interval.clear()
    }
}