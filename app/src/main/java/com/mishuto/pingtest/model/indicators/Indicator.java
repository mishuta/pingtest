package com.mishuto.pingtest.model.indicators;

import static java.lang.Math.log;

import com.mishuto.pingtest.R;

/**
 * Модель индикатора параметра.
 * Используется в прогресс-баре
 * Created by Michael Shishkin on 17.06.2020
 */
public class Indicator {

    @FunctionalInterface
    public interface TernaryFunc {
        /**
         *  функция изменения значения индикатора в зависимости от значения входного параметра
         * @param a нижняя граница входного параметра
         * @param b верхняя граница изменения входного параметра
         * @param x входной параметр
         * @return значение индикатора в диапазоне 0-1
         */
        double apply(long a, long b, long x);
    }

    public static final TernaryFunc X_LOG_FUNC = (low, hi, x) -> log(x - low + 1) / log(hi - low + 1) ;             // ~ ln(x). затухающий рост (для верхнего сегмента)
    public static final TernaryFunc X_UNIFORM_FUNC = (low, hi, x) -> (double) (x - low) / (hi - low);               // ~ x. Линейный рост (для среднего сегмента)
    public static final TernaryFunc REVERSE_XY_LOG_FUNC = (low, hi, x) -> 1 - log(hi - x + 1) / log(hi - low + 1);  // ~ -ln(-x). Прогрессирующий рост (для нижнего сегмента)

    final static float BOTTOM = 0;          // нижняя граница значений индиктора
    final static float DOWN_MID = 0.33f;    // нижняя граница среднего сегмента
    final static float DOWN_HI = 0.67f;     // верхняя граница среднего сегмента
    final static float TOP = 1;             // верхняя граница значений индикатора

    /**
     * позиция сегмента индикатора. Для трех сегментов может быть 5 позиций
     */
    public enum Position {
        BELOW_LOW(R.string.score1),     // ниже нижней границы LOW. 0%
        LOW(R.string.score2),           // красный сегмент. [0-33%)
        MEDIUM(R.string.score3),        // желтый сегмент [33-67%)
        HIGH(R.string.score4),          // зеленый сегмент. [67-100%).
        ABOVE_HIGH(R.string.score5);    // выше верхней границы HIGH. 100%

        private final int nameId;

        Position(int label) {
            this.nameId = label;
        }

        public int getScoreId() {
            return nameId;
        }
    }

    /**
     * Сегмент индикатора. Для каждого индикатора существуют три сегмента: нижний, средний, верхний
     */
    public static class Segment {
        private final TernaryFunc mDistFunction;    // функция, для определения значения сегмента индикатора, в зависимости от входного параметра
        private final long mLowBand;                // нижняя граница значений входного параметра для сегмена
        private final long mHiBand;                 // верхняя граница значений входного параметра для сегмента
        final private float mBottom;                // нижняя граница значений сегмента индикатора
        final private float mTop;                   // верхняя граница

        Segment(TernaryFunc distFunction, long lowBand, long hiBand, float bottom, float top) {
            mLowBand = lowBand;
            mHiBand = hiBand;
            mDistFunction = distFunction;
            mBottom = bottom;
            mTop = top;
        }

        // вычисление значения индикатора на сегменте
        public double computeSegmentValue(long x) {
            if(x >= mLowBand && x <= mHiBand)
                return mDistFunction.apply(mLowBand, mHiBand, x) * (mTop - mBottom) + mBottom;

            else if(x < mLowBand)
                return mBottom;

            else
                return mTop;
        }

        public long getLowBand() {
            return mLowBand;
        }

        public long getHiBand() {
            return mHiBand;
        }
    }

    /**
     * Объект для хранения границ сегментов,
     * т.к. почти всегда это предопределенные константы
     */
    public static class Thresholds {
        public final long excellent, good, medium, bad;

        public Thresholds(long excellent, long good, long medium, long bad) {
            this.excellent = excellent;
            this.good = good;
            this.medium = medium;
            this.bad = bad;
        }
    }

    private final int sign;                   // направление значений. Если лучшее значение > худшего, то sign = 1, если наоборот то -1
    private long mIndicatorParameter;   // значениие входного параметра
    private final Segment mLow, mMed, mHigh;  // три сегмента

    public Indicator(Thresholds t) {
        if(t.excellent > t.good && t.good > t.medium && t.medium > t.bad)
            sign = 1;
        else if(t.excellent < t.good && t.good < t.medium && t.medium < t.bad)
            sign = -1;
        else
            throw new IllegalArgumentException();

        mLow = new Segment(REVERSE_XY_LOG_FUNC, sign * t.bad, sign * t.medium, BOTTOM, DOWN_MID);
        mMed = new Segment(X_UNIFORM_FUNC, sign * t.medium, sign * t.good, DOWN_MID, DOWN_HI);
        mHigh = new Segment(X_LOG_FUNC, sign * t.good, sign * t.excellent, DOWN_HI, TOP);
    }

    /**
     * @return сегмент индикатора
     */
    public Segment getIndicatorSegment() {
        if(mIndicatorParameter < mMed.getLowBand())
            return mLow;
        else if (mIndicatorParameter >= mMed.getHiBand())
            return mHigh;
        else
            return mMed;
    }

    public Position getPosition() {
        if(mIndicatorParameter < mLow.mLowBand)
            return Position.BELOW_LOW;
        else if(mIndicatorParameter < mMed.mLowBand)
            return Position.LOW;
        else if (mIndicatorParameter < mHigh.mLowBand)
            return Position.MEDIUM;
        else if(mIndicatorParameter <= mHigh.mHiBand)
            return Position.HIGH;
        else
            return Position.ABOVE_HIGH;
    }

    /**
     * @return значение индикатора [0-1]
     */
    public double computeIndicatorValue() {
        return getIndicatorSegment().computeSegmentValue(mIndicatorParameter);
    }

    public void setIndicatorParameter(long indicatorParameter) {
        mIndicatorParameter = sign * indicatorParameter;
    }

    long getIndicatorParameter() {
        return sign * mIndicatorParameter;
    }
}
