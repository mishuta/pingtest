package com.mishuto.pingtest.model.ping;

import com.mishuto.pingtest.model.ping.cmd.PingCmdImpl;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Реализация пинга посредством стандартной линуксовой команды ping
 * Created by Michael Shishkin on 15.06.2021
 */
public class CmdImpl extends PingImpl {

    public CmdImpl(int ordinal, InetAddress host, int timeout) throws IOException {
        super(ordinal, host, timeout);
    }

    @Override
    protected Reach reach(InetAddress host) throws IOException {
        return reach(host.getHostAddress(), getTimeout());
    }

    public static Reach reach(String address, int timeout) throws IOException {
        try {
            return new Reach(true, new PingCmdImpl().execPing(address, timeout));
        }
        catch (PingCmdImpl.ParseNotFoundException exception) {
            return new Reach(false, timeout);
        }
    }
}
