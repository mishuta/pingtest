package com.mishuto.pingtest.model;

import androidx.annotation.NonNull;

/**
 * DTO объект для передачи информации о хосте
 * Created by Michael Shishkin on 12.12.2021
 */
public final class HostInfo {
    final public String ip;
    final public String name;

    public HostInfo(String ip, String name) {
        this.ip = ip;
        this.name = name;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("host ip: %s. name: %s", ip, name);
    }
}
