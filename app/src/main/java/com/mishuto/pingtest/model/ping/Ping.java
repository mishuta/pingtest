package com.mishuto.pingtest.model.ping;

import java.time.LocalDateTime;

/**
 * Интерфейс для объектов Ping
 * Created by Michael Shishkin on 01.01.2021
 */
/* todo Ping интерфейс заменить дата классом Ping (PingMock отваливается)
    создать функциональный интерфейс PingAction { fun reach(ordinal: Int, host: InetAddress, timeout: Int) : Ping }
    в EmulatorImpl убрать костыль из companion object
 */

public interface Ping  {

    long getLatency();              // задержка пинга
    long getEffectiveLatency();     // задержка без учета таймаута. Используется для вычисления чистой задержки TCP-пинга
    boolean isLost();               // считается ли пакет потерянным (обе попытки пинга неудачны)
    LocalDateTime getTimeStamp();   // время создания пинга
    boolean isError();              // наличие ошибки соединения (помимо таймаута). Например переключение 4G/WiFi
    int getOrdinal();               // порядковый номер пинга в серии
    boolean wasICMPFail();          // была ли ошибка в первой попытке

    default String asString() {     // аналог toString, который нельзя использовать в интерфейсе как default
        return "#" + getOrdinal() +
                ". Lat:" + getLatency() +
                ". Eff Lat:" + getEffectiveLatency() +
                (isLost() ? ". Lost" : "") +
                (isError() ? ". Error" : "");
    }
}
