package com.mishuto.pingtest.model.stat

import com.mishuto.pingtest.common.FixCircularBuffer
import com.mishuto.pingtest.model.ping.Ping
import java.time.Duration
import java.util.Queue
import kotlin.math.abs

/**
 * Потоковый расчет статистики
 * Created by Michael Shishkin on 13.06.2020 (refactored 06/04/2024)
 */
class StreamPingStat(private val pings: Queue<Ping>) : LivePingStatistic {
    
    constructor(pingsNumber: Int) : this(FixCircularBuffer(pingsNumber))
    
    @Synchronized
    override fun addNew(ping: Ping) {
        pings.add(ping)
    }
    
    @Synchronized
    override fun clear() = pings.clear()
    
    
    @get:Synchronized
    override val average: Long
        get() = pings.filter { !it.isLost }
            .map { it.effectiveLatency }
            .average()
            .toLong()
    
    @get:Synchronized
    override val lost: Long
        get() = pings.count { it.isLost }.toLong()
    
    @get:Synchronized
    override val jitter: Long
        get() {
            var prev: Long = 0
            var sum: Long = 0
            var n: Long = 0
            pings.filter { !it.isLost }.forEach { ping ->               // для всех непотерянных
                if (n++ > 0) sum += abs(prev - ping.effectiveLatency)   // начиная с 1 элемента суммируем дельты latency(i) - latency(i-1)
                prev = ping.effectiveLatency
            }
            return if (n < 2) 0 else sum / (n - 1) // джиттер рассчитывается для очереди из 2+ элементов
        }
    
    @get:Synchronized
    override val best: Long
        get() = pings.filter { !it.isLost }.minOfOrNull { it.effectiveLatency } ?: 0
    
    @get:Synchronized
    override val worst: Long
        get() = pings.filter { !it.isLost }.maxOfOrNull { it.effectiveLatency } ?: 0
    
    @get:Synchronized
    override val duration: Long
        get() = if (pings.isNotEmpty())
            pings.last().let { last -> Duration.between(pings.element().timeStamp, last.timeStamp).toMillis() + last.latency }
        else 0
    
    @get:Synchronized
    override val quantity: Long
        get() = pings.size.toLong()
}
