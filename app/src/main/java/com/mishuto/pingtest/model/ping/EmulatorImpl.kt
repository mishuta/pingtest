package com.mishuto.pingtest.model.ping

import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Assert
import com.mishuto.pingtest.common.Resources.getBoolean
import com.mishuto.pingtest.common.Resources.getInt
import java.io.IOException
import java.lang.Thread.sleep
import java.net.InetAddress
import java.util.Random
import kotlin.math.min

/**
 * Версия пинга для эмуляторов Android (isReachable не доступна для эмулятора)
  * Created by Michael Shishkin on 01.01.2021 (refactored on Kotlin 17/03/24)
 */
class EmulatorImpl(ordinal: Int, host: InetAddress, timeout: Int) : PingImpl(ordinal, host, timeout) {
    companion object {
        private val pingMinBound = getInt(R.integer.ping_time_min)
        private val pingMaxBound = getInt(R.integer.ping_time_max)
        private val pingAvg = getInt(R.integer.ping_avg)
        private val lostP = getInt(R.integer.lost_prob) / 1000f
        private val icmpFailP = getInt(R.integer.icmp_fail_prob) / 1000f
        private val exceptionP = getInt(R.integer.exception_prob) / 1000f
        private val errorP = getInt(R.integer.err_prob) / 1000f
        private val needEmulatePingDuring = getBoolean(R.bool.emulate_ping_duration)
        private val random = Random()
    }
    
    @Synchronized
    override fun reach(host: InetAddress): Reach {
        if (random.nextFloat() < exceptionP) throw IOException("network is unreachable")
        if (random.nextFloat() < errorP) return Reach(false, 5)
        if (random.nextFloat() < lostP) return Reach(false, 2L * timeout)
            .also { if (needEmulatePingDuring) sleep(it.latency) }
        
        var latency = min(computeLatency(), 2L * timeout)
        if (random.nextFloat() < icmpFailP) latency += timeout
        if (needEmulatePingDuring) sleep(latency)
        return Reach(true, latency)
    }
    
    private fun computeLatency(): Long {
        Assert.that(pingAvg in pingMinBound + 1 until pingMaxBound, "$pingMinBound < $pingAvg < $pingMaxBound")
        var lowBound = -1.0
        var upBound = 1.0
        val ratio = (pingMaxBound.toDouble() - pingAvg) / (pingAvg - pingMinBound)
        if (ratio > 1) upBound = ratio //upBound >=1
        else lowBound = -1 / ratio // lowBound <= -1
        val rnd = Math.max(lowBound, Math.min(random.nextGaussian(), upBound)) // приводим к диапазону [lowBound;upBound]
        return ((pingAvg - pingMinBound) * rnd + pingAvg).toLong()
    }
}
