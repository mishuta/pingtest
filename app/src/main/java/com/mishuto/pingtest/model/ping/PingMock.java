package com.mishuto.pingtest.model.ping;

import static java.lang.Thread.sleep;

import java.time.LocalDateTime;

/**
 * Mock объект для пинга для реализации тестов
 * Находится в основном коде, т.к. используется и обычными тестами и androidTest
 * Created by Michael Shishkin on 01.01.2021
 */
public class PingMock implements Ping {
    private long mPingLatency;
    private long mEffectiveLatency;
    private boolean isLost = false;
    private boolean isError = false;
    private boolean wasICMPFail = false;
    private int mOrdinal;
    private LocalDateTime mTimeStamp;

    private static int count = 0;

    public PingMock(long latency) throws InterruptedException {
        mPingLatency = latency;
        mEffectiveLatency = latency;
        mOrdinal = count++;
        sleep(1L);
        mTimeStamp = LocalDateTime.now();
    }

    @Override
    public long getLatency() {
        return mPingLatency;
    }

    @Override
    public long getEffectiveLatency() {
        return mEffectiveLatency;
    }

    @Override
    public boolean isLost() {
        return isLost || isError;
    }

    @Override
    public LocalDateTime getTimeStamp() {
        return mTimeStamp;
    }

    @Override
    public boolean isError() {
        return isError;
    }

    @Override
    public int getOrdinal() {
        return mOrdinal;
    }

    @Override
    public boolean wasICMPFail() {
        return wasICMPFail;
    }

    public PingMock setPingLatency(long pingLatency) {
        mPingLatency = pingLatency;
        return this;
    }

    public PingMock setEffectiveLatency(long effectiveLatency) {
        mEffectiveLatency = effectiveLatency;
        return this;
    }

    public PingMock setLost(boolean lost) {
        isLost = lost;
        return this;
    }

    public PingMock setError(boolean error) {
        isError = error;
        return this;
    }

    public PingMock setWasICMPFail(boolean wasICMPFail) {
        this.wasICMPFail = wasICMPFail;
        return this;
    }

    public PingMock setOrdinal(int ordinal) {
        mOrdinal = ordinal;
        return this;
    }

    public PingMock setTimeStamp(LocalDateTime timeStamp) {
        mTimeStamp = timeStamp;
        return this;
    }
}
