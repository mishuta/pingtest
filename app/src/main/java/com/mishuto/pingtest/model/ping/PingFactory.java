package com.mishuto.pingtest.model.ping;

import com.mishuto.pingtest.common.Environment;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Фабрика создания объектов, имплементирующих Ping
 * Created by Michael Shishkin on 01.01.2021
 */
public class PingFactory {
    public interface Creator {
        Ping create(int ordinal, InetAddress host, int timeout) throws IOException;
    }

    private static final PingFactory sFactory = new PingFactory();
    private Creator mCreator;   // внешний конструктор, который может быть проинжектирован в фабрику

    private PingFactory() { }

    public static PingFactory getInstance() {
        return sFactory;
    }

    public void setCreator(Creator creator) {
        mCreator = creator;
    }

    public Ping create(int ordinal, InetAddress host, int timeout) throws IOException {
        if(mCreator != null )
            return mCreator.create(ordinal, host, timeout);

        else if(Environment.INSTANCE.isEmulator())
            return new EmulatorImpl(ordinal, host, timeout);

        else
            return new PingImpl(ordinal, host, timeout);
    }
}
