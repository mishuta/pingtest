package com.mishuto.pingtest.model.stat

import com.mishuto.pingtest.common.Utils
import com.mishuto.pingtest.model.indicators.GroupIndicators

interface PingStatistic {
    val average: Long   // средний пинг
    val lost: Long      // потерянные
    val jitter: Long    // джиттер
    val best: Long      // минимальный пинг, мс
    val worst: Long     // максимальный пинг, мс
    val quantity: Long  // количество пингов в статистике
    val duration: Long  // длительность статистики, мс
    
    fun isEmpty(): Boolean =    // объект пуст
        synchronized(this) {
            quantity == 0L
        }
    
    fun getLostPermille(): Int =
        synchronized(this) {
            if (quantity == 0L) 0 else (1000 * lost / quantity).toInt()
        }
    
    fun asString(): String? =
        if (isEmpty()) "is empty"
        else "quantity: $quantity. lost: $lost. avg: $average. best: $best. worst: $worst. jitter: $jitter"
    
    companion object {
        @JvmStatic
        fun PingStatistic.overallQuality() = Utils.percent(GroupIndicators.overallFromStat(this).totalIndicator.computeIndicatorValue())
    }
}