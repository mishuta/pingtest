package com.mishuto.pingtest.model;

import com.mishuto.pingtest.App;
import com.mishuto.pingtest.R;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Исключения во время пинга
 * Created by Michael Shishkin on 28.05.2021
 */
abstract public class PingException extends IOException {

    private static String getString(int id, Object ... objects) {
        return App.getAppContext().getString(id, objects);
    }

    // эксепшн таймаута операции
    public static class TimeoutException extends PingException {
        @Override
        public String getMessage() {
            return getString(R.string.timeout_resolve_err);
        }
    }

    // эксепшн для прочих сетевых ошибок
    public static class GeneralException extends PingException {
        IOException mException;
        String mHost;

        public GeneralException(IOException exception, String host) {
            mException = exception;
            mHost = host;
        }

        @Override
        public String getMessage() {
            if(mException instanceof UnknownHostException)
                return  getString(R.string.cant_resolve_err, mHost);
            else
                return mException.getMessage();
        }
    }
}
