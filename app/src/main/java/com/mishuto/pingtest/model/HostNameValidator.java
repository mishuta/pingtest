package com.mishuto.pingtest.model;

import static java.util.regex.Pattern.matches;

import java.util.function.Predicate;

/**
 * Валидатор hostname/ip
 * Created by Michael Shishkin on 28.06.2020
 */
public class HostNameValidator implements Predicate<CharSequence> {
    private static final String IP4_SEG = "(25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])";
    public static final String IP4_MATCHER = "(" + IP4_SEG + "\\.){3}" + IP4_SEG;

    private static final String IP6_SEG = "[0-9a-f]{1,4}";
    public static final String IP6_MATCHER = "(" +
            "("+IP6_SEG+":){7,7}"+IP6_SEG+"|" +               // 1:2:3:4:5:6:7:8
            "("+IP6_SEG+":){1,7}:|" +                         // 1::                                 1:2:3:4:5:6:7::
            "("+IP6_SEG+":){1,6}:"+IP6_SEG+"|" +              // 1::8               1:2:3:4:5:6::8   1:2:3:4:5:6::8
            "("+IP6_SEG+":){1,5}(:"+IP6_SEG+"){1,2}|" +       // 1::7:8             1:2:3:4:5::7:8   1:2:3:4:5::8
            "("+IP6_SEG+":){1,4}(:"+IP6_SEG+"){1,3}|" +       // 1::6:7:8           1:2:3:4::6:7:8   1:2:3:4::8
            "("+IP6_SEG+":){1,3}(:"+IP6_SEG+"){1,4}|" +       // 1::5:6:7:8         1:2:3::5:6:7:8   1:2:3::8
            "("+IP6_SEG+":){1,2}(:"+IP6_SEG+"){1,5}|" +       // 1::4:5:6:7:8       1:2::4:5:6:7:8   1:2::8
            IP6_SEG+":((:"+IP6_SEG+"){1,6})|" +               // 1::3:4:5:6:7:8     1::3:4:5:6:7:8   1::8
            ":((:"+IP6_SEG+"){1,7}|:)|" +                     // ::2:3:4:5:6:7:8    ::2:3:4:5:6:7:8  ::8       ::
            "fe80:(:"+IP6_SEG+"){0,4}%[0-9a-zA-Z]{1,}|" +     // fe80::7:8%eth0     fe80::7:8%1  (link-local IPv6 addresses with zone index)
            "::(ffff(:0{1,4}){0,1}:){0,1}"+IP4_MATCHER+"|" +  // ::255.255.255.255  ::ffff:255.255.255.255  ::ffff:0:255.255.255.255 (IPv4-mapped IPv6 addresses and IPv4-translated addresses)
            "("+IP6_SEG+":){1,4}:"+IP4_MATCHER+")";           // 2001:db8:3:4::192.0.2.33  64:ff9b::192.0.2.33 (IPv4-Embedded IPv6 Address)

    // Выражение для проверки hostname
    private static final String HOST_MATCHER =
            "(" +               // вариант 1. <Одиночный хост> (localhost)
                "[a-zA-Z]" +            // первый символ всегда буква
                "[-0-9a-zA-Z]{0,61}" +  // от 0 до 61 символа, включающие буквы, цифры и дефис
                "[0-9a-zA-Z]" +         // последний символ буква или цифра. Всего не более 63 символов
            ")" +
            "|(" +                      // вариант 2. <Один или несколько саб-доменов>.<домен первого уровня> (www.google.com)
                "(" +
                 "[0-9a-zA-Z]([-0-9a-zA-Z]{0,61}[0-9a-zA-Z])?" +    // саб-домен начинается и заканчивается буквой или цифрой, посредине может быть дефис. Длина 1..63
                 "\\.)" +                                           // после саб-домена идет точка.
                 "+" +                                              // Саб-домен может повторятся 1 или более раз
                "[a-zA-Z]{2,6}" +                                   // домен 1-го уровня состоит из 2-6 букв
            ")" +
            "|(" + IP4_MATCHER + ")" +                          // вариант 3. <ip адрес> (192.168.1.1)
            "|" + IP6_MATCHER;

    @Override
    public boolean test(CharSequence host) {
        return matches(HOST_MATCHER, host);
    }
}
