package com.mishuto.pingtest.model.ping.cmd;

import static com.mishuto.pingtest.common.Const.SEC;
import static com.mishuto.pingtest.common.Utils.logger;
import static com.mishuto.pingtest.common.Utils.readInputStreamToString;

import static java.lang.Math.min;

import android.annotation.SuppressLint;

import com.mishuto.pingtest.model.stat.PingStatData;
import com.mishuto.pingtest.model.stat.PingStatDataBuilder;
import com.mishuto.pingtest.model.stat.PingStatistic;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Locale;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Реализация пинга через Unix-команду ping
 * Created by Michael Shishkin on 16.06.2021
 */
@SuppressLint("DefaultLocale")
public class PingCmdImpl implements PingCmd {
    private final static String NUMBER = "(\\d+\\.\\d+|\\d+)"; // паттерн для числа с точкой или целого
    Command mCommand = new Command();

    @Override
    public int execPing(String address, int timeout) throws IOException, ParseNotFoundException {
        String pingCmd = String.format("ping -c 1 -n -w %d %s", timeout / SEC, address);
        return new Parser(mCommand.execute(pingCmd)).parseNext("time=\\d+");
    }

    @Override
    public PingStatistic execSeries(String address, int n, int interval, int deadline) throws IOException {
        try {
            mCommand.mLock.lock();

            if(Thread.currentThread().isInterrupted())  // если interrupt() был вызван раньше чем execSeries
                throw new InterruptedIOException();     // сразу выходим

            float intervalSec = (float) interval / SEC;
            String cmd = String.format(Locale.ENGLISH, "ping -c %d -i %.1f -w %d %s", n, intervalSec, deadline / SEC, address);
            Parser parser = new Parser(mCommand.execute(cmd));

            long quantity = min(parser.parseNext(NUMBER +" packets transmitted"), n);   // "залипающую" серию обрезаем до n,
            long lost = quantity - min(parser.parseNext(NUMBER + " received"), n);      // когда на ping -c 20 приходит "144 packets transmitted, 20 received...")

            return new PingStatDataBuilder()
                    .setQuantity(quantity)
                    .setLost(lost)
                    .setDuration(parser.parseNext("time " + NUMBER))
                    .setBest(parser.parseNext("mdev = " + NUMBER))
                    .setAvg(parser.parseNext("/" + NUMBER))
                    .setWorst(parser.parseNext("/" + NUMBER))
                    .setJitter(parser.parseNext("/" + NUMBER))
                    .make();
        }
        catch (ParseNotFoundException e) {
            logger.e(e);
            return new PingStatData(0, n, 0, 0, 0, n, mCommand.getElapsed());
        }
        finally {
            mCommand.mLock.unlock();
        }
    }

    @Override
    public void interrupt() {
        mCommand.mLock.lock();
        mCommand.forceClose();
        mCommand.mLock.unlock();
        logger.d("executing command interrupted");
    }

    static class Command {
        private Process mCmdProcess;
        long mElapsed;
        private final SingleLocker mLock = new SingleLocker();
        volatile private boolean isInterrupted;

        String execute(String cmd) throws IOException {
            mLock.lock();
            logger.d("execute cmd: " + cmd);
            Instant start = Instant.now();
            try {
                isInterrupted = false;
                mCmdProcess = Runtime.getRuntime().exec(cmd);
                logger.d("execution ran");
                mLock.unlock();

                String cmdErr = readInputStreamToString(mCmdProcess.getErrorStream());
                String cmdOut = readInputStreamToString(mCmdProcess.getInputStream());
                logger.d(cmdErr + cmdOut);

                if (isInterrupted)
                    throw new InterruptedIOException("CMD Process was interrupted");

                if(!cmdErr.isEmpty())
                    throw new IOException(cmdErr);

                return cmdOut;
            }
            finally {
                mLock.lock();
                mElapsed = Duration.between(start, Instant.now()).toMillis();
                mCmdProcess.destroy();
                mCmdProcess = null;
                mLock.unlock();
            }
        }

        void forceClose() {
            mLock.lock();
            if(mCmdProcess != null && mCmdProcess.isAlive()) {
                logger.d("terminating cmd process...");
                mCmdProcess.destroyForcibly();
                isInterrupted = true;
            }
            mLock.unlock();
        }

        public long getElapsed() {
            return mElapsed;
        }

    }

    private static class Parser {
        Matcher mMatcher;

        public Parser(String sample) {
            mMatcher = Pattern.compile("").matcher(sample);
        }

        // искать следующее вхождение
        public int parseNext(String patternStr) throws ParseNotFoundException {
            if(!patternStr.equals(mMatcher.pattern().pattern()))
                mMatcher.usePattern(Pattern.compile(patternStr));

            Matcher regionalMatcher = Pattern.compile(NUMBER).matcher(find(mMatcher));
            return (Math.round(Float.parseFloat(find(regionalMatcher))));
        }

        private String find(Matcher matcher) throws ParseNotFoundException {
            if(!matcher.find())
                throw new ParseNotFoundException(matcher.pattern().pattern() + " not found");
            return matcher.group();
        }
    }

    // класс, который получает лок для одного и того же потока только один раз
    private static class SingleLocker extends ReentrantLock {
        @Override
        public void lock() {
            if (Thread.currentThread() != getOwner()) {
                super.lock();
                //d("SingleLocker.lock");
            }
        }

        // разлочивать можно любое количество раз
        @Override
        public void unlock() {
            if(isLocked() && isHeldByCurrentThread() ) { // разлочиваваются только собственные локи
                super.unlock();
                //d("SingleLocker.unlock");
            }
        }
    }
}