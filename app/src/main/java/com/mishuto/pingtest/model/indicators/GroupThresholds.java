package com.mishuto.pingtest.model.indicators;

import static com.mishuto.pingtest.model.indicators.Indicator.Thresholds;

/**
 * Предельные значения для каждого сегмента каждого инидкатора, собранные в группы
 * Created by Michael Shishkin on 20.06.2020
 */
public enum GroupThresholds {
    GAMES(
        new Thresholds(20, 50, 150, 500),   // ping, мс
        new Thresholds(10, 20, 70, 100),    // jitter, мс
        new Thresholds(0, 10, 20, 80)),     // lost, промилле

    VOIP(
        new Thresholds(100, 300, 500, 1000),
        new Thresholds(10, 30, 120, 300),
        new Thresholds(3, 10, 20, 60)),

    VIDEO(
        new Thresholds(200, 1000, 4000, 10000),
        new Thresholds(100, 500, 2000, 5000),
        new Thresholds(20, 50, 100, 150)),

    OVERALL(
        new Thresholds(30, 100, 300, 3000),
        new Thresholds(10, 30, 150, 1500),
        new Thresholds(5, 10, 40, 150));

    final Thresholds ping, jitter, loss;

    GroupThresholds(Thresholds ping, Thresholds jitter, Thresholds loss) {
        this.ping = ping;
        this.jitter = jitter;
        this.loss = loss;
    }

}
