package com.mishuto.pingtest.model.ping.cmd;

import com.mishuto.pingtest.model.stat.PingStatistic;

import java.io.IOException;

/**
 * Интерфейс пингования через unix-команды
 * Created by Michael Shishkin on 21.06.2021
 */
public interface PingCmd {

    /**
     * Выполенение одиночной команды unix ping.
     * @param address ip-адрес/host.
     * @param timeout таймаут пинга, мс (дожен быть >1000, округляется до целого числа в сек)
     * @return latency, мс
     * @throws IOException эксепшн при выполнении команды, например неверный адрес
     * @throws ParseNotFoundException Бросается в случае неуспеха пинга (таймаут или недоступность)
     */
    int execPing(String address, int timeout) throws IOException, ParseNotFoundException;

    /**
     * Выполнение N последовательных пингов со сбором статистики
     * @param address ip-адрес/host.
     * @param n количество пингов в серии
     * @param interval интервал между пингами, мс
     * @param deadline - таймаут всей серии, мс (дожен быть >1000, округляется до целого числа в сек)
     * @return статистика серии
     * @throws IOException эксепшн при выполнении команды, например неверный адрес
     */
    PingStatistic execSeries(String address, int n, int interval, int deadline) throws IOException, ParseNotFoundException;

    /**
     * прерывание исполнения команды во внешнем процессе
     */
    void interrupt();

    // ошибка парсинга вывода, когда не найден искомый параметр
    class ParseNotFoundException extends Exception {
        ParseNotFoundException(String errMes) {
            super(errMes);
        }
    }
}
