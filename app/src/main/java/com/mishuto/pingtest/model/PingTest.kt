package com.mishuto.pingtest.model

import com.mishuto.pingtest.common.Const
import com.mishuto.pingtest.common.minus
import com.mishuto.pingtest.data.generateId
import com.mishuto.pingtest.model.stat.PingStatData
import com.mishuto.pingtest.model.stat.PingStatistic
import com.mishuto.pingtest.settings.SettingsPrefManager
import java.time.Instant
import java.time.LocalDateTime

/**
 * Состояние теста
 * Created by Michael Shishkin on 03.09.2022
 */
class PingTest {
    val testId: Long = generateId()
    
    // момент старта теста. Может не совпадать с моментом сбора статистики, т.к. первый пинг стартует после резолва
    val started: LocalDateTime = LocalDateTime.now()
    lateinit var hostInfo: HostInfo
    var statistic: PingStatistic = PingStatData()
    
    // момент останова теста. Может немного отставать относительно завершения статистики, т.к. незавершенный пинг, не попавший в статистику, имеет длительность
    var finished: LocalDateTime? = null
    var lastRenewed: Instant? = null
    
    fun finish() {
        finished = LocalDateTime.now()
    }
    
    fun renew() {
        lastRenewed = Instant.now()
        statistic = PingStatData()
    }
    
    fun isTestTimeExpired() = SettingsPrefManager.preferences.testDuration.let { testDuration ->
        testDuration != Const.INFINITY && (if (finished != null) finished else LocalDateTime.now())!!.minus(started) > testDuration
    }
}