package com.mishuto.pingtest;

import com.mishuto.pingtest.model.ping.Ping;
import com.mishuto.pingtest.model.ping.PingFactory;
import com.mishuto.pingtest.model.ping.PingMock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import static android.os.SystemClock.sleep;

/**
 * Слой между тестом-клиентом и PingHolder.
 * На вход принимает массив тестовых пингов PingMock.
 * PingHolder создает пинги в отдельных потоках
 * Клиент вызывает await, чтобы заблокировать тестовый поток, пока все пинги в массиве не будут "созданы"
 * PingService через PingHolder.
 * <p>
 * Сценарий работы тест-клиента:
 *   Создать PingCreateHolder, передав в конструтор массив пробных пингов
 *   запустить PingService.
 *   Вызвать await(), чтобы дождаться пока все пробные пинги не "создадутся" сервисом
 *   Остановить сервис
 * <p>
 * Created by Michael Shishkin on 02.01.2021
 */
public class PingMockCreator {
    final PingMock[] mPings;                    // массив пингов
    int mCount = 0;                             // счетчик "созданных" пингов
    ReentrantLock mLock = new ReentrantLock();  // локер
    Condition mCondition = mLock.newCondition();// условие исчерпания массива

    public PingMockCreator(PingMock[] pings) {
        mPings = pings;
        PingFactory.getInstance().setCreator((o, h, t) -> createNext(o));
    }

    // имитирует создание пинга, забирая готовый мок-объект из массива
    private Ping createNext(int ordinal) {
        mLock.lock();
        Ping ping = mPings[mCount++].setOrdinal(ordinal);
        mCondition.signal();    // сообщаем тестовому потоку, что счетчик изменился
        mLock.unlock();
        return ping;
    }

    // блокировка тестирующего потока, пока не создастся нужное количество пингов
    public void await() throws InterruptedException {
        mLock.lock();
        while (mCount < mPings.length)  // ждать, пока не закончится массив
            mCondition.await();
        mLock.unlock();
        sleep(50);                      // даем отработать сервису после последнего вызова пинга
    }
}