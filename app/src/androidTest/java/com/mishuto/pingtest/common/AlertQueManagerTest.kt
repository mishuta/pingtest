package com.mishuto.pingtest.common

import android.app.Activity
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.mishuto.pingtest.common.Utils.createDialogBuilder
import com.mishuto.pingtest.utils.MainActivityBaseTest
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import org.junit.Test

/**
 * AlertQueManager тест
 * Created by Michael Shishkin on 08.03.2024
 */
class AlertQueManagerTest : MainActivityBaseTest() {
    
    
    class TestDialog(private val tag: String, val alertQueManager: AlertQueManager, activity: Activity) {
        var dismissed = false
        private val dialog = createDialogBuilder(activity)
            .setTitle(tag)
            .setMessage(tag)
            .create()
        
        fun enqueue() = alertQueManager.enqueue(tag, dialog) { dismissed = true }
    }
    
    @Test
    fun one_dialog_in_que_should_be_showed_immediately() {
        scenario.onActivity {
            // Создание AlertQueManager должно происходить в главном потоке.
            // Колбэк onActivity {} отрабатывает мгновенно, т.к. к моменту scenario.launchActivity уже обработало.
            // Поэтому перед следующей командой onView (которая должна запускаться в тестовом потоке) можно не ставить задержку, если тест не ломается.
            TestDialog("testDialog", AlertQueManager(it), it).enqueue()
        }
        onView(withId(android.R.id.message)).check(matches(withText("testDialog")))
    }
    
    @Test
    fun second_dialog_in_que_should_be_showed_after_release_previous_one() {
        scenario.onActivity {
            val alertQueManager = AlertQueManager(it)
            TestDialog("testDialog1", alertQueManager, it).enqueue()
            TestDialog("testDialog2", alertQueManager, it).enqueue()
        }
        onView(withId(android.R.id.message)).check(matches(withText("testDialog1")))
        onView(withText("testDialog2")).check(doesNotExist())
        pressBack()
        
        onView(withId(android.R.id.message)).check(matches(withText("testDialog2")))
        onView(withText("testDialog1")).check(doesNotExist())
    }
    
    @Test
    fun dialog_still_showed_after_activity_restart() {
        scenario.onActivity {
            TestDialog("testDialog", AlertQueManager(it), it).enqueue()
        }
        // stop/start activity
        scenario.moveToState(Lifecycle.State.CREATED)
        scenario.moveToState(Lifecycle.State.RESUMED)
        onView(withId(android.R.id.message)).check(matches(withText("testDialog")))
    }
    
    @Test
    fun queue_must_not_contain_more_then_one_element_with_this_tag() {
        scenario.onActivity {
            val alertQueManager = AlertQueManager(it)
            TestDialog("testDialog", alertQueManager, it).enqueue()
            TestDialog("testDialog", alertQueManager, it).enqueue()
        }
        pressBack()
        onView(withText("testDialog")).check(doesNotExist())
    }
    
    @Test
    fun test_onDismiss() {
        var dialog: TestDialog? = null
        scenario.onActivity {
            dialog = TestDialog("testDialog", AlertQueManager(it), it)
            dialog!!.enqueue()
        }
        assertFalse(dialog!!.dismissed)
        pressBack()
        assertTrue(dialog!!.dismissed)
    }
    
    @Test
    fun onDismiss_does_not_call_before_activity_restarted_and_calls_after() {
        var dialog: TestDialog? = null
        scenario.onActivity {
            dialog = TestDialog("testDialog", AlertQueManager(it), it)
            dialog!!.enqueue()
        }
        scenario.moveToState(Lifecycle.State.CREATED)
        scenario.moveToState(Lifecycle.State.RESUMED)
        assertFalse(dialog!!.dismissed)
        pressBack()
        assertTrue(dialog!!.dismissed)
    }
}