package com.mishuto.pingtest.common.tickers;

import static com.mishuto.pingtest.common.Assertions.assertEqualsWithAccuracy;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static java.lang.Thread.sleep;
import static java.time.Instant.now;

import com.mishuto.pingtest.common.Timer;

import java.time.Instant;

/**
 * Имплементация тестов для всех видов тикеров
 * Created by Michael Shishkin on 29.03.2021
 */
public class TickerTestImp {

    private Instant mInstant;
    private final static int ACCURACY = 50; //ms
    private long measuredDuration;

    void startTest(TickerStrategy strategy, int delay) throws Exception {
        Ticker ticker = createAndGoTicker(strategy, false, delay);        // запускаем тикер
        assertTrue(ticker.isStarted());
        waitTickAndCheck(ticker, delay);
    }

    synchronized private void waitTickAndCheck(Ticker ticker, long delay) throws Exception {
        wait();                           // ждем завершения тикера
        assertEqualsWithAccuracy(delay, measuredDuration, ACCURACY); // убеждаемся что тикнул во-время
        sleep(1);                   // ждем еще мс, чтобы дать возможность завершить тикеру поток, после коллбэка и сбросить isStarted
        assertFalse(ticker.isStarted()); // правильный статус
    }

    void stopTest(TickerStrategy strategy, int delay) throws Exception {
        Ticker ticker = createAndGoTicker(strategy, false, delay);       // запускаем тикер

        synchronized (this) {
            sleep(delay/2);                              // через delay/2 мс стопим
            ticker.stop();
            assertFalse(ticker.isStarted());                // застоплен
            wait(delay);                                    // ждем и убеждаемся что тикер не сработал
            assertEquals(measuredDuration, 0);
        }
    }

    void infinityTest(TickerStrategy strategy, int delay) throws Exception {
        int n = 4; // итераций
        Ticker ticker = createAndGoTicker(strategy, true, delay);

        synchronized (this) {
            while (n-- > 0)
                wait();

            ticker.stop();
            System.out.println("mDuration is " + measuredDuration);
            assertFalse(ticker.isStarted());
            assertEqualsWithAccuracy(delay * 4L, measuredDuration, ACCURACY);
        }
    }

    // повторный старт на том же тикере
    void restartTest(TickerStrategy strategy, int delay) throws Exception {
        Ticker ticker = createAndGoTicker(strategy, false, delay);
        sleep(delay/2);
        ticker.stop();
        assertFalse(ticker.isStarted());
        ticker.start();
        time();
        assertTrue(ticker.isStarted());
        waitTickAndCheck(ticker, delay);
    }

    Ticker createAndGoTicker(TickerStrategy strategy, boolean isInfinity, int delay) {
        Ticker ticker =  new Ticker(()->            // создаем тикер
        {
            synchronized (this) {
                stopWatch();                        // по завершении получаем время таймера
                notify();                           // освобождаем монитор
            }
        }, isInfinity, delay,  strategy);

        ticker.start();                             // стартуем
        time();                                     // засекаем время

        return ticker;
    }


    void time() {
        mInstant = now();
        measuredDuration = 0;
    }

    void stopWatch() {
        measuredDuration = Timer.millsSince(mInstant);
    }
}