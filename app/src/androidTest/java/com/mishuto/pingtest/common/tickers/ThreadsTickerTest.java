package com.mishuto.pingtest.common.tickers;

import org.junit.Test;

/**
 * Тест тикеров на потоках
 * Created by Michael Shishkin on 30.03.2021
 */
public class ThreadsTickerTest {

    private final static int DELAY_THREAD = 100; // ms
    private final TickerTestImp mImp = new TickerTestImp();

    @Test
    public void threadsTickerStart() throws Exception {
        mImp.startTest(createTicker(), DELAY_THREAD);
    }

    @Test
    public void threadsTickerStop() throws Exception {
        mImp.stopTest(createTicker(), DELAY_THREAD);
    }

    @Test
    public void threadsTickerInfinity() throws Exception {
        mImp.infinityTest(createTicker(), DELAY_THREAD);
    }

    @Test
    public void threadsTickerRestart() throws Exception {
        mImp.restartTest(createTicker(), DELAY_THREAD);
    }

    private ThreadsTicker createTicker() {
        return new ThreadsTicker(getClass().getSimpleName());
    }
}