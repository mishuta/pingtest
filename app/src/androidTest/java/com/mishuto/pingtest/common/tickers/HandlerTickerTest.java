package com.mishuto.pingtest.common.tickers;

import org.junit.Test;

/**
 * Тест тикера на HandlerThread
 * Created by Michael Shishkin on 30.03.2021
 */
public class HandlerTickerTest {
    private final static int DELAY_HANDLER = 100; // ms
    private final TickerTestImp mImp = new TickerTestImp();

    @Test
    public void handlerTickerStop() throws Exception {
        mImp.stopTest(new HandlerTicker(), DELAY_HANDLER);
    }

    @Test
    public void handlerTickerInfinity() throws Exception {
        mImp.infinityTest(new HandlerTicker(), DELAY_HANDLER);
    }

    @Test
    public void handlerTickerRestart() throws Exception {
        mImp.restartTest(new HandlerTicker(), DELAY_HANDLER);
    }

    @Test
    public void handlerTickerStart() throws Exception {
        mImp.startTest(new HandlerTicker(), DELAY_HANDLER);
    }


}