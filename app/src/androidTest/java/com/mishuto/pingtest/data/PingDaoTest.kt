package com.mishuto.pingtest.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mishuto.pingtest.common.net.NetTypeObject
import com.mishuto.pingtest.data.EventDao.Companion.deleteTestAndOlderAnywhere
import com.mishuto.pingtest.model.ping.PingMock
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.time.LocalDateTime.now

/**
 * Тест механизма заворачивания БД
 * Created by Michael Shishkin on 17.09.2022
 */
@RunWith(AndroidJUnit4::class)
class PingDAOTest {
    
    private val startKey: Long = 100
    
    @Before
    fun clearDb() {
        deleteTestAndOlderAnywhere(startKey + 100)
    }
    
    @Test
    fun ordinaryInsert() {
        fillDb(3, 3)
        assertEquals(9, pingDAO.getCount())
        assertEquals(9, netChangeDAO.getCount())
        assertEquals(9, errMessageDAO.getCount())
    }
    
    //старые тесты удаляются так, чтобы не превышать лимит по строкам
    @Test
    fun wrapOldTests() {
        fillDb(4, 3, 10)
        
        listOf(pingDAO, netChangeDAO, errMessageDAO).forEach {
            assertEquals(9, it.getCount())
            assertEquals(0, it.getCountForTest(startKey))
            assertEquals(3, it.getCountForTest(startKey + 1))
        }
    }
    
    @Test
    fun allOldKeysMustBeDeleted() {
        fillDb(2, 3)
        
        listOf(pingDAO, netChangeDAO, errMessageDAO).forEach {
            it.deleteTestAndOlder(startKey + 1)
            assertEquals(0, it.getCount())
        }
    }
    
    @Test
    fun wrapCurrentTest() {
        fillDb(1, 12, 10)
        assertEquals((10 + 1) / 2 + (12 - 10), pingDAO.getCount())
        assertEquals(12, netChangeDAO.getCount())
    }
    
    // заполнение БД с враппингом. Тесты имеют key=startKey, startKey+1c, ...
    private fun fillDb(testCount: Int, entitiesInTest: Int, tableLimit: Int = 1000) {
        var key = startKey
        repeat(testCount) {
            repeat(entitiesInTest) {
                pingDAO.insert(PingEntity(PingMock(1).setTimeStamp(now()), key), tableLimit)
                netChangeDAO.insert(NetTypeEventEntity(generateId(), key, now(), NetTypeObject(1, "", 1), NetTypeObject(1, "", 1)))
                errMessageDAO.insert(ErrMessageEntity(generateId(), key, now(), ""))
                println("xxx key=${key} count=${pingDAO.getCount()}")
            }
            key++
        }
        
    }
}