package com.mishuto.pingtest.model.ping.cmd;

import com.mishuto.pingtest.common.Logger;
import com.mishuto.pingtest.model.stat.PingStatistic;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.stream.LongStream;

import static android.os.SystemClock.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Тест аснхронного пакетного пинга
 * Created by Michael Shishkin on 19.06.2021
 */

public class AsyncPingCmdTest implements AsyncPingCmd.OnAsyncPingCmdListener {

    PingStatistic mStatistic;
    boolean interrupted;

    @Before
    public void setUp() {
        Logger.INSTANCE.setStdOutOnly(true);
    }

    @Test
    synchronized public void execAsyncSer() throws Exception {
        int N = 4;
        int INTERVAL = 500;
        mStatistic = null;

        AsyncPingCmd asyncPingCmd = new AsyncPingCmd();
        asyncPingCmd.mPingCmdSrv = new PingCmdImpl();                       // подменяем эмулятор пинга на штатный
        asyncPingCmd.execAsyncSeries(this, "127.0.0.1", N, INTERVAL, N * INTERVAL); // запускаем тест
        Instant start = Instant.now();
        wait();                                                 // ждем onPingCmdSuccess
        Duration duration = Duration.between(start, Instant.now());
        assertEquals(N, mStatistic.getQuantity());
        assertEquals((N-1) * INTERVAL, duration.toMillis(), 100);
        Logger.INSTANCE.d("ok");
    }

    @Test
    synchronized public void interruptTest() {
        // тестируем interrupt до вызова PingCmdImpl.execSeries, до вызова Runtime.getRuntime().exec(cmd), после вызова exec(cmd)
        LongStream.of(0, 2, 500).forEach(this::interrupt);
    }

    synchronized public void interrupt(long delay) {
        int N = 4;
        int INTERVAL = 500;
        mStatistic = null;
        interrupted = false;

        Logger.INSTANCE.d("delay=" + delay);
        AsyncPingCmd asyncPingCmd = new AsyncPingCmd();
        asyncPingCmd.mPingCmdSrv = new PingCmdImpl();   // чтобы не выбирался эмулятор
        ((PingCmdImpl)asyncPingCmd.mPingCmdSrv).mCommand = new TestCommand();   // подменяем на свой, с задержкой
        asyncPingCmd.execAsyncSeries(this, "127.0.0.1", N, INTERVAL,  N * INTERVAL);
        sleep(delay);
        Instant start = Instant.now();
        asyncPingCmd.interrupt();
        Duration duration = Duration.between(start, Instant.now());
        assertEquals(0, duration.getSeconds());
        assertTrue(interrupted);
        assertNull(mStatistic);
        Logger.INSTANCE.d("ok");
    }


    @Override
    synchronized public void onPingCmdSuccess(PingStatistic statistic) {
        mStatistic = statistic;
        notify();
    }

    @Override
    synchronized public void onPingCmdFault(Exception e) {
       throw new Error(e);
    }

    @Override
    public void onPingCmdInterrupted() {
        interrupted = true;
    }

    private static class TestCommand extends PingCmdImpl.Command {
        @Override
        String execute(String cmd) throws IOException {
            sleep(5);
            return super.execute(cmd);
        }
    }
}