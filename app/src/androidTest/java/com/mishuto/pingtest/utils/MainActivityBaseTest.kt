package com.mishuto.pingtest.utils

import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.rule.GrantPermissionRule
import com.mishuto.pingtest.controller.main.MainActivity
import com.mishuto.pingtest.controller.main.MainFragment
import org.junit.Before
import org.junit.Rule

/**
 * Вспомогательный класс для тестов с MainActivity
 * Created by Michael Shishkin on 08.03.2024
 */
open class MainActivityBaseTest {
    
    lateinit var scenario: ActivityScenario<MainActivity>
    
    @JvmField
    @Rule
    val permissionRule: GrantPermissionRule = GrantPermissionRule.grant(*Utils.requiredGrants())
    
    protected lateinit var activity: MainActivity
    protected val mainFragment
        get() = activity.supportFragmentManager.fragments[0] as MainFragment
    
    @Before
    fun setUp() {
        scenario = launchActivity()
        scenario.onActivity { activity = it }
    }
}

