package com.mishuto.pingtest.utils

import android.Manifest.permission
import android.content.Context
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice


/**
 * Прочие утилиты для тестов
 * Created by Michael Shishkin on 30.10.2023
 */

object Utils {
    
    val uiDevice: UiDevice
        get() = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    
    val context: Context
        get() = ApplicationProvider.getApplicationContext()
    
    fun requiredGrants(): Array<String> =
        mutableListOf<String>().apply {
            add(permission.READ_PHONE_STATE)
            if (VERSION.SDK_INT >= VERSION_CODES.TIRAMISU) add(permission.POST_NOTIFICATIONS)
        }.toTypedArray()
}