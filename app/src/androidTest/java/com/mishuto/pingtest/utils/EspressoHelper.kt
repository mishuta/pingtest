package com.mishuto.pingtest.utils

import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import com.google.android.material.R.id.snackbar_action
import com.google.android.material.R.id.snackbar_text
import com.google.android.material.tabs.TabLayout.TabView
import com.mishuto.pingtest.common.getPrivateProperty
import com.mishuto.pingtest.utils.Utils.context
import com.mishuto.pingtest.view.createImageFrom
import com.mishuto.pingtest.view.sameAs
import com.mishuto.pingtest.view.setTintColor
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import java.lang.Thread.sleep
import java.util.regex.Pattern

/**
 * Статические объекты с методами для поддержки espresso
 * Created by Michael Shishkin on 03.11.2023
 */
object EspressoHelper {
    fun clickOn(resId: Int): ViewInteraction = onView(ViewMatchers.withId(resId)).perform(click())
    
    //автоподбор паузы при неуспешной операции
    fun waitFor(stepInterval: Long = 50, maxDelay: Long = 1000, operation: () -> ViewInteraction) {
        for (i in stepInterval..maxDelay step stepInterval) {
            try {
                operation.invoke()
                return
            }
            catch (t: Throwable) {
                if (t.javaClass.name.contains("espresso")) {
                    sleep(stepInterval)
                    println("[EH]: has been waiting for: $i ms")
                }
                else
                    throw t
            }
        }
        println("[EH]: Wait timeout exceeded. Last try")
        operation.invoke()
    }
}

object Matchers {
    fun hasBackgroundColor(colorId: Int): Matcher<View> =
        object : TypeSafeMatcher<View>() {
            
            override fun matchesSafely(item: View): Boolean =
                item.background.let { it is ColorDrawable && it.color == colorId }
            
            
            override fun describeTo(description: Description) {
                description.appendText("has background color with ID: $colorId")
            }
            
            override fun describeMismatchSafely(item: View?, mismatchDescription: Description?) {
                mismatchDescription?.appendText("was ${(item?.background as ColorDrawable).color}")
            }
        }
    
    //ищет ImageView по drawableId и colorId, если tint для ImageView задавался программно
    fun withDrawable(@DrawableRes drawableId: Int, colorId: Int? = null) =
        object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("ImageView with drawable same as drawable with id $drawableId")
                if (colorId != null) description.appendText(" and color with id $colorId")
            }
            
            override fun matchesSafely(view: View): Boolean {
                val expectedImageView = context.createImageFrom(drawableId)
                    .also { if (colorId != null) it.setTintColor(colorId) }
                
                return view is ImageView && view.sameAs(expectedImageView)
            }
        }
    
    
    fun withRegExp(regex: String) =
        object : BoundedMatcher<View, TextView>(TextView::class.java) {
            private val pattern = Pattern.compile(regex)
            
            override fun describeTo(description: Description) {
                description.appendText("TextView with pattern $regex")
            }
            
            override fun matchesSafely(item: TextView) =
                item.text?.let { pattern.matcher(it).matches() } ?: false
        }
}

// обход неработающего openActionBarOverflowOrOptionsMenu
fun openOptionsMenu(): ViewInteraction = onView(ViewMatchers.isAssignableFrom(ActionMenuItemView::class.java)).perform(click(dummyAction()))

fun dummyAction(): ViewAction =
    object : ViewAction {
        override fun getDescription(): String = "doing nothing"
        
        override fun getConstraints(): TypeSafeMatcher<View> =
            object : TypeSafeMatcher<View>() {
                override fun describeTo(description: Description) {
                    description.appendText("empty matcher")
                }
                
                override fun matchesSafely(item: View?): Boolean = true
            }
        
        override fun perform(uiController: UiController?, view: View?) {}
    }

object RecyclerViews {
    fun hasItemCounts(expectedCount: Int): Matcher<View> =
        object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
            
            override fun describeTo(description: Description) {
                description.appendText("has $expectedCount items")
            }
            
            override fun matchesSafely(item: RecyclerView): Boolean {
                val adapter = item.adapter
                return adapter?.itemCount == expectedCount
            }
        }
    
    fun withPosition(recyclerViewMatcher: Matcher<View>, position: Int): Matcher<View> =
        object : TypeSafeMatcher<View>() {
            private var currentPos: Int = 0
            
            override fun matchesSafely(item: View): Boolean = recyclerViewMatcher.matches(item.parent) && position == currentPos++
            
            override fun describeTo(description: Description) {
                description.appendText("with position $position")
            }
        }
}

object SnackBars {
    fun onSnackBarText(): ViewInteraction = onView(ViewMatchers.withId(snackbar_text))
    fun onSnackBarAction(): ViewInteraction = onView(ViewMatchers.withId(snackbar_action))
}

@Suppress("unused")
object SeekBars {
    fun hasValue(expectedValue: Int): Matcher<View> {
        return object : BoundedMatcher<View, SeekBar>(SeekBar::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("expected: $expectedValue")
            }
            
            override fun matchesSafely(seekBar: SeekBar): Boolean {
                println(seekBar.progress)
                return seekBar.progress == expectedValue
            }
        }
    }
    
    fun changeProgressTo(delta: Int) = changeProgress(delta, true)
    fun setProgress(progress: Int) = changeProgress(progress, false)
    
    private fun changeProgress(position: Int, asDelta: Boolean): ViewAction {
        return object : ViewAction {
            override fun getDescription(): String {
                return "Set Seekbar progress to $position"
            }
            
            override fun getConstraints(): Matcher<View> {
                return ViewMatchers.isAssignableFrom(SeekBar::class.java)
            }
            
            override fun perform(uiController: UiController?, view: View) {
                val seekBar = view as SeekBar
                if (asDelta)
                    seekBar.progress += position
                else
                    seekBar.progress = position
                val mOnSeekBarChangeListener = seekBar.getPrivateProperty<SeekBar, SeekBar.OnSeekBarChangeListener>("mOnSeekBarChangeListener")
                mOnSeekBarChangeListener?.onProgressChanged(seekBar, seekBar.progress, true)
            }
        }
    }
}

object TabLayouts {
    //матчит вкладку tabItem:TabView с заданным индексом внутри TabLayout
    //передается матчер TabLayout и индекс вкладки tabItem
    fun onTabLayoutWithIndex(tabLayout: Matcher<View>, index: Int): Matcher<View> {
        return object : BoundedMatcher<View, TabView>(TabView::class.java) {
            override fun matchesSafely(item: TabView): Boolean {
                return tabLayout.matches(item.parent.parent) && item === (item.parent as ViewGroup).getChildAt(index)
            }
            
            override fun describeTo(description: Description) {
                tabLayout.describeTo(description.appendText("tabItem for index: $index "))
            }
        }
    }
}
