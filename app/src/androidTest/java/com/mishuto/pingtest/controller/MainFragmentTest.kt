package com.mishuto.pingtest.controller

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.mishuto.pingtest.PingMockCreator
import com.mishuto.pingtest.R
import com.mishuto.pingtest.SettingsMock
import com.mishuto.pingtest.common.Const
import com.mishuto.pingtest.common.getPrivateProperty
import com.mishuto.pingtest.controller.main.FabController
import com.mishuto.pingtest.controller.main.MainFragment
import com.mishuto.pingtest.model.ping.PingFactory
import com.mishuto.pingtest.model.ping.PingMock
import com.mishuto.pingtest.service.ServiceManager
import com.mishuto.pingtest.settings.SettingsPrefManager.preferences
import com.mishuto.pingtest.utils.EspressoHelper.clickOn
import com.mishuto.pingtest.utils.MainActivityBaseTest
import com.mishuto.pingtest.utils.TabLayouts.onTabLayoutWithIndex
import com.mishuto.pingtest.utils.Utils.context
import com.mishuto.pingtest.utils.Utils.uiDevice
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import org.hamcrest.Matchers
import org.junit.After
import org.junit.BeforeClass
import org.junit.Test
import java.lang.Thread.sleep
import java.time.LocalDateTime

/**
 * Тест основного функционала
 * Created by Michael Shishkin on 03.01.2021 (Refactored on kotlin on 07.03.24)
 */
class MainFragmentTest : MainActivityBaseTest() {
    
    companion object {
        @BeforeClass
        @JvmStatic
        fun beforeClass() {
            preferences = SettingsMock() // подменяем сеттинги для всех тестов
        }
        
        private fun getString(id: Int, vararg formatArgs: Any): String {
            return context.getString(id, *formatArgs)
        }
    }
    
    @After
    fun tearDown() {
        PingFactory.getInstance().setCreator(null)
    }
    
    // перед стартом монитора все значения параметров должны быть установлены
    @Test
    fun testInitialState() {
        checkParam(R.id.average, 0.ms())
        checkParam(R.id.jitter, 0.ms())
        checkParam(R.id.best, 0.ms())
        checkParam(R.id.worst, 0.ms())
        checkParam(R.id.lost, 0f.toLostStr())
        checkParam(R.id.host, "domain.name")
        checkParam(R.id.ip, "0.0.0.0")
    }
    
    // параметры после набора пробных пингов доджны совпадать с расчетными
    @Test
    fun testParams() {
        val pingMockCreator = PingMockCreator( arrayOf(PingMock(300), PingMock(100), PingMock(200), PingMock(10).setLost(true)))
        clickFAB()
        pingMockCreator.await()
        clickFAB()
        checkParam(R.id.average, 200.ms())
        checkParam(R.id.jitter, 150.ms()) // ((300-100) + |100-200|)/2
        checkParam(R.id.best, 100.ms())
        checkParam(R.id.worst, 300.ms())
        checkParam(R.id.lost, 25f.toLostStr())
        checkParam(R.id.host, "localhost")
        checkParam(R.id.ip, "127.0.0.1")
        onView(ViewMatchers.withId(R.id.net_quality_value)).check(ViewAssertions.matches(ViewMatchers.withText("0 %")))
        onView(ViewMatchers.withId(R.id.game_val)).check(ViewAssertions.matches(ViewMatchers.withText(Matchers.containsString(getString(R.string.score1)))))
        onView(ViewMatchers.withId(R.id.vcs_value)).check(ViewAssertions.matches(ViewMatchers.withText(Matchers.containsString(getString(R.string.score1)))))
        onView(ViewMatchers.withId(R.id.video_val)).check(ViewAssertions.matches(ViewMatchers.withText(Matchers.containsString(getString(R.string.score1)))))
    }
    
    //параметры после останова монитора и поворота не должны сбрасываться
    @Test
    fun paramSavesAfterChangeOrientation() {
        val pingMockCreator = PingMockCreator(arrayOf(PingMock(300), PingMock(100), PingMock(200)))
        clickFAB()
        pingMockCreator.await()
        clickFAB()
        uiDevice.setOrientationLeft()
        checkParam(R.id.average, 200.ms())
        checkParam(R.id.jitter, 150.ms())
        checkParam(R.id.best, 100.ms())
        checkParam(R.id.worst, 300.ms())
        checkParam(R.id.lost, 0f.toLostStr())
        checkParam(R.id.host, "localhost")
        checkParam(R.id.ip, "127.0.0.1")
        onView(ViewMatchers.withId(R.id.net_quality_value)).check(ViewAssertions.matches(ViewMatchers.withText("33 %"))) // jitter = 150
        onView(ViewMatchers.withId(R.id.game_val)).check(ViewAssertions.matches(ViewMatchers.withText(Matchers.containsString(getString(R.string.score1)))))
        onView(ViewMatchers.withId(R.id.vcs_value)).check(ViewAssertions.matches(ViewMatchers.withText(Matchers.containsString(getString(R.string.score2)))))
        onView(ViewMatchers.withId(R.id.video_val)).check(ViewAssertions.matches(ViewMatchers.withText(Matchers.containsString(getString(R.string.score4)))))
        uiDevice.setOrientationNatural()
    }
    
    // в логе должны выводиться пинги и стандартные сообщения об ошибках
    @Test
    fun testLogView() {
        val pingMockCreator = PingMockCreator( arrayOf(
                PingMock(10),
                PingMock(20).setError(true).setLost(true),
                PingMock(30).setWasICMPFail(true),
                PingMock(40).setLost(true))
        )
        onView(onTabLayoutWithIndex(ViewMatchers.withId(R.id.tabs_header), 1)).perform(ViewActions.click())
        clickFAB()
        pingMockCreator.await()
        clickFAB()
        onView(ViewMatchers.withId(R.id.log)).check(ViewAssertions.matches(ViewMatchers.withSubstring(getString(R.string.num_ms, 10))))
        onView(ViewMatchers.withId(R.id.log)).check(ViewAssertions.matches(ViewMatchers.withSubstring(getString(R.string.num_ms, 20))))
        onView(ViewMatchers.withId(R.id.log)).check(ViewAssertions.matches(ViewMatchers.withSubstring(getString(R.string.log_error))))
        onView(ViewMatchers.withId(R.id.log)).check(ViewAssertions.matches(ViewMatchers.withSubstring(getString(R.string.num_ms, 30))))
        onView(ViewMatchers.withId(R.id.log)).check(ViewAssertions.matches(ViewMatchers.withSubstring(getString(R.string.log_icmp_timeout))))
        onView(ViewMatchers.withId(R.id.log)).check(ViewAssertions.matches(ViewMatchers.withSubstring(getString(R.string.num_ms, 40))))
        onView(ViewMatchers.withId(R.id.log)).check(ViewAssertions.matches(ViewMatchers.withSubstring(getString(R.string.log_timeout))))
    }
    
    // после рестарта лог обнуляется
    @Test
    fun testLogAfterRenew() {
        val pingMockCreator = PingMockCreator(arrayOf(
                PingMock(10).setTimeStamp(LocalDateTime.now().minusSeconds(Const.PING_TIMEOUT_LIMIT / 1000 + 1)),
                PingMock(20).setTimeStamp(LocalDateTime.now()))
        )
        onView(onTabLayoutWithIndex(ViewMatchers.withId(R.id.tabs_header), 1)).perform(ViewActions.click())
        clickFAB()
        pingMockCreator.await()
        clickFAB()
        onView(ViewMatchers.withId(R.id.log)).check(ViewAssertions.matches(ViewMatchers.withSubstring(getString(R.string.num_ms, 20))))
        onView(ViewMatchers.withId(R.id.log)).check(ViewAssertions.matches(Matchers.not(ViewMatchers.withSubstring(getString(R.string.num_ms, 10)))))
    }
    
    @Test
    fun testStartsAndStopsByFabClick() {
        clickFAB()
        assertTrue(ServiceManager.isStarted())
        val fabController = mainFragment.getPrivateProperty<MainFragment, FabController>("mFabController")!!
        assertEquals(FabController.State.IN_PROGRESS, fabController.state)
        
        clickFAB()
        assertFalse(ServiceManager.isStarted())
        assertEquals(FabController.State.STOPPED, fabController.state)
    }
    
    @Test
    fun testStopsByTimer() {
        (preferences as SettingsMock).apply { testDuration = 250; pingInterval = 100 }
        clickFAB()
        sleep(300)
        assertFalse(ServiceManager.isStarted())
        mainFragment.getPrivateProperty<MainFragment, FabController>("mFabController")!!
            .apply { assertEquals(FabController.State.STOPPED, state) }
        (preferences as SettingsMock).apply { testDuration = 10000; pingInterval = 1000 }
    }
    
    private fun Float.toLostStr() = getString(R.string.percent1, this)
    private fun Int.ms() = getString(R.string.num_ms, this)
    
    private fun clickFAB(): ViewInteraction {
        return clickOn(R.id.fab)
    }
    
    private fun checkParam(resId: Int, value: String?) {
        onView(Matchers.allOf(ViewMatchers.isDescendantOfA(ViewMatchers.withId(resId)), ViewMatchers.withId(R.id.value)))
            .check(ViewAssertions.matches(ViewMatchers.withText(value)))
    }
}