package com.mishuto.pingtest.controller.billing

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.rule.GrantPermissionRule
import com.android.billingclient.api.BillingClient.BillingResponseCode.*
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.Utils.getString
import com.mishuto.pingtest.controller.main.MainActivity
import com.mishuto.pingtest.controller.features.premium.billing.google.BillingState
import com.mishuto.pingtest.controller.features.premium.billing.google.GoogleBillingEmulator
import com.mishuto.pingtest.controller.features.premium.billing.google.PremiumPurchaseStatus
import com.mishuto.pingtest.data.resultDAO
import com.mishuto.pingtest.utils.EspressoHelper.waitFor
import com.mishuto.pingtest.utils.SnackBars.onSnackBarText
import com.mishuto.pingtest.utils.Utils.requiredGrants
import com.mishuto.pingtest.utils.openOptionsMenu
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.not
import org.junit.*
import org.junit.Assert.assertTrue

/**
 * Тесты UI поддержки биллинга
 * Created by Michael Shishkin on 07.11.2023
 */
class GoogleBillingTest {
    @get:Rule
    val rule = activityScenarioRule<MainActivity>()
    
    @JvmField
    @Rule
    var permissionRule: GrantPermissionRule = GrantPermissionRule.grant(*requiredGrants())
    
    companion object {
        @BeforeClass
        @JvmStatic
        fun setup() {
            resultDAO.deleteEntries(resultDAO.getResultsInReverseOrder())
            with(GoogleBillingEmulator) {
                errState = BillingState(OK)
                errOn_init = OK
                purchaseStateAfter_onPurchase = com.mishuto.pingtest.controller.features.premium.billing.google.PremiumPurchaseStatus.COMPLETED
                errAfter_onPurchase = OK
                delay = 0
                
            }
        }
    }
    
    @Before
    fun setUp() {
        with(GoogleBillingEmulator) {
            isPremiumPaid = false
            errState = BillingState(OK)
            premiumPurchaseStatus = null
            errAfter_showPremiumProductOffer = OK
        }
    }
    
    @Test
    fun check_no_access_to_premium_functions() {
        openOptionsMenu()
        onView(withText(R.string.results)).perform(click())
        onView(withText(R.string.premium_feature_history)).check(matches(isDisplayed()))
        
        openOptionsMenu()
        onView(withText(R.string.action_share)).perform(click())
        onView(withText(R.string.premium_feature_export)).check(matches(isDisplayed()))
    }
    
    @Test
    fun check_access_to_premium_functions() {
        GoogleBillingEmulator.isPremiumPaid = true
        
        openOptionsMenu()
        onView(withText(R.string.results)).perform(click())
        onView(withText(R.string.results)).check(matches(isDisplayed()))
        pressBack()
        
        openOptionsMenu()
        onView(withText(R.string.action_share)).perform(click())
        onSnackBarText().check(matches(withText(R.string.no_data_to_share)))
    }
    
    @Test
    fun check_indicate_premium_off() {
        waitFor {
            onView(
                allOf(
                    withId(R.id.toolbar),
                    hasDescendant(withText(R.string.premium))
                )
            )
                .check(doesNotExist())
        }
    }
    
    @Test
    fun check_indicate_premium_on() {
        GoogleBillingEmulator.isPremiumPaid = true
        onView(
            allOf(
                withId(R.id.toolbar),
                hasDescendant(withText(R.string.premium))
            )
        )
            .check(matches(isDisplayed()))
    }
    
    @Test
    fun check_purchase_dialog_without_premium() {
        openOptionsMenu()
        onView(withText(R.string.premium)).perform(click())
        
        onView(withId(R.id.premium_state_label)).check(matches(withText(R.string.billing_not_purchased_premium)))
        onView(withId(R.id.billing_purchase_state_label)).check(matches(not(isDisplayed())))
        onView(withId(R.id.billing_error_label)).check(matches(not(isDisplayed())))
        onView(withId(R.id.premium_advantages_layout)).check(matches(isDisplayed()))
        onView(withId(R.id.get_premium_button)).check(matches(isDisplayed()))
    }
    
    @Test
    fun checkSwitchToPremium() {
        openOptionsMenu()
        onView(withText(R.string.premium)).perform(click())
        onView(withId(R.id.get_premium_button)).perform(click())
        onSnackBarText().check(matches(withText(R.string.billing_premium_is_purchased)))
        assertTrue(GoogleBillingEmulator.isPremiumPaid)
    }
    
    @Test
    fun check_purchase_dialog_with_premium() {
        GoogleBillingEmulator.isPremiumPaid = true
        openOptionsMenu()
        onView(withText(R.string.premium)).perform(click())
        
        onView(withId(R.id.premium_state_label)).check(matches(withText(R.string.billing_premium_is_purchased)))
        onView(withId(R.id.billing_purchase_state_label)).check(matches(not(isDisplayed())))
        onView(withId(R.id.billing_error_label)).check(matches(not(isDisplayed())))
        onView(withId(R.id.premium_advantages_layout)).check(matches(not(isDisplayed())))
        onView(withId(R.id.get_premium_button)).check(matches(not(isDisplayed())))
    }
    
    @Test
    fun check_purchase_dialog_with_previous_error() {
        GoogleBillingEmulator.errState = BillingState(SERVICE_DISCONNECTED)
        openOptionsMenu()
        onView(withText(R.string.premium)).perform(click())
        onView(withId(R.id.premium_state_label)).check(matches(withText(R.string.billing_not_purchased_premium)))
        onView(withId(R.id.billing_purchase_state_label)).check(matches(not(isDisplayed())))
        onView(withId(R.id.billing_error_label)).check(matches(withText(containsString(getString(R.string.billing_cannot_connect)))))
        onView(withId(R.id.premium_advantages_layout)).check(matches(not(isDisplayed())))
        onView(withId(R.id.get_premium_button)).check(matches(isDisplayed()))
    }
    
    @Test
    fun check_purchase_dialog_with_failed_purchase_state() {
        GoogleBillingEmulator.errState = BillingState(BILLING_UNAVAILABLE)
        GoogleBillingEmulator.premiumPurchaseStatus = PremiumPurchaseStatus.FAILED.also { it.postEvent() }
        openOptionsMenu()
        onView(withText(R.string.premium)).perform(click())
        onView(withId(R.id.premium_state_label)).check(matches(withText(R.string.billing_not_purchased_premium)))
        onView(withId(R.id.billing_purchase_state_label)).check(matches(withText(PremiumPurchaseStatus.FAILED.mesId)))
        onView(withId(R.id.billing_error_label)).check(matches(withText(containsString(getString(R.string.billing_unavailable)))))
        onView(withId(R.id.premium_advantages_layout)).check(matches(not(isDisplayed())))
        onView(withId(R.id.get_premium_button)).check(matches(isDisplayed()))
    }
    
    @Test
    fun dialog_must_updated_after_purchaseStatusChanged() {
        openOptionsMenu()
        onView(withText(R.string.premium)).perform(click())
        onView(withId(R.id.billing_purchase_state_label)).check(matches(not(isDisplayed())))
        GoogleBillingEmulator.premiumPurchaseStatus = PremiumPurchaseStatus.NOT_COMPLETED.also { it.postEvent() }
        waitFor { onView(withId(R.id.billing_purchase_state_label)).check(matches(withText(PremiumPurchaseStatus.NOT_COMPLETED.mesId))) }
    }
    
    @Suppress("SameParameterValue")
    @Test
    fun snack_must_show_after_purchaseStatusChanged() {
        GoogleBillingEmulator.premiumPurchaseStatus = PremiumPurchaseStatus.NOT_COMPLETED.also { it.postEvent() }
        waitFor { onSnackBarText().check(matches(withText(PremiumPurchaseStatus.NOT_COMPLETED.mesId))) }
    }
    
    @Test
    fun dialog_must_updated_after_premiumPaidStatusChanged() {
        openOptionsMenu()
        onView(withText(R.string.premium)).perform(click())
        onView(withId(R.id.premium_state_label)).check(matches(withText(R.string.billing_not_purchased_premium)))
        GoogleBillingEmulator.isPremiumPaid = true
        onView(withId(R.id.premium_state_label)).check(matches(withText(R.string.billing_premium_is_purchased)))
    }
    
    @Test
    fun errIn_showPremiumProductOffer() {
        GoogleBillingEmulator.errAfter_showPremiumProductOffer = ITEM_UNAVAILABLE
        openOptionsMenu()
        onView(withText(R.string.premium)).perform(click())
        onView(withId(R.id.get_premium_button)).perform(click())
        
        onView(withId(android.R.id.message)).check(matches(withText(containsString(getString(R.string.product_not_available)))))
    }
}