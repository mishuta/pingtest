package com.mishuto.pingtest.controller.results

import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onIdle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.longClick
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.SdkSuppress
import com.mishuto.pingtest.R
import com.mishuto.pingtest.SettingsMock
import com.mishuto.pingtest.common.net.NetworkType
import com.mishuto.pingtest.controller.features.results.ResultsActivity
import com.mishuto.pingtest.controller.features.results.list.ItemsGenerator
import com.mishuto.pingtest.data.ResultEntity
import com.mishuto.pingtest.data.pingDAO
import com.mishuto.pingtest.data.resultDAO
import com.mishuto.pingtest.model.HostInfo
import com.mishuto.pingtest.model.PingTest
import com.mishuto.pingtest.model.stat.PingStatData
import com.mishuto.pingtest.model.stat.PingStatistic.Companion.overallQuality
import com.mishuto.pingtest.utils.Matchers.withDrawable
import com.mishuto.pingtest.utils.RecyclerViews.hasItemCounts
import com.mishuto.pingtest.utils.RecyclerViews.withPosition
import com.mishuto.pingtest.utils.SnackBars.onSnackBarAction
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.anyOf
import org.hamcrest.CoreMatchers.startsWith
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

/**
 * Тесты ResultList/ResultDetails фрагментов
 * Created by Michael Shishkin on 03.11.2023
 */
class ResultsActivityTest {
    
    private lateinit var scenario: ActivityScenario<ResultsActivity>
    
    @Before
    fun setup() {
        resultDAO.deleteEntries(resultDAO.getResultsInReverseOrder())
    }
    
    @After
    fun tearDown() {
        if (::scenario.isInitialized)
            scenario.close()
    }
    
    @Test
    fun empty_state_if_no_results() {
        scenario = launchActivity()
        onView(withId(R.id.result_list)).check(matches(hasItemCounts(0)))
        onView(withId(R.id.empty_state_ico)).check(matches(isDisplayed()))
        onView(withId(R.id.empty_state_mes)).check(matches(isDisplayed()))
    }
    
    @Test
    fun new_test_adds_new_result() {
        generateResults()
        scenario = launchActivity()
        
        onView(withId(R.id.result_list)).check(matches(hasItemCounts(1)))
        onView(withId(R.id.result_list)).check(matches(hasDescendant(allOf(withId(R.id.host), withText("host")))))
        onView(withId(R.id.result_list)).check(matches(hasDescendant(allOf(withId(R.id.net_quality_value), withText("100 %")))))
    }
    
    @SdkSuppress(minSdkVersion = 28) // Android P API level is 28
    @Test
    fun check_test_details() {
        val startTest = LocalDateTime.of(2024, 1, 1, 0, 0, 0)
        val pingTest = spyk<PingTest>().apply { hostInfo = HostInfo("1.1.1.1", "host.com") }
        every { pingTest.started } returns startTest
        pingTest.finished = pingTest.started.plusSeconds(7)
        
        mockkObject(pingDAO, resultDAO)
        every { pingDAO.getCountForTest(any()) } returns 1
        every { resultDAO.getStatistics(any()) } returns
                PingStatData(average = 10, lost = 1, jitter = 20, best = 5, worst = 50, quantity = 100, duration = 2000)
        val resultEntity = ResultEntity.createEntity(SettingsMock(), pingTest)
        unmockkObject(resultDAO, pingDAO)
        
        resultDAO.insert(resultEntity)
        
        scenario = launchActivity()
        onView(withId(R.id.host)).perform(click())
        onIdle()    // переключение на details
        onView(
            allOf(
                isDescendantOfA(withId(R.id.test_params_layout)),
                hasSibling(withText(R.string.start))
            )
        ).check(matches(withText(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT, FormatStyle.MEDIUM).format(startTest))))
        onView(hasSibling(withText(R.string.ping_timeout))).check(matches(withText(startsWith(("1 ")))))
        onView(hasSibling(withText(R.string.interval))).check(matches(withText(startsWith(("500 ")))))
        onView(hasSibling(withText(R.string.host_title))).check(matches(withText("host.com")))
        onView(hasSibling(withText(R.string.ip_addr))).check(matches(withText("1.1.1.1")))
        onView(hasSibling(withText(R.string.moving_average))).check(matches(withText(startsWith("10"))))
        onView(hasSibling(withText(R.string.quality))).check(matches(withText("${resultEntity.statistics.overallQuality()} %")))
        onView(hasSibling(withText(R.string.main_net_type))).check(matches(withText(NetworkType.getNetType().description)))
        onView(hasSibling(withText(R.string.duration))).check(matches(withText("0:00:02")))
        onView(hasSibling(withText(R.string.quantity))).check(matches(withText("100")))
        onView(hasSibling(withText(R.string.jitter))).check(matches(withText(startsWith("20"))))
        onView(hasSibling(withText(R.string.lost))).check(matches(anyOf(withText("1.0 %"), withText("1,0 %"))))
        onView(hasSibling(withText(R.string.best))).check(matches(withText(startsWith("5"))))
        onView(hasSibling(withText(R.string.worst))).check(matches(withText(startsWith("50"))))
    }
    
    @Test
    fun test_vanishes_in_results_after_delete_in_details() {
        generateResults()
        scenario = launchActivity()
        onView(withId(R.id.host)).perform(click())
        onIdle()    // переключение на details
        onView(withId(R.id.delete_action)).perform(click())
        onIdle()    // переключение на list
        onView(withId(R.id.result_list)).check(matches(hasItemCounts(0)))
    }
    
    @Test
    fun delete_and_undo_test_details() {
        generateResults()
        scenario = launchActivity()
        onView(withId(R.id.host)).perform(click())
        onIdle()    // переключение на details
        onView(withId(R.id.delete_action)).perform(click())
        onIdle()    // переключение на list
        onSnackBarAction().perform(click())
        onView(withId(R.id.result_list)).check(matches(hasItemCounts(1)))
        onView(withId(R.id.result_list)).check(matches(hasDescendant(allOf(withId(R.id.host), withText("host")))))
        onView(withId(R.id.result_list)).check(matches(hasDescendant(allOf(withId(R.id.net_quality_value), withText("100 %")))))
    }
    
    @Test
    fun check_nongroup_menu() {
        generateResults(1)
        scenario = launchActivity()
        checkOrdinalToolbar()
    }
    
    @Test
    fun long_click_switches_to_selected_group() {
        generateResults(2)
        scenario = launchActivity()
        onView(withPosition(withId(R.id.result_list), 1)).perform(longClick())
        
        onView(withId(R.id.action_check_all)).check(matches(isDisplayed()))
        onView(withId(R.id.action_share)).check(matches(isDisplayed()))
        onView(withId(R.id.action_delete)).check(matches(isDisplayed()))
        onView(withId(R.id.toolbar)).check(matches(hasDescendant(withText("1"))))
        onView((withDrawable(R.drawable.ic_baseline_close_24))).check(matches(isDisplayed()))
    }
    
    @Test
    fun click_item_to_add_to_selected_group() {
        generateResults(2)
        scenario = launchActivity()
        onView(withPosition(withId(R.id.result_list), 1)).perform(longClick())
        onView(withPosition(withId(R.id.result_list), 0)).perform(click())
        onView(withId(R.id.toolbar)).check(matches(hasDescendant(withText("2"))))
    }
    
    @Test
    fun select_all() {
        generateResults(5)
        scenario = launchActivity()
        onView(withPosition(withId(R.id.result_list), 0)).perform(longClick())
        onView(withId(R.id.action_check_all)).perform(click())
        onView(withId(R.id.toolbar)).check(matches(hasDescendant(withText("5"))))
    }
    
    @Test
    fun discard_all() {
        generateResults(2)
        scenario = launchActivity()
        onView(withPosition(withId(R.id.result_list), 0)).perform(longClick())
        onView(withPosition(withId(R.id.result_list), 1)).perform(click())
        onView(withId(R.id.action_discard_all)).perform(click())
        checkOrdinalToolbar()
    }
    
    @Test
    fun change_menu_icon_when_all_selected() {
        generateResults(5)
        scenario = launchActivity()
        onView(withPosition(withId(R.id.result_list), 0)).perform(longClick())
        onView(withId(R.id.action_check_all)).perform(click())
        
        onView(withId(R.id.action_discard_all)).check(matches(isDisplayed()))
        onView(withId(R.id.action_check_all)).check(doesNotExist())
    }
    
    @Test
    fun change_menu_icon_after_deselect_item() {
        generateResults(5)
        scenario = launchActivity()
        onView(withPosition(withId(R.id.result_list), 0)).perform(longClick())
        onView(withId(R.id.action_check_all)).perform(click())
        onView(withPosition(withId(R.id.result_list), 1)).perform(click())
        
        onView(withId(R.id.action_discard_all)).check(doesNotExist())
        onView(withId(R.id.action_check_all)).check(matches(isDisplayed()))
        
    }
    
    @Test
    fun remove_from_selected_group() {
        generateResults(5)
        scenario = launchActivity()
        onView(withPosition(withId(R.id.result_list), 0)).perform(longClick())
        onView(withId(R.id.action_check_all)).perform(click())
        onView(withPosition(withId(R.id.result_list), 0)).perform(click())
        onView(withId(R.id.toolbar)).check(matches(hasDescendant(withText("4"))))
    }
    
    @Test
    fun unselect_all_must_change_menu() {
        generateResults(3)
        scenario = launchActivity()
        onView(withPosition(withId(R.id.result_list), 0)).perform(longClick())
        onView(withPosition(withId(R.id.result_list), 0)).perform(click())
        checkOrdinalToolbar()
    }
    
    @Test
    fun delete_selected() {
        generateResults(3)
        scenario = launchActivity()
        onView(withPosition(withId(R.id.result_list), 0)).perform(longClick())
        onView(withPosition(withId(R.id.result_list), 1)).perform(click())
        onView(withPosition(withId(R.id.result_list), 2)).perform(click())
        onView(withId(R.id.action_delete)).perform(click())
        
        onView(withId(R.id.empty_state_ico)).check(matches(isDisplayed()))
        onView(withId(R.id.empty_state_mes)).check(matches(isDisplayed()))
    }
    
    @Test
    fun undo_deleted() {
        generateResults(3)
        scenario = launchActivity()
        onView(withPosition(withId(R.id.result_list), 0)).perform(longClick())
        onView(withPosition(withId(R.id.result_list), 1)).perform(click())
        onView(withPosition(withId(R.id.result_list), 2)).perform(click())
        onView(withId(R.id.action_delete)).perform(click())
        onSnackBarAction().perform(click())
        
        onView(withId(R.id.result_list)).check(matches(hasItemCounts(3)))
    }
    
    private fun generateResults(count: Int = 1) = runBlocking {
        ItemsGenerator.generate(count, 5) {}
    }
    
    private fun checkOrdinalToolbar() {
        onView(withId(R.id.action_check_all)).check(doesNotExist())
        onView(withId(R.id.action_share)).check(doesNotExist())
        onView(withId(R.id.action_delete)).check(doesNotExist())
        onView(withId(R.id.toolbar)).check(matches(hasDescendant(withText(R.string.results))))
        onView((withDrawable(R.drawable.ic_baseline_arrow_back_24))).check(matches(isDisplayed()))
    }
}
