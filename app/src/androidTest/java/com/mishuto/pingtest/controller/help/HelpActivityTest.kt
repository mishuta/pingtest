package com.mishuto.pingtest.controller.help

import android.widget.TextView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.web.assertion.WebViewAssertions.webMatches
import androidx.test.espresso.web.sugar.Web.onWebView
import androidx.test.espresso.web.webdriver.DriverAtoms.findElement
import androidx.test.espresso.web.webdriver.DriverAtoms.getText
import androidx.test.espresso.web.webdriver.Locator
import androidx.test.ext.junit.rules.activityScenarioRule
import com.mishuto.pingtest.BuildConfig
import com.mishuto.pingtest.R
import com.mishuto.pingtest.controller.features.help.HelpActivity
import com.mishuto.pingtest.utils.Matchers.withRegExp
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Rule
import org.junit.Test


/**
 * Тест справки
 * Created by Michael Shishkin on 07.11.2023
 */
class HelpActivityTest {
    
    @get:Rule
    val rule = activityScenarioRule<HelpActivity>()
    
    @Test
    fun checkVersion() {
        onView(withId(R.id.version_value)).check(matches(withText(BuildConfig.VERSION_NAME)))
        onView(withId(R.id.version_value)).check(matches(withRegExp("""\d+.\d+(.\d+){0,2}.D""")))
    }
    
    @Test
    fun testHistory() {
        onView(withId(R.id.history)).perform(click())
        onView(withId(R.id.header)).check(matches(withText(R.string.version_history)))
        onView(
            allOf(
                isDescendantOfA(withId(R.id.history)),
                isAssignableFrom(TextView::class.java),
                withText("5.0")
            )
        )
            .perform(scrollTo())
            .check(matches(isDisplayed()))
    }
    
    @Test
    fun testArticle() {
        onView(withText(R.string.how_it_works_title)).perform(click())
        onView(withId(R.id.header)).check(matches(withText(R.string.how_it_works_title)))
        onView(withId(R.id.content)).check(matches(withSubstring("ICMP ECHO")))
    }
    
    @Test
    fun checkPrivacy() {
        onView(withText(R.string.privacy_policy)).perform(click())
        onWebView(withId(R.id.webview))
            .forceJavascriptEnabled()
            .withElement(findElement(Locator.XPATH, "/html/body/h1"))
            .check(webMatches(getText(), equalTo("Privacy Policy")))
    }
}