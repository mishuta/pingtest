package com.mishuto.pingtest.service

import android.app.Notification
import android.os.SystemClock.sleep
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiObject2
import androidx.test.uiautomator.Until
import com.mishuto.pingtest.PingMockCreator
import com.mishuto.pingtest.R
import com.mishuto.pingtest.SettingsMock
import com.mishuto.pingtest.common.*
import com.mishuto.pingtest.controller.main.FabController
import com.mishuto.pingtest.controller.main.MainFragment
import com.mishuto.pingtest.model.PingTest
import com.mishuto.pingtest.model.ping.PingFactory
import com.mishuto.pingtest.model.ping.PingMock
import com.mishuto.pingtest.model.stat.PingStatData
import com.mishuto.pingtest.model.stat.PingStatistic
import com.mishuto.pingtest.model.stat.PingStatistic.Companion.overallQuality
import com.mishuto.pingtest.model.stat.StreamPingStat
import com.mishuto.pingtest.service.PingNotification.NOTIFICATION_ID
import com.mishuto.pingtest.service.PingNotificationTest.NotificationMock.Companion.createNotificationWithStat
import com.mishuto.pingtest.settings.SettingsPrefManager.preferences
import com.mishuto.pingtest.utils.EspressoHelper.clickOn
import com.mishuto.pingtest.utils.MainActivityBaseTest
import com.mishuto.pingtest.utils.Utils.context
import com.mishuto.pingtest.utils.Utils.uiDevice
import io.mockk.*
import junit.framework.TestCase.*
import org.junit.After
import org.junit.BeforeClass
import org.junit.Test
import java.util.regex.Pattern


/** Тесты нотификаций
 * Created by Michael Shishkin on 26.02.2024
 */
class PingNotificationTest : MainActivityBaseTest() {
    
    companion object {
        @BeforeClass
        @JvmStatic
        fun onInit() {
            // ставим большую задержку между пингами, чтобы вьюхи в нотификации не успели обновиться
            preferences = SettingsMock().apply { pingInterval = 10_000 }
        }
    }
    
    private val stopButtonCaption = Pattern.compile("${context.getString(R.string.stop_action)}|${context.getString(R.string.stop_action).uppercase()}")
    
    @After
    fun cleanUp() {
        //стопим сервис и убираем нотификации, если есть
        ServiceManager.stopService()
        uiDevice.pressBack()
        PingFactory.getInstance().setCreator(null)
    }
    
    @Test
    fun notificationShowsWhenTestRunning() {
        clickOn(R.id.fab)
        openNotifications()
        assertNotNull(findHeaderByAppName() ?: findHeaderByTitle())
    }
    
    @Test
    fun notificationHidesAfterTestIsStopped() {
        clickOn(R.id.fab)
        sleep(100)
        clickOn(R.id.fab)
        openNotifications()
        assertTrue(findHeaderByAppName() == null && findHeaderByTitle() == null)
    }
    
    @Test
    fun testStopsAfterClickOnNotificationsStopButton() {
        clickOn(R.id.fab)
        ServiceManager.bind("testStopsAfterClickOnNotificationsStopButton"){}
        openNotifications()
        tryExpandNotification()
        //click on Stop
        uiDevice.findObject(By.text(stopButtonCaption)).click()
        
        uiDevice.pressBack()
        sleep(100)
        assertFalse(ServiceManager.isStarted())
        mainFragment.getPrivateProperty<MainFragment, FabController>("mFabController")!!
            .apply { assertEquals(FabController.State.STOPPED, state) }
        
        ServiceManager.unbind("testStopsAfterClickOnNotificationsStopButton")
    }
    
    @Test
    fun checkNotificationIcon4() {
        val statistic = PingStatData(10, 0, 0, 1, 10, 100, 100)
        val iconResId = createNotificationWithStat(statistic).smallIcon.resId
        assertEquals(R.drawable.notify4, iconResId)
    }
    
    @Test
    fun checkNotificationIcon0() {
        val statistic = PingStatData(10, 90, 0, 1, 10, 100, 100)
        val iconResId = createNotificationWithStat(statistic).smallIcon.resId
        assertEquals(R.drawable.notify0, iconResId)
    }
    
    @Test
    fun checkNotificationIcon1() {
        val statistic = PingStatData(100, 10, 0, 1, 10, 100, 100)
        val iconResId = createNotificationWithStat(statistic).smallIcon.resId
        assertEquals(R.drawable.notify1, iconResId)
    }
    
    @Test
    fun checkNotificationIcon3() {
        val statistic = PingStatData(100, 0, 0, 1, 10, 100, 100)
        val iconResId = createNotificationWithStat(statistic).smallIcon.resId
        assertEquals(R.drawable.notify3, iconResId)
    }
    
    @Test
    fun same_stat_data_should_be_filtered() {
        val statistic = PingStatData(100, 0, 0, 1, 10, 100, 100)
        val mock = NotificationMock()
        mock.updateWithStat(statistic)
        assertTrue(mock.notification.isCaptured)
        
        // повторный апдейт с той же статистикой не вызывает нотификацию
        mock.notification.clear()
        mock.updateWithStat(PingStatData(statistic))
        assertFalse(mock.notification.isCaptured)
    }
    
    class NotificationMock {
        val notification = slot<Notification>()
        private val notificationHelperMock = spyk<NotificationHelper>() {
            every { notificationManager() } returns mockk {
                every { notify(NOTIFICATION_ID, capture(notification)) } returns Unit
            }
        }
        
        val pingNotification = PingNotification()
        
        init {
            pingNotification.setPrivateProperty("mHelper", notificationHelperMock)
        }
        
        fun updateWithStat(statistic: PingStatistic) {
            val pingTest = PingTest()
            pingTest.hostInfo = mockk()
            pingTest.statistic = statistic
            
            pingNotification.updateNotification(pingTest)
            
            sleep(50)
        }
        
        companion object {
            fun createNotificationWithStat(statistic: PingStatistic): Notification =
                with(NotificationMock())
                {
                    updateWithStat(statistic)
                    notification.captured
                }
        }
    }
    
    private fun getPingNotification() = ServiceManager
        .getPrivateProperty<ServiceManager, PingService>("pingService")!!
        .getPrivateProperty<PingService, PingNotification>("pingNotification")!!
    
    @Test
    fun checkValuesInNotification() {
        (preferences as SettingsMock).pingInterval = 100
        getPingNotification().setPrivateProperty("updateIntervalTimer", Timer(10)) // сокращаем частоту обновления нотификации
        
        val probes = arrayOf(PingMock(10), PingMock(50), PingMock(100), PingMock(4000).setLost(true))
        val creator = PingMockCreator(probes)
        val stat = StreamPingStat(100)
        probes.forEach { stat.addNew(it) }
        
        clickOn(R.id.fab)
        creator.await()
        //ставим задержку на получение след пингов, чтобы он не влиял на данные в нотификации
        PingFactory.getInstance().setCreator { _, _, _ -> sleep(5000).let { PingMock(1) } }
        
        openNotifications()
        tryExpandNotification()
        assertTrue(viewWithRes("ping_val").contains(stat.average.toInt()))
        assertTrue(viewWithRes("jitter_value").contains(stat.jitter.toInt()))
        assertTrue(viewWithRes("lost_val").contains(stat.getLostPermille() / 10))
        assertTrue(viewWithRes("overall_value").contains(stat.overallQuality()))
        assertEquals(ServiceManager.pingTest.hostInfo.name, viewWithRes("host_val"))
        
        (preferences as SettingsMock).pingInterval = 10_000
    }
    
    private fun viewWithRes(res: String) = uiDevice.findObject(By.res("com.mishuto.pingtest.debug:id/$res")).text
    private fun String.contains(n: Int): Boolean = contains(n.toString())
    
    private fun openNotifications(){
        uiDevice.openNotification()
        uiDevice.wait(Until.hasObject(By.res("android:id/expand_button")), 2000).also { assertNotNull(it) }
        
    }
    
    private fun tryExpandNotification(){
        // если заголовок=notif_title значит нотификация свернута
        findHeaderByTitle()?.let { header ->
            findButtonFrom(header)?.click()
                ?: throw AssertionError("expand button not found in ${header.text}")
        }
    }
    
    //todo просто двоеточия должно быть достаточно
    private fun findHeaderByTitle(): UiObject2? = uiDevice.findObject(By.textStartsWith(context.getString(R.string.notif_title).substringBefore(':') + ':'))
    private fun findHeaderByAppName(): UiObject2? = uiDevice.findObject(By.text(context.getString(R.string.app_title)))
    
    private fun findButtonFrom(header: UiObject2): UiObject2? {
        val maxDepth = 5
        var currentParent = header.parent
        for (depth in 1..maxDepth) {
            val button = currentParent.findObject(By.clazz(android.widget.Button::class.java))
            if(button != null) return button
            currentParent = currentParent.parent
        }
        return null
    }
}