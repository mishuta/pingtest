package com.mishuto.pingtest.service

import androidx.test.espresso.Espresso
import com.mishuto.pingtest.common.dispatchers.EventDispatcher
import com.mishuto.pingtest.common.getPrivateProperty
import org.junit.Test
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock
import kotlin.test.*


/**
 * ServiceManagerTest
 * Created by Michael Shishkin on 15.03.2024
 */
class ServiceManagerTest {
    
    init {
        //если кто-то был привязан к сервису - отвязываем
        ServiceManager.getPrivateProperty<ServiceManager, MutableMap<String,*>>("clients")!!
            .forEach { ServiceManager.unbind(it.key) }
    }
    
    @Test
    fun if_service_unbound_getData_must_throw_err() {
        assertFalse(ServiceManager.isBound())
        assertFailsWith<IllegalStateException> { ServiceManager.pingTest }
    }
    
    @Test
    fun test_statuses() {
        ServiceManager.bind("test") {
            assertEquals(ServiceManager.State.BOUND, ServiceManager.status("test"))
            ServiceManager.unbind("test")
            assertEquals(ServiceManager.State.UNBOUND, ServiceManager.status("test"))
        }
        assertEquals(ServiceManager.State.BINDING, ServiceManager.status("test"))
        Espresso.onIdle()
    }
    
    @Test
    fun test_start_stop_service() {
        val lock = ReentrantLock()
        val condition = lock.newCondition()
        var pingReceived = false
        
        ServiceManager.bind("test") {}
        EventDispatcher.register(PingService.PING_EVENT, "test") {
            lock.lock()
            pingReceived = true
            condition.signal()
            lock.unlock()
        }
        
        ServiceManager.startService()
        lock.lock()
        condition.await(3000, TimeUnit.MILLISECONDS)
        lock.unlock()
        
        assertTrue(pingReceived)
        assertTrue(ServiceManager.isStarted())
        ServiceManager.stopService()
        
        assertFalse(ServiceManager.isStarted())
        ServiceManager.unbind("test")
    }
    
    @Test
    fun service_unbound_if_all_clients_are_unbounded() {
        assertFalse(ServiceManager.isBound())
        ServiceManager.bind("test1") {}
        
        Espresso.onIdle()
        assertTrue(ServiceManager.isBound())
        
        var instantRun = false
        ServiceManager.bind("test2") { instantRun = true }
        assertTrue(instantRun)
        assertTrue(ServiceManager.isBound())
        
        ServiceManager.unbind("test1")
        assertTrue(ServiceManager.isBound())
        
        ServiceManager.unbind("test2")
        assertFalse(ServiceManager.isBound())
    }
    
    @Test
    fun unbind_before_bound() {
        ServiceManager.bind("test") { throw AssertionError("must not called") }
        ServiceManager.unbind("test")
        Espresso.onIdle()
        assertFalse(ServiceManager.isBound())
    }
    
    @Test
    fun two_services_unbind_before_bound() {
        ServiceManager.bind("test1") { throw AssertionError("must not called") }
        ServiceManager.bind("test2") { throw AssertionError("must not called") }
        ServiceManager.unbind("test1")
        ServiceManager.unbind("test2")
        Espresso.onIdle()
        assertFalse(ServiceManager.isBound())
    }
}