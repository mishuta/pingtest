package com.mishuto.pingtest.settings.preferences.host

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.hasSibling
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isNotEnabled
import androidx.test.espresso.matcher.ViewMatchers.withChild
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.activityScenarioRule
import com.mishuto.pingtest.utils.Matchers.hasBackgroundColor
import com.mishuto.pingtest.R
import com.mishuto.pingtest.utils.EspressoHelper.clickOn
import com.mishuto.pingtest.utils.RecyclerViews.hasItemCounts
import com.mishuto.pingtest.utils.Utils.context
import org.hamcrest.Matchers
import org.junit.Rule
import org.junit.Test


/**
 * Тесты HostListActivity
 * Created by Michael Shishkin on 30.10.2023
 */

class HostListActivityTest {
    @get:Rule
    val rule = activityScenarioRule<HostListActivity>()

    private val dataManager = HostSetting.getInstance()
    private val hasSelectedMatcher = hasBackgroundColor(context.getColor(R.color.colorItemSelected))

    @Test
    fun default_hosts_must_be_present_after_initialized() {
        onView(withId(R.id.hosts_recycler_view))
            .check(matches(hasItemCounts(HostSetting.getDefaultHostsArr().size)))
    }

    @Test
    fun addHost() {
        clickOn(R.id.fab)
        onView(withId(R.id.host_name)).perform(typeText("host"))
        onView(withText(android.R.string.ok)).perform(click())

        onView(withChild(withText("host"))).check(matches(hasSelectedMatcher))

        dataManager.removeHost("host")
    }

    @Test
    fun removeHost() {
        onView(Matchers.allOf(
            withId(R.id.menu_layout),
            hasSibling(withText("google.com"))
        )).perform(click())

        onView(withText(R.string.delete)).perform(click())
        onView(withText("google.com")).check(doesNotExist())

        dataManager.addHost("google.com")
    }

    @Test
    fun changeHost() {
        onView(Matchers.allOf(
            withId(R.id.menu_layout),
            hasSibling(withText("google.com"))
        )).perform(click())

        onView(withText(R.string.edit)).perform(click())
        onView(withId(R.id.host_name)).perform(replaceText("microsoft.com"))
        onView(withText(android.R.string.ok)).perform(click())

        onView(withChild(withText("microsoft.com"))).check(matches(hasSelectedMatcher))

        dataManager.changeHost("microsoft.com", "google.com")
    }

    @Test
    fun host_with_incorrect_name_cannot_be_added() {
        clickOn(R.id.fab)
        onView(withId(R.id.host_name)).perform(typeText("host."))
        onView(withText(android.R.string.ok)).check(matches(isNotEnabled()))
        onView(withText(R.string.invalid_format)).check(matches(isDisplayed()))
    }

    @Test
    fun host_with_existing_name_cannot_be_accepted() {
        onView(Matchers.allOf(
            withId(R.id.menu_layout),
            hasSibling(withText("google.com"))
        )).perform(click())

        onView(withText(R.string.edit)).perform(click())
        onView(withId(R.id.host_name)).perform(replaceText("8.8.8.8"))

        onView(withText(android.R.string.ok)).check(matches(isNotEnabled()))
        onView(withText(R.string.already_exists)).check(matches(isDisplayed()))
    }
}