package com.mishuto.pingtest.settings.preferences

import android.widget.ImageView
import androidx.appcompat.widget.SwitchCompat
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.SdkSuppress
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.SharedPref
import com.mishuto.pingtest.controller.features.premium.billing.google.GoogleBillingEmulator
import com.mishuto.pingtest.controller.ui.FloatingPing
import com.mishuto.pingtest.settings.SettingsPrefManager
import com.mishuto.pingtest.settings.app.SettingsActivity
import com.mishuto.pingtest.utils.Matchers.withDrawable
import com.mishuto.pingtest.utils.SnackBars.onSnackBarText
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkObject
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Test

/**
 * Тесты настройки FloatingWindow
 * Created by Michael Shishkin on 10.03.2024
 */
class FloatingWindowPrefTest {
    private lateinit var scenario: ActivityScenario<SettingsActivity>
    
    @Test
    fun floatingWindow_NotAllowed_hidesSwitch() {
        GoogleBillingEmulator.isPremiumPaid = false
        scenario = launchActivity()
        
        onIcon().check(matches(withDrawable(R.drawable.outline_lock_24, R.color.colorError)))
        onSwitch().check(matches(not(isDisplayed())))
        onSummary().check(matches(withText(R.string.floating_window_on_summary)))
    }
    
    @Test
    fun floatingWindow_Off() {
        GoogleBillingEmulator.isPremiumPaid = true
        SharedPref.putBoolean(SettingsPrefManager.showFloatingWindowKey, false)
        scenario = launchActivity()
        
        onIcon().check(matches(not(isDisplayed())))
        onSwitch().check(matches(isNotChecked()))
        onSummary().check(matches(withText(R.string.floating_window_on_summary)))
    }
    
    @Test
    fun floatingWindow_notAllowed_showsSnack() {
        GoogleBillingEmulator.isPremiumPaid = false
        scenario = launchActivity()
        
        onSummary().perform(click())
        onSnackBarText().check(matches(withText(R.string.floating_window_premium_alert)))
    }
    
    @Test
    fun floatingWindow_OffToNotPermitted_showsDialog() {
        GoogleBillingEmulator.isPremiumPaid = true
        SharedPref.putBoolean(SettingsPrefManager.showFloatingWindowKey, false)
        scenario = launchActivity()
        
        onSwitch().perform(click())
        onView(withText(R.string.floating_window_permission_request)).check(matches(isDisplayed()))
    }
    
    @Test
    fun floatingWindow_NotPermitted_showsWarning() {
        GoogleBillingEmulator.isPremiumPaid = true
        SharedPref.putBoolean(SettingsPrefManager.showFloatingWindowKey, true)
        scenario = launchActivity()
        
        onSwitch().check(matches(isChecked()))
        onIcon().check(matches(withDrawable(R.drawable.outline_warning_amber_24)))
        onSummary().check(matches(withText(R.string.floating_window_no_permission_summary)))
    }
    
    @SdkSuppress(minSdkVersion = 28)
    @Test
    fun floatingWindow_On_Permitted() {
        GoogleBillingEmulator.isPremiumPaid = true
        SharedPref.putBoolean(SettingsPrefManager.showFloatingWindowKey, true)
        mockkObject(FloatingPing)
        every { FloatingPing.hasPermission } returns true
        
        scenario = launchActivity()
        
        onSwitch().check(matches(isChecked()))
        onIcon().check(matches(not(isDisplayed())))
        onSummary().check(matches(withText(R.string.floating_window_on_summary)))
        
        unmockkObject(FloatingPing)
    }
    
    private fun onIcon() = onView(
        allOf(
            isAssignableFrom(ImageView::class.java),
            withParent(hasSibling(withChild(withText(R.string.floating_window_on_title))))
        )
    )
    
    private fun onSwitch() = onView(
        allOf(
            isAssignableFrom(SwitchCompat::class.java),
            withParent(hasSibling(withChild(withText(R.string.floating_window_on_title))))
        )
    )
    
    private fun onSummary() = onView(allOf(withId(android.R.id.summary), hasSibling(withText(R.string.floating_window_on_title))))
}