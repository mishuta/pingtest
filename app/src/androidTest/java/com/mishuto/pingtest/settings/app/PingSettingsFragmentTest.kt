package com.mishuto.pingtest.settings.app

import android.widget.SeekBar
import android.widget.Switch
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.msToTimeUnits
import com.mishuto.pingtest.settings.SettingsImpl
import com.mishuto.pingtest.settings.SettingsPrefManager.preferences
import com.mishuto.pingtest.utils.SeekBars.setProgress
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Тест страницы настроек пинга
 * Created by Michael Shishkin on 09.03.2024
 */
class PingSettingsFragmentTest{
    @get:Rule
    val rule = activityScenarioRule<SettingsActivity>()
    
    init {
        preferences = SettingsImpl()
    }
    
    @Before
    fun setUp() {
        onView(withText(R.string.ping_test_settings)).perform(click())
    }
    
    /* тесты на соответствие переменным SettingsPrefManager.preferences установленным на странице параметрам */
    
    @Test
    fun checkPingInterval() {
        onSeekBarWithTitle(R.string.interval).perform(setProgress(2))
        onSeekBarValueWithTitle(R.string.interval).check(matches(withText(msToTimeUnits(preferences.pingInterval))))
    }
    
    @Test
    fun checkPingTimeout() {
        onSeekBarWithTitle(R.string.ping_timeout).perform(setProgress(2))
        onSeekBarValueWithTitle(R.string.ping_timeout).check(matches(withText(msToTimeUnits(preferences.pingTimeout))))
    }
    
    @Test
    fun checkNumberOfPings() {
        onSeekBarWithTitle(R.string.moving_sample_title).perform(setProgress(0))
        onSeekBarValueWithTitle(R.string.moving_sample_title).check(matches(withText(preferences.movingPingsNumber.toString())))
    }
    
    @Test
    fun checkAutostart() {
        onView(isAssignableFrom(Switch::class.java)).perform(click())
        onView(isAssignableFrom(Switch::class.java)).check(matches(if(preferences.isAutostart) isChecked() else isNotChecked()))
    }
    
    @Test
    fun checkStopAfter() {
        onSeekBarWithTitle(R.string.test_duration).perform(setProgress(2))
        onSeekBarValueWithTitle(R.string.test_duration).check(matches(withText(msToTimeUnits(preferences.testDuration))))
    }
    
    private fun onSeekBarValueWithTitle(resId: Int) = onView(hasSibling(seekBarWithTitleMatcher(resId)))
    
    private fun onSeekBarWithTitle(resId: Int) = onView(seekBarWithTitleMatcher(resId))
    
    private fun seekBarWithTitleMatcher(resId: Int) =
        allOf(
            isAssignableFrom(SeekBar::class.java),
            withParent(hasSibling(withChild(withText(resId))))
        )
}