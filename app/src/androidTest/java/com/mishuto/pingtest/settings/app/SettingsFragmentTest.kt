package com.mishuto.pingtest.settings.app

import android.os.Build
import androidx.test.espresso.Espresso.onIdle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import com.mishuto.pingtest.R
import com.mishuto.pingtest.common.wrappers.PMUtils
import com.mishuto.pingtest.settings.preferences.sound.SoundPrefManager
import com.mishuto.pingtest.settings.preferences.theme.AppTheme
import com.mishuto.pingtest.utils.EspressoHelper.waitFor
import com.mishuto.pingtest.utils.Matchers.withDrawable
import io.mockk.every
import io.mockk.mockkStatic
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Rule
import org.junit.Test

/**
 * Тесты Settings
 * Created by Michael Shishkin on 31.10.2023
 */
class SettingsFragmentTest {
    
    @get:Rule
    val rule = activityScenarioRule<SettingsActivity>()
    
    @Test
    fun hostChange() {
        onView(withText(R.string.host_title)).check(matches(hasSibling(withText("8.8.8.8"))))
        onView(withText(R.string.host_title)).perform(click())
        onView(withText("google.com")).perform(click())
        waitFor { onView(withText(R.string.host_title)).check(matches(hasSibling(withText("google.com")))) }
    }
    
    @Test
    fun checkSoundSetting() {
        assertFalse(SoundPrefManager.isSoundOn)
        onView(withText(R.string.sound)).perform(scrollTo(), click())
        onView(withText(R.string.off)).perform(click())
        onView(withText(R.string.lost_only)).perform(click())
        onView(withText(R.string.on)).check(matches(isDisplayed()))
        pressBack()
        onView(hasSibling(withText(R.string.sound))).check(matches(withText(R.string.lost_only)))
    }
    
    @Test
    fun checkColorThemeChange() {
        assertEquals(AppTheme.SYSTEM, AppTheme.getCurrentTheme())
        onView(withText(R.string.color_theme)).perform(scrollTo(), click())
        onView(withText(R.string.dark)).perform(click())
        waitFor {
            onView(hasSibling(withText(R.string.color_theme)))
                .perform(scrollTo())
                .check(matches(withText(R.string.dark)))
        }
        onView(withText(R.string.color_theme))
            .check(matches(hasTextColor(android.R.color.white)))  // в settings используется своя палитра для текста. В темной теме - белый цвет
        
        rule.scenario.onActivity { AppTheme.SYSTEM.activate() } // возвращаем схему в SYSTEM
    }
    
    //todo вынести в BatteryOptimizationPrefTest и отрефакторить
    @Test
    fun checkButteryOptimizationMode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) { //mockkStatic работает только с P+
            mockkStatic(PMUtils::class)
            every { PMUtils.isIgnoringBatteryOptimizations() } returns false
            checkButteryOptimizationPref(true)
            
            every { PMUtils.isIgnoringBatteryOptimizations() } returns true
            refreshUpdatablePrefs()
            checkButteryOptimizationPref(false)
        }
        else
            checkButteryOptimizationPref(true)
    }
    
    private fun checkButteryOptimizationPref(optimization: Boolean) {
        val drawable = if (optimization) R.drawable.ic_error else R.drawable.ic_baseline_check_circle_24
        val messageId = if (optimization) R.string.not_ok_disabled else R.string.ok_enabled
        
        onView(withParent(withId(R.id.pref_widget))).perform(scrollTo())
            .check(matches(withDrawable(drawable)))
            .check(matches(withParent(withParent(withParent(hasSibling(withChild(withText(messageId))))))))
    }
    
    private fun refreshUpdatablePrefs() {
        rule.scenario.onActivity {
            (it.supportFragmentManager.fragments[0] as SettingsFragment).onResume()
        }
        onIdle()
    }
    
}