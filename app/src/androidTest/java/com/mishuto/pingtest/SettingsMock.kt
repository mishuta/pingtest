package com.mishuto.pingtest

import com.mishuto.pingtest.common.Const
import com.mishuto.pingtest.settings.Settings
import com.mishuto.pingtest.settings.preferences.sound.SoundScope
import com.mishuto.pingtest.settings.preferences.sound.SoundSettings

/**
 * Мок-объект для Settings.
 * Created by Michael Shishkin on 04.01.2021 refactored 16.02.2024
 */
data class SettingsMock(
    override var pingTimeout: Int = 1000,
    override var host: String = "localhost",
    override var pingInterval: Int = 500,
    override var movingPingsNumber: Int = 50,
    override var isKeepScreenOn: Boolean = false,
    override var testDuration: Int = Const.HOUR,
    override val isAutostart: Boolean = false,
    override val showFloatingWindow: Boolean = false
) : Settings {
    
    override val soundSettings: SoundSettings
        get() = object: SoundSettings {
            override val isSoundOn: Boolean
                get() = true
            override val scope: SoundScope
                get() = SoundScope.ALL
        }
}
