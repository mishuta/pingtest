package com.mishuto.pingtest.common

import com.mishuto.pingtest.common.Assertions.assertEqualsWithAccuracy
import com.mishuto.pingtest.common.IntAggregator.Companion.AVG
import com.mishuto.pingtest.common.IntAggregator.Companion.MAX
import com.mishuto.pingtest.common.IntAggregator.Companion.MIN
import org.junit.Test
import java.lang.Thread.sleep
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertTrue

/**
 * VarsAggregatorTest
 * Created by Michael Shishkin on 30.03.2024
 */
class VarsAggregatorTest {
    private val aggregator = IntAggregator("test")
    
    @Test
    fun testIntValuesByCount() {
        var n = 1
        var finished = false
        
        val varsAggregator = VarsAggregator(listOf(aggregator), 10) {
            when (n++) {
                1 -> {
                    assertEquals(10, aggregator.count)
                    assertEquals(1, aggregator.aggregatedResults()[MIN])
                    assertEquals(10, aggregator.aggregatedResults()[MAX])
                    assertEquals((1..10).sum() / 10, aggregator.aggregatedResults()[AVG])
                }
                
                2 -> {
                    assertEquals(10, aggregator.count)
                    assertEquals(11, aggregator.aggregatedResults()[MIN])
                    assertEquals(20, aggregator.aggregatedResults()[MAX])
                    assertEquals((11..20).sum() / 10, aggregator.aggregatedResults()[AVG])
                    finished = true
                }
                
                else -> throw AssertionError()
            }
        }
        
        for (i in 1..20) {
            varsAggregator.add(IntValue("test", i))
        }
        assertTrue(finished)
    }
    
    @Test
    fun testIntValuesByTimer() {
        var start: Long = 0
        var finished = false
        
        val varsAggregator = VarsAggregator(listOf(aggregator), maxDelay = 100) {
            assertEqualsWithAccuracy(100, Instant.now().toEpochMilli() - start, 5)
            assertEquals(1, aggregator.aggregatedResults()[MIN])
            assertEquals(aggregator.count, aggregator.aggregatedResults()[MAX])
            finished = true
        }
        
        sleep(50) // имитируем время на инициализацию
        for (i in 1..100) {
            if (finished) break
            
            varsAggregator.add(IntValue("test", i))
            
            if (i == 1) start = Instant.now().toEpochMilli()
            
            sleep(1)
        }
        
        assertTrue(finished)
    }
}