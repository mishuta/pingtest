package com.mishuto.pingtest.common;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Test;

/**
 * Created by Michael Shishkin on 02.08.2020
 */
public class FixCircularBufferTest {
    @Test
    public void add() {
        FixCircularBuffer<Integer> buffer = new FixCircularBuffer<>(2);
        buffer.add(0);
        buffer.add(1);
        buffer.add(2);

        assertThat(buffer, hasSize(2));
        assertThat(buffer, contains(1,2));
    }
}