package com.mishuto.pingtest.common

import org.junit.Test
import kotlin.test.assertEquals

/**
 * Тесты рефлексии
 * Created by Michael Shishkin on 04.03.2024
 */
class UtilsKtTest{
    
    @Suppress("unused")
    class TestedClass {
        private var a: Int = 0
        
        fun getA() = a
        fun setA(i: Int) { a = i }
        
        private fun twice(i: Int) = i + i
    }
    
    object TestedObject {
        private var a: Int = 0
        
        fun getA() = a
        fun setA(i: Int) { a = i }
    }
    
    @Test
    fun getPrivatePropertyClass() {
        val t = TestedClass()
        t.setA(1)
        assertEquals(1, t.getPrivateProperty<TestedClass, Int>("a"))
    }
    
    @Test
    fun getPrivatePropertyObject() {
        val t = TestedObject
        t.setA(1)
        assertEquals(1, TestedObject.getPrivateProperty<TestedObject, Int>("a"))
    }
    
    @Test
    fun setPrivateProperty() {
        val t = TestedClass()
        t.setPrivateProperty("a", 10)
        assertEquals(10, t.getA())
    }
    
    @Test
    fun setPrivatePropertyObject() {
        val t = TestedObject
        t.setPrivateProperty("a", 10)
        assertEquals(10, t.getA())
    }
    
    @Test
    fun callPrivateFunc() {
        val t = TestedClass()
        assertEquals(10, t.callPrivateFunc("twice", 5))
    }
}