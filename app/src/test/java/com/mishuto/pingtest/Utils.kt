package com.mishuto.pingtest

import androidx.preference.PreferenceManager
import com.mishuto.pingtest.common.SharedPref
import io.mockk.*

/**
 * Тулзы для ветки tests
 * Created by Michael Shishkin on 17.01.2024
 */
object Utils {
    fun mockkSharedPref() {
        mockkObject(App)
        every { App.appContext } returns mockk()
        mockkStatic(PreferenceManager::class)
        every { PreferenceManager.getDefaultSharedPreferences(any()) } returns mockk()
        mockkObject(SharedPref)
    }
}