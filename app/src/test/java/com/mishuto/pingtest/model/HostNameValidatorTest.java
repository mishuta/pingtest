package com.mishuto.pingtest.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.function.Predicate;

/**
 * Тестирование валидатора
 * Created by Michael Shishkin on 28.06.2020
 */
public class HostNameValidatorTest {

    Predicate<CharSequence> predicate = new HostNameValidator();

    @Test
    public void singleHost() {

        assertTrue(predicate.test("localhost"));
        assertTrue(predicate.test("LOCALHOST"));
        assertTrue(predicate.test("local-host"));
        assertTrue(predicate.test("local9host0"));
        assertTrue(predicate.test("a23456789012345678901234567890123456789012345678901234567890123"));

        assertFalse(predicate.test("-localhost"));
        assertFalse(predicate.test("9localhost"));
        assertFalse(predicate.test("localhost."));
        assertFalse(predicate.test("a234567890123456789012345678901234567890123456789012345678901234"));
    }

    @Test
    public void subDomain() {
        assertTrue(predicate.test("google.ru"));
        assertTrue(predicate.test("WWW.google.ru"));
        assertTrue(predicate.test("welcome.museum"));
        assertTrue(predicate.test("www.mail.google.com"));
        assertTrue(predicate.test("9be89.dns.nextdns.io"));
        assertTrue(predicate.test("i.ua"));

        assertFalse(predicate.test("-9be89.dns.nextdns.io"));
        assertFalse(predicate.test("google-.com"));
        assertFalse(predicate.test("google.c"));
        assertFalse(predicate.test("google.co=m"));
    }

    @Test
    public void ip4() {

        assertTrue(predicate.test("1.1.2.1"));
        assertTrue(predicate.test("9.91.255.1"));
        assertTrue(predicate.test("029.91.255.1"));
        assertTrue(predicate.test("255.255.255.255"));
        assertTrue(predicate.test("0.0.0.0"));

        assertFalse(predicate.test("129.391.255.1"));
        assertFalse(predicate.test("129.91.2a5.1"));
        assertFalse(predicate.test("129.91.25"));
        assertFalse(predicate.test("192.168.256.126"));
        assertFalse(predicate.test("192..255.126"));
        assertFalse(predicate.test("192.255.255.126."));
    }

    @Test
    public void ip6() {
        assertTrue(predicate.test("2001:0db8:85a3:0000:0000:8a2e:0370:7334"));
        assertTrue(predicate.test("2001:db8::1"));
        assertTrue(predicate.test("::1"));
        assertTrue(predicate.test("::"));
        assertTrue(predicate.test("::ffff:192.0.2.128"));

        assertFalse(predicate.test("2001:DB8::1"));
        assertFalse(predicate.test(" 2001:db8:0:0:1::1"));
    }
}