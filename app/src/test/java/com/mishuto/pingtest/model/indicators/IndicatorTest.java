package com.mishuto.pingtest.model.indicators;

import static com.mishuto.pingtest.model.indicators.Indicator.DOWN_HI;
import static com.mishuto.pingtest.model.indicators.Indicator.DOWN_MID;
import static com.mishuto.pingtest.model.indicators.Indicator.Position.ABOVE_HIGH;
import static com.mishuto.pingtest.model.indicators.Indicator.Position.BELOW_LOW;
import static com.mishuto.pingtest.model.indicators.Indicator.Position.HIGH;
import static com.mishuto.pingtest.model.indicators.Indicator.Position.LOW;
import static com.mishuto.pingtest.model.indicators.Indicator.Position.MEDIUM;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;

import org.junit.Test;

/**
 * Тестирование класса Indicator
 * Created by Michael Shishkin on 20.06.2020
 */
public class IndicatorTest {
    Indicator PING = new Indicator(new Indicator.Thresholds(2, 30, 110, 300));
    Indicator JITTER = new Indicator(new Indicator.Thresholds(2, 10, 50, 100));
    Indicator LOSS = new Indicator(new Indicator.Thresholds(0, 5, 20, 80));

    // для макс. значения параметра, значение индикатора = 1
    @Test
    public void maxIndicator() {
        PING.setIndicatorParameter(2);
        assertThat(PING.getPosition(), is(HIGH));
        assertThat(PING.computeIndicatorValue(), is(1.0));
    }

    // для значения параметра выше макс, значение индикатора = 1
    @Test
    public void aboveMaxIndicator() {
        PING.setIndicatorParameter(1);
        assertThat(PING.getPosition(), is(ABOVE_HIGH));
        assertThat(PING.computeIndicatorValue(), is(1.0));
    }

    // для мин. значения параметра, значение индикатора = 0
    @Test
    public void minIndicator() {
        PING.setIndicatorParameter(300);
        assertThat(PING.getPosition(), is(LOW));
        assertThat(PING.computeIndicatorValue(), is(0.0));
    }

    // для значения параметра ниже мин., значение индикатора = 0
    @Test
    public void belowMinIndicator() {
        PING.setIndicatorParameter(500);
        assertThat(PING.getPosition(), is(BELOW_LOW));
        assertThat(PING.computeIndicatorValue(), is(0.0));
    }

    // закон для верхнего сегмента должен быть логарифмическим
    @Test
    public void logLawHigh() {
        PING.setIndicatorParameter(27);             // берем три нижних значения верхнего сегмента
        double y1 = PING.computeIndicatorValue();

        PING.setIndicatorParameter(28);
        double y2 = PING.computeIndicatorValue();

        PING.setIndicatorParameter(29);
        double y3 = PING.computeIndicatorValue();

        assertThat(y1, lessThan(1.0));              // индикатор д.б < 1
        assertThat(y1, greaterThan(y2));            // функция монотонно убывает
        assertThat(y2, greaterThan(y3));
        assertThat((float)y3, greaterThan(DOWN_HI));// нижнее значение выше нижней границы верхн. сегмента
        assertThat(y1 - y2, lessThan(y2-y3));       // производная < 0
    }

    // закон для нижнего сегмента должен быть логарифмическим
    @Test
    public void logLawLow() {
        PING.setIndicatorParameter(200);
        double y1 = PING.computeIndicatorValue();

        PING.setIndicatorParameter(201);
        double y2 = PING.computeIndicatorValue();

        PING.setIndicatorParameter(202);
        double y3 = PING.computeIndicatorValue();

        assertThat((float)y1, lessThan(DOWN_MID));
        assertThat(y1, greaterThan(y2));
        assertThat(y2, greaterThan(y3));
        assertThat(y3, greaterThan(0.0));
        assertThat(y1 - y2, greaterThan(y2-y3));
    }

    // попадание в нужные сегменты на границе сегментов
    @Test
    public void loBound() {
        PING.setIndicatorParameter(110);
        assertThat(PING.getPosition(), is(MEDIUM));
        double hi = PING.computeIndicatorValue();

        PING.setIndicatorParameter(111);
        assertThat(PING.getPosition(), is(LOW));
        double low = PING.computeIndicatorValue();

        assertThat(hi, greaterThan(low));
    }

    // попадание в нужные сегменты на границе сегментов
    @Test
    public void hiBound() {
        PING.setIndicatorParameter(30);
        assertThat(PING.getPosition(), is(HIGH));
        double hi = PING.computeIndicatorValue();

        PING.setIndicatorParameter(31);
        assertThat(PING.getPosition(), is(MEDIUM));
        double low = PING.computeIndicatorValue();

        assertThat(hi, greaterThan(low));
    }

    // средний сегменм имеет линейную функцию
    @Test
    public void linearLawMed() {
        PING.setIndicatorParameter(60);
        float y1 = (float) PING.computeIndicatorValue();

        PING.setIndicatorParameter(61);
        float y2 = (float)PING.computeIndicatorValue();

        PING.setIndicatorParameter(62);
        float y3 = (float) PING.computeIndicatorValue();

        assertThat(y1, lessThan(DOWN_HI));
        assertThat(y1, greaterThan(y2));
        assertThat(y2, greaterThan(y3));
        assertThat(y3, greaterThan(DOWN_MID));
        assertThat(y1 - y2, equalTo(y2 - y3));
    }

    // из нескольких индикаторов интегральный равен наихудшему
    @Test
    public void getTotalIndicatorSegment() {
        GroupIndicators groupIndicators = new GroupIndicators(GroupThresholds.OVERALL);
        GroupIndicators.GroupValue groupValue = new GroupIndicators.GroupValue(5, 30, 70);
        groupIndicators.setGroupValue(groupValue);

        long value = groupIndicators.getTotalIndicator().getIndicatorParameter();
        assertThat(value, is(70L));
    }
}