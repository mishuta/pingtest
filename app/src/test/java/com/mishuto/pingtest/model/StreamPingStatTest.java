package com.mishuto.pingtest.model;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;

import com.mishuto.pingtest.model.ping.Ping;
import com.mishuto.pingtest.model.ping.PingMock;
import com.mishuto.pingtest.model.stat.StreamPingStat;

import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDateTime;

/**
 * Тест расчета статистики
 * Created by Michael Shishkin on 15.06.2020
 */
public class StreamPingStatTest {
    static Ping[] PING_ARR;
    static StreamPingStat sPingStat = new StreamPingStat(4);

    @BeforeClass
    public static void setupClass() throws Exception  {
        PING_ARR = new Ping[]{
                new PingMock(5).setLost(true), // не должен попасть в буфер, т.к. будет вытеснен последним элементом
                new PingMock(5).setTimeStamp(LocalDateTime.of(2020,1,1,0,0,0)),
                new PingMock(25),
                new PingMock(1000).setLost(true),
                new PingMock(50).setTimeStamp(LocalDateTime.of(2020,1,1,0,0,3))
        };
        for (Ping p : PING_ARR)
            sPingStat.addNew(p);
    }

    @Test
    public void sizeOfBuffer() {
        assertEquals(sPingStat.getQuantity(), 4);
    }

    @Test
    public void getAverage() {
        assertThat(sPingStat.getAverage(), is((5+25+50)/3L) );
    }

    @Test
    public void getLostPacks() {    // % потерянных
        assertThat((long)sPingStat.getLostPermille(), is(1000 / sPingStat.getQuantity()));
    }

    @Test
    public void getJitter() {
        assertThat(sPingStat.getJitter(), is(((25L - 5) + (50 - 25))/2));
    }

    @Test
    public void getBest() {
        assertThat(sPingStat.getBest(), is(5L));
    }

    @Test
    public void getWorst() {
        assertThat(sPingStat.getWorst(), is(50L));
    }

    @Test
    public void getDuration() {
        assertThat(sPingStat.getDuration(), is(3000L + 50));
    }
}