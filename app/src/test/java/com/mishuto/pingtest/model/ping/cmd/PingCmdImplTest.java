package com.mishuto.pingtest.model.ping.cmd;

import static org.junit.Assert.assertEquals;

import com.mishuto.pingtest.common.Logger;
import com.mishuto.pingtest.model.stat.PingStatistic;

import org.junit.Before;
import org.junit.Test;

/**
 * Тест регекспов для PingCmdImpl
 * Created by Michael Shishkin on 17.06.2021
 */
public class PingCmdImplTest {

    @Before
    public void setUp()  {
        Logger.INSTANCE.setStdOutOnly(true);
    }

    static class CommandMock extends PingCmdImpl.Command {
        String mMockOut;

        public CommandMock(String mockOut) {
            mMockOut = mockOut;
        }

        @Override
        String execute(String cmd) {
            return mMockOut;
        }
    }

    final static String PING_RECEIVED = "PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.\n" +
            "    64 bytes from 127.0.0.1: icmp_seq=1 ttl=164 time=123.456 ms\n" +
            "    \n" +
            "    --- 127.0.0.1 ping statistics ---\n" +
            "    1 packets transmitted, 1 received, 0% packet loss, time 0ms\n" +
            "    rtt min/avg/max/mdev = 0.056/0.056/0.056/0.000 ms";

    final static String SOME_LOST = "PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.\n" +
            "    \n" +
            "    --- 127.0.0.1 ping statistics ---\n" +
            "    13 packets transmitted, 12 received, 7% packet loss, time 363915ms\n" +
            "    rtt min/avg/max/mdev = 24.456/31.656/103/2.1 ms";

    final static String ALL_NOT_RECEIVED = "    PING ya.ru (87.250.250.242) 56(84) bytes of data.\n" +
            "    \n" +
            "    --- ya.ru ping statistics ---\n" +
            "    10 packets transmitted, 0 received, 100% packet loss, time 9345ms";

    @Test
    public void execPing() throws Exception {
        PingCmdImpl pingCmdImpl = new PingCmdImpl();
        pingCmdImpl.mCommand = new CommandMock(PING_RECEIVED);

        int t = pingCmdImpl.execPing("127.0.0.1", 1000);
        assertEquals(t, 123 );
    }

    @Test
    public void execSeries() throws Exception {
        PingCmdImpl pingCmdImpl = new PingCmdImpl();
        pingCmdImpl.mCommand = new CommandMock(SOME_LOST);
        PingStatistic statistic = pingCmdImpl.execSeries("1.2.3.4", 20, 1, 1);

        assertEquals(32, statistic.getAverage());
        assertEquals(363915, statistic.getDuration());
        assertEquals(24, statistic.getBest());
        assertEquals(103, statistic.getWorst());
        assertEquals(2, statistic.getJitter());
        assertEquals(76, statistic.getLostPermille());
        assertEquals(1, statistic.getLost());
        assertEquals(13, statistic.getQuantity());
    }

    @Test
    public void execSeriesFault() throws Exception {
        PingCmdImpl pingCmdImpl = new PingCmdImpl();
        pingCmdImpl.mCommand = new CommandMock(ALL_NOT_RECEIVED);
        PingStatistic statistic = pingCmdImpl.execSeries("1.2.3.4", 10, 1, 1);
        assertEquals(0, statistic.getAverage());
        assertEquals(0, statistic.getBest());
        assertEquals(0, statistic.getWorst());
        assertEquals(0, statistic.getJitter());
        assertEquals(1000, statistic.getLostPermille());
        assertEquals(10, statistic.getLost());
        assertEquals(10, statistic.getQuantity());
    }

    @Test
    public void sticky() throws Exception {
        PingCmdImpl pingCmdImpl = new PingCmdImpl();
        pingCmdImpl.mCommand = new CommandMock("144 packets transmitted, 20 received, 86% packet loss, time 360915ms\nrtt min/avg/max/mdev = 24.456/31.656/103/2.1 ms");
        PingStatistic statistic = pingCmdImpl.execSeries("1.2.3.4", 15, 1, 1);

        assertEquals(0, statistic.getLostPermille());
        assertEquals(0, statistic.getLost());
        assertEquals(15, statistic.getQuantity());
    }
}