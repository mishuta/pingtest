package com.mishuto.pingtest.model.stat

import com.mishuto.pingtest.model.ping.PingMock
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Before
import org.junit.Test
import kotlin.math.abs

/**
 * Тесты для BoundlessStat
 * Created by Michael Shishkin on 27.01.2022
 */
class BoundlessStatTest {
    private val pingArr = arrayOf(
        PingMock(4).setLost(true),
        PingMock(10),
        PingMock(20),
        PingMock(30),
        PingMock(40).setError(true),
        PingMock(500).setEffectiveLatency(60)
    )

    private val statistic = BoundlessStat()

    @Before
    fun setUp() {
        pingArr[5].timeStamp = pingArr[0].timeStamp.plusSeconds(10)
        pingArr.forEach { p->statistic.addNew(p) }
    }

    @Test
    fun average() {
        assertEquals((10+20+30+60)/4, statistic.average)
    }

    @Test
    fun lost() {
        assertEquals(2, statistic.lost)
    }

    @Test
    fun jitter() {
        assertEquals((abs(10-20) + abs(20-30) + abs(30-60))/3L, statistic.jitter)
    }

    @Test
    fun best() {
        assertEquals(10, statistic.best)
    }

    @Test
    fun worst() {
        assertEquals(60, statistic.worst)
    }

    @Test
    fun quantity() {
        assertEquals(6, statistic.quantity)
    }

    @Test
    fun duration() {
        assertEquals(10_000 + pingArr[5].latency, statistic.duration)
    }

    @Test
    fun empty() {
        assertFalse(statistic.isEmpty())
    }

    @Test
    fun lostPPM() {
        assertEquals(1000*2/6, statistic.getLostPermille())
    }

    @Test
    fun lostPPM0() {
        val s = BoundlessStat()
        assertEquals(0, s.getLostPermille())
    }
}